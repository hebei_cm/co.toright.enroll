/*
 Template Name: Upcube - Bootstrap 4 Admin Dashboard
 Author: Themesdesign
 Website: www.themesdesign.in
 File: Datatable js
 */

$(document).ready(function() {
    $("#datatable-buttons").dataTable().fnDestroy();
    initTable();
} );

$.fn.dataTable.defaults.oLanguage = {
    "sProcessing": "处理中...",
    "sLengthMenu": "显示 _MENU_ 项结果",
    "sZeroRecords": "没有匹配结果",
    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
    "sInfoPostFix": "",
    "sSearch": "搜索：",
    "sUrl": "",
    "sEmptyTable": "表中数据为空",
    "sLoadingRecords": "载入中...",
    "sInfoThousands": ",",
    "oPaginate": {
        "sFirst": "首页",
        "sPrevious": "上页",
        "sNext": "下页",
        "sLast": "末页"
    },
    "oAria": {
        "sSortAscending": ": 以升序排列此列",
        "sSortDescending": ": 以降序排列此列"
    }
};

function initTable() {
   var table = $('#datatable-buttons').DataTable({
        // language: {
        //     url: '//cdn.datatables.net/plug-ins/1.10.22/i18n/Chinese.json'
        // },
        "processing": true,
        ajax:"/college/list",
        "columns": [
            { "data": "id" },
            { "data": "collegeName" },
            { "data": "president" },
            { "data": "vicePresident",
                visible: false},
            { "data": "collegeAddress" ,
                visible: false},
            { "data": "collegeTel" },
            { "data": "zipCode" ,
                visible: false},
            { "data": "collegeNetAddress" },
            // { "data": "description",visible: false },
        ],
       "columnDefs": [{
           "targets": [7],  // 目标列位置，下标从0开始
           "data": "collegeNetAddress",  // 数据列名
           "render": function (data, type, full) {  // 返回自定义内容
               return "<a href='"+ data +"' style='color: rgba(11,125,12,0.95);'>" + data + "</a>";
           }
       }, {
           "targets" : [8],
           "data" :  "specialityInfoList" ,
           "render" :  function (data, type, full) {  // 返回自定义内容
               return  "<a href='#' id='spec_info'>查看专业</a>";
           }
       },
           // 增加一列，包括删除和修改，同时将我们需要传递的数据传递到链接中
       {
           "targets" : [9],
           "data" :  "description" ,

           "render" :  function (data, type, full) {  // 返回自定义内容
               return  "<a id='college_desc' href='#'>查看详情</a>";
           }
       },{
               "targets" : [10],  // 目标列位置，下标从0开始
               "data" :  "collegeId" ,  // 数据列名
               "render" :  function (data, type, full) {  // 返回自定义内容
                   //class='btn-sm btn-primary m-1'
                   //class='btn-sm btn-danger m-1'
                   return  "<a id='btn_edit' style='text-decoration:underline;' href='#'>编辑</a></br>" +
                       "<a id='btn_delete' style='text-decoration:underline;' href='#'>删除</a>" ;
               }
           }],
        // "dom": 'Bfrtip',//表示在Buttons在filter左上方显示
        "lengthChange":false,//表示取消用户自选每页显示信息
       //表格初始化完成后的回调函数
       "fnInitComplete": function (oSettings, json) {
       }

    });

    //初始化导出工具按钮组
    new $.fn.dataTable.Buttons( table, {
        "buttons": [
            {extend: 'copy',
                text: "复制所有内容",

            }, {
                extend: 'excelHtml5',
                text:'下载Excel',
                customize: function( xlsx ) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    $('row c[r^="C"]', sheet).attr( 's', '2' );
                }
            },
            {
                extend: 'pdfHtml5',
                text:'导出PDF',
            },
            {extend: 'print',
                text: "打印",
                customize: function(win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            },
            {extend: "colvis",
            text: '显示/隐藏列'}],
    } );

    table.buttons().container()
        .appendTo($('#datatable-buttons_wrapper .col-md-6:eq(0)'));

    //查看专业信息按钮事件
    $('#datatable-buttons tbody').on( 'click', '#spec_info', function () {
        var data = table.row( $(this).parents('tr') ).data();
        // alert(data.specialityInfoList[2].specName);
        showSpecModal(data);
    } );

    //按钮点击后就会跳转到这
    $('#datatable-buttons tbody').on( 'click', '#btn_edit', function () {
        //这一行获取当前点击列的行数据，是一个对象，可以data.xxx;
        var data = table.row( $(this).parents('tr') ).data();
        // console.log(data.collegeId);
        alert("编辑"+data.collegeName+","+data.id);
    } );

    //删除按钮回调函数
    $('#datatable-buttons tbody').on( 'click', '#btn_delete', function () {
        var data = table.row( $(this).parents('tr') ).data();
        deleteCollege(data.id);
    } );

    //学院简介按钮
    $('#datatable-buttons tbody').on( 'click', '#college_desc', function () {
        var data = table.row( $(this).parents('tr') ).data();
        alert(data.description);
    } );
}

function deleteCollege(id) {
    alert("删除"+id);
}
