// ;			//初始化模态框内容
// $("#addRole").on('hidden.bs.modal', '.modal', function () {
//     $(this).removeData('bs.modal');
// });

var totalRecord,currentPage;
//1、页面加载完成以后，直接去发送ajax请求,要到分页数据
$(function(){
    //去首页
    // to_page(1);
});
//
// function initTable(pn) {
//     to_page(pn);
//
// }
function to_page(pn){
    $.ajax({
        url:"/role/role-page",
        data:"pn="+pn,
        type:"GET",
        cache: false,
        success:function(result){
            $("#roles_card").html(result);
        }
    });
}


//新增角色按钮提交表单
$("#btn_add_role").click(function () {
    //获取表单元素值,封装到json中
    var param = {
        "roleCode":$("#roleCode").val(),
        "roleDesc":$("#roleDesc").val(),
        "roleStatus": $('input:radio[name="roleStatus"]:checked').val(),
    };
    //请求/add,携带param作为add方法的参数
    $.ajax({
        url:"/role/addNewRole",
        data:param,
        type:"POST",
        dataType:"json",
        success:function (data) {
            if(data.success){
                alert("添加成功");
                $("#btn_modal_close").click();
            }
            if (!data.success){
                alert(data.msg+"请重新输入");
                // $("#form_msg_alert").attr("class","alert alert-danger").append(txt);
            }

        }

    })
});

//回显修改角色表格内容
function edit_role(obj){
    var $td= $(obj).parents('tr').children('td');
    var td_role_id = $td.eq(0).text();
    var td_role_desc = $td.eq(1).text();
    var td_creat_time = $td.eq(2).text();
    var td_role_status = $td.eq(3).text().replace(/(^\s*)|(\s*$)/g, "")=="可用"?"0":"1";

    $("#edit_roleID").val(td_role_id);
    $("#edit_roleDesc").val(td_role_desc);
    $("#edit_creatTime").val(td_creat_time);
    $("input[type=radio][name='edit_role_status'][value='"+td_role_status+"']").attr("checked",'checked');//设置value=2的项目为当前选中项
}

//提交编辑角色表格内容
$("#btn_comit_role").click(function () {
    var parm = {
        "roleId":$("#edit_roleID").val(),
        "roleDesc":$("#edit_roleDesc").val(),
        "roleStatus": $('input:radio[name="edit_role_status"]:checked').val(),
    }
    //请求/edit,携带param作为add方法的参数
    $.ajax({
        url:"/role/editRole",
        data:parm,
        type:"POST",
        dataType:"json",
        success:function (data) {
            if(data.success){
                alert("修改成功");
                $("#btn_edit_madle").click();
                window.location.reload();
            }
            if (!data.success){
                alert(data.msg+"请重新输入");
                // $("#form_msg_alert").attr("class","alert alert-danger").append(txt);
            }

        }

    })
});


//配置角色权限
function config_role(obj) {
    var $td= $(obj).parents('tr').children('td');
    var roleId = $td.eq(0).text();
    $("#config_role_id").val(roleId);
    $.ajax({
        url:'/role/getRoleDesc/'+parseInt(roleId),
        type:'get',
        dataType:'json',
        success:function (result) {
            $("#config_role_desc").val(result.msg);
            // console.log(result);
        }
    })
    $(function () {
        //参数
        var $td= $(obj).parents('tr').children('td');
        var roleId = $td.eq(0).text();
        //请求路径
        var url = "/role/permis/"+roleId;
        $.get(url,function (data) {
            //调用初始化方法；
            initzTree(data);
        },"json")
    });
}

//提交选中的权限节点
function submitCheckedNodes() {
    var nodes = new Array();
    nodes = zTreeObj.getCheckedNodes(true);//获取选中的节点
    var str = "";
    for( i =0; i<nodes.length; i++){
        if (str!=""){
            str +=",";
        }
        str += nodes[i].id;
    }
    //JQ的代码
    // alert(str);
    $("#permisIds").val(str);
}

//提交角色权限配置表单
function roleConfigPermisFormComit() {
    var parm = {
        "roleId":$("#config_role_id").val(),
        "permisIds":$("#permisIds").val(),
    }
    $.ajax({
        url:"/role/permis/save",
        data:parm,
        type:"POST",
        dataType:"json",
        success:function (data) {
            if(data.success){
                alert("修改成功");
                $("#btn_configModal_cancle").click();
                window.location.reload();
            }
            if (!data.success){
                alert(data.msg+"请重新输入");
                // $("#form_msg_alert").attr("class","alert alert-danger").append(txt);
            }

        }

    })
}

<!--ztree配置-->
var setting = {
    view: {
        addHoverDom: addHoverDom,
        removeHoverDom: removeHoverDom,
        selectedMulti: false
    },
    check: {
        enable: true
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    edit: {
        enable: true
    }
};



//回调函数
function initzTree(data) {
    //初始化树
    zTreeObj = $.fn.zTree.init($("#treeDemo"), setting, data);
}

//
// $(document).ready(function(){
// 	//页面一加载就在页面上生成树
// 	//初始化方法
// 	var obj = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
// 	//obj指的就是ztree对象
// 	//展开所有节点
// });

var newCount = 1;
function addHoverDom(treeId, treeNode) {
    var sObj = $("#" + treeNode.tId + "_span");
    if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
    var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
        + "' title='add node' onfocus='this.blur();'></span>";
    sObj.after(addStr);
    var btn = $("#addBtn_"+treeNode.tId);
    if (btn) btn.bind("click", function(){
        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        zTree.addNodes(treeNode, {id:(100 + newCount), pId:treeNode.id, name:"new node" + (newCount++)});
        return false;
    });
};
function removeHoverDom(treeId, treeNode) {
    $("#addBtn_"+treeNode.tId).unbind().remove();
};

