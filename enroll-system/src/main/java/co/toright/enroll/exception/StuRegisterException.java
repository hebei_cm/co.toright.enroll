package co.toright.enroll.exception;

import co.toright.enroll.constant.ResultEnum;

//用来处理学生注册过程中异常
public class StuRegisterException extends BusinessException {

    private int code;
    private String msg;

    public StuRegisterException(Exception e, int code, String msg) {
        super(e);
        this.code = code;
        this.msg = msg;
    }

    public StuRegisterException(Exception e, String exceptionDesc, int code, String msg) {
        super(e, exceptionDesc);
        this.code = code;
        this.msg = msg;
    }

    public StuRegisterException(String exceptionDesc, int code, String msg) {
        super(exceptionDesc);
        this.code = code;
        this.msg = msg;
    }

    public StuRegisterException(int code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
