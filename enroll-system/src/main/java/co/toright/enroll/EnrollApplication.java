package co.toright.enroll;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAsync
@EnableScheduling
@MapperScan("co.toright.enroll.mapper")
@ComponentScan(basePackages = {"co.toright"})
@SpringBootApplication
public class EnrollApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnrollApplication.class, args);
    }

}
