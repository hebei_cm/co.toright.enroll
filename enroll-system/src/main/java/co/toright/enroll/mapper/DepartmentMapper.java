package co.toright.enroll.mapper;

import co.toright.enroll.entity.Department;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-27
 */
@Repository
public interface DepartmentMapper extends BaseMapper<Department> {

}
