package co.toright.enroll.mapper;

import co.toright.enroll.entity.Permission;
import co.toright.enroll.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 后台角色表 Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Repository
public interface RoleMapper extends BaseMapper<Role> {

    @Select("select * from sys_permis where id in (select permission_id from sys_role_permis where role_id = #{roleId})")
    List<Permission> getRolePermissions(Integer roleId);

    @Select("select * from sys_role")
    List<Role> findAllRole();


    @Select("select count(*) from sys_role where (role_code=#{roleCode} or role_desc=#{roleDesc})")
    int countRolesByRoleCode(@Param("roleCode")String roleCode,@Param("roleDesc")String roleDesc);

    @Insert("insert into sys_role(values(id,role_code,role_desc,crete_time,update_time,delete_status) values " +
            "(#{id},#{roleCode},#{roleDesc},now(),now(),#{deleteStatus}")
    int insertRole(Role role);

}
