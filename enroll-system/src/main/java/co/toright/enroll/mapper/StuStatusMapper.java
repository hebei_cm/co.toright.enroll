package co.toright.enroll.mapper;

import co.toright.enroll.entity.StuStatus;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-03-05
 */
public interface StuStatusMapper extends BaseMapper<StuStatus> {

}
