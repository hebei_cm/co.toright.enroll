package co.toright.enroll.mapper;

import co.toright.enroll.entity.Permission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 后台权限表 Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Repository
public interface PermissionMapper extends BaseMapper<Permission> {

}
