package co.toright.enroll.mapper;

import co.toright.enroll.entity.noticeEntity.Message;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 私信信息表,包含了所有用户的私信信息 Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
public interface MessageMapper extends BaseMapper<Message> {

}
