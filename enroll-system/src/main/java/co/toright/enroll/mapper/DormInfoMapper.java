package co.toright.enroll.mapper;

import co.toright.enroll.entity.DormInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
public interface DormInfoMapper extends BaseMapper<DormInfo> {

}
