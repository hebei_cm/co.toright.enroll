package co.toright.enroll.mapper;

import co.toright.enroll.entity.PayItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-07
 */
public interface PayItemMapper extends BaseMapper<PayItem> {

}
