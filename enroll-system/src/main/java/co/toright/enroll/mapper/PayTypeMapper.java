package co.toright.enroll.mapper;

import co.toright.enroll.entity.PayType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-05
 */
public interface PayTypeMapper extends BaseMapper<PayType> {

}
