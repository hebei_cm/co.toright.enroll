package co.toright.enroll.mapper;

import co.toright.enroll.entity.Permission;
import co.toright.enroll.entity.Role;
import co.toright.enroll.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 运营后台用户表 Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    @Select("select * from  sys_role where id in(select role_id from sys_user_role where user_id in(select id from sys_user where username = #{username}))")
    List<Role> findRolesByUsername(String username);


}
