package co.toright.enroll.mapper;

import co.toright.enroll.entity.TeacherInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-13
 */
@Repository
public interface TeacherInfoMapper extends BaseMapper<TeacherInfo> {

}
