package co.toright.enroll.mapper;

import co.toright.enroll.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 角色-权限关联表 Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Repository
public interface RolePermisMapper extends BaseMapper<RolePermission> {

}
