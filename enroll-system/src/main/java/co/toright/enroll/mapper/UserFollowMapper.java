package co.toright.enroll.mapper;

import co.toright.enroll.entity.noticeEntity.UserFollow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户关注表,记录了所有用户的关注信息 Mapper 接口
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
public interface UserFollowMapper extends BaseMapper<UserFollow> {

}
