package co.toright.enroll.config.shiro;

import co.toright.enroll.entity.Permission;
import co.toright.enroll.entity.Role;
import co.toright.enroll.entity.User;
import co.toright.enroll.entity.UserRole;
import co.toright.enroll.service.RoleService;
import co.toright.enroll.service.SysUserRoleService;
import co.toright.enroll.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.mgt.SessionsSecurityManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;

import org.apache.shiro.session.mgt.DefaultSessionManager;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;


public class UserRealm extends AuthorizingRealm {


    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    SysUserRoleService roleUserService;



    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        //获取身份信息
        String primaryPrincipal = (String) principals.getPrimaryPrincipal();
        System.out.println("调用授权认证："+primaryPrincipal);


        //根据主身份信息获取角色和权限信息
        User rolesByUsername = userService.findRolesByUsername(primaryPrincipal);
        System.out.println("Realm测试"+rolesByUsername);
        List<Role> roles = rolesByUsername.getRoles();


        //授权角色信息
        if (!CollectionUtils.isEmpty(roles)){
            SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
            roles.forEach(role->{
                simpleAuthorizationInfo.addRole(role.getRoleCode());

                //权限信息
                List<Permission> permissions = role.getPermissions();
                if (!CollectionUtils.isEmpty(permissions)){
                    permissions.forEach(perm -> {
                        simpleAuthorizationInfo.addStringPermission(perm.getPermisCode());
                    });
                }
            });
            return simpleAuthorizationInfo;
        }

        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        System.out.println("============");
        String principal = (String) token.getPrincipal();
        QueryWrapper<User> wrapper = new QueryWrapper();
        wrapper.eq("username", principal);
        User user = userService.getOne(wrapper);
        UserRole relation = roleUserService.getOne(new QueryWrapper<UserRole>().eq("user_id", user.getId()));
        int count = roleService.count(new QueryWrapper<Role>().eq("id", relation.getRoleId()).eq("role_status", 0));
        if(count!=0){
            return new SimpleAuthenticationInfo(user.getUsername(),user.getPassword(),this.getName());
        }
        return null;
    }

    /**
     * 重写方法,清除当前用户的的 授权缓存
     * @param principals
     */
    @Override
    public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
    }

    /**
     * 重写方法，清除当前用户的 认证缓存
     * @param principals
     */
    @Override
    public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
        super.clearCachedAuthenticationInfo(principals);
    }


    @Override
    public void clearCache(PrincipalCollection principals) {
        super.clearCache(principals);
    }

    /**
     * 自定义方法：清除所有 授权缓存
     */
    public void clearAllCachedAuthorizationInfo() {
        getAuthorizationCache().clear();
    }

    /**
     * 自定义方法：清除所有 认证缓存
     */
    public void clearAllCachedAuthenticationInfo() {
        getAuthenticationCache().clear();
    }

    /**
     * 自定义方法：清除所有的  认证缓存  和 授权缓存
     */
    public void clearAllCache() {
        clearAllCachedAuthenticationInfo();
        clearAllCachedAuthorizationInfo();
    }

}
