package co.toright.enroll.config.shiro.filter;

import co.toright.enroll.util.Response;
import com.alibaba.fastjson.JSON;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.servlet.AdviceFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Ajax请求处理 用于前后台分离的场景
 */
public class AjaxFilter extends AdviceFilter {
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        // 先判断是不是ajax请求 ajax请求都会自带一个请求头X-Requested-With
        // 如果有值而且是XMLHttpRequest那就可以确定是个ajax请求 返回json数据就行
        HttpServletRequest req = (HttpServletRequest) request;

        if ("XMLHttpRequest".equals(req.getHeader("X-Requested-With"))) {
            // 获取到当前的登录对象 如果是没有经过认证的用户就获取不到认证信息
            Subject subject = SecurityUtils.getSubject();

            if (subject.getPrincipal() == null) {
                HttpServletResponse resp = (HttpServletResponse) response;

                // 设置响应类型和编码字符 不然中文乱码
                resp.setContentType("application/json;charset=utf-8");
                resp.setCharacterEncoding("UTF-8");

                // Message是我写的一个包装类,用来向前台返回数据
                resp.getWriter().write(JSON.toJSONString(Response.error("请登陆后操作")));
                return false;
            } else {
                // 经过认证的话就放过去 让下一个过滤器处理
                return true;
            }
        } else {
            // 不是ajax请求的话也放过去 让下一个过滤器处理
            return true;
        }
    }
}