package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DormInfo implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "dorm_id", type = IdType.AUTO)
    private Integer dormId;

    private Integer dormNo;

    /**
     * 已居住人数
     */
    private Integer alreadyLived;

    private String remark;

    /**
     * 0可用，1不可用
     */
    private String avaliable;

    /**
     * 宿舍可容纳人数
     */
    private Integer dormCapacity;

    /**
     * 宿舍费用
     */
    private Double dormFee;

    private Integer belong_floor_id;

    private Integer dormTypeId;

}
