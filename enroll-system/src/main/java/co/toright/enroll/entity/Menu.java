package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Data
@TableName("sys_menu")
@EqualsAndHashCode(callSuper = false)
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 父菜单id
     */
    private Integer parentId;

    /**
     * 菜单描述
     */
    private String menuDesc;

    /**
     * 菜单url
     */
    private String menuUrl;

    /**
     * 权限id
     */
    private Integer permisId;

    /**
     * 开启状态
     */
    private Integer enableFlag;

    /**
     * 菜单排序
     */
    private Integer menuSort;

    /**
     * 创建时间
     */
      @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
