package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AreaInfo implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "area_id", type = IdType.AUTO)
    private Integer areaId;

    /**
     * 区域名
     */
    private String areaName;

    /**
     * 区域信息
     */
    private String areaAddress;

    @TableField(exist = false)
    private List<CollegeInfo> collegeInfoList;

    @TableField(exist = false)
    private List<BuildInfo> buildInfoList;
}
