package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 专业信息
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SpecialityInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "spec_id", type = IdType.AUTO)
    private Integer specId;

    /**
     * 专业代码
     */
    private String specCode;

    /**
     * 专业名称
     */
    private String specName;

    /**
     * 门类代码
     */

    private String categoryCode;

    /**
     * 门类名称
     */
    private String categoryName;

    /**
     * 学科代码
     */
    @TableField("belong_college_id")
    private Integer belongCollegeId;

    @TableField(exist = false)
    private List<ClassesInfo> classesInfoList;

    /**
     *专业在学院中的排序
     */
    private Integer sortInCollege;
}
