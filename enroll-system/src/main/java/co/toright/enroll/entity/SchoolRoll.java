package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SchoolRoll implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String studentId;

    /**
     * 生源地
     */
    private String nativePlace;

    /**
     * 学号
     */
    private String studentNo;

    /**
     * 姓名
     */
    private String studentName;

    /**
     * 身份证号
     */
    private String idCard;


    /**
     * 录取类别
     */
    private String admissionCategory;

    /**
     * 外语语种
     */
    private String foreignLanguage;

    /**
     * 培养形式
     */
    private String trainningForm;

    /**
     * 学习形式
     */
    private String learnForm;

    /**
     * 学位类别
     */
    private String degreeCategory;

    /**
     * 培养计划id
     */
    private String trainingPlanId;

    /**
     * 已修学分
     */
    private Integer haveCredit;

    /**
     * 是否毕业，0未毕业，1毕业
     */
    private Integer isGraduated;

    private Date creatTime;

      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


    private String headImg;

    private Boolean gender;
}
