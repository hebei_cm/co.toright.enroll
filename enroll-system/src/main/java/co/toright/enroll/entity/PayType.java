package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PayType implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 收费类型名称
     */
    private String payTypeName;

    /**
     * 缴费备注
     */
    private String paymentRemark;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date creatTime;

    /**
     * 收费项截止日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date deadlineTime;

    /**
     * 发布人id
     */
    private Integer releasePeopleId;

    /**
     * 发布人姓名
     */
    private String releasePeopleName;

    /**
     * 收费项
     */
    private Integer chargingItemId;

    /**
     * 启用状态,0启用，1禁用
     */
    private Integer enabled;


}
