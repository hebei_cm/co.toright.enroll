package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserDetailInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户详细信息表
     */
      @TableId(value = "user_id", type = IdType.ID_WORKER)
    private Integer userId;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 用户头像
     */
    private String headImg;

    /**
     * “0”为女，“1”为男
     */
    private String gender;

    /**
     * 出生日期
     */
    private Date birthdate;

    /**
     * 联系电话
     */
    private Integer telPhone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 家庭地址
     */
    private String honeAddress;


}
