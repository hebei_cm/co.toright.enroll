package co.toright.enroll.entity.importEntity;

import co.toright.enroll.entity.SpecialityInfo;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 学院信息
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-07
 */
@Data
public class CollegeEntity extends BaseRowModel {

    /**
     * 学院代码
     */
    @ExcelProperty("学院代码")
    private String collegeCode;

    /**
     * 学院名
     */
    @ExcelProperty("学院名称")
    private String collegeName;

    /**
     * 院长
     */
    @ExcelProperty("院长")
    private String president;

    /**
     * 副院长
     */
    @ExcelProperty("副院长")
    private String vicePresident;

    /**
     * 学院地址	            
     */
    @ExcelProperty("学院地址")
    private String collegeAddress;

    /**
     * 学院电话
     */
    @ExcelProperty("学院电话")
    private String collegeTel;

    /**
     * 学院邮政编码
     */
    @ExcelProperty("邮编")
    private String zipCode;

    /**
     * 学院网址
     */
    @ExcelProperty("学院网址")
    private String collegeNetAddress;

    /**
     * 学院简介
     */
    @ExcelProperty("简介")
    private String description;


}
