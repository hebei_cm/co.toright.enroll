package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PayItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 收费项名称
     */
    private String payItemName;

    private Integer payTypeId;

    /**
     * 发布人id
     */
    private Integer releasePeopleId;

    /**
     * 收费金额
     */
    private BigDecimal chargeAmount;

    /**
     * 启用状态，0启用，1禁用
     */
    private Integer enabled;

    /**
     * 审核人id
     */
    private Integer verifierId;

    /**
     * 收费的专业id
     */
    private Integer chargedSpecId;

    /**
     * 审核状态,0未审核，1审核中，2通过，3未通过
     */
    private Integer auditStatus;

    /**
     * 审核建议
     */
    private String auditMsg;

    /**
     * 审核时间
     */
    private Date auditTime;

    private String remark;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date endTime;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone="GMT+8")
    private Date creatTime;
}
