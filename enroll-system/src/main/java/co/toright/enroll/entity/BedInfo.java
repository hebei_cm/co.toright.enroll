package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-03-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BedInfo implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 床位号
     */
    private Integer bedNo;

    private Integer belongStuId;

    private Integer belongDormId;

    private Integer belongSpecId;

    private Integer belongFloorId;

    private Integer belongBuildId;

    private Integer bedType;

}
