package co.toright.enroll.entity.noticeEntity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Notify implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer senderId;

    private Integer reciverId;

    /**
     * 消息类型announcement公告/remin提醒/message私信
     */
    private String msgTitle;

    /**
     * 0未读，1已读
     */
    private Boolean isRead;

    private String msgContent;

      @TableField(fill = FieldFill.INSERT)
    private Date createTime;


}
