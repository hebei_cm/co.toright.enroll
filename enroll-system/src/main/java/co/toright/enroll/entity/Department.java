package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Department implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学院id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 学院名
     */
    private String name;

    /**
     * 院长
     */
    private String president;

    /**
     * 副院长
     */
    private String vicePresident;

    /**
     * 学院地址	            
     */
    private String address;

    /**
     * 学院电话
     */
    private String phone;

    /**
     * 学院邮政编码
     */
    private String zipCode;

    /**
     * 学院网址
     */
    private String networkAddress;

    /**
     * 学院简介
     */
    private String description;


}
