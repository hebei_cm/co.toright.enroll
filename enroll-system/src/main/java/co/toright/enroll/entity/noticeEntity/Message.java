package co.toright.enroll.entity.noticeEntity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 私信信息表,包含了所有用户的私信信息
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 对应通知消息的id
     */
    private Integer notifyId;

    /**
     * 发送者用户ID
     */
    private Integer senderId;

    /**
     * 接受者用户ID
     */
    private Integer reciverId;

    /**
     * 消息内容,最长长度不允许超过1000
     */
    private String content;

    /**
     * 创建时间:按当前时间自动创建
     */
      @TableField(fill = FieldFill.INSERT)
    private Date createTime;


}
