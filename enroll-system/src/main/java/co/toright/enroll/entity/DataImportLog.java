package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DataImportLog implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 数据名称
     */
    private String dataName;

    /**
     * 数据上传部门
     */
    private String dataDept;

      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 上传总数
     */
    private Integer dataCount;


}
