package co.toright.enroll.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class InitialStudentData implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String newStuName;

    /**
     * 身份证号
     */
    private String newStuIdCard;

    /**
     * 源地生:省/市/地区
     */
    private String originOfStu;

    /**
     * 新生籍贯
     */
    private String nativePlace;

    private Date dateOfBirth;

    private String applyingMajorCode;

    /**
     * 性别，0女，1男
     */
    private Boolean gender;

    /**
     * 高考总分
     */
    private BigDecimal totalScore;

      @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 逻辑删除字段
     */
    @TableLogic
    private Integer deleted;


}
