package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ClassesInfo implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 班级代码
     */
    private Integer classCode;

    /**
     * 辅导员id
     */
    private Integer counselorId;

    /**
     * 辅导员姓名
     */
    private String counselorName;

    /**
     * 班级名称
     */
    private String className;

    /**
     * 学生数量
     */
    private Integer stuNumbers;

    /**
     * 所属专业id
     */
    private Integer belongSpecId;

    private String belongSpecName;

    /**
     * 所属学院id
     */
    private Integer belongCollegeId;

    private String belongCollegeName;

    /**
     * 年级
     */
    private String grade;

    @TableField(exist = false)
    private List<StudentInfo> studentInfoList;

}
