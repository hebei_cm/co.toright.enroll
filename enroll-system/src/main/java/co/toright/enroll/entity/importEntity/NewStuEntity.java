package co.toright.enroll.entity.importEntity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-13
 */
@Data
public class NewStuEntity extends BaseRowModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 0代表未分配
     */
    @ExcelProperty("学号(可不填)")
    private Integer stuNo;

    /**
     * 准考证号
     */
    @ExcelProperty("准考证号")
    private String admissionId;

    @ExcelProperty("学生姓名")
    private String stuName;

    /**
     * 所属专业id
     */
    @ExcelProperty("所属专业代码(选填)")
    private Integer belongSpecId;

    /**
     * 所属班级，为0代表未分配
     */
    @ExcelProperty("所属班级代码(选填)")
    private Integer belongClassId;

    /**
     * 身份证号
     */
    @ExcelProperty("身份证号")
    private String idCard;

    /**
     * 性别1男0女
     */
    @ExcelProperty("性别")
    private Integer gender;

    @ExcelProperty("出生日期")
    private Date birthday;

    @ExcelProperty("籍贯")
    private String province;

    @ExcelProperty("高考分数")
    private Integer mark;

}
