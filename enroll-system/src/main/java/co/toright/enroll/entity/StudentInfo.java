package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class  StudentInfo implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 0代表未分配
     */
    private String stuNo;

    private String idCard;

    private Integer stuDetailId;

    private String stuName;

    /**
     * 所属专业id
     */
    private Integer belongSpecId;

    /**
     * 所属班级，为0代表未分配
     */
    private Integer belongClassId;


    /**
     * 性别1男0女
     */
    private Boolean gender;


    private Integer belongDormId;

}
