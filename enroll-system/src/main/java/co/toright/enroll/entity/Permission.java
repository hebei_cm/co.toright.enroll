package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 后台权限表
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Data
@TableName("sys_Permis")
@EqualsAndHashCode(callSuper = false)
public class Permission implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自定id,主要供前端展示权限列表分类排序使用.
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 菜单的中文释义
     */
    private String permisName;

    /**
     * 权限代码
     */
    private String permisCode;

    /**
     * 父菜单id
     */

    private Integer parentId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 权限排序
     */
    private Integer permisSort;

    /**
     * 创建时间
     */
      @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
      @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
