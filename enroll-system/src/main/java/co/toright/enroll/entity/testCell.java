package co.toright.enroll.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class testCell extends BaseRowModel{


    @ExcelProperty("正文")
    private String content;

    @ExcelProperty("标题")
    private String title;

    @ExcelProperty("来源")
    private String sourceType;

    @ExcelProperty("网址")
    private String website;

}
