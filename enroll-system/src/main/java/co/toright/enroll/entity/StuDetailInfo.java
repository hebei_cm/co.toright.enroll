package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class StuDetailInfo implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "stu_detail_id", type = IdType.AUTO)
    private Integer stuDetailId;

    private String homeAddress;

    /**
     * 电话
     */
    private String stuPhone;

    private String fatherName;

    private Integer fatherPhone;

    private String fatherWork;

    private String matherName;

    private Integer motherPhone;

}
