package co.toright.enroll.entity.importEntity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
@Data
public class BuildEntity extends BaseRowModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty("楼栋名称")
    private String buildName;

    @ExcelProperty("楼栋管理员")
    private String houseMaster;

    @ExcelProperty("联系电话")
    private String houseTel;

    //0为未定义，1为男生，2为女生宿舍
    @ExcelProperty("宿舍类型(1为男生，2为女生宿舍)")
    private Integer buildType;
}
