package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BuildInfo implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "build_id", type = IdType.AUTO)
    private Integer buildId;

    private String buildName;

    private String houseMaster;

    private String houseTel;

    @TableField(exist = false)
    private List<FloorInfo> floorInfos;


    //0为未定义，1为男生，2为女生宿舍
    private Integer buildType;

    private Integer belongAreaId;

    @TableField(exist = false)
    private Integer floorCount;

}
