package co.toright.enroll.entity.noticeEntity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户关注表,记录了所有用户的关注信息
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserFollow implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private Integer uid;

    /**
     * 关注的用户id
     */
    private Integer followUid;


}
