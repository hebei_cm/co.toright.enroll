package co.toright.enroll.entity.importEntity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
@Data
public class DormEntity extends BaseRowModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty("宿舍号")
    private Integer dormNo;

    /**
     * 宿舍楼栋信息，0未指定
     */
    @ExcelProperty("楼栋号(可不填)")
    private Integer dormBuildId;

    /**
     * 已居住人数
     */
    @ExcelProperty("已居住人数")
    private Integer alreadyLived;


    /**
     * 宿舍可容纳人数
     */
    @ExcelProperty("可容纳人数")
    private Integer dormCapacity;

    /**
     * 宿舍费用
     */
    @ExcelProperty("宿舍费用")
    private Double dormFee;


    @ExcelProperty("备注")
    private String remark;
}
