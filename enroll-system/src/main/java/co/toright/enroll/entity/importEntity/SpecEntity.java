package co.toright.enroll.entity.importEntity;

import co.toright.enroll.entity.ClassesInfo;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 专业批量导入模板
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-07
 */
@Data
public class SpecEntity extends BaseRowModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 专业代码
     */
    @ExcelProperty("专业代码")
    private String specCode;

    /**
     * 专业名称
     */
    @ExcelProperty("专业名称")
    private String specName;

    /**
     * 门类代码
     */
    @ExcelProperty("门类代码")
    private String categoryCode;

    /**
     * 门类名称
     */
    @ExcelProperty("门类名称")
    private String categoryName;

    /**
     * 学科代码
     */
    @ExcelProperty("学科代码")
    private Integer belongCollegeId;

}
