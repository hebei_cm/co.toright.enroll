package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TeacherInfo implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 教工号
     */
    private Integer 

facultyId;

    /**
     * 教师姓名
     */
    private String teacherName;

    private Integer detailInfoId;

    private Integer belongCollegeId;


}
