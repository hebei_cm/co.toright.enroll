package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-03-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class StuStatus implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "status_id", type = IdType.AUTO)
      private Integer id;


    private Integer stuId;

    private String stuNo;

    private String idCard;

    private Integer printReport;

    /**
     * 宿舍分配状态，0未分配，1已分配
     */
    private Integer allocationOfDorm;

    private Integer medicalInsura;

    private Integer regist;

    private Integer fileSubmit;

    private Integer uniformComfirm;

    private Integer payStatus;

    private Integer belongSpecId;

    private Integer belongClassId;


}
