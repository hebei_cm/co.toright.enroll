package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DormStuRelation implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer stuNo;

    private Integer dormId;

    private Integer class_id;

    private Integer spec_id;


}
