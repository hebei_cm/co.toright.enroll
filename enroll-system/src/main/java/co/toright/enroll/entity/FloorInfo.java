package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class FloorInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 楼层id
     */
      @TableId(value = "floor_id", type = IdType.AUTO)
    private Integer floorId;

    private Integer floorNo;

    /**
     * 所属楼栋id
     */
    private Integer belongBuildId;

    private Integer dormCount;

    @TableField(exist = false)
    private List<DormInfo> dormInfos;

}
