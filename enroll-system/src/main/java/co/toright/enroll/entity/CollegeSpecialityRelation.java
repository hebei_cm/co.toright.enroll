package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 学院专业关系
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CollegeSpecialityRelation implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String collegeCode;

    private String specCode;


}
