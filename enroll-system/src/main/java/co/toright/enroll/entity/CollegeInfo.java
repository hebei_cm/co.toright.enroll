package co.toright.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 学院信息
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CollegeInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学院id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 学院代码
     */
    private Integer collegeCode;

    /**
     * 学院名
     */
    private String collegeName;

    /**
     * 院长
     */
    private String president;

    /**
     * 副院长
     */
    private String vicePresident;

    /**
     * 学院地址	            
     */
    private String collegeAddress;

    /**
     * 学院电话
     */
    private String collegeTel;

    /**
     * 学院邮政编码
     */
    private String zipCode;

    /**
     * 学院网址
     */
    private String collegeNetAddress;

    /**
     * 学院简介
     */
    private String description;

    private Integer belongAreaId;


    @TableField(exist = false)
    private List<SpecialityInfo> specialityInfoList;

    @TableField(exist = false)
    private List<BuildInfo> buildInfoList;


}
