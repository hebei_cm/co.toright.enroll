package co.toright.enroll.util;

import java.text.Collator;
import java.util.*;
import java.util.stream.Collectors;

public class Sort {

    /**
     *
     * @param a String[]
     * @return String[]
     * 根据姓名排序
     */
    public static String[] getSortOfChinese(String[] a) {
        // Collator 类是用来执行区分语言环境这里使用CHINA
        Comparator cmp = Collator.getInstance(java.util.Locale.CHINA);

        // JDKz自带对数组进行排序。
        Arrays.sort(a, cmp);
        return a;
    }
}
