package co.toright.enroll.util;

import javax.xml.crypto.Data;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;

public class GetNowTime {

    static Calendar cal = Calendar.getInstance();
    //当前日期
    static int day = cal.get(Calendar.DATE);
    //月份
    static int month = cal.get(Calendar.MONTH) + 1;
    //年份
    static int year = cal.get(Calendar.YEAR);
    //一周的第几天
    static int dow = cal.get(Calendar.DAY_OF_WEEK);
    //一月中的第几天
    static int dom = cal.get(Calendar.DAY_OF_MONTH);
    //一年中的第几天
    static int doy = cal.get(Calendar.DAY_OF_YEAR);

    public static Date getCurrentTime(){

        return cal.getTime();
    }

    public static int getDay() {
        return day;
    }

    public static int getMonth() {
        return month;
    }

    public static int getYear() {
        return year;
    }

    public static int getDow() {
        return dow;
    }

    public static int getDom() {
        return dom;
    }

    public static int getDoy() {
        return doy;
    }
}
