package co.toright.enroll.util;

import co.toright.enroll.constant.Constants;
import co.toright.enroll.entity.InitialStudentData;
import co.toright.enroll.entity.SpecialityInfo;
import co.toright.enroll.service.InitialStudentDataService;
import co.toright.enroll.service.SpecialityInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/fake")
public class CreatFakeData {
    @Autowired
    SpecialityInfoService spService;

    @Autowired
    InitialStudentDataService initialStudentDataService;
    /**
     * 根据每个专业生成30个新生假数据
     * @return
     */
    @GetMapping("/creatNewStu")
    @Transactional
    @ResponseBody
    public Response fakeStuData(){
        List<String> spec_code = spService.list(new QueryWrapper<SpecialityInfo>()
                .select("spec_code")).stream().map(item -> item.getSpecCode())
                .collect(Collectors.toList());
        for(String code :spec_code){
            List<InitialStudentData> list = new ArrayList<>();
            for (int i =1;i<=30;i++){
                InitialStudentData studentData = new InitialStudentData();
                studentData.setNewStuName(Constants.xing[(int) (Math.random()*Constants.xing.length)]+Constants.xing[(int) (Math.random()*Constants.xing.length)]+Constants.xing[(int) (Math.random()*Constants.xing.length)]);
                String citys[] = {"北京","广东/深圳","山东/青岛","江苏/无锡","河南、洛阳","上海","河北/保定","浙江/舟山","香港","山西/太原",
                        "陕西/西安","湖南/长沙","重庆","福建/泉州","天津","云南/大理","四川/成都",
                        "广西/柳州","安徽","海南","江西/宜春","湖北/武汉","辽宁","内蒙古"};
                Random r = new Random();
                int randomInt = r.nextInt(10000)%citys.length;
                String address = citys[randomInt];
                studentData.setOriginOfStu(address);
                studentData.setNativePlace((i%2)==0?citys[randomInt]:address);
                int max=100,min=1;
                int mm = (int) (Math.random()*(max-min)+min);
                int dd = (int) (Math.random()*(max-min)+min);


                //随机生成生日 17-20岁
                long begin = System.currentTimeMillis() - 630720000000L;//20年内
                long end = System.currentTimeMillis() - 536112000000L; //17年内
                long rtn = begin + (long) (Math.random() * (end - begin));
                Date d1 = new Date(rtn);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                String birth = simpleDateFormat.format(d1);

                String idNo = IdCardUtil.getIdNo(birth, (i % 2) == 0 ? true : false);
                studentData.setNewStuIdCard(idNo);
                String substring = idNo.substring(8, 14);
                DateFormat df = new SimpleDateFormat("yyyyMMdd");

                studentData.setDateOfBirth(d1);
                studentData.setApplyingMajorCode(code);
                studentData.setGender((i % 2) == 0 ? true : false);

                Random random = new Random();
                int two = random.nextInt(99);
                String mark = "4"+ Integer.toString(two);
                studentData.setTotalScore(BigDecimal.valueOf(Integer.parseInt(mark)));
                list.add(studentData);
            }
            boolean b = initialStudentDataService.saveBatch(list);
        }
        return Response.error("成功");
    }
}
