package co.toright.enroll.util;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import net.sourceforge.pinyin4j.PinyinHelper;

public class Pinyin4jUtil {


    /**
     * 得到汉字字符串中文首字母
     * 比如：北京 -> B
     *
     * @param str
     * @return
     */
    public static String getFirstLetter(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }

        String convert = "";
        char word = str.charAt(0);
        String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(word);
        if (pinyinArray != null) {
            convert += pinyinArray[0].charAt(0);
        } else {
            convert += word;
        }
        return convert.toUpperCase();
    }
}
