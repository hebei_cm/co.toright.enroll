package co.toright.enroll.vo;

import co.toright.enroll.entity.Role;
import co.toright.enroll.entity.User;
import co.toright.enroll.entity.UserDetailInfo;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserListVo extends User{

    private List<Role> roles;

    private List<UserDetailInfo> userDetailInfoList;
}
