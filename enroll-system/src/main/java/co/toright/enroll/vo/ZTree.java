package co.toright.enroll.vo;

import lombok.Data;

import java.util.List;

@Data
public class ZTree {
    private Integer id;
    private String name;
    private Boolean checked;
    private Boolean nocheck;
    private Boolean isParent;
    private Boolean halfCheck;
    private Boolean open;
    private List<ZTree> children;
}
