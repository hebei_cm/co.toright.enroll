package co.toright.enroll.vo;

import co.toright.enroll.entity.BuildInfo;
import co.toright.enroll.entity.CollegeInfo;
import lombok.Data;

import java.util.List;

@Data
public class CollegeVo {

    private Integer collegeId;

    private String collegeName;

    private Integer belongAreaId;

    private List<SpecVo> specVos;

    private List<BuildVo> buildVos;

}
