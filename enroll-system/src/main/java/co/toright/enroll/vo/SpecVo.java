package co.toright.enroll.vo;

import lombok.Data;

@Data
public class SpecVo {

    private Integer specId;

    private String specName;

    private Integer belongCollegeId;

}
