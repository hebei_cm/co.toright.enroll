package co.toright.enroll.vo;

import lombok.Data;

@Data
public class AreaVo {

    private Integer AreaId;
    private String AreaName;
}
