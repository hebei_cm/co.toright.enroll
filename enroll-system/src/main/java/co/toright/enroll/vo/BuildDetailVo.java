package co.toright.enroll.vo;

import co.toright.enroll.entity.BuildInfo;
import co.toright.enroll.entity.FloorInfo;
import lombok.Data;

import java.util.List;

@Data
public class BuildDetailVo extends BuildInfo {

    private Integer floorCount;

    private Integer roomCount;

    private String belongArea;

}
