package co.toright.enroll.vo;

import lombok.Data;

@Data
public class BedInfoVo {
    private Integer id;
    private Integer bedNo;
    private String belongStuName;
    private String belongSpecName;
}
