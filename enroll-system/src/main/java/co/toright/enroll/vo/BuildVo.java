package co.toright.enroll.vo;

import lombok.Data;

@Data
public class BuildVo {

    private Integer buildId;

    private String buildName;

    private Integer buildType;
}
