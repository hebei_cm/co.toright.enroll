package co.toright.enroll.service;

import co.toright.enroll.entity.FloorInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-25
 */
public interface FloorInfoService extends IService<FloorInfo> {

    //返回封装好楼层宿舍list
    List<FloorInfo> getFloorsWithDormBy(Integer buildId);

    //查询宿舍数量到floorInfo中
    List<FloorInfo> getFloorsByBuildId(Integer buildId);

    //根据楼栋ID返回楼层数量和宿舍数量
    Map<String ,Integer> getFloorDormCountByBuildId(Integer buildId);

}
