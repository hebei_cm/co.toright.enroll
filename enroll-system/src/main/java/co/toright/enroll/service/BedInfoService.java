package co.toright.enroll.service;

import co.toright.enroll.entity.BedInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-03-06
 */
public interface BedInfoService extends IService<BedInfo> {

}
