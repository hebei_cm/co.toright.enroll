package co.toright.enroll.service.impl;

import co.toright.enroll.entity.StudentPayItem;
import co.toright.enroll.mapper.StudentPayItemMapper;
import co.toright.enroll.service.StudentPayItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-07
 */
@Service
public class StudentPayItemServiceImpl extends ServiceImpl<StudentPayItemMapper, StudentPayItem> implements StudentPayItemService {

}
