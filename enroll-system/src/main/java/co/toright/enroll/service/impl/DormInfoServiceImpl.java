package co.toright.enroll.service.impl;

import co.toright.enroll.entity.DormInfo;
import co.toright.enroll.entity.FloorInfo;
import co.toright.enroll.mapper.DormInfoMapper;
import co.toright.enroll.service.DormInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
@Service
public class DormInfoServiceImpl extends ServiceImpl<DormInfoMapper, DormInfo> implements DormInfoService {

    @Override
    public Integer getDormCountsByFloorId(Integer floorId) {
        Integer count = baseMapper.selectCount(new QueryWrapper<DormInfo>().eq("belong_floor_id", floorId));
        return count;
    }
}
