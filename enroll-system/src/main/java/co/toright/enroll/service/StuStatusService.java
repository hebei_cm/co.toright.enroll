package co.toright.enroll.service;

import co.toright.enroll.entity.StuStatus;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-03-05
 */
public interface StuStatusService extends IService<StuStatus> {

}
