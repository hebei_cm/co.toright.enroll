package co.toright.enroll.service.impl;

import co.toright.enroll.entity.CollegeInfo;
import co.toright.enroll.mapper.CollegeInfoMapper;
import co.toright.enroll.service.CollegeInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-07
 */
@Service
public class CollegeInfoServiceImpl extends ServiceImpl<CollegeInfoMapper, CollegeInfo> implements CollegeInfoService {

}
