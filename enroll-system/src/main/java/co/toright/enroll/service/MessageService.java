package co.toright.enroll.service;

import co.toright.enroll.entity.noticeEntity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 私信信息表,包含了所有用户的私信信息 服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
public interface MessageService extends IService<Message> {

}
