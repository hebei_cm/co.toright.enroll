package co.toright.enroll.service.strategyService;

public interface DivideClassStrategy {

    String doOperation();

}
