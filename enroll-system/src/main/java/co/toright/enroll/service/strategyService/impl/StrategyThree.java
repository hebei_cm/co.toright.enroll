package co.toright.enroll.service.strategyService.impl;

import co.toright.enroll.service.strategyService.DivideClassStrategy;
import org.springframework.stereotype.Component;

@Component("three")
public class StrategyThree implements DivideClassStrategy {
    @Override
    public String doOperation() {
        return "three";
    }
}
