package co.toright.enroll.service.impl;

import co.toright.enroll.entity.BuildCollegeRelation;
import co.toright.enroll.mapper.BuildCollegeRelationMapper;
import co.toright.enroll.service.BuildCollegeRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-24
 */
@Service
public class BuildCollegeRelationServiceImpl extends ServiceImpl<BuildCollegeRelationMapper, BuildCollegeRelation> implements BuildCollegeRelationService {

}
