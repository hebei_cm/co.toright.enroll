package co.toright.enroll.service.strategyService.impl;

import co.toright.enroll.service.strategyService.DivideClassStrategy;
import org.springframework.stereotype.Component;

@Component("one")
public class StrategyOne implements DivideClassStrategy {
    @Override
    public String doOperation() {
        return "one";
    }
}