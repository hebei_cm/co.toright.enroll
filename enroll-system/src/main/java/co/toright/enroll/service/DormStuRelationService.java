package co.toright.enroll.service;

import co.toright.enroll.entity.DormStuRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
public interface DormStuRelationService extends IService<DormStuRelation> {

}
