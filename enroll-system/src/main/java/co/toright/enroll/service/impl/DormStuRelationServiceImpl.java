package co.toright.enroll.service.impl;

import co.toright.enroll.entity.DormStuRelation;
import co.toright.enroll.mapper.DormStuRelationMapper;
import co.toright.enroll.service.DormStuRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
@Service
public class DormStuRelationServiceImpl extends ServiceImpl<DormStuRelationMapper, DormStuRelation> implements DormStuRelationService {

}
