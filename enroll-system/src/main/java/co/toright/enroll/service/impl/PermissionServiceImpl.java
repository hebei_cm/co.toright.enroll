package co.toright.enroll.service.impl;


import co.toright.enroll.entity.Permission;
import co.toright.enroll.mapper.PermissionMapper;
import co.toright.enroll.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 后台权限表 服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

}
