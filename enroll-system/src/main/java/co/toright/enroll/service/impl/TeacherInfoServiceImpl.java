package co.toright.enroll.service.impl;

import co.toright.enroll.entity.TeacherInfo;
import co.toright.enroll.mapper.TeacherInfoMapper;
import co.toright.enroll.service.TeacherInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-13
 */
@Service
public class TeacherInfoServiceImpl extends ServiceImpl<TeacherInfoMapper, TeacherInfo> implements TeacherInfoService {

}
