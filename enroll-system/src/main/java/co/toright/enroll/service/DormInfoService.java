package co.toright.enroll.service;

import co.toright.enroll.entity.DormInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
public interface DormInfoService extends IService<DormInfo> {

    //通过楼层编号得到该楼层数量
    Integer getDormCountsByFloorId(Integer floorId);
}
