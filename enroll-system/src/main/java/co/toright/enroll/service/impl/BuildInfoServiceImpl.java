package co.toright.enroll.service.impl;

import co.toright.enroll.entity.BuildInfo;
import co.toright.enroll.mapper.BuildInfoMapper;
import co.toright.enroll.service.BuildInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
@Service
public class BuildInfoServiceImpl extends ServiceImpl<BuildInfoMapper, BuildInfo> implements BuildInfoService {

}
