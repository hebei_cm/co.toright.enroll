package co.toright.enroll.service.impl;

import co.toright.enroll.entity.UserRole;
import co.toright.enroll.mapper.SysUserRoleMapper;
import co.toright.enroll.service.SysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, UserRole> implements SysUserRoleService {

}
