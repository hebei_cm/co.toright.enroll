package co.toright.enroll.service;

import co.toright.enroll.entity.PayType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-05
 */
public interface PayTypeService extends IService<PayType> {

}
