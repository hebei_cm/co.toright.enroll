package co.toright.enroll.service;

import co.toright.enroll.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色-权限关联表 服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
public interface RolePermisService extends IService<RolePermission> {

}
