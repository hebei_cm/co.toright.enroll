package co.toright.enroll.service.impl;

import co.toright.enroll.entity.BedInfo;
import co.toright.enroll.mapper.DormBedMapper;
import co.toright.enroll.service.BedInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-03-06
 */
@Service
public class BedInfoServiceImpl extends ServiceImpl<DormBedMapper, BedInfo> implements BedInfoService {

}
