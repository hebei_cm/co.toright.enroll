package co.toright.enroll.service;

import co.toright.enroll.entity.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 后台权限表 服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
public interface PermissionService extends IService<Permission> {

}
