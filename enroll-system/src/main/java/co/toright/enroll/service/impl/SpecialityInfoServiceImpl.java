package co.toright.enroll.service.impl;

import co.toright.enroll.entity.SpecialityInfo;
import co.toright.enroll.mapper.SpecialityInfoMapper;
import co.toright.enroll.service.SpecialityInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-07
 */
@Service
public class SpecialityInfoServiceImpl extends ServiceImpl<SpecialityInfoMapper, SpecialityInfo> implements SpecialityInfoService {

}
