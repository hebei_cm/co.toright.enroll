package co.toright.enroll.service.impl;

import co.toright.enroll.entity.noticeEntity.Message;
import co.toright.enroll.mapper.MessageMapper;
import co.toright.enroll.service.MessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 私信信息表,包含了所有用户的私信信息 服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService {

}
