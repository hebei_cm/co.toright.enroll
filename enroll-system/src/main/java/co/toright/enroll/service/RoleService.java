package co.toright.enroll.service;

import co.toright.enroll.entity.Role;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 后台角色表 服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
public interface RoleService extends IService<Role> {

    boolean isRoleExist(Role role);

    Page<Role> findAllRole(Integer pn);

    boolean insertRole(Role role);
}
