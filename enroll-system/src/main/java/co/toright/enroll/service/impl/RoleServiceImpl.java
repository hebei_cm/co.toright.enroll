package co.toright.enroll.service.impl;


import co.toright.enroll.entity.Role;
import co.toright.enroll.mapper.RoleMapper;
import co.toright.enroll.service.RoleService;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 后台角色表 服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    RoleMapper roleMapper;

    @Override
    public boolean isRoleExist(Role role) {
        int count = roleMapper.countRolesByRoleCode(role.getRoleCode(), role.getRoleCode());
        if (count>1){
            return true;
        }
        return false;
    }

    @Override
    public Page<Role> findAllRole(Integer pn) {
        List<Role> roles = roleMapper.findAllRole();
        Page<Role> page = new Page<>(pn,5);
        page.setRecords(roles);
        return page;
    }

    @Override
    public boolean insertRole(Role role) {
        QueryWrapper<Role> wrapper = new QueryWrapper<Role>().eq("role_code", role.getRoleCode())
                .or().eq("role_desc", role.getRoleDesc());
        Integer count = roleMapper.selectCount(wrapper);
        if (count>0){
            return false;
        }
        roleMapper.insert(role);
        return true;
    }
}
