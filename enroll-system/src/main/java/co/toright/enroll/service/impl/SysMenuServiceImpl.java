package co.toright.enroll.service.impl;

import co.toright.enroll.entity.Menu;
import co.toright.enroll.mapper.SysMenuMapper;
import co.toright.enroll.service.SysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, Menu> implements SysMenuService {

}
