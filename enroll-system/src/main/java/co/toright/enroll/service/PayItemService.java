package co.toright.enroll.service;

import co.toright.enroll.entity.PayItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-07
 */
public interface PayItemService extends IService<PayItem> {

}
