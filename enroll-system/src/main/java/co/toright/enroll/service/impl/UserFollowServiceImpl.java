package co.toright.enroll.service.impl;

import co.toright.enroll.entity.noticeEntity.UserFollow;
import co.toright.enroll.mapper.UserFollowMapper;
import co.toright.enroll.service.UserFollowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户关注表,记录了所有用户的关注信息 服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
@Service
public class UserFollowServiceImpl extends ServiceImpl<UserFollowMapper, UserFollow> implements UserFollowService {

}
