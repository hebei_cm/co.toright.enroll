package co.toright.enroll.service.impl;

import co.toright.enroll.entity.SchoolRoll;
import co.toright.enroll.mapper.SchoolRollMapper;
import co.toright.enroll.service.SchoolRollService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-17
 */
@Service
public class SchoolRollServiceImpl extends ServiceImpl<SchoolRollMapper, SchoolRoll> implements SchoolRollService {

}
