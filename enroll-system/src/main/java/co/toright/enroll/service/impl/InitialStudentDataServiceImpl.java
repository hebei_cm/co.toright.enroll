package co.toright.enroll.service.impl;

import co.toright.enroll.entity.InitialStudentData;
import co.toright.enroll.mapper.InitialStudentDataMapper;
import co.toright.enroll.service.InitialStudentDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-16
 */
@Service
public class InitialStudentDataServiceImpl extends ServiceImpl<InitialStudentDataMapper, InitialStudentData> implements InitialStudentDataService {

}
