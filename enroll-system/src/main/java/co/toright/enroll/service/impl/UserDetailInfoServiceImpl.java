package co.toright.enroll.service.impl;

import co.toright.enroll.entity.UserDetailInfo;
import co.toright.enroll.mapper.UserDetailInfoMapper;
import co.toright.enroll.service.UserDetailInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-09
 */
@Service
public class UserDetailInfoServiceImpl extends ServiceImpl<UserDetailInfoMapper, UserDetailInfo> implements UserDetailInfoService {

}
