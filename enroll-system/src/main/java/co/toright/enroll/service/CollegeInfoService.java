package co.toright.enroll.service;

import co.toright.enroll.entity.CollegeInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-07
 */
public interface CollegeInfoService extends IService<CollegeInfo> {

}
