package co.toright.enroll.service;

import co.toright.enroll.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 运营后台用户表 服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
public interface UserService extends IService<User> {

    User findRolesByUsername(String username);

}
