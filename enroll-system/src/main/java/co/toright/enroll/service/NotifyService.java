package co.toright.enroll.service;

import co.toright.enroll.entity.noticeEntity.Notify;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
public interface NotifyService extends IService<Notify> {

}
