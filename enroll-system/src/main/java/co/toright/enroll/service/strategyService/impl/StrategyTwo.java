package co.toright.enroll.service.strategyService.impl;

import co.toright.enroll.service.strategyService.DivideClassStrategy;
import org.springframework.stereotype.Component;

@Component("two")
public class StrategyTwo implements DivideClassStrategy {
    @Override
    public String doOperation() {
        return "two";
    }
}
