package co.toright.enroll.service;

import co.toright.enroll.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
public interface SysMenuService extends IService<Menu> {

}
