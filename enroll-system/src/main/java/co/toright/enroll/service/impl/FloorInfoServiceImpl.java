package co.toright.enroll.service.impl;

import co.toright.enroll.entity.DormInfo;
import co.toright.enroll.entity.FloorInfo;
import co.toright.enroll.mapper.FloorInfoMapper;
import co.toright.enroll.service.DormInfoService;
import co.toright.enroll.service.FloorInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-25
 */
@Service
public class FloorInfoServiceImpl extends ServiceImpl<FloorInfoMapper, FloorInfo> implements FloorInfoService {

    @Autowired
    DormInfoService dorService;

    @Override
    public List<FloorInfo> getFloorsWithDormBy(Integer buildId) {
        List<DormInfo> list = dorService.list();
        Map<Integer, List<DormInfo>> listMap = list.stream().collect(Collectors.groupingBy(DormInfo::getBelong_floor_id));
        List<FloorInfo> list1 = baseMapper.selectList(new QueryWrapper<FloorInfo>()
                .eq("belong_build_id", buildId));
        if (!list1.isEmpty()){
            List<FloorInfo> floorInfos =
                    list1.stream().map(floor -> {
                        Integer id = floor.getFloorId();
                        if (listMap.containsKey(id)) {
                            floor.setDormInfos(listMap.get(id));
                        }
                        return floor;
                    }).collect(Collectors.toList());
            return floorInfos;
        }
        return null;
    }

    @Override
    public List<FloorInfo> getFloorsByBuildId(Integer buildId) {
        List<FloorInfo> floorInfos = baseMapper.selectList(new QueryWrapper<FloorInfo>()
                .eq("belong_build_id", buildId))
                .stream().map(floor -> {
                    Integer id = floor.getFloorId();
                    int count = dorService.count(new QueryWrapper<DormInfo>().eq("belong_floor_id", id));
                    floor.setDormCount(count);
                    return floor;
                }).collect(Collectors.toList());

        return floorInfos;
    }

    @Override
    public Map<String, Integer> getFloorDormCountByBuildId(Integer buildId) {
        Map<String ,Integer> map = new HashedMap();
        List<FloorInfo> floorInfos = baseMapper.selectList(new QueryWrapper<FloorInfo>()
                .select("floor_id")
                .eq("belong_build_id", buildId));
        map.put("floorCount", floorInfos.size());
        if (floorInfos.size()!=0){
            //所有宿舍数量
            Integer roomCount = 0;
            for (FloorInfo floorInfo : floorInfos){
                Integer id = floorInfo.getFloorId();
                Integer counts = dorService.getDormCountsByFloorId(id);
                roomCount +=counts;
            }
            map.put("roomCount",roomCount);
        }
        return map;
    }
}
