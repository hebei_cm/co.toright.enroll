package co.toright.enroll.service.impl;

import co.toright.enroll.entity.PayType;
import co.toright.enroll.mapper.PayTypeMapper;
import co.toright.enroll.service.PayTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-05
 */
@Service
public class PayTypeServiceImpl extends ServiceImpl<PayTypeMapper, PayType> implements PayTypeService {

}
