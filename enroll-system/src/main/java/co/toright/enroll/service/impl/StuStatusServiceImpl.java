package co.toright.enroll.service.impl;

import co.toright.enroll.entity.StuStatus;
import co.toright.enroll.mapper.StuStatusMapper;
import co.toright.enroll.service.StuStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-03-05
 */
@Service
public class StuStatusServiceImpl extends ServiceImpl<StuStatusMapper, StuStatus> implements StuStatusService {

}
