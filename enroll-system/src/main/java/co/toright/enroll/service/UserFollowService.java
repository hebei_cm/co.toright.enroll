package co.toright.enroll.service;

import co.toright.enroll.entity.noticeEntity.UserFollow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户关注表,记录了所有用户的关注信息 服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
public interface UserFollowService extends IService<UserFollow> {

}
