package co.toright.enroll.service.impl;


import co.toright.enroll.entity.Permission;
import co.toright.enroll.entity.Role;
import co.toright.enroll.entity.User;
import co.toright.enroll.mapper.PermissionMapper;
import co.toright.enroll.mapper.RoleMapper;
import co.toright.enroll.mapper.UserMapper;
import co.toright.enroll.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 运营后台用户表 服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    UserMapper userMapper;

    @Autowired
    RoleMapper roleMapper;

    @Autowired
    PermissionMapper permissionMapper;

    @Override
    public User findRolesByUsername(String username) {
        User user = userMapper.selectOne(new QueryWrapper<User>().eq("username", username));
        List<Role> roles = userMapper.findRolesByUsername(username);
        for (Role role : roles) {
            List<Permission> rolePermissions = roleMapper.getRolePermissions(role.getId());
            role.setPermissions(rolePermissions);
        }
        user.setRoles(roles);
        return user;
    }
}
