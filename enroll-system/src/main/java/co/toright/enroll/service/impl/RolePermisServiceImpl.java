package co.toright.enroll.service.impl;


import co.toright.enroll.entity.RolePermission;
import co.toright.enroll.mapper.RolePermisMapper;
import co.toright.enroll.service.RolePermisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色-权限关联表 服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Service
public class RolePermisServiceImpl extends ServiceImpl<RolePermisMapper, RolePermission> implements RolePermisService {

}
