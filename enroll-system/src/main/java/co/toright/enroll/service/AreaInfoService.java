package co.toright.enroll.service;

import co.toright.enroll.entity.AreaInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
public interface AreaInfoService extends IService<AreaInfo> {

}
