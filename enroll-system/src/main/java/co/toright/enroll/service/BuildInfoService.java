package co.toright.enroll.service;

import co.toright.enroll.entity.BuildInfo;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
public interface BuildInfoService extends IService<BuildInfo> {



}
