package co.toright.enroll.service.impl;

import co.toright.enroll.entity.DataImportLog;
import co.toright.enroll.mapper.DataImportLogMapper;
import co.toright.enroll.service.DataImportLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-06
 */
@Service
public class DataImportLogServiceImpl extends ServiceImpl<DataImportLogMapper, DataImportLog> implements DataImportLogService {

}
