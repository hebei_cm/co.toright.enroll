package co.toright.enroll.service;

import co.toright.enroll.entity.SchoolRoll;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-17
 */
public interface SchoolRollService extends IService<SchoolRoll> {

}
