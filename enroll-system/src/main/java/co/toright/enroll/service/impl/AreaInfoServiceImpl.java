package co.toright.enroll.service.impl;

import co.toright.enroll.entity.AreaInfo;
import co.toright.enroll.mapper.AreaInfoMapper;
import co.toright.enroll.service.AreaInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-20
 */
@Service
public class AreaInfoServiceImpl extends ServiceImpl<AreaInfoMapper, AreaInfo> implements AreaInfoService {

}
