package co.toright.enroll.controller;

import co.toright.enroll.entity.DataImportLog;
import co.toright.enroll.service.DataImportLogService;
import co.toright.enroll.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/dataLog")
public class DataLogController {
    @Autowired
    DataImportLogService dataImportLogService;


    @GetMapping("/importLog")
    @ResponseBody
    public Response getDataLog(){
        List<DataImportLog> list = dataImportLogService.list();
        return Response.success(list);
    }
}
