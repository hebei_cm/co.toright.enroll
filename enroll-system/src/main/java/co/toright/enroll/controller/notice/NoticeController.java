package co.toright.enroll.controller.notice;

import co.toright.enroll.entity.InitialStudentData;
import co.toright.enroll.entity.PayItem;
import co.toright.enroll.entity.User;
import co.toright.enroll.entity.noticeEntity.Notify;
import co.toright.enroll.service.*;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.collections.map.HashedMap;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/notice")
public class NoticeController {
    @Autowired
    PayItemService payItemService;

    @Autowired
    SocketServer socketServer;

    @Autowired
    NotifyService notifyService;

    @Autowired
    UserService userService;

    @Autowired
    InitialStudentDataService initialStudentDataService;

    @GetMapping("/getAllNotice")
    @ResponseBody
    @RequiresPermissions("allowPublishFeeItem")
    public Response gteAllNotice(){
        int unAuditFeeItem = payItemService.count(new QueryWrapper<PayItem>()
                .eq("audit_status", 0));
        Map<String ,Object> map = new HashedMap();
        int beProcessedStu = initialStudentDataService.count();
        //待审核收费项
        map.put("unAuditFeeItem", unAuditFeeItem);
        map.put("beProcessedStu", beProcessedStu);
        //待审核
        return Response.success(map);
    }

    //发布通知
    @PostMapping("/releaseNotify")
    @ResponseBody
    @Transactional
    public Response releaseNotify(@RequestBody Map<String,String> map){
        String userId = map.get("userId");
        List<Integer> ids = userService.list(new QueryWrapper<User>().select("id"))
                .stream().map(item -> item.getId()).collect(Collectors.toList());
        List<Notify> list = new ArrayList<>();
        for (Integer i :ids){
            Notify notify = new Notify();
            notify.setMsgTitle(map.get("head"));
            notify.setMsgContent(map.get("content"));
            notify.setIsRead(false);
            notify.setReciverId(i);
            notify.setSenderId(Integer.parseInt(userId));
            list.add(notify);
        }
        notifyService.saveBatch(list);
        socketServer.sendAll("1");
        return Response.success();
    }

    //获取在线角色
    @GetMapping("/getOnlineRoles")
    @ResponseBody
    public Response getOnlineRoles(){
        List<String> onlineUsers = socketServer.getOnlineUsers();
        return Response.success(onlineUsers);
    }

    //获取当前用户未读通知
    @GetMapping("/getMyUnreadNotice/{userId}")
    @ResponseBody
    public Response getMyUnreadNotice(@PathVariable("userId")Integer userId){
        List<Notify> list = notifyService.list(new QueryWrapper<Notify>()
                .select("id","msg_title","msg_content")
                .eq("reciver_id", userId).eq("is_read", 0));
        return Response.success(list);
    }

    //刷新用户未读消息数
    @GetMapping("/refreshUnreadItems/{userId}")
    @ResponseBody
    public Response refreshUnreadItems(@PathVariable("userId")Integer userId){
        int count = notifyService.count(new QueryWrapper<Notify>()
                .eq("reciver_id", userId).eq("is_read", 0));
        return Response.success(count);
    }

    //用户确认已读
    @GetMapping("/comfirmHavingRead/{itemId}")
    @ResponseBody
    public Response comfirmHavingRead(@PathVariable("itemId")Integer itemId){

        notifyService.update(new UpdateWrapper<Notify>().set("is_read", 1)
                .eq("id", itemId));
        return Response.success();
    }
}
