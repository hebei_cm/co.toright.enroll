package co.toright.enroll.controller;


import co.toright.enroll.entity.*;
import co.toright.enroll.service.*;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-07
 */
@Controller
@RequestMapping("/specMgr")
public class SpecialityInfoController {
    @Autowired
    SpecialityInfoService spService;

    @Autowired
    StudentInfoService studentInfoService;

    @Autowired
    CollegeInfoService collegeInfoService;


    @Autowired
    ClassesInfoService classService;

    @Autowired
    BedInfoService bedService;
    @Autowired
    BuildInfoService buildInfoService;
    @Autowired
    FloorInfoService floorInfoService;
    @Autowired
    DormInfoService dormInfoService;

    /**
     * 返回专业id+专业名称，用于选择专业多选框
     */
    @GetMapping("/getSpecWithIdAndName")
    @ResponseBody
    public Response muiltChooseInput(){

        List<SpecialityInfo> list = spService.list(new QueryWrapper<SpecialityInfo>().select("spec_id", "spec_name"));
        return Response.success(list);
    }


    //为dormbed添加两列
//    @GetMapping("/add")
//    @ResponseBody
//    @Transactional
//    public Response addColumn(){
//        List<BuildInfo> builds = buildInfoService.list();
//        for (BuildInfo buildInfo : builds){
//            List<FloorInfo> floorInfos = floorInfoService.list(new QueryWrapper<FloorInfo>()
//                    .eq("belong_build_id", buildInfo.getBuildId()));
//            for (FloorInfo floorInfo : floorInfos){
//                List<DormInfo> dorms = dormInfoService.list(new QueryWrapper<DormInfo>().eq("belong_floor_id", floorInfo.getFloorId()));
//
//                for (DormInfo dormInfo :dorms){
//                    List<DormBed> beds = bedService.list(new QueryWrapper<DormBed>().eq("belong_dorm_id", dormInfo.getDormId()));
//                    for (DormBed dormBed :beds){
//                        dormBed.setBedType(buildInfo.getBuildType());
//                        bedService.saveOrUpdate(dormBed);
//                    }
//                }
//
//            }
//        }
//        return Response.success();
//    }

    /**
     * 获取专业宿舍情况
     * @param
     * @return
     */
    @GetMapping("/getDormsBySpecId/{specId}")
    @ResponseBody
    public Response getDormsBySpecId(@PathVariable("specId")Integer specId){
        SpecialityInfo specInfo = spService.getById(specId);
        List<StudentInfo> studentInfos = studentInfoService.list(new QueryWrapper<StudentInfo>()
                .select("id", "belong_spec_id", "gender", "belong_dorm_id")
                .eq("belong_spec_id", specId));
        int womanCounts = (int) studentInfos.stream().filter(item -> item.getGender()).count();
        int manCounts = (int) studentInfos.stream().filter(item -> !item.getGender()).count();


        List<BedInfo> beds = bedService.list(new QueryWrapper<BedInfo>().eq("belong_spec_id", specId));
        int hasDormCounts = beds.size();

        int hasManDorms = (int) beds.stream().filter(item -> item.getBedType() == 1).count();
        int hasWomanDorms = (int) beds.stream().filter(item -> item.getBedType() == 2).count();

        int needManDorms = manCounts-hasManDorms;
        int needWomanDorms = womanCounts-hasWomanDorms;
        Map<String,Object> map = new HashMap<>();
        map.put("specName", specInfo.getSpecName());
        map.put("totals", studentInfos.size());
        map.put("totalsOfMan", manCounts);
        map.put("totalsOfWoman", womanCounts);
        map.put("totalsOfHasDorms",hasDormCounts);
        map.put("needManDorms",needManDorms);
        map.put("needWomanDorms",needWomanDorms);
        return Response.success(map);
    }

    @GetMapping("/getSpecsByCollegeId/{id}")
    @ResponseBody
    public Response getSpecsByCollegeId(@PathVariable("id") Integer collegeId){
        //找出学院下所有专业，以及未关联学院的专业
        List<SpecialityInfo> ListOfAllSpec = spService.list(new QueryWrapper<SpecialityInfo>()
                .select("spec_id", "spec_name", "category_name", "belong_college_id")
                .eq("belong_college_id", collegeId).or()
                .eq("belong_college_id", 0));

        List<Map<String, Object>> collect = ListOfAllSpec.stream().map(spec -> {
            Map<String, Object> map = new HashedMap();
            map.put("specId", spec.getSpecId());
            map.put("specName", spec.getSpecName());
            map.put("categoryName", spec.getCategoryName());
            if (spec.getBelongCollegeId() == collegeId) {
                map.put("checked", true);
            } else {
                map.put("checked", false);
            }
            return map;
        }).collect(Collectors.toList());

        return Response.success(collect);
    }

    /**
     * 保存专业和学院的关系
     * @param
     * @return
     */
    @PostMapping("/submitSpecToCollege")
    @ResponseBody
    public Response submitSpecToCollege(@RequestBody Map<String ,Object> parm){
        //addPart为需要新增关系的部分，removePart为需要删除的关系
        List<Integer> addPart,removePart;

        Integer collegeId = (Integer) parm.get("collegeId");

//        System.out.println(parm);
        List<String> strIds = (List<String>) parm.get("ids");
        //提交的学院专业关系
        List<Integer> ids = new ArrayList<>();
        for (String i :strIds){
            ids.add(Integer.valueOf(i));
        }
        //找出已有关系
        List<Integer> havingIds = spService.list(new QueryWrapper<SpecialityInfo>()
                .eq("belong_college_id", collegeId).select("spec_id"))
                .stream().map(spec -> spec.getSpecId()).collect(Collectors.toList());
        //和已有关系做交集
        List<Integer> intersection = havingIds.parallelStream().filter(item -> ids.contains(item)).collect(Collectors.toList());

        //新关系减去交集得到需要新增ids
        addPart = ids.parallelStream().filter(item -> !intersection.contains(item)).collect(Collectors.toList());


        //旧关系减去交集得到需要删除的ids
        removePart = havingIds.parallelStream().filter(item->!intersection.contains(item)).collect(Collectors.toList());

        try{
            if (addPart.isEmpty() && removePart.isEmpty()){
                return Response.warn("您选中的结果，暂时没有需要修改的部分！");
            }else {
                if (!addPart.isEmpty()){
                    for (Integer specId : addPart) {
                       spService.update(new UpdateWrapper<SpecialityInfo>()
                               .eq("spec_id", specId)
                               .set("belong_college_id", collegeId));
                    }
                }
                if (!removePart.isEmpty()){
                    for (Integer specId : removePart) {
                        spService.update(new UpdateWrapper<SpecialityInfo>()
                                .eq("spec_id", specId)
                                .set("belong_college_id", 0));
                    }
                }
            }
        }catch (Exception e){
            return Response.error("修改失败,保存过程中出现错误！");
        }

        return Response.success("保存成功，新增【"+addPart.size()+"】条数据，移除【"+removePart.size()+"】条数据！");
    }

    @RequestMapping("/list")
    public String list(Model model) {
        List<SpecialityInfo> specList = spService.list();
        List<CollegeInfo> collegeList = collegeInfoService.list();
        for (CollegeInfo college : collegeList) {
            List<SpecialityInfo> spList = specList.stream().filter(
                    spec -> college.getId() == spec.getBelongCollegeId()
            ).collect(Collectors.toList());
            college.setSpecialityInfoList(spList);
        }
        model.addAttribute("collegelist", collegeList);
        return "menu/baseDataMgr/specialityMgr";
    }

    @ResponseBody
    @GetMapping("/findAllSpec")
    public Response findAllSpec() {
        List<SpecialityInfo> list = spService.list();
        return Response.success(list);

    }

    @ResponseBody
    @GetMapping("/findSpec/{collegeId}")
    public Response findOneSpec(@PathVariable(value = "collegeId") Integer id) {
        SpecialityInfo info = spService.getById(id);
        List<SpecialityInfo> list = new ArrayList<>();
        list.add(info);
        return Response.success(list);
    }
}

//批量生成班级
//    @GetMapping("/insertIntoClass")
//    @ResponseBody
//    public Response insertIntoClass(){
//        List<SpecialityInfo> list = spService.list();
//        List<ClassesInfo> classesInfos = new ArrayList<>();
//        for(SpecialityInfo sp : list){
//            Integer specId = sp.getSpecId();
//            String specName = sp.getSpecName();
//            Integer belongCollegeId = sp.getBelongCollegeId();
//            for(int i=1;i<4;i++) {
//                ClassesInfo classesInfo = new ClassesInfo();
//                classesInfo.setClassCode(String.valueOf(specId + i));
//                classesInfo.setClassName(specName+i+"班");
//                classesInfo.setBelongCollegeId(belongCollegeId);
//                classesInfo.setBelongSpecId(specId);
//                classesInfo.setBelongSpecName(specName);
//                classesInfo.setGrade(String.valueOf(2021));
//                classesInfos.add(classesInfo);
//            }
//        }
//        boolean b = classService.saveBatch(classesInfos);
//        return Response.success(b);
//    }

    //批量生成学生
//    @GetMapping("/creatNewStudent")
//    @ResponseBody
//    public Response creatNewStudent(){
//        Random r = new Random();
//        List<ClassesInfo> list = classService.list();
//        List<StudentInfo> list1 = new ArrayList<>();
//        for(ClassesInfo classes: list){
//            for (int i=0;i<30;i++){
//                StringBuilder rs = new StringBuilder();
//                StringBuilder sno = new StringBuilder();
//                StudentInfo student = new StudentInfo();
//                for (int j = 0; j < 14; j++) {
//                    rs.append(r.nextInt(10));
//                    sno.append(r.nextInt(10));
//                }
//                student.setAdmissionId(String.valueOf(rs));
//                student.setStuName(Constants.xing[(int) (Math.random()*Constants.xing.length)]+Constants.xing[(int) (Math.random()*Constants.xing.length)]+Constants.xing[(int) (Math.random()*Constants.xing.length)]);
//                student.setBelongClassId(classes.getId());
//                student.setBelongSpecId(classes.getBelongSpecId());
//                student.setStuNo(Integer.valueOf(sno.toString()));
//                student.setStuDetailId(Integer.valueOf(sno.toString()));
//                list1.add(student);
//            }
//        }
//        boolean b = studentInfoService.saveBatch(list1);
//        return Response.success(b);
//    }
//}

