package co.toright.enroll.controller;


import co.toright.enroll.entity.*;
import co.toright.enroll.service.*;
import co.toright.enroll.util.Response;
import co.toright.enroll.vo.BuildVo;
import co.toright.enroll.vo.CollegeVo;
import co.toright.enroll.vo.SpecVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-07
 */
@Controller
@RequestMapping("/college")
public class CollegeInfoController {
    @Autowired
    AreaInfoService areaInfoService;

    @Autowired
    BuildInfoService buildInfoService;

    @Autowired
    CollegeInfoService coService;

    @Autowired
    SpecialityInfoService spInfoService;

    @Autowired
    BuildCollegeRelationService bcrService;

    @Autowired
    SpecialityInfoService specService;

    @Autowired
    StudentInfoService stuService;

    @Autowired
    StuStatusService stuStatusService;

    @Autowired
    BedInfoService bedService;

    @Autowired
    ClassesInfoService claService;

    /**
     * 根据学院id获取专业编号名称
     * @param collegeId
     * @return
     */
    @GetMapping("/getSpecByCollegeId/{collegeId}")
    @ResponseBody
    public Response getSpecByCollegeId(@PathVariable("collegeId") Integer collegeId){
        List<SpecialityInfo> specLists = specService.list(new QueryWrapper<SpecialityInfo>().select("spec_id", "spec_name").eq("belong_college_id", collegeId));
        List<Map<String ,Object>> list = new ArrayList<>();
        for (SpecialityInfo sp : specLists){
            Map<String,Object> map = new HashMap<>();
            map.put("specId", sp.getSpecId());
            map.put("specName", sp.getSpecName());
            list.add(map);
        }
        return Response.success(list);
    }

    /**
     * 获取所有楼栋，如果传入id在楼栋所属学院数组中，则置为checked：true
     */
    @GetMapping("/getBuildsByCollegeId/{collegeId}")
    @ResponseBody
    public Response getBuildsByCollegeId(@PathVariable("collegeId")Integer collegeId){
        //获得所有build
        QueryWrapper<BuildInfo> queryWrapper = new QueryWrapper<BuildInfo>()
                .select("build_id", "build_name", "build_type");
        List<BuildInfo> buildInfoList = buildInfoService.list(queryWrapper);
        //找到build——college关系表,获取学院下所有build的id
        List<Integer> ids = bcrService.list(new QueryWrapper<BuildCollegeRelation>().eq("college_id", collegeId))
                .stream().map(bcr -> bcr.getBuildId()).collect(Collectors.toList());
        //将已关联的学院checked值设为true，否则未false
        List<Map<String, Object>> collect = buildInfoList.stream().map(bu -> {
            Map<String, Object> map = new HashMap<>();
            map.put("buildId", bu.getBuildId());
            map.put("buildName", bu.getBuildName());
            map.put("buildType", bu.getBuildType());
            if (ids.contains(bu.getBuildId())){
                map.put("checked", true);
            }else {
                map.put("checked", false);
            }
            return map;
        }).collect(Collectors.toList());
        return Response.success(collect);
    }

    /**
     * 获取每个校区下的学院和楼栋的关系
     * @return
     */
    @GetMapping("/getAllCollegeOnArea")
    @ResponseBody
    public Response getAllCollegeOnArea(){
        //所有楼栋信息的Stream
        List<BuildVo> voList = buildInfoService.list(new QueryWrapper<BuildInfo>()
                .select("build_id", "build_name"))
                .stream().map(bu -> {
                    BuildVo buildVo = new BuildVo();
                    buildVo.setBuildId(bu.getBuildId());
                    buildVo.setBuildName(bu.getBuildName());
                    return buildVo;
                }).collect(Collectors.toList());
        //根据楼栋学院关系，查出学院下所有楼栋,通过map收集，key未学院ID，value为楼栋ID
        Map<Integer, List<BuildCollegeRelation>> buildCollegeRelationMap = bcrService.list().stream().collect(Collectors.groupingBy(BuildCollegeRelation::getCollegeId));


        //根据专业所属id分组
        Map<Integer, List<SpecVo>> specCollectByCollegeId = specService.list(new QueryWrapper<SpecialityInfo>()
                .select("spec_id", "spec_name", "belong_college_id")).stream().filter(spec->spec.getBelongCollegeId()!=0).map(spec -> {
            SpecVo specVo = new SpecVo();
            specVo.setSpecId(spec.getSpecId());
            specVo.setSpecName(spec.getSpecName());
            specVo.setBelongCollegeId(spec.getBelongCollegeId());
            return specVo;
        }).collect(Collectors.groupingBy(spec -> spec.getBelongCollegeId()));

        //将楼栋信息、专业信息加入到学院
        Map<Integer, List<CollegeVo>> collegeListMap = coService.list(new QueryWrapper<CollegeInfo>()
                .select("id", "college_name","belong_area_id"))
                .stream().map(co -> {
                    CollegeVo collegeVo = new CollegeVo();
                    collegeVo.setCollegeId(co.getId());
                    collegeVo.setCollegeName(co.getCollegeName());
                    collegeVo.setBelongAreaId(co.getBelongAreaId());
                    if (buildCollegeRelationMap.containsKey(co.getId())){
                        //属于该学院的所有楼栋id
                        List<Integer> buildIds = buildCollegeRelationMap.get(co.getId()).stream().map(bu -> bu.getBuildId()).collect(Collectors.toList());
                        List<BuildVo> collect = voList.stream().filter(bu->buildIds.contains(bu.getBuildId())).collect(Collectors.toList());
                        collegeVo.setBuildVos(collect.isEmpty()?null:collect);
                    }

                    if (specCollectByCollegeId.containsKey(co.getId())){
                        List<SpecVo> specVos = specCollectByCollegeId.get(co.getId());
                        collegeVo.setSpecVos(specVos.isEmpty()?null:specVos);
                    }
                    return collegeVo;
        }).collect(Collectors.groupingBy(CollegeVo::getBelongAreaId));


        Map<String ,List<CollegeVo>> map = new HashMap<>();
        List<AreaInfo> areaInfoList = areaInfoService.list();
        for (AreaInfo areaInfo :areaInfoList){
            if (collegeListMap.containsKey(areaInfo.getAreaId())){
                map.put(areaInfo.getAreaName(), collegeListMap.get(areaInfo.getAreaId()));
            }
        }
        return Response.success(map);
    }

    @GetMapping("/collegelist")
    @ResponseBody
    public Response collegeList(){
        List<CollegeInfo> collegeList = coService.list();
        return Response.success(collegeList);
    }
    /**
     * 通过id获取校区下所有学院
     */
    @GetMapping("/getallcollege/{id}")
    @ResponseBody
    public Response getAllCollegeByAreaId(@PathVariable("id")Integer areaId){
        List<CollegeInfo> collegeInfos = coService.list(new QueryWrapper<CollegeInfo>().eq("belong_area_id", areaId));
        StringBuilder sb = new StringBuilder();
        for (CollegeInfo college: collegeInfos){
            if(sb.length()>0 && !"null".equals(sb.toString()) &&!"".equals(sb.toString())) {

                //stringBuilder不为空，StringBuffer同理
                sb.append(","+college.getCollegeName());
            }
            else {
                sb.append(college.getCollegeName());
            }
        }
        //将所在校区的所有学院以字符串的方式返回
        System.out.println(sb);
        return Response.success(sb);
    }

    /**
     * 获取未分配校区的学院信息
     * @param
     * @return
     */
    @GetMapping("/Unallocated/{id}")
    @ResponseBody
    public Response Unallocated(@PathVariable("id")Integer areaId){
        List<CollegeInfo> list = coService.list();
//        System.out.println(list);
        List<CollegeInfo> collect = list.stream().filter(college -> {
            if (college.getBelongAreaId() == 0 || college.getBelongAreaId() == areaId) {
                return true;
            } else {
                return false;
            }
        }).collect(Collectors.toList());

        //获取未分配校区和本校区学院
        List<Map<String ,Object>> myCollegeList = new ArrayList<>();
        for (CollegeInfo college:collect){
            Map<String ,Object> map = new HashedMap();
            map.put("collegeId", college.getId());
            map.put("collegeName", college.getCollegeName());
            if (college.getBelongAreaId()==0){
                map.put("checked", false);
            }else {
                map.put("checked", true);
            }
            myCollegeList.add(map);
        }
        return Response.success(myCollegeList);
    }

    @GetMapping("/list")
    @ResponseBody
    public Response collegeTable(Integer pn){
        List<CollegeInfo> list = coService.list();
        List<SpecialityInfo> specInfos = spInfoService.list();

        List<CollegeInfo> objList = list.stream().map(college -> {
            //过滤出该学院下专业信息
            List<SpecialityInfo> collect = specInfos.stream().filter(spec -> college.getId()==(spec.getBelongCollegeId())
            ).collect(Collectors.toList());
            college.setSpecialityInfoList(collect);
            return college;
        }).collect(Collectors.toList());

        return Response.success(objList);

    }

    /**
     * 获取学院列表，select用
     *
     */
    @GetMapping("/getCollegeOption")
    @ResponseBody
    public Response getCollegeOption(){
        List<CollegeInfo> list = coService.list(new QueryWrapper<CollegeInfo>()
                .select("id", "college_name"));
        List<Map<String,Object>> collect = new ArrayList<>();
        for (CollegeInfo co : list){
            Map<String ,Object> map = new HashMap<>();
            map.put("collegeId", co.getId());
            map.put("collegeName", co.getCollegeName());
            collect.add(map);
        }
        return Response.success(collect);
    }

    /**
     * 根据学院id查看学院所有新生人数，男生，女生人数
     */
    @GetMapping("/getCollegeNum/{id}")
    @ResponseBody
    public Response getCollegeNum(@PathVariable("id") String id){
        Integer counts= 0;
        Integer manCounts = 0;
        Integer womanCounts = 0;
        Integer manHasDormCounts = 0;
        Integer womanHasDormCounts = 0;

        List<Integer> ids = specService.list(new QueryWrapper<SpecialityInfo>()
                .select("spec_id").eq("belong_college_id", id))
                .stream().map(co -> co.getSpecId()).collect(Collectors.toList());

       for (Integer i : ids){
           List<StudentInfo> list = stuService.list(new QueryWrapper<StudentInfo>().select("id", "belong_spec_id", "belong_dorm_id", "gender")
                   .eq("belong_spec_id", i));
           Integer womanCount = Math.toIntExact(list.stream().filter(item -> item.getGender()).count());
           Integer manCount = Math.toIntExact(list.stream().filter(item -> !item.getGender()).count());

           List<BedInfo> beds = bedService.list(new QueryWrapper<BedInfo>().eq("belong_spec_id", i));
           Integer manHasDorm = Math.toIntExact(beds.stream().filter(item -> item.getBedType() == 1).count());
           Integer womanHasDorm = Math.toIntExact(beds.stream().filter(item -> item.getBedType()==2).count());
           manCounts +=manCount;
           womanCounts +=womanCount;
           manHasDormCounts += manHasDorm;
           womanHasDormCounts += womanHasDorm;
           counts += (manCount+womanCount);

       }
       Map<String ,Integer> map = new HashMap<>();
       map.put("all", counts);
       map.put("man", manCounts);
       map.put("woman", womanCounts);
       map.put("manHasDormCounts", manHasDormCounts);
       map.put("womanHasDormCounts", womanHasDormCounts);
        System.out.println(counts);
        return Response.success(map);
    }

    /**
     * 为新生添加迎新状态
     * @return
     */
    @GetMapping("/addStatus")
    @ResponseBody
    @Transactional
    public Response add(){
        List<StudentInfo> list = stuService.list();
        for (StudentInfo item :list){
            StuStatus stuStatus = new StuStatus();
            stuStatus.setStuId(item.getId());
            stuStatus.setStuNo(item.getStuNo());
            stuStatus.setBelongSpecId(item.getBelongSpecId());
            stuStatus.setBelongClassId(item.getBelongClassId());
            stuStatusService.saveOrUpdate(stuStatus);
        }
        return Response.success();
    }


    /**
     * 获取学院、专业、班级、新生、总数量
     */
    @GetMapping("/getCollegeLabs")
    @ResponseBody
    public Response getCollegeLabs(){
        int collegeCounts = coService.count();
        int specCounts = specService.count();
        int classCounts = claService.count();
        int newStuCounts = stuService.count();
        Map<String ,Integer> map = new HashMap<>();
        map.put("collegeCounts",collegeCounts);
        map.put("specCounts",specCounts);
        map.put("classCounts",classCounts);
        map.put("newStuCounts",newStuCounts);
        return Response.success(map);
    }
}

