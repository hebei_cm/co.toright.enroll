package co.toright.enroll.controller.notice;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 私信信息表,包含了所有用户的私信信息 前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
@RestController
@RequestMapping("/enroll/message")
public class MessageController {

}

