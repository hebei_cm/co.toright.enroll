package co.toright.enroll.controller;

import co.toright.enroll.entity.User;
import co.toright.enroll.service.UserService;
import co.toright.enroll.util.QRCodeGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.zxing.WriterException;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.misc.BASE64Decoder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
public class QrImgController {

    @Autowired
    UserService userService;

    @GetMapping(value="/qrimage")
    public ResponseEntity<byte[]> getQRImage() {

        String principal = (String) SecurityUtils.getSubject().getPrincipal();
        //二维码内的信息
//        User user = userService.getOne(new QueryWrapper<User>().eq("username", principal));
//        user.setPassword("");

        String info = "当前用户是： "+principal;

        System.out.println(info);

        byte[] qrcode = null;
        try {
            qrcode = QRCodeGenerator.getQRCodeImage(info, 360, 360);
        } catch (WriterException e) {
            System.out.println("Could not generate QR Code, WriterException :: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Could not generate QR Code, IOException :: " + e.getMessage());
        }

        // Set headers
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);

        return new ResponseEntity<byte[]> (qrcode, headers, HttpStatus.CREATED);
    }

    @PostMapping("/saveimg")
    @ResponseBody
    public Map<String,String> getImg(@RequestParam("img") String base64Data){
        System.out.println("==上传图片==");
        System.out.println("==接收到的数据=="+base64Data);

        String dataPrix = ""; //base64格式前头
        String data = "";//实体部分数据
        if(base64Data==null||"".equals(base64Data)){
            String resule="上传失败，上传图片数据为空";
            Map<String ,String > map = new HashMap<>();
            map.put("result", resule);
            return map;
        }else {
            base64Data.substring(22);
            String [] d = base64Data.split("base64,");//将字符串分成数组
            if(d != null && d.length == 2){
                dataPrix = d[0];
                data = d[1];
            }else {
                String resule="上传失败，数据不合法";
                Map<String ,String > map = new HashMap<>();
                map.put("result", resule);
                return map;
            }
        }
        String suffix = "";//图片后缀，用以识别哪种格式数据
        //data:image/jpeg;base64,base64编码的jpeg图片数据
        if("data:image/jpeg;".equalsIgnoreCase(dataPrix)){
            suffix = ".jpg";
        }else if("data:image/x-icon;".equalsIgnoreCase(dataPrix)){
            //data:image/x-icon;base64,base64编码的icon图片数据
            suffix = ".ico";
        }else if("data:image/gif;".equalsIgnoreCase(dataPrix)){
            //data:image/gif;base64,base64编码的gif图片数据
            suffix = ".gif";
        }else if("data:image/png;".equalsIgnoreCase(dataPrix)){
            //data:image/png;base64,base64编码的png图片数据
            suffix = ".png";
        }else {
            String resule="上传图片格式不合法";
            Map<String ,String > map = new HashMap<>();
            map.put("result", resule);
            return map;
        }
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        String tempFileName=uuid+suffix;
        String imgFilePath = "C:\\Users\\Administrator\\Desktop\\"+tempFileName;//新生成的图片
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            //Base64解码
            byte[] b = decoder.decodeBuffer(data);
            for(int i=0;i<b.length;++i) {
                if(b[i]<0) {
                    //调整异常数据
                    b[i]+=256;
                }
            }
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            String imgurl="http://xxxxxxxx/"+tempFileName;
            //imageService.save(imgurl);
            Map<String ,String > map = new HashMap<>();
            map.put("result", imgurl);
            return map;
        } catch (IOException e) {
            e.printStackTrace();
            String result = "上传的图片不合法";
            Map<String ,String > map = new HashMap<>();
            map.put("result", result);
            return map;
        }
    }
}
