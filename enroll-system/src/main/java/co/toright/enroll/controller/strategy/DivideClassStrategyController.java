package co.toright.enroll.controller.strategy;

import co.toright.enroll.factory.FactoryForStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/strategy")
public class DivideClassStrategyController {

    @Autowired
    FactoryForStrategy factoryForStrategy;

    //新生分班策略
    @GetMapping("/divideClass/{key}")
    @ResponseBody
    public String strategy(@PathVariable("key") String key) {
        String result;
        try {
            result = factoryForStrategy.getDivideClassStrategy(key).doOperation();
        } catch (Exception e) {
            result = e.getMessage();
        }
        return result;
    }

}
