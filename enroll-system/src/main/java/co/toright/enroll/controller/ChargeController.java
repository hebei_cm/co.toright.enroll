package co.toright.enroll.controller;

import co.toright.enroll.entity.*;
import co.toright.enroll.service.PayItemService;
import co.toright.enroll.service.PayTypeService;
import co.toright.enroll.service.SpecialityInfoService;
import co.toright.enroll.service.UserService;
import co.toright.enroll.util.MathUtils;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.collections.map.HashedMap;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/charge")
public class ChargeController {

    @Autowired
    PayTypeService payTypeService;

    @Autowired
    SpecialityInfoService specService;

    @Autowired
    PayItemService payItemService;

    @Autowired
    UserService userService;


    /**
     * 发布收费项
     * @return
     */
    @PostMapping("/publishChargeItem")
    @ResponseBody
    @Transactional
    public Response publishCharge(@RequestBody Map<String ,Object> map){
        System.out.println(map);
        List<String> Strings = (List<String>) map.get("muiltSpec");
        List<Integer> muiltSpec = new ArrayList<>();
        for(String s : Strings){
            muiltSpec.add(Integer.valueOf(s));
        }

        // 用于计数线程是否执行完成
        CountDownLatch countDownLatch = new CountDownLatch(muiltSpec.size());
        System.out.println("---- start ----");
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        long start = System.currentTimeMillis();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        for(Integer i : muiltSpec){
            cachedThreadPool.execute(() -> {
                try {
                    System.out.println(Thread.currentThread().getName()+"开始");
                    String typeId = (String) map.get("feeType");
                    PayType type = payTypeService.getById((Serializable) map.get("feeType"));
                    SpecialityInfo spec = specService.getById(i);
                    final String FeeName = type.getPayTypeName()+"---"+spec.getSpecName();
                    PayItem payItem = new PayItem();
                    //从session中拿到当前用户的信息
                    Subject subject = SecurityUtils.getSubject();
                    Session subjectSession = subject.getSession();
                    User userInfo = (User) subjectSession.getAttribute("userInfo");

                    //存储收费专业id
                    payItem.setChargedSpecId(i);
                    //存储发布人id
                    payItem.setReleasePeopleId(userInfo.getId());
                    //存储收费类型id
                    payItem.setPayTypeId(Integer.valueOf(typeId));
                    //存储收费项名称
                    payItem.setPayItemName(type.getPayTypeName()+"---"+spec.getSpecName());
                    //存储审核状态/默认0未审核1审核中2通过3未通过
                    payItem.setAuditStatus(0);
                    payItem.setAuditMsg("");
                    //存储收费金额
                    BigDecimal feeAmount = MathUtils.getBigDecimal(map.get("feeAmount"));
                    //起始时间
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");//注意月份是MM
                    Date time1 = simpleDateFormat.parse((String) map.get("start"));
                    Date time2 = simpleDateFormat.parse((String) map.get("end"));
                    payItem.setStartTime(time1);
                    payItem.setEndTime(time2);
                    payItem.setRemark((String) map.get("remark"));
                    payItem.setChargeAmount(feeAmount);
                    //先判断收费项名称，该专业是否拥有，每个专业只能有一种收费类型，《其他》除外
                    int count = payItemService.count(new QueryWrapper<PayItem>().eq("pay_item_name", FeeName)
                            .eq("enabled", 0));
                    if (count==0){
                        //保存发布成功项
                        atomicInteger.incrementAndGet();
                        payItemService.save(payItem);
                    }else {
                        //保存发布失败项
                        payItem.setAuditMsg("该专业收费项已存在，请勿重复发布！");
                        payItem.setEnabled(1);
                        //1000为系统审核
                        payItem.setVerifierId(1000);
                        payItem.setAuditStatus(3);
                        payItemService.save(payItem);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
                finally {
                    countDownLatch.countDown();
                }
            });
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("任务耗时:" + (end - start) + "毫秒");
        return Response.success("成功保存"+atomicInteger.get()+"项，失败"+(Strings.size()-atomicInteger.get())+"项！");
    }

    /**
     * 获取最新10条发布的发布内容
     */
    @GetMapping("/getNewPublishItem")
    @ResponseBody
    public Response getNewPublishItem(){
        //从session中拿到当前用户的信息
        Subject subject = SecurityUtils.getSubject();
        Session subjectSession = subject.getSession();
        User userInfo = (User) subjectSession.getAttribute("userInfo");

        Page<PayItem> page = new Page<>(1, 10);

        Page<PayItem> list = payItemService.page(page, new QueryWrapper<PayItem>()
                .select("id", "pay_item_name", "audit_status","charge_amount", "creat_time","audit_msg")
                .eq("release_people_id", userInfo.getId())
                .orderByDesc("creat_time"));
        return Response.success(list);
    }

    /**
     * 获取收费项发布者信息
     */
    @GetMapping("/getPublisher")
    @ResponseBody
    public  Response getPublisher(){
        List<Integer> publisherIds = payItemService.list(new QueryWrapper<PayItem>().select("DISTINCT release_people_id"))
                .stream().map(item -> item.getReleasePeopleId()).collect(Collectors.toList());
        List<User> list = userService.list(new QueryWrapper<User>().select("id", "username", "realname")
                .in("id", publisherIds));
        return Response.success(list);
    }
}
