package co.toright.enroll.controller;

import co.toright.enroll.entity.AreaInfo;
import co.toright.enroll.entity.BuildCollegeRelation;
import co.toright.enroll.entity.BuildInfo;
import co.toright.enroll.service.AreaInfoService;
import co.toright.enroll.service.BuildCollegeRelationService;
import co.toright.enroll.service.BuildInfoService;
import co.toright.enroll.util.Response;
import co.toright.enroll.vo.AreaVo;
import co.toright.enroll.vo.BuildVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.models.auth.In;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RequestMapping("/buildCollegeRelation")
@Controller
public class BuildCollegeRelationController {

    @Autowired
    BuildCollegeRelationService bcrService;
    @Autowired
    AreaInfoService areaService;
    @Autowired
    BuildInfoService buService;

    /**
     *获取楼栋信息到下拉框中
     * @return
     */
    @GetMapping("/getBuildList/{areaId}")
    @ResponseBody
    public Response getBuildList(@PathVariable("areaId") Integer areaId){
        System.out.println(areaId);
        List<BuildVo> collect = buService.list(new QueryWrapper<BuildInfo>().select("build_id", "build_name")
                .eq("belong_area_id", areaId))
                .stream().map(bu -> {
                    BuildVo buildVo = new BuildVo();
                    buildVo.setBuildId(bu.getBuildId());
                    buildVo.setBuildName(bu.getBuildName());
                    return buildVo;
                }).collect(Collectors.toList());
        return Response.success(collect);
    }

    //获取校区列表到下拉框中
    @GetMapping("/getAreaList")
    @ResponseBody
    public Response getAreaList(){
        List<AreaInfo> list = areaService.list();
        List<AreaVo> collect = list.stream().map(area -> {
            AreaVo areaVo = new AreaVo();
            areaVo.setAreaId(area.getAreaId());
            areaVo.setAreaName(area.getAreaName());
            return areaVo;
        }).collect(Collectors.toList());
        return Response.success(collect);
    }

    /**
     * 保存楼栋学院关系
     */
    @PostMapping("/saveRelation")
    @ResponseBody
    @Transactional
    public Response saveRelation(@RequestBody Map<String,Object> parms){
        List<Integer> removePart;
        List<Integer> addPart;

        Integer collegeId = Integer.valueOf(parms.get("collegeId").toString());

        List<String> strs = (List<String>) parms.get("ids");
        //前端传送的选中楼栋ids
        List<Integer> ids = new ArrayList<>();
        for(String i :strs){
            ids.add(Integer.valueOf(i));
        }

        //当前学院已有楼栋ids
        List<BuildCollegeRelation> list = bcrService.list(new QueryWrapper<BuildCollegeRelation>()
                .eq("college_id", collegeId));
        List<Integer> havingIds = new ArrayList<>();
        for(BuildCollegeRelation bu : list){
            havingIds.add(bu.getBuildId());
        }

        try {
            //交集（新∩旧）
            List<Integer> intersection = ids.parallelStream().filter(item -> havingIds.contains(item)).collect(Collectors.toList());
            //旧-（新∩旧）=要删除的部分
           removePart = havingIds.parallelStream().filter(item -> !intersection.contains(item)).collect(Collectors.toList());
            if (!removePart.isEmpty()){
                removePart.forEach(build->bcrService.remove(new QueryWrapper<BuildCollegeRelation>()
                        .eq("build_id", build)));
            }
            //新-（新∩旧）=要新增的部分
            addPart = ids.parallelStream().filter(item -> !intersection.contains(item)).collect(Collectors.toList());
            if (!addPart.isEmpty()){
                for (Integer i : addPart){
                    BuildCollegeRelation bcr = new BuildCollegeRelation();
                    bcr.setBuildId(i);
                    bcr.setCollegeId(collegeId);
                    bcrService.save(bcr);
                }
            }
        }catch (Exception e){
            return Response.error("删除或保存过程中出错！！！");
        }
        return Response.success("操作成功,新添："+addPart.size()+"条数据，移除:"+removePart.size()+"条数据!");
    }
}
