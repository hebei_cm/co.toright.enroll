package co.toright.enroll.controller;


import co.toright.enroll.constant.Constants;
import co.toright.enroll.entity.*;
import co.toright.enroll.events.NewStuRegisterService;
import co.toright.enroll.exception.StuRegisterException;
import co.toright.enroll.service.*;
import co.toright.enroll.util.GetNowTime;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.collections.map.HashedMap;
import org.ehcache.impl.internal.concurrent.ConcurrentHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.text.Collator;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-13
 */
@Controller
@RequestMapping("/classes")
public class ClassesInfoController {
    @Autowired
    ClassesInfoService classesInfoService;
    @Autowired
    SpecialityInfoService spService;
    @Autowired
    StudentInfoService studentInfoService;
    @Autowired
    InitialStudentDataService initialStudentDataService;
    @Autowired
    NewStuRegisterService newStuRegisterService;
    @Autowired
    CollegeInfoService collegeService;
    /**
     * 去班级管理界面
     * @return
     */
    @GetMapping("/classesMgr")
    public String toClassMgr(){
        return "menu/baseDataMgr/classesMgr";
    }

    @GetMapping("/getClassInfo/{id}")
    @ResponseBody
    public Response getClassInfo(@PathVariable("id")Integer id){
        ClassesInfo classesInfo = classesInfoService.getById(id);
        Integer belongSpecId = classesInfo.getBelongSpecId();
        SpecialityInfo specialityInfo = spService.getById(belongSpecId);
        int count = studentInfoService.count(new QueryWrapper<StudentInfo>().eq("belong_class_id", classesInfo.getId()));
        classesInfo.setStuNumbers(count);
        Map<String,Object> map = new HashedMap();
        map.put("classInfo", classesInfo);
        map.put("specInfo", specialityInfo);
        return Response.success(map);
    }

    /**
     * 获取班级的选择列表
     * @return
     */
    @GetMapping("/getClassOption/{specCode}")
    @ResponseBody
    public Response getClassOption(@PathVariable("specCode")String specCode){
        SpecialityInfo spec = spService.getOne(new QueryWrapper<SpecialityInfo>()
                .select("spec_id","spec_code")
                .eq("spec_code", specCode));
        List<ClassesInfo> list = classesInfoService.list(new QueryWrapper<ClassesInfo>()
                .select("id", "class_name")
                .eq("belong_spec_id", spec.getSpecId()));
        return Response.success(list);
    }

    /**
     * 保存新生班级关系
     */
    @PostMapping("/saveStuClassRelation")
    @ResponseBody
    //在抛出StuRegisterException.class异常时回滚
    @Transactional(rollbackFor = {RuntimeException.class})
    public Response saveStuClassRelation(@RequestBody Map<String,Object> map){
        List<Integer> ids = (List<Integer>) map.get("ids");
        Integer classId = (Integer) map.get("classId");
        int max = 0;
        //生成学号策略学院代号+专业排序+班级排序
        ClassesInfo classesInfo = classesInfoService.getById(classId);
        SpecialityInfo spec = spService.getById(classesInfo.getBelongSpecId());
        CollegeInfo college = collegeService.getById(spec.getBelongCollegeId());
        String prefix = String.format("%02d", college.getCollegeCode())
                +String.format("%02d", spec.getSortInCollege())
                +String.format("%02d", classesInfo.getClassCode());
        //获取当前班级下最大学号
         max = studentInfoService.list(new QueryWrapper<StudentInfo>()
                .select("stu_no").eq("belong_class_id", classId))
                .stream().mapToInt(item -> {
                    String stuNo = item.getStuNo();
                    return Integer.parseInt(stuNo.substring(10));
                }).max().orElse(0);
        //保存失败的学生id
        List<Integer> faildIds = new ArrayList<>();
        ReentrantLock reentrantLock = new ReentrantLock();
        try{
            reentrantLock.lock();
            for (int i =0;i<ids.size();i++){
                //初始表中的学生id
                int initStuId = ids.get(i);
                // 0 代表前面补充0
                // 4 代表长度为4
                // d 代表参数为正数型
                //学号生成模式：年级+学院号+专业排序+班级排序+个人排序
                //System.out.println("sno="+"2021"+prefix+String.format("%02d", count+i+1));
                //TODO 为每个新生分配学号
                String id = "2021"+prefix+String.format("%02d", max+i+1);
                //1、逻辑删除初始表
                //2、创建学生表实体记录
                //3、关联班级，专业，学院，关系
                //4、生成学号
                //5、创建学籍卡
                //6、检查有无需补充收费项
                //7、创建新生账号
                InitialStudentData one = initialStudentDataService.getOne(new QueryWrapper<InitialStudentData>()
                        .eq("id", initStuId));
                boolean register = newStuRegisterService.register(one, id, classId);
                if (!register){
                    faildIds.add(i);
                }
                //将初始化成功的学生从初始表中移除
                initialStudentDataService.removeById(initStuId);
            }
            reentrantLock.unlock();
        }catch (Exception e){
            return Response.success("多个班级正在保存中！");
        }
        return Response.success("关系保存成功！失败的有【"+faildIds+"】"+"最大学号"+max);
    }


    /**
     * 根据学生创建班级
     * @return
     */
    @PostMapping("/creatNewClassesByStus")
    @ResponseBody
    public synchronized Response creatNewClassesByStus(@RequestBody HashMap<String ,Object> parm){
        List<InitialStudentData> collect;
        Map<Integer,List<InitialStudentData>> map =new ConcurrentHashMap<>();
        try{
            ArrayList<Integer> ids = (ArrayList<Integer>) parm.get("ids");
            Integer classCounts = (Integer) parm.get("classCounts");
            Integer stragetyId = (Integer) parm.get("stragetyId");
            String specCode = (String) parm.get("specCode");

            List<InitialStudentData> students = initialStudentDataService.listByIds(ids);

            //排序后结果放入collect中
            collect = students.stream().sorted((p1, p2) -> {
                Comparator comparator = Collator.getInstance(Locale.CHINA);
                int compare = comparator.compare(p1.getOriginOfStu(), p2.getOriginOfStu());
                return compare;
            }).collect(Collectors.toList());

            for(int i =0 ;i<collect.size();i++){
                int i1 = i % classCounts;
                if (map.containsKey(i1)){
                    List<InitialStudentData> strings = map.get(i1);
                    strings.add(collect.get(i));
                    map.put(i1,strings);
                }else{
                    List<InitialStudentData> list = new ArrayList<>();
                    list.add(collect.get(i));
                    map.put(i1, list);
                }
            }
            Response response = creatNewClass(specCode, map);
            if (!response.getSuccess()){
                throw new StuRegisterException(200, response.getMsg());
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            return Response.success("空指针异常");
        }catch (StuRegisterException e){
            return Response.error(e.getMsg());
        }
        catch (ClassCastException e){
            return Response.error("类型转换异常");
        }
        return Response.success("保存成功");
    }

    public Response creatNewClass(String specCode,Map<Integer,List<InitialStudentData>> map){
        Integer sort = 0;
        //找到专业
        SpecialityInfo spec = spService.getOne(new QueryWrapper<SpecialityInfo>()
                .eq("spec_code", specCode));
        //根据该专业下班级的代码排序，获取最大班级的排序
        List<ClassesInfo> list = classesInfoService.list(new QueryWrapper<ClassesInfo>().select("class_code")
                .eq("belong_spec_id", spec.getSpecId()).orderByDesc("class_code").last("limit 1"));
        if (list.size()>0){
            sort = list.get(0).getClassCode();
        }
        //当前年
        int year = GetNowTime.getYear();

        //遍历map中集合，用来创建班级，以及保存班级学生关系
        try{
            for (Map.Entry<Integer, List<InitialStudentData>> entry : map.entrySet()) {
                Integer mapKey = entry.getKey();
                List<InitialStudentData> mapValue = entry.getValue();
                Integer cno = mapKey+sort;
                System.out.println("生成"+spec.getSpecName()+Constants.getChineseNum(cno)+"班");
                ClassesInfo classesInfo = new ClassesInfo();
                classesInfo.setClassCode(cno+1);
                classesInfo.setClassName(spec.getSpecName()+Constants.getChineseNum(cno)+"班");
                classesInfo.setBelongSpecId(spec.getSpecId());
                classesInfo.setBelongSpecName(spec.getSpecName());
                classesInfo.setBelongCollegeId(spec.getBelongCollegeId());
                classesInfo.setGrade(String.valueOf(year));
//                System.out.println("班级信息"+classesInfo);
                boolean save = classesInfoService.save(classesInfo);
                if (save){
                    Integer classId = classesInfo.getId();
                    //班内根据姓名排序
                    List<Integer> collect = mapValue.stream().sorted((p1, p2) -> {
                        Comparator comparator = Collator.getInstance(Locale.CHINA);
                        int compare = comparator.compare(p1.getNewStuName(), p2.getNewStuName());
                        return compare;
                    }).map(item->item.getId()).collect(Collectors.toList());

                    Map<String ,Object> claStuMap = new HashedMap();
                    claStuMap.put("classId", classId);
                    claStuMap.put("ids",collect);
                    saveStuClassRelation(claStuMap);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
            return Response.error("保存出错");
        }
        return Response.success();
    }
}

