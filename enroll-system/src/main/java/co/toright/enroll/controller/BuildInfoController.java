package co.toright.enroll.controller;

import co.toright.enroll.entity.*;
import co.toright.enroll.service.*;
import co.toright.enroll.util.Response;
import co.toright.enroll.vo.BuildDetailVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/build")
public class BuildInfoController {
    @Autowired
    BuildInfoService buService;
    @Autowired
    FloorInfoService floorInfoService;
    @Autowired
    DormInfoService dormService;
    @Autowired
    AreaInfoService areaInfoService;
    @Autowired
    BuildCollegeRelationService buildCollegeRelationService;
    @Autowired
    FloorInfoService foService;
    @Autowired
    BedInfoService bedInfoService;

    /**
     * 找出学院下公寓
     */
    @GetMapping("/getBuildByCollegeId/{collegeId}")
    @ResponseBody
    public Response getBuildByCollegeId(@PathVariable("collegeId") Integer collegeId){
        //所有楼栋
        List<BuildInfo> allBuilds = buService.list();
        //所有男生楼栋
        List<BuildInfo> allmanBuilds = allBuilds.stream().filter(item -> item.getBuildType() == 1).collect(Collectors.toList());
        List<BuildInfo> allwomanBuilds = allBuilds.stream().filter(item -> item.getBuildType() == 2).collect(Collectors.toList());
        //学院选中的楼
        List<Integer> buildIds = buildCollegeRelationService.list(new QueryWrapper<BuildCollegeRelation>()
                .eq("college_id", collegeId)).stream().map(item -> item.getBuildId()).collect(Collectors.toList());

        List<Map<String ,Object>> manBuilds = new ArrayList<>();
        for (BuildInfo bu : allmanBuilds){
            Map<String ,Object> map = new HashMap<>();
            map.put("id", bu.getBuildId());
            map.put("name", bu.getBuildName());
            map.put("pId", 0);
            map.put("checked", buildIds.contains(bu.getBuildId())?true:false);
            manBuilds.add(map);
        }
        for (BuildInfo bu: allmanBuilds){
            List<FloorInfo> floorInfos = floorInfoService.list(new QueryWrapper<FloorInfo>().eq("belong_build_id", bu.getBuildId()));
            for (FloorInfo item : floorInfos){
                Map<String ,Object> map = new HashMap<>();
                map.put("id", bu.getBuildId()+"-"+item.getFloorId());
                map.put("name", item.getFloorNo()+"楼");
                map.put("pId", bu.getBuildId());
                manBuilds.add(map);
            }
        }

        List<Map<String ,Object>> womanBuilds = new ArrayList<>();
        for (BuildInfo bu : allwomanBuilds){
            Map<String ,Object> map = new HashMap<>();
            map.put("id", bu.getBuildId());
            map.put("name", bu.getBuildName());
            map.put("pId", 0);
            map.put("checked", buildIds.contains(bu.getBuildId())?true:false);
            womanBuilds.add(map);
        }
        for (BuildInfo bu: allwomanBuilds){
            List<FloorInfo> floorInfos = floorInfoService.list(new QueryWrapper<FloorInfo>().eq("belong_build_id", bu.getBuildId()));
            for (FloorInfo item : floorInfos){
                Map<String ,Object> map = new HashMap<>();
                map.put("id", bu.getBuildId()+"-"+item.getFloorId());
                map.put("name", item.getFloorNo()+"楼");
                map.put("pId", bu.getBuildId());
                womanBuilds.add(map);
            }
        }

        Map<String ,Object> collects = new HashMap<>();
        collects.put("manBuildTree", manBuilds);
        collects.put("womanBuildTree", womanBuilds);
        return Response.success(collects);
    }



    /**
     * 找出所有楼栋的楼层数量以及宿舍数量
     * @return
     */
    //TODO 需要优化慢sql
    @GetMapping("/getBuilds")
    @ResponseBody
    public Response getBuilds(){
        List<AreaInfo> areaInfos = areaInfoService.list();
        Map<Integer ,String > areaMap = new HashedMap();
        for (AreaInfo ar : areaInfos){
            areaMap.put(ar.getAreaId(),ar.getAreaName());
        }

        //找出所有builds
        List<BuildInfo> buildInfos = buService.list();
        //根据楼栋Id,得到该楼栋楼层和宿舍数量
        List<BuildDetailVo> collect = buildInfos.stream().map(bu -> {
            Map<String, Integer> map = floorInfoService.getFloorDormCountByBuildId(bu.getBuildId());
            BuildDetailVo buildDetailVo = new BuildDetailVo();
            BeanUtils.copyProperties(bu, buildDetailVo);
            buildDetailVo.setFloorCount(map.get("floorCount"));
            buildDetailVo.setRoomCount(map.get("roomCount"));
            if (areaMap.containsKey(bu.getBelongAreaId())){
                buildDetailVo.setBelongArea(areaMap.get(bu.getBelongAreaId()));
            }
            return buildDetailVo;
        }).collect(Collectors.toList());
        return Response.success(collect);
    }

    /**
     * buildCount公寓数量
     * roomCount宿舍数量
     * stuCount已住人数
     * emptyBedCount空闲床位
     */
    @GetMapping("/getLabs")
    @ResponseBody
    public Response getLabs(){
        Integer buildCoun = buService.count();
        Integer roomCount = dormService.count();
        int stuCount = bedInfoService.count(new QueryWrapper<BedInfo>().ne("belong_stu_id", 0));
        int emptyBed = bedInfoService.count(new QueryWrapper<BedInfo>().eq("belong_stu_id", 0));
        //完成查询所有已住人数，和空闲床位
        Map<String ,Integer> map = new HashedMap();
        map.put("buildCount",buildCoun);
        map.put("roomCount",roomCount);
        map.put("stuCount",stuCount);
        map.put("emptyBedCount",emptyBed);
        return Response.success(map);
    }

//    @GetMapping("/bus")
//    @ResponseBody
//    @Transactional
//    public Response createfloor(){
////        List<Integer> collect = buService.list().stream().map(bu -> bu.getBuildId()).collect(Collectors.toList());
////        for (Integer id :collect){
////            for (int i =1;i<6;i++){
////                FloorInfo fo = new FloorInfo();
////                fo.setFloorNo(i);
////                fo.setBelongBuildId(id);
////                floorInfoService.save(fo);
////            }
////        }
////        return Response.success();
//        List<FloorInfo> collect = floorInfoService.list();
////        System.out.println(infos);
//
//        for (FloorInfo floorInfo : collect){
//            for (int i =1;i<6;i++){
//                DormInfo dorm = new DormInfo();
//                dorm.setDormNo(floorInfo.getFloorNo()*100+i);
//                dorm.setAlreadyLived(0);
//                if (i>3){
//                    dorm.setDormCapacity(6);
//                    dorm.setDormFee(800.00);
//                }else{
//                    dorm.setDormCapacity(4);
//                    dorm.setDormFee(1000.00);
//                }
//                dorm.setBelong_floor_id(floorInfo.getFloorId());
//                dormService.save(dorm);
//            }
//        }
//        return Response.success();
//    }
}
