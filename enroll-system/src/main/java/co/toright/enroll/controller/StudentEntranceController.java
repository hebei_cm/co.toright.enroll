package co.toright.enroll.controller;

import co.toright.enroll.entity.StudentInfo;
import co.toright.enroll.entity.User;
import co.toright.enroll.service.StudentInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/entrance")
public class StudentEntranceController {
    @Autowired
    StudentInfoService stuService;

    @GetMapping("/toentrance")
    public String toEntranceHandle(Model model){
        String name = (String) SecurityUtils.getSubject().getPrincipal();
        StudentInfo stu = stuService.getOne(new QueryWrapper<StudentInfo>().eq("stu_no", Integer.valueOf(name)));
        model.addAttribute("stuInfo", stu);
        return "menu/stuEntranceHandle/entranceHandle";
    }
}
