package co.toright.enroll.controller;


import co.toright.enroll.config.shiro.UserRealm;
import co.toright.enroll.entity.Permission;
import co.toright.enroll.entity.Role;
import co.toright.enroll.entity.RolePermission;
import co.toright.enroll.mapper.RoleMapper;
import co.toright.enroll.service.PermissionService;
import co.toright.enroll.service.RolePermisService;
import co.toright.enroll.service.RoleService;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import freemarker.template.SimpleDate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import javax.websocket.server.PathParam;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 后台角色表 前端控制器
 * </p>
 *
 * @author 谭
 * @since 2021-01-22
 */
@Controller
@RequestMapping("/role")
public class RoleController {
    @Autowired
    RoleMapper roleMapper;

    @Autowired
    RoleService roleService;

    @Autowired
    PermissionService permisService;

    @Autowired
    RolePermisService rolePermisService;

    @GetMapping("/list")
    public String roleList(
            @RequestParam(value = "pn",defaultValue = "1") Integer pn,ModelMap map) {
        Page<Role> page = new Page<>(pn, 5);
        Page<Role> rolePage = roleService.page(page,null);
        map.addAttribute("pageInfo", rolePage);
        System.out.println(rolePage.getCurrent()+","+rolePage.getTotal());
        return "menu/sysPermisMgr/rolemanage";
    }

    @GetMapping("/getRoleDesc/{id}")
    @ResponseBody
    public Response getRoleDesc(@PathVariable("id")Integer id){
        Role role = roleService.getById(id);
        return Response.success(role.getRoleDesc());
    }


    @PostMapping("/addNewRole")
    @ResponseBody
    public Response addNewRole(@RequestParam("roleCode") String roleCode,
                             @RequestParam("roleDesc")String roleDesc,
                             @RequestParam("roleStatus")String roleStatus){
//        System.out.println(roleCode+","+roleDesc+","+roleStatu);
        if (!StringUtils.isEmpty(roleCode) && !StringUtils.isEmpty(roleDesc) && !StringUtils.isEmpty(roleStatus)) {

            Role role = new Role();
            role.setRoleCode(roleCode);
            role.setRoleDesc(roleDesc);
            role.setRoleStatus(roleStatus);
            boolean result = roleService.insertRole(role);
            if (result){
                return Response.success("角色添加成功！");
            }
            return Response.error("该角色代码或名称已经存在，请更换角色代码或角色名");
        }
        return Response.error("添加失败,请检查表单内容是否有误");
    }

    @PostMapping("/editRole")
    @ResponseBody
    public Response editRole(@RequestParam("roleId")String roleID,@RequestParam("roleDesc")String roleDesc,@RequestParam("roleStatus") String roleStatus){

        System.out.println(roleDesc+">"+roleStatus);

        UpdateWrapper<Role> wrapper = new UpdateWrapper<Role>().eq("id", roleID);
        Role role = new Role();
        role.setRoleDesc(roleDesc);
        role.setRoleStatus(roleStatus);
        boolean update = roleService.update(role, wrapper);
        if (update){
            return Response.success(update);
        }else{
            return Response.error("修改失败");
        }

    }

    /**
     * 获取当前角色权限
     * @param model
     * @return
     */
    @GetMapping("/permis/{roleId}")
    @ResponseBody
    public List<Map<String,Object>> roleTable(@PathVariable("roleId") String roleID, Model model){

        //先把角色对象查询出来
//        List<Role> roleList = roleService.findAllRole();

        int id = Integer.parseInt(roleID);

        //查询出数据库中的所有的权限的数据
        List<Permission> permisList = permisService.list();


//        List<Permission> bigPermis = permisList.stream().filter(permission ->
//             permission.getParentId() == 0
//        ).collect(Collectors.toList());


        //角色所拥有的菜单全部查询出来；
        List<Permission> rolePermissions = roleMapper.getRolePermissions(id);

        //创建list集合用于存储数据
        List<Map<String,Object>> list = new ArrayList<>();

        //遍历数据
        for(Permission permis:permisList){
            //创建map集合
            Map<String,Object> map = new HashMap();
            //这个格式为了符合ztree数据格式
            map.put("id",permis.getId());
            map.put("pId", permis.getParentId());
            map.put("name",permis.getPermisName());
            //判断当前角色是否拥有该菜单，如果有则默认选中状态；
            if (rolePermissions.contains(permis)){
                map.put("checked", "true");
            }else{
                map.put("checked", "false");
            }
            //把map存入list中
            list.add(map);
        }
        //把list转换成json的字符串，

        return list;
    }

    /**
     * 保存提交的用户权限配置表单
     */
    @PostMapping("/permis/save")
    @ResponseBody
    @Transactional
    public Response saveRolePermis(@RequestParam("roleId")String roleId,@RequestParam("permisIds")String permisIds){
//        System.out.println(roleId);
//        System.out.println(permisIds);
        Role role = roleService.getById(roleId);
        if ("1".equals(role.getRoleStatus())){
            return Response.error("该角色状态不可用，请将其启用后再试!");
        }

        if (!StringUtils.isEmpty(permisIds)){
            String[] split = permisIds.split(",");
            List<RolePermission> list = new ArrayList<>();
            rolePermisService.remove(new QueryWrapper<RolePermission>().eq("role_id", roleId));
            for (String permis : split){
                RolePermission roleps = new RolePermission();
                roleps.setRoleId(Integer.parseInt(roleId));
                roleps.setPermissionId(Integer.parseInt(permis));
                list.add(roleps);
            }
            if (!CollectionUtils.isEmpty(list)){
                boolean b = rolePermisService.saveBatch(list);
                if (b){
                    //添加成功之后 清除缓存
                    DefaultWebSecurityManager securityManager = (DefaultWebSecurityManager) SecurityUtils.getSecurityManager();
                    UserRealm userRealm = (UserRealm) securityManager.getRealms().iterator().next();
                    //清除权限 相关的缓存
                    userRealm.clearAllCache();
                    return Response.success("角色权限修改成功");
                }
                else{
                    return Response.error("权限修改失败");
                }
            }
        }

        return Response.error("请至少赋予一项权限！");
    }

    /**
     * 角色的分页查询
     * @param pn //要查询的页码
     * @return
     */
    @GetMapping("/role-page")
    public String getEmpsWithJson(
            @RequestParam(value = "pn",defaultValue = "1") Integer pn,ModelMap map) {
        Page<Role> page = new Page<>(pn, 5);
        Page<Role> rolePage = roleService.page(page, null);
        map.addAttribute("pageInfo", rolePage);
        return "menu/sysPermisMgr/rolemanage :: #roles_card";
    }
}

