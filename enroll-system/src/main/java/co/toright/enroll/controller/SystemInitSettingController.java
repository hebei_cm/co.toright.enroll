package co.toright.enroll.controller;

import co.toright.enroll.entity.*;
import co.toright.enroll.entity.importEntity.*;
import co.toright.enroll.service.*;
import co.toright.enroll.util.Response;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.models.auth.In;
import org.apache.commons.collections.CollectionUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/sysInit")
public class SystemInitSettingController {
    @Autowired
    DataImportLogService daService;

    @Autowired
    CollegeInfoService coService;

    @Autowired
    AreaInfoService areaInfoService;

    @Autowired
    SpecialityInfoService spService;

    @Autowired
    StudentInfoService stuService;

    @Autowired
    BuildInfoService buildInfoService;

    @Autowired
    DormInfoService dormInfoService;

    @GetMapping("/batchImport")
    public String tobatchImport(){
        return "menu/enrollConfig/batchImport";
    }

    @GetMapping("/getAreaInfo")
    @ResponseBody
    public Response getAreaInfo(){
        List<AreaInfo> list = areaInfoService.list();
        //收集属于每个校区的学院
        List<Object> collect = list.stream().map(area -> {
            Integer areaId = area.getAreaId();
            //查出该校区下所有学院
            List<CollegeInfo> infoList = coService.list(new QueryWrapper<CollegeInfo>().select("id","college_name","belong_area_id")
                    .eq("belong_area_id", areaId));
            area.setCollegeInfoList(infoList);
            //查出该校区下所有楼栋
            List<BuildInfo> buildInfos = buildInfoService.list(new QueryWrapper<BuildInfo>().select("build_id","build_name","belong_area_id")
                    .eq("belong_area_id", areaId));
            area.setCollegeInfoList(infoList);
            area.setBuildInfoList(buildInfos);
            return area;
        }).collect(Collectors.toList());
        return Response.success(collect);
    }

    @PostMapping("/saveAreaInfo")
    @ResponseBody
    public Response saveAreaInfo(@RequestBody(required = true) AreaInfo areaInfo){
//        System.out.println(areaInfo);
        int count = areaInfoService.count(new QueryWrapper<AreaInfo>().eq("area_name", areaInfo.getAreaName()));
        if (count!=0){
            return Response.error("该校区已存在，请勿重复添加");
        }else {
            areaInfoService.save(areaInfo);
            return Response.success("校区添加成功！");
        }
    }

    /**
     * 保存选中的校区学院关系
     * @param
     * @return
     */
    @PostMapping("/saveAreaCollegeRelation")
    @ResponseBody
    public Response saveAreaCollegeRelation(@RequestBody Map<String, Object> params){
        //被校区选中的学院id值集合
        ArrayList<Integer> ids = (ArrayList<Integer>) params.get("ids");
        //校区id
        Integer areaId = (Integer) params.get("areaId");
        //将之前该校区下所有学院所属校区id置空
        List<CollegeInfo> list = coService.list(new QueryWrapper<CollegeInfo>().eq("belong_area_id", areaId));
        for(CollegeInfo co :list){
            co.setBelongAreaId(0);
            coService.updateById(co);
        }
        //如果关联学院未空
        if (ids.isEmpty()){
            return Response.success("当前校区所关联学院已清空！！！");
        }

        //选中的学院
        List<CollegeInfo> checkdList = coService.listByIds(ids);
        for(CollegeInfo co : checkdList){
            co.setBelongAreaId(areaId);
        }
        boolean b = coService.updateBatchById(checkdList);
        if (b){
            return Response.success("更新成功");
        }else{
            return Response.error("更新失败");
        }
    }

    /**
     * 删除学院信息
     * @param id
     * @return
     */
    @GetMapping("/removeArea/{id}")
    @ResponseBody
    public Response deleteArea(@PathVariable("id") Integer id){
        int count = coService.count(new QueryWrapper<CollegeInfo>().eq("belong_area_id", id));
        if (count==0){
            boolean b = areaInfoService.removeById(id);
            return Response.success("删除成功！");
        }
        else{
            return Response.error("删除失败！请检查该校区是否有关联的学院");
        }
    }

    @PostMapping("/uploadCollege")
    @ResponseBody
    public Response uploadCollege(@RequestParam("file") MultipartFile file)throws IOException {
        List<CollegeEntity> list = EasyExcel.read(file.getInputStream()).head(CollegeEntity.class).sheet().doReadSync();
        List<CollegeInfo> collegeInfoList = new ArrayList<>();
        for(CollegeEntity co :list){
            CollegeInfo collegeInfo = new CollegeInfo();
                BeanUtils.copyProperties(co, collegeInfo);
                collegeInfoList.add(collegeInfo);
        }
        boolean b = coService.saveBatch(collegeInfoList);
        if (b){
            //将导入记录插入到导入日志中；
            DataImportLog dataImportLog = new DataImportLog();
            dataImportLog.setDataName("学院信息");
            dataImportLog.setDataDept("系统管理员");
            dataImportLog.setDataCount(collegeInfoList.size());
            daService.save(dataImportLog);
            return Response.success("批量导入成功,一共:"+collegeInfoList.size()+"条数据！");
        }
        return Response.error("导入失败,请检查重试！");
    }
    /**
     * 上传专业信息
     */
    @PostMapping("/uploadSpec")
    @ResponseBody
    public Response uploadSpec(@RequestParam("file") MultipartFile file)throws IOException{
        List<SpecEntity> list = EasyExcel.read(file.getInputStream()).head(SpecEntity.class).sheet().doReadSync();
        List<SpecialityInfo> specEntities = new ArrayList<>();
        for(SpecEntity sp:list){
            SpecialityInfo spi = new SpecialityInfo();
            BeanUtils.copyProperties(sp, spi);
            specEntities.add(spi);
        }
        boolean b = spService.saveBatch(specEntities);
        if (b){
            //将导入记录插入到导入日志中；
            DataImportLog dataImportLog = new DataImportLog();
            dataImportLog.setDataName("专业信息");
            String name = (String) SecurityUtils.getSubject().getPrincipal();
            dataImportLog.setDataDept(name);
            dataImportLog.setDataCount(specEntities.size());
            daService.save(dataImportLog);
            return Response.success("批量导入成功,一共:"+specEntities.size()+"条数据！");
        }
        return Response.error("导入失败,请检查重试！");
    }

    /**
     * 上传新生信息
     */
    @PostMapping("/uploadNewStu")
    @ResponseBody
    public Response uploadNewStu(@RequestParam("file") MultipartFile file)throws IOException{
        List<NewStuEntity> list = EasyExcel.read(file.getInputStream()).head(NewStuEntity.class).sheet().doReadSync();
        List<StudentInfo> studentInfos = new ArrayList<>();
        for(NewStuEntity newStu:list){
            StudentInfo studentInfo = new StudentInfo();
            BeanUtils.copyProperties(newStu, studentInfo);
            studentInfos.add(studentInfo);
        }
        boolean b = stuService.saveBatch(studentInfos);
        if (b){
            //将导入记录插入到导入日志中；
            DataImportLog dataImportLog = new DataImportLog();
            dataImportLog.setDataName("专业信息");
            String name = (String) SecurityUtils.getSubject().getPrincipal();
            dataImportLog.setDataDept(name);
            dataImportLog.setDataCount(studentInfos.size());
            daService.save(dataImportLog);
            return Response.success("批量导入成功,一共:"+studentInfos.size()+"条数据！");
        }
        return Response.error("导入失败,请检查重试！");
    }

    /**
     * 上传楼栋信息
     */
    @PostMapping("/uploadBuild")
    @ResponseBody
    public Response uploadBuild(@RequestParam("file") MultipartFile file)throws IOException{
        List<BuildEntity> list = EasyExcel.read(file.getInputStream()).head(BuildEntity.class).sheet().doReadSync();
        List<BuildInfo> buildInfos = new ArrayList<>();
        for(BuildEntity buildEntity:list){
            BuildInfo buildInfo = new BuildInfo();
            BeanUtils.copyProperties(buildEntity, buildInfo);
            buildInfos.add(buildInfo);
        }
        boolean b = buildInfoService.saveBatch(buildInfos);
        if (b){
            //将导入记录插入到导入日志中；
            DataImportLog dataImportLog = new DataImportLog();
            dataImportLog.setDataName("楼栋信息");
            String name = (String) SecurityUtils.getSubject().getPrincipal();
            dataImportLog.setDataDept(name);
            dataImportLog.setDataCount(buildInfos.size());
            daService.save(dataImportLog);
            return Response.success("批量导入成功,一共:"+buildInfos.size()+"条数据！");
        }
        return Response.error("导入失败,请检查重试！");
    }

    /**
     * 上传宿舍信息
     */
    @PostMapping("/uploadDorm")
    @ResponseBody
    public Response uploadDorm(@RequestParam("file") MultipartFile file)throws IOException{
        List<DormEntity> list = EasyExcel.read(file.getInputStream()).head(DormEntity.class).sheet().doReadSync();
        List<DormInfo> dormInfos = new ArrayList<>();
        for(DormEntity dormEntity:list){
            DormInfo dormInfo = new DormInfo();
            BeanUtils.copyProperties(dormEntity, dormInfo);
            dormInfos.add(dormInfo);
        }
        boolean b = dormInfoService.saveBatch(dormInfos);
        if (b){
            //将导入记录插入到导入日志中；
            DataImportLog dataImportLog = new DataImportLog();
            dataImportLog.setDataName("宿舍信息");
            String name = (String) SecurityUtils.getSubject().getPrincipal();
            dataImportLog.setDataDept(name);
            dataImportLog.setDataCount(dormInfos.size());
            daService.save(dataImportLog);
            return Response.success("批量导入成功,一共:"+dormInfos.size()+"条数据！");
        }
        return Response.error("导入失败,请检查重试！");
    }

    /**
     * 下载学院导入模板
     */
    @GetMapping("/downCollegeTemplate")
    public void downCollegeTemplate(HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        CollegeInfo co = new CollegeInfo();
        //"这里填写学院代码，会与专业代码挂钩，一定要准确"
        co.setCollegeCode(1);
        co.setCollegeName("学院名称");
        co.setPresident("院长");
        co.setVicePresident("选填");
        co.setCollegeAddress("学院地址");
        co.setCollegeTel("学院电话");
        co.setZipCode("选填");
        co.setCollegeNetAddress("选填");
        co.setDescription("简介");
        List<CollegeInfo> list = new ArrayList<>();
        list.add(co);
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        String fileName = "批量导入学院信息模板";
        Sheet sheet = new Sheet(1, 0,CollegeEntity.class);
        //设置自适应宽度
        sheet.setAutoWidth(Boolean.TRUE);
        // 第一个 sheet 名称
        sheet.setSheetName("sheet1");
        writer.write(list, sheet);
        //通知浏览器以附件的形式下载处理，设置返回头要注意文件名有中文
        response.setHeader("Content-disposition", "attachment;filename=" + new String( fileName.getBytes("gb2312"), "ISO8859-1" ) + ".xlsx");
        writer.finish();
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("utf-8");
        out.flush();
    }

    /**
     * 下载专业导入模板
     */
    @GetMapping("/downSpecTemplate")
    public void downSpecTemplate(HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        String fileName = "批量导入专业信息模板";
        Sheet sheet = new Sheet(1, 0,SpecEntity.class);
        //设置自适应宽度
        sheet.setAutoWidth(Boolean.TRUE);
        // 第一个 sheet 名称
        sheet.setSheetName("sheet1");
        writer.write(null, sheet);
        //通知浏览器以附件的形式下载处理，设置返回头要注意文件名有中文
        response.setHeader("Content-disposition", "attachment;filename=" + new String( fileName.getBytes("gb2312"), "ISO8859-1" ) + ".xlsx");
        writer.finish();
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("utf-8");
        out.flush();
    }

    /**
     * 下载专业导入模板
     */
    @GetMapping("/downStuTemplate")
    public void downStuTemplate(HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        String fileName = "批量导入新生信息模板";
        Sheet sheet = new Sheet(1, 0, NewStuEntity.class);
        //设置自适应宽度
        sheet.setAutoWidth(Boolean.TRUE);
        // 第一个 sheet 名称
        sheet.setSheetName("sheet1");
        writer.write(null, sheet);
        //通知浏览器以附件的形式下载处理，设置返回头要注意文件名有中文
        response.setHeader("Content-disposition", "attachment;filename=" + new String( fileName.getBytes("gb2312"), "ISO8859-1" ) + ".xlsx");
        writer.finish();
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("utf-8");
        out.flush();
    }
    /**
     * 下载楼栋导入模板
     */
    @GetMapping("/downBuildTemplate")
    public void downBuildTemplate(HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        String fileName = "批量导入楼栋信息模板";
        Sheet sheet = new Sheet(1, 0, BuildEntity.class);
        //设置自适应宽度
        sheet.setAutoWidth(Boolean.TRUE);
        // 第一个 sheet 名称
        sheet.setSheetName("sheet1");
        writer.write(null, sheet);
        //通知浏览器以附件的形式下载处理，设置返回头要注意文件名有中文
        response.setHeader("Content-disposition", "attachment;filename=" + new String( fileName.getBytes("gb2312"), "ISO8859-1" ) + ".xlsx");
        writer.finish();
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("utf-8");
        out.flush();
    }
    /**
     * 下载宿舍导入模板
     */
    @GetMapping("/downDormTemplate")
    public void downDormTemplate(HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
        String fileName = "批量导入宿舍信息模板";
        Sheet sheet = new Sheet(1, 0, DormEntity.class);
        //设置自适应宽度
        sheet.setAutoWidth(Boolean.TRUE);
        // 第一个 sheet 名称
        sheet.setSheetName("sheet1");
        writer.write(null, sheet);
        //通知浏览器以附件的形式下载处理，设置返回头要注意文件名有中文
        response.setHeader("Content-disposition", "attachment;filename=" + new String( fileName.getBytes("gb2312"), "ISO8859-1" ) + ".xlsx");
        writer.finish();
        response.setContentType("multipart/form-data");
        response.setCharacterEncoding("utf-8");
        out.flush();
    }

    /**
     * 获取未被其他校区选中的楼栋，以及选中的楼栋
     * @param areaId
     * @return
     */
    @GetMapping("/contactBuildToArea/{areaId}")
    @ResponseBody
    public Response contactBuildToArea(@PathVariable("areaId")Integer areaId){
        List<Map<String ,Object>> list = new ArrayList<>();
        List<BuildInfo> buildInfos = buildInfoService.list();
        List<BuildInfo> collect = buildInfos.stream().filter(build->{
            if (build.getBelongAreaId()==areaId||build.getBelongAreaId()==0){
                return true;
            }else {
                return false;
            }
        }).map(build -> {
            Map<String, Object> map = new HashMap<>();
            map.put("buildId", build.getBuildId());
            map.put("buildName", build.getBuildName());
            map.put("buildType", build.getBuildType());
            if (build.getBelongAreaId() == areaId) {
                map.put("checked", true);
            } else {
                map.put("checked", false);
            }
            list.add(map);
            return build;
        }).collect(Collectors.toList());
        return Response.success(list);
    }

    /**
     * 保存选中的楼栋到校区
     * @param
     * @return
     */
    @PostMapping("/saveAreaBuildRelation")
    @ResponseBody
    public Response saveBuildToArea(@RequestBody Map<String, Object> params){
        //被校区选中的楼栋id值集合
        ArrayList<Integer> ids = (ArrayList<Integer>) params.get("ids");
        //校区id
        Integer areaId = (Integer) params.get("areaId");
        //将之前该校区下所有楼栋所属校区id置空
        List<BuildInfo> list = buildInfoService.list(new QueryWrapper<BuildInfo>().eq("belong_area_id", areaId));
        for(BuildInfo bu :list){
            bu.setBelongAreaId(0);
            buildInfoService.updateById(bu);
        }
        //如果关联楼栋为空
        if (ids.isEmpty()){
            return Response.success("当前校区所关联楼栋已清空！！！");
        }

        //选中的楼栋
        List<BuildInfo> checkdList = buildInfoService.listByIds(ids);
        for(BuildInfo buildInfo : checkdList){
            buildInfo.setBelongAreaId(areaId);
        }
        boolean b = buildInfoService.updateBatchById(checkdList);
        if (b){
            return Response.success("更新成功");
        }else{
            return Response.error("更新失败");
        }
    }
}
