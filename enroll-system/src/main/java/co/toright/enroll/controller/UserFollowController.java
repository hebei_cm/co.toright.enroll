package co.toright.enroll.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户关注表,记录了所有用户的关注信息 前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-12
 */
@RestController
@RequestMapping("/enroll/user-follow")
public class UserFollowController {

}

