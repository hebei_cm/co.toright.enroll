package co.toright.enroll.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-07
 */
@RestController
@RequestMapping("/enroll/student-pay-item")
public class StudentPayItemController {

}

