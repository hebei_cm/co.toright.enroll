package co.toright.enroll.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-13
 */
@RestController
@RequestMapping("/enroll/teacher-info")
public class TeacherInfoController {

}

