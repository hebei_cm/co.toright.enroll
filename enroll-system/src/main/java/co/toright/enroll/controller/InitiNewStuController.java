package co.toright.enroll.controller;

import co.toright.enroll.entity.InitialStudentData;
import co.toright.enroll.entity.SpecialityInfo;
import co.toright.enroll.service.InitialStudentDataService;
import co.toright.enroll.service.SpecialityInfoService;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/initialStus")
public class InitiNewStuController {

    @Autowired
    SpecialityInfoService specService;

    @Autowired
    InitialStudentDataService initStuService;

    //获取待处理学生的专业
    @GetMapping("/getProcessedSpec")
    @ResponseBody
    public Response getProcessedSpec(){
        List<String> collect = initStuService.list(
                new QueryWrapper<InitialStudentData>()
                        .select("distinct applying_major_code")).stream()
                .map(item -> item.getApplyingMajorCode())
                .collect(Collectors.toList());

        List<Map<String ,String >> list = new ArrayList<>();
        //对专业分别计数
        if (collect.size()>0){
            for (String s :collect){
                Map<String ,String > map = new HashedMap();
                SpecialityInfo spec = specService.getOne(
                        new QueryWrapper<SpecialityInfo>()
                                .select("spec_id","spec_name").eq("spec_code", s));
                int count = initStuService.count(new QueryWrapper<InitialStudentData>().eq("applying_major_code", s));
                map.put("specCode", s);
                map.put("specName", spec.getSpecName());
                map.put("counts", Integer.toString(count));
                list.add(map);
            }
            return Response.success(list);
        }
        return Response.error("暂无需要处理的专业新生");
    }

    @GetMapping("/getStus/{specCode}")
    @ResponseBody
    public Response getStus(@PathVariable("specCode")String specCode){
        List<InitialStudentData> list = initStuService.list(new QueryWrapper<InitialStudentData>()
                .eq("applying_major_code", specCode).eq("deleted", 0));
        return Response.success(list);
    }
}
