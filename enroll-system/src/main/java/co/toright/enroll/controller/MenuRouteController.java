package co.toright.enroll.controller;

import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/menuRoute")
public class MenuRouteController {


    /**
     * 跳转首页面
     * @return
     */
    @GetMapping("/index")
    public String mainPage(){
        return "menu/index";
    }

    /**
     * 角色管理
     * @return
     */
    @RequiresRoles("sysAdmin")
    @GetMapping("/rolemanage")
    public String rolemanage(){
        return "redirect:/role/list";
    }

    /**
     * 学生入学办理菜单
     * @return
     */
    @GetMapping("/admissionProcessing")
    public String admissionProcessing(){
        return "menu/admissionProcessing";
    }

    /**
     * authorityManage权限管理菜单
     */
    @RequiresRoles("sysAdmin")
    @GetMapping("/permis")
    public String authorityManage(){
        return "redirect:/permis/list";
    }




    /**
     * columnManage栏目管理
     */
    @RequiresRoles("sysAdmin")
    @GetMapping("/columnManage")
    public String columnManage(){
        return "menu/columnManage";
    }


    /**
     * studentConsultation新生咨询
     */
    @GetMapping("/studentConsultation")
    public String studentConsultation(){
        return "menu/studentConsultation";
    }

    /**
     * enrollDataQuery迎新查询
     */
    @GetMapping("/enrollDataQuery")
    public String enrollDataQuery(){
        return "menu/enrollDataQuery";
    }

    /**
     * enrollDataReportForm 迎新报表
     */
    @GetMapping("/enrollDataReportForm")
    public String enrollDataReportForm(){
        return "menu/enrollDataReportForm";
    }

    /**
     * enrollNotice迎新公告
     */
    @GetMapping("/enrollNotice")
    public String enrollNotice(){
        return "menu/enrollNotice";
    }

    /**
     * enterTheSchool走进学校
     */
    @GetMapping("/enterTheSchool")
    public String enterTheSchool(){
        return "menu/enterTheSchool";
    }

    /**
     * campusRules校园校规
     */
    @GetMapping("/campusRules")
    public String campusRules(){
        return "menu/campusRules";
    }

    /**
     * onlineConsultation在线咨询
     */
    @GetMapping("/onLineQA")
    public String onlineConsultation(){
        return "menu/onLineQA";
    }
    /**
     * commonProblem常见问题
     */
    @GetMapping("/commonProblem")
    public String commonProblem(){
        return "menu/commonProblem";
    }

    /**
     * personalInfo个人信息
     */
    @GetMapping("/personalInfo")
    public String personalInfo(){
        return "menu/personalInfo";
    }

    /**
     * enrollHandle迎新办理
     */
    @GetMapping("/enrollHandle")
    public String enrollHandle(){
        return "menu/enrollHandle";
    }


    /**
     * advanced-highlight
     */
    @GetMapping("/advancedHighlight")
    public String advancedHighlight(){
        return "index";
    }


    /**
     * QAReplay咨询回复
     */
    @GetMapping("/QAReplay")
    public String QAReplay(){
        return "redirect:/websocket/admin";
    }

    /**
     * buildList公寓清单
     */
    @GetMapping("/buildList")
    public String buildList(){
        return "menu/dormitoryMgr/buildList";
    }

    /**
     * distribution宿舍分配
     */
    @GetMapping("/distribution")
    public String distribution(){
        return "menu/dormitoryMgr/distribution";
    }

    /**
     * checkin入住登记
     */
    @GetMapping("/checkin")
    public String checkin(){
        return "menu/dormitoryMgr/checkin";
    }

    /**
     * roomManage房间管理
     */
    @GetMapping("/roomManage")
    public String roomManage(){
        return "menu/dormitoryMgr/roomManage";
    }

    /**
     * basicSettings公寓管理/基础设置
     */
    @GetMapping("/basicSettings")
    public String basicSettings(){
        return "menu/dormitoryMgr/basicSettings";
    }

    /**
     * 学院信息分类查询collegeClassifiedQuery
     */
    @GetMapping("/collegeClassifiedQuery")
    public String  collegeClassifiedQuery(){
        return "/menu/collegeMgr/collegeClassifiedQuery";
    }

    /**
     * 在线交流
     * onLineChat
     */
    @GetMapping("/onLineChat")
    public String onLineChat(){
        return "menu/onLineChat/onLineChat";
    }

    /**
     * publishFeeType标定收费类型
     */
    @GetMapping("/publishFeeType")
    public String publishFeeType(){
        return "menu/financicalMgr/publishFeeType";
    }

    /**
     * publishFeeItem发布收费项
     */
    @GetMapping("/publishFeeItem")
    public String publishFeeItem(){
        return "menu/financicalMgr/publishFeeItem";
    }

    /**
     * reviewFeeItem审核收费项
     */
    @GetMapping("/queryFeeItem")
    public String queryFeeItem(){
        return "menu/financicalMgr/queryFeeItem";
    }

    /**
     * 发布通知releaseNotice
     */
    @GetMapping("/releaseNotice")
    public String releaseNotice(){
        return "menu/noticeMgr/releaseNotice";
    }

    /**
     * 初始化新生信息initiNewStuInfo
     */
    @GetMapping("/initiNewStuInfo")
    public String initiNewStuInfo(){
        return "menu/newStuMgr/initiNewStuInfo";
    }
}

