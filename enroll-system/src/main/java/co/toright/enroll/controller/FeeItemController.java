package co.toright.enroll.controller;

import co.toright.enroll.entity.PayItem;
import co.toright.enroll.entity.StudentInfo;
import co.toright.enroll.entity.StudentPayItem;
import co.toright.enroll.entity.User;
import co.toright.enroll.service.PayItemService;
import co.toright.enroll.service.StudentInfoService;
import co.toright.enroll.service.StudentPayItemService;
import co.toright.enroll.service.UserService;
import co.toright.enroll.util.Response;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.models.auth.In;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//审核收费项
@Controller
@RequestMapping("/feeItem")
public class FeeItemController {

    @Autowired
    PayItemService payItemService;

    @Autowired
    UserService userService;

    @Autowired
    StudentInfoService stuService;

    @Autowired
    StudentPayItemService studentPayItemService;
    //根据条件查询收费项
    @PostMapping("/searchFeeItemByCondition")
    @ResponseBody
    public Response searchFeeItemByCondition(@RequestBody Map<String,Object> map){
        Integer pn = (Integer) map.get("pn");
        String  status = (String) map.get("status");
        Page<PayItem> page = new Page<>(pn, 10);
        QueryWrapper<PayItem> wrapper = new QueryWrapper<PayItem>();

        if (map.get("type")!=null){
            String type = (String) map.get("type");
            if (Integer.parseInt(type)!=0){
                wrapper.and((obj)->{
                    obj.eq("pay_type_id", type);
                });
            }
        }
        if (map.get("name")!=null){
            String name = (String) map.get("name");
                wrapper.and((obj)->{
                    obj.like("pay_item_name", name);
                });
        }
        if (map.get("publisher")!=null){
            String publisher = (String) map.get("publisher");
            if (Integer.parseInt(publisher)!=0){
                wrapper.and((obj)->{
                    obj.eq("release_people_id", publisher);
                });
            }
        }
        String range = (String) map.get("range");
        if (!StringUtils.isEmpty(range)){
            String[] split = range.split(";");
            Integer low = Integer.parseInt(split[0]);
            Integer height = Integer.parseInt(split[1]);
            wrapper.between("charge_amount",low,height);
        }
//        System.out.println(type+","+name+","+publisher+",低"+low+",高"+height);
        Page<PayItem> list = payItemService.page(page, wrapper.eq("audit_status", Integer.parseInt(status)).orderByDesc("creat_time"));
        return Response.success(list);
    }

    //允许发布收费项
    @GetMapping("/allowPublishFeeItem/{id}")
    @ResponseBody
    @Transactional
    @RequiresPermissions("allowPublishFeeItem")
    public Response allowPublishFeeItem(@PathVariable("id")Integer feeItemId, HttpSession session){
        Date date = new Date();
        int count = payItemService.count(new QueryWrapper<PayItem>()
                .eq("id", feeItemId).ge("start_time", date).lt("end_time", date));
        //先检查是否合法
        //首先审核状态只能是未审核
        if (count!=1){
            PayItem payItem = payItemService.getById(feeItemId);
            if (payItem.getAuditStatus()==0){
                payItem.setAuditStatus(2);
                //将审核人信息写入发布项中
                User userInfo = (User) session.getAttribute("userInfo");
                payItem.setVerifierId(userInfo.getId());
                payItem.setAuditTime(new Date());
                //保存收费项
                payItemService.saveOrUpdate(payItem);
                //通知该发布项所有对象的收费通知
                //1.先查看该发布项是否在studentPayItem中已经存在
                //1.1先清除所有该学生此发布项
                List<StudentInfo> stus = stuService.list(new QueryWrapper<StudentInfo>()
                        .eq("belong_spec_id", payItem.getChargedSpecId()));
                List<StudentPayItem> collect = new ArrayList<>();
                if (stus.size()>0){
                    for (StudentInfo stu :stus){
                        StudentPayItem studentPayItem = new StudentPayItem();
                        studentPayItem.setStudentNo(stu.getStuNo());
                        studentPayItem.setIdCard(stu.getIdCard());
                        studentPayItem.setIsCompleted(0);
                        studentPayItem.setAmountReceived(payItem.getChargeAmount());
                        studentPayItem.setPayItemId(payItem.getId());
                        collect.add(studentPayItem);
                    }
                    if (collect.size()>0){
                        boolean b = studentPayItemService.saveBatch(collect);
                        if (b){
                            return Response.success(payItem.getPayItemName()+"审核并发布成功【"+collect.size()+"】被通知到");
                        }else{
                            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                            return Response.success(payItem.getPayItemName()+"为学生添加收费项时失败！");
                        }
                    }
                }
                return Response.success(payItem.getPayItemName()+"审核并发布成功，但该收费专业下，无学生");
            }
        }
        return Response.success("该条收费项不存在或被已被审核，无法操作！");
    }
}
