package co.toright.enroll.controller;

import co.toright.enroll.entity.BedInfo;
import co.toright.enroll.service.BedInfoService;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Controller
@RequestMapping("/bed")
public class BedInfoController {
    @Autowired
    BedInfoService bedService;

    @PostMapping("/saveRelation")
    @ResponseBody
    @Transactional
    public Response saveBedsRelationWithSpec(@RequestBody Map<String ,Object> parms) {
        Integer specId = (Integer) parms.get("specId");
        List<Integer> ids = (List<Integer>) parms.get("bedIds");
        Integer type = (Integer) parms.get("type");
        if ("".equals(specId) || specId.intValue()==0){
            return Response.success("当前未选中任何专业");
        }
        List<Integer> beforHaves = bedService.list(new QueryWrapper<BedInfo>()
                .select("id").eq("belong_spec_id", specId).eq("bed_type", type))
                .stream().map(item -> item.getId()).collect(Collectors.toList());

        //交集部分，不删
        List<Integer> intersection = ids.stream().filter(item -> beforHaves.contains(item)).collect(Collectors.toList());

        //需要新增部分
        List<Integer> addPart = ids.stream().filter(item -> !intersection.contains(item)).collect(Collectors.toList());

        //需要删除部分
        List<Integer> removePart = beforHaves.stream().filter(item -> !intersection.contains(item)).collect(Collectors.toList());

        try {
            List<BedInfo> addBeds = new ArrayList<>();
            for (Integer i : addPart){
                BedInfo bedInfo = new BedInfo();
                bedInfo.setId(i);
                bedInfo.setBelongSpecId(specId);
                addBeds.add(bedInfo);
            }
            bedService.saveOrUpdateBatch(addBeds);

            List<BedInfo> removeBeds = new ArrayList<>();
            for (Integer i : removePart){
                BedInfo bedInfo = new BedInfo();
                bedInfo.setId(i);
                bedInfo.setBelongSpecId(0);
                removeBeds.add(bedInfo);
            }
            bedService.saveOrUpdateBatch(removeBeds);
        }catch (Exception e){
            //TODO  上线时删除
          System.out.println("操作数据库时发生错误！");
        }
        return Response.success("修改成功！新增"+(type==1?'男':'女')+"生宿舍关系【"+addPart.size()+"】条，移除【"+removePart.size()+"】条！");
    }
}
