package co.toright.enroll.controller;

import co.toright.enroll.entity.BuildInfo;
import co.toright.enroll.entity.DormInfo;
import co.toright.enroll.entity.FloorInfo;
import co.toright.enroll.service.BuildInfoService;
import co.toright.enroll.service.DormInfoService;
import co.toright.enroll.service.FloorInfoService;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/floor")
public class FloorController {
    @Autowired
    BuildInfoService buService;
    @Autowired
    FloorInfoService foService;
    @Autowired
    DormInfoService dormService;

    @PostMapping("/listById")
    public Response getListById(@RequestBody Map<String,Object> map){
        if (map.isEmpty()){
            return Response.error("请先选择校区！");
        }
        //根据buildId查询所有楼层以及楼层下所有宿舍
        List<FloorInfo> floorInfos = foService.list(new QueryWrapper<FloorInfo>().eq("belong_build_id", map.get("buildId")));
        List<FloorInfo> collect = floorInfos.stream().map(floor -> {
            List<DormInfo> dormInfos = dormService.list(new QueryWrapper<DormInfo>()
                    .eq("belong_floor_id", floor.getFloorId()));
            floor.setDormInfos(dormInfos.isEmpty()?null:dormInfos);
            return floor;
        }).sorted(Comparator.comparing(FloorInfo::getFloorNo).reversed()).collect(Collectors.toList());

        //查询楼栋信息
        Integer buildId = (Integer) map.get("buildId");
        BuildInfo buildInfo = buService.getById(buildId);
        buildInfo.setFloorInfos(collect);
        buildInfo.setFloorCount(collect.size());

        return Response.success(buildInfo);
    }
}
