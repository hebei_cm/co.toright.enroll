package co.toright.enroll.controller;

import co.toright.enroll.config.shiro.filter.ShiroSessionListener;
import co.toright.enroll.entity.Permission;
import co.toright.enroll.entity.User;
import co.toright.enroll.entity.UserDetailInfo;
import co.toright.enroll.mapper.RoleMapper;
import co.toright.enroll.service.*;
import co.toright.enroll.util.Response;
import co.toright.enroll.vo.UserListVo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.models.auth.In;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RoleMapper roleMapper;

    @Autowired
    RoleService roleService;

    @Autowired
    PermissionService permisService;

    @Autowired
    RolePermisService rolePermisService;

    @Autowired
    UserDetailInfoService userDetailInfoService;

    @Autowired
    ShiroSessionListener sessionListener;


    /**
     * 获取当前在线人数
     * @param
     * @return
     */
    @GetMapping("/getOnlinePeople")
    @ResponseBody
    public Response getOnlinePeople(){
        AtomicInteger sessionCount = sessionListener.getSessionCount();
        return Response.success(sessionCount);
    }

    @RequestMapping("/list")
    public String userList(Model model){
        List<User> list = userService.list();
        model.addAttribute("userList", list);
        return "menu/sysPermisMgr/userPermisMGR";
    }


    @RequestMapping("/users")
    @ResponseBody
    public Response getUsers(
            @RequestParam(value = "pn", defaultValue = "1") Integer pn){
        Page<User> page = new Page<>(pn,2);
        Page<User> userPage = userService.page(page, null);
        return Response.success(userPage);
    }


    @RequestMapping("/ajaxList")
    @ResponseBody
    public List<Map<String,Object>> roleTable(Model model) {

        //先把角色对象查询出来
//        List<Role> roleList = roleService.findAllRole();

        int id = 4;

        //查询出数据库中的所有的权限的数据
        List<Permission> permisList = permisService.list();


//        List<Permission> bigPermis = permisList.stream().filter(permission ->
//             permission.getParentId() == 0
//        ).collect(Collectors.toList());


        //角色所拥有的菜单全部查询出来；
        List<Permission> rolePermissions = roleMapper.getRolePermissions(id);

        //创建list集合用于存储数据
        List<Map<String, Object>> list = new ArrayList<>();

        //遍历数据
        for (Permission permis : permisList) {
            //创建map集合
            Map<String, Object> map = new HashMap();
            //这个格式为了符合ztree数据格式
            map.put("id", permis.getId());
            map.put("pId", permis.getParentId());
            map.put("name", permis.getPermisName());
            //判断当前角色是否拥有该菜单，如果有则默认选中状态；
            if (rolePermissions.contains(permis)) {
                map.put("checked", "true");
            } else {
                map.put("checked", "false");
            }
            //把map存入list中
            list.add(map);
        }
        //把list转换成json的字符串，

        return list;
    }
}
