package co.toright.enroll.controller;


import co.toright.enroll.entity.PayType;
import co.toright.enroll.service.PayTypeService;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-04-05
 */
@Controller
@RequestMapping("/payType")
public class PayTypeController {
    @Autowired
    PayTypeService payTypeService;

    @GetMapping("/typesList")
    @ResponseBody
    public Response getAllPaytypes(){
        List<PayType> list = payTypeService.list();
        return Response.success(list);
    }
    @GetMapping("/getTypes")
    @ResponseBody
    public Response getTypes(){
        List<PayType> list = payTypeService.list(new QueryWrapper<PayType>().select("id","pay_type_name"));
        return Response.success(list);
    }
}

