package co.toright.enroll.controller;

import co.toright.enroll.config.shiro.filter.ShiroSessionListener;
import co.toright.enroll.entity.Role;
import co.toright.enroll.entity.User;
import co.toright.enroll.service.UserService;
import co.toright.enroll.util.CaptchaUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@Controller
public class LoginController {

    @Autowired
    UserService userService;

    @Autowired
    ShiroSessionListener sessionListener;


    /**
     * 验证码方法
     * @param session
     * @param response
     * @throws IOException
     */
    @GetMapping("/checkImg")
    public void getImage(HttpSession session, HttpServletResponse response) throws IOException{
        //生成验证码
        String code = CaptchaUtil.generateVerifyCode(4);
        //验证码放入session中
        session.setAttribute("code", code);
        ServletOutputStream os = response.getOutputStream();
        response.setContentType("image/png");
        CaptchaUtil.outputImage(110, 40, os, code);
    }

    @RequestMapping({"/","/login"})
    public String login(){
        return "login";
    }

    @RequestMapping("/test")
    public String test(){
        return "/test";
    }


    @RequestMapping({"/register"})
    public String toRegister(){
        return "register";
    }


    @PostMapping({"/toregister"})
    public String register(@RequestParam("username") String username,
                           @RequestParam("password") String password){
        System.out.println("=============到注册页面");
        User user = new User();
        user.setUsername(username);
        Md5Hash md5Hash = new Md5Hash(password);
        user.setPassword(md5Hash.toHex());
        boolean b = userService.save(user);
        System.out.println(b);
        return "login222";
    }


    @PostMapping("/index")
    public String index(@RequestParam(value = "username",required = true) String username,
                 @RequestParam(value = "password",required = true) String password,
                 @RequestParam(value = "rememberMe",required = false) boolean rememberMe,
                 @RequestParam("code")String code,Model model){
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password) || StringUtils.isEmpty(code)){
            model.addAttribute("msg", "请输入完整用户名和密码！");
            return "/login";
        }
        //校验验证码
        //session中的验证码
        String sessionCaptcha = (String) SecurityUtils.getSubject().getSession().getAttribute("code");
        if (null == code || !code.equalsIgnoreCase(sessionCaptcha)) {
            model.addAttribute("msg","验证码错误！");
            return "/login";
        }
        try {
            //获取主体对象
            Subject subject = SecurityUtils.getSubject();
            subject.login(new UsernamePasswordToken(username, password,rememberMe));
                //获取用户信息
            //将用户真实姓名放入session中
            User user = userService.findRolesByUsername(username);
            String realname = user.getRealname();
            List<Role> roles = user.getRoles();
            //去掉session中用户的密码
            user.setPassword("");
            Session subjectSession = subject.getSession();
            subjectSession.setAttribute("userInfo", user);
            subjectSession.setAttribute("username", realname);
            subjectSession.setAttribute("roles", roles);
            return "redirect:/menuRoute/index";
        }catch (UnknownAccountException e){
            e.printStackTrace();
            //TODO 上线删除
            System.out.println("用户名错误");
            model.addAttribute("msg", "用户名错误！");
            return "login222";
        }catch (IncorrectCredentialsException e){
            e.printStackTrace();
            System.out.println("用户名错误");
            model.addAttribute("msg", "密码错误！");
            return "/font/login";
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("系统未知错误");
            model.addAttribute("msg", "系统未知错误！");
            return "/login";
        }
    }

    @GetMapping("/logout")
    public String logout(){
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "redirect:/login";
    }
}
