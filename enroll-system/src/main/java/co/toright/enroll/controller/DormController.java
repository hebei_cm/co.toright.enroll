package co.toright.enroll.controller;

import co.toright.enroll.entity.*;
import co.toright.enroll.service.*;
import co.toright.enroll.util.Response;
import co.toright.enroll.vo.ZTree;
import co.toright.enroll.vo.BedInfoVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/dorm")
public class DormController {

    @Autowired
    DormInfoService dormService;
    @Autowired
    BedInfoService bedInfoService;
    @Autowired
    BuildInfoService buService;
    @Autowired
    FloorInfoService foService;
    @Autowired
    SpecialityInfoService spService;
    @Autowired
    StudentInfoService stuService;


    /**
     * 批量分配宿舍
     * @return
     */
    @GetMapping("/batchAllocationDorm")
    public String batchAllocationDorm(){
        return "menu/enrollConfig/batchAllocationDorm";
    }

    /**
     * 组合男生公寓树
     */
    @GetMapping("/getManBuildTreeBySpecId/{specId}")
    @ResponseBody
    public Response getManBuildTreeBySpecId(@PathVariable("specId")Integer specId){
        //找出该专业已有床位
        List<Integer> bedsOfSpec = bedInfoService.list(new QueryWrapper<BedInfo>()
                .eq("belong_spec_id", specId))
                .stream().map(item->item.getId()).collect(Collectors.toList());
        //男生公寓
        List<BuildInfo> manBuilds = buService.list(new QueryWrapper<BuildInfo>()
                .select("build_id", "build_name")
                .eq("build_type", 1));
        List<ZTree> manTrees = combinationZtree(bedsOfSpec, manBuilds);
        return Response.success(manTrees);
    }

    /**
     * 组合女生公寓树
     */
    @GetMapping("/getWomanBuildTreeBySpecId/{specId}")
    @ResponseBody
    public Response getWomanBuildTreeBySpecId(@PathVariable("specId")Integer specId){
        //找出该专业已有床位
        List<Integer> bedsOfSpec = bedInfoService.list(new QueryWrapper<BedInfo>()
                .eq("belong_spec_id", specId))
                .stream().map(item->item.getId()).collect(Collectors.toList());
        //女生公寓
        List<BuildInfo> womenBuilds = buService.list(new QueryWrapper<BuildInfo>()
                .select("build_id", "build_name")
                .eq("build_type", 2));


        List<ZTree> manTrees = combinationZtree(bedsOfSpec, womenBuilds);
        return Response.success(manTrees);
    }

    /**
     * 组合出公寓树
     * @param bedsOfSpec//专业已有床位
     * @param list//男生/女生公寓list
     * @return
     */
    public List<ZTree> combinationZtree(List<Integer>bedsOfSpec,List<BuildInfo> list){
        List<ZTree> buildTrees = list.stream().map(item -> {
            List<ZTree> floorList = foService.list(new QueryWrapper<FloorInfo>().eq("belong_build_id", item.getBuildId()))
                    .stream().map(floor -> {
                        List<ZTree> dormLists = dormService.list(new QueryWrapper<DormInfo>().eq("belong_floor_id", floor.getFloorId()))
                                .stream().map(dorm -> {
                                    List<ZTree> beds = bedInfoService.list(new QueryWrapper<BedInfo>().eq("belong_dorm_id", dorm.getDormId()))
                                            .stream().filter(bed->bedsOfSpec.contains(bed.getId()) || bed.getBelongSpecId()==0)
                                            .map(bed -> {
                                                ZTree bedTree = new ZTree();
                                                bedTree.setId(bed.getId());
                                                bedTree.setName(bed.getBedNo() + "号床--可用");
                                                bedTree.setChecked(bedsOfSpec.contains(bed.getId()));
                                                return bedTree;
                                            }).collect(Collectors.toList());
                                    if (beds.size()==0){
                                        return null;
                                    }else{
                                        ZTree dormTree = new ZTree();
                                        dormTree.setIsParent(true);
                                        dormTree.setName(dorm.getDormNo() + "室");
                                        dormTree.setChildren(beds);
                                        return dormTree;
                                    }
                                }).filter(dorm->dorm!=null).collect(Collectors.toList());
                        if (dormLists.size()==0){
                            return null;
                        }else {
                            ZTree floorTree = new ZTree();
                            floorTree.setName(floor.getFloorNo() + "层");
                            floorTree.setIsParent(true);
                            floorTree.setChildren(dormLists);
                            return floorTree;
                        }
                    }).filter(floor->floor!=null).collect(Collectors.toList());
            if (floorList.size()==0){
                ZTree buildTree = new ZTree();
                buildTree.setName(item.getBuildName()+"暂无可用宿舍！");
                buildTree.setIsParent(true);
                buildTree.setNocheck(true);
                return buildTree;
            }else{
                ZTree buildTree = new ZTree();
                buildTree.setName(item.getBuildName());
                buildTree.setIsParent(true);
                buildTree.setChildren(floorList);
                return buildTree;
            }
        }).filter(item->item!=null).collect(Collectors.toList());
        return buildTrees;
    }

    /**
     * 组合成楼-楼层-宿舍-床位树
     */
    @GetMapping("/bedsTreeData")
    @ResponseBody
    public Response bedsTreeData(){
        List<BuildInfo> list = buService.list();
        List<ZTree> collects = new ArrayList<>();
        for (BuildInfo bu : list){
            ZTree zTree = new ZTree();
            zTree.setName(bu.getBuildName());
            List<ZTree> children = foService.list(new QueryWrapper<FloorInfo>().eq("belong_build_id", bu.getBuildId()))
                    .stream().map(item -> {
                        ZTree zTree1 = new ZTree();
                        zTree1.setId(item.getFloorId());
                        zTree1.setName(item.getFloorNo() + "层");
                        return zTree1;
                    }).collect(Collectors.toList());
            zTree.setChildren(children);
            collects.add(zTree);
        }
        return Response.success(collects);
    }

    /**
     * 添加床位
     */
    @GetMapping("/addBeds")
    @ResponseBody
    @Transactional
    public Response addBeds(){
        List<DormInfo> list = dormService.list();
        for (DormInfo dormInfo : list){
            for (int i =1;i<=dormInfo.getDormCapacity();i++){
                BedInfo bedInfo = new BedInfo();
                bedInfo.setBedNo(i);
                bedInfo.setBelongDormId(dormInfo.getDormId());
                bedInfoService.save(bedInfo);
            }
        }
        return null;
    }

    /**
     * 根据宿舍id获取床位信息
     */
    @GetMapping("/getbedsInfo/{dormId}")
    @ResponseBody
    public Response getbedsInfoById(@PathVariable("dormId")Integer dormId){
        DormInfo dormInfo = dormService.getById(dormId);

        List<BedInfo> beds = bedInfoService.list(new QueryWrapper<BedInfo>()
                .eq("belong_dorm_id", dormId));

        Map<String,Object> map = new HashMap<>();
        map.put("dormInfo",dormInfo);

        List<BedInfoVo> bedInfoVos = beds.stream().map(bed -> {
            BedInfoVo bedInfoVo = new BedInfoVo();
            bedInfoVo.setId(bed.getId());
            bedInfoVo.setBedNo(bed.getBedNo());
            SpecialityInfo specInfo = spService.getOne(new QueryWrapper<SpecialityInfo>()
                    .select("spec_name").eq("spec_id", bed.getBelongSpecId()));
            StudentInfo studentInfo = stuService.getOne(new QueryWrapper<StudentInfo>()
                    .select("stu_name").eq("id", bed.getBelongStuId()));
            System.out.println(specInfo+","+studentInfo);
            if (specInfo!=null){
                bedInfoVo.setBelongSpecName(specInfo.getSpecName());
                if (studentInfo!=null){
                    bedInfoVo.setBelongStuName(studentInfo.getStuName());
                }
            }
            return bedInfoVo;
        }).collect(Collectors.toList());

        map.put("bedsInfo", bedInfoVos);
        return Response.success(map);
    }
}
