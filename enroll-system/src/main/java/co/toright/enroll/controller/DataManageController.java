package co.toright.enroll.controller;


import co.toright.enroll.entity.CollegeInfo;
import co.toright.enroll.entity.DataImportLog;
import co.toright.enroll.service.CollegeInfoService;
import co.toright.enroll.service.DataImportLogService;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *   * </p>
 *
 * @author 谭永健
 * @since 2021-02-06
 */
@Controller
@RequestMapping("/dataMgr")
public class DataManageController {

    @Autowired
    DataImportLogService dataImportLogService;

    @Autowired
    CollegeInfoService collegeInfoService;

    /**
     * enrollDataManage迎新日志管理
     */
    @RequiresRoles("sysAdmin")
    @GetMapping("/datalog")
    public String datalog(Model model){
        List<DataImportLog> list = dataImportLogService.list();
        model.addAttribute("lists", list);
        return "menu/baseDataMgr/dataLog";
    }

    /**
     * collegeSpecMgr学院管理
     */
    @RequiresRoles("sysAdmin")
    @GetMapping("/collegeMgr")
    public String collegeSpecMgr(Model model){
        List<CollegeInfo> list = collegeInfoService.list();
        model.addAttribute("collegeInfo", list);
        return "menu/baseDataMgr/collegeMgr";
    }
}

