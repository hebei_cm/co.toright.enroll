package co.toright.enroll.controller.font;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/font")
public class FontComtroller {

    @GetMapping("/index")
    public String index(){
        return "/font/index";
    }

}
