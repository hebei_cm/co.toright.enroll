package co.toright.enroll.controller;


import co.toright.enroll.entity.Permission;
import co.toright.enroll.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * <p>
 * 后台权限表 前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-26
 */
@Controller
@RequestMapping("/permis")
public class PermisController {

    @Autowired
    PermissionService permissionService;

    @GetMapping("/list")
    public String list(Model model){
        List<Permission> list = permissionService.list();
        model.addAttribute("permisList", list);
        return "userPermisMGR";
    }

}

