package co.toright.enroll.controller;


import co.toright.enroll.entity.ClassesInfo;
import co.toright.enroll.entity.CollegeInfo;
import co.toright.enroll.entity.SpecialityInfo;
import co.toright.enroll.entity.StudentInfo;
import co.toright.enroll.service.ClassesInfoService;
import co.toright.enroll.service.CollegeInfoService;
import co.toright.enroll.service.SpecialityInfoService;
import co.toright.enroll.service.StudentInfoService;
import co.toright.enroll.util.Response;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.github.classgraph.InfoList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-02-13
 */
@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    StudentInfoService stuService;
    @Autowired
    SpecialityInfoService specialityInfoService;
    @Autowired
    ClassesInfoService classesInfoService;
    @Autowired
    StudentInfoService studentInfoService;

    /**
     * 将学生信息下未分配班级的学生找出
     * belong_class_id ==0 and belong_spec_id ==data.specId
     */
    @GetMapping("/unallocatedh/{specId}")
    @ResponseBody
    public Response unallocatedh(@PathVariable("specId")Integer specId){
        QueryWrapper<StudentInfo> wrapper = new QueryWrapper<StudentInfo>()
                .eq("belong_spec_id", specId).eq("belong_class_id", 0);
        List<StudentInfo> list = stuService.list(wrapper);
        return Response.success(list);
    }

    /**
     * 获得所有的所有学生
     */
    @GetMapping("/getAtudentAll")
    @ResponseBody
    public Response getAtudentAll(){
        List<StudentInfo> list = stuService.list();
        return Response.success(list);
    }

    /**
     * 获得所在班级的所有学生
     */
    @GetMapping("/getStudentByCId/{classId}")
    @ResponseBody
    public Response getStudentByCId(@PathVariable("classId")Integer id){
        List<StudentInfo> belong_class_id = stuService.list(new QueryWrapper<StudentInfo>().eq("belong_class_id", id));
        return Response.success(belong_class_id);
    }


    /**
     * 查出每个专业下每个班级
     */
    @GetMapping("/getAllstudent")
    @ResponseBody
    public List<SpecialityInfo> getAllStudentByClass(){
        List<SpecialityInfo> specialityInfoList = specialityInfoService.list();
        List<ClassesInfo> classesInfoList = classesInfoService.list();
        //这一步是添加上专业下所有学生，没有必要，数据量太大
//        for (ClassesInfo cla : classesInfoList){
//            List<StudentInfo> studentInfoList = stuService.list(new QueryWrapper<StudentInfo>().eq("belong_class_id", cla.getId()));
//            cla.setStudentInfoList(studentInfoList);
//        }
        for (SpecialityInfo sp: specialityInfoList){
            List<ClassesInfo> infoList = classesInfoList.stream().filter(classes ->
                    classes.getBelongSpecId()==sp.getSpecId()).collect(Collectors.toList());
            sp.setClassesInfoList(infoList);
        }
        return specialityInfoList;
    }
}

