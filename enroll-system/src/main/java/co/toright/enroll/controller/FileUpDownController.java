package co.toright.enroll.controller;

import co.toright.enroll.entity.DataImportLog;
import co.toright.enroll.entity.testCell;
import co.toright.enroll.service.DataImportLogService;
import co.toright.enroll.util.Response;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dataManage")
public class FileUpDownController {
//
//    @Autowired
//    DataImportLogService dataImportLogService;
//    @Autowired
//    SubsectionInfoService subsectionInfoService;
//
//    @PostMapping("/upFile")
//    @ResponseBody
//    private Response parseMultipartFile(@RequestParam("file") MultipartFile file) throws IOException {
//        List<testCell> list = EasyExcel.read(file.getInputStream()).head(testCell.class).sheet().doReadSync();
//
//
//        List<SubsectionInfo> subList = new ArrayList<>();
//        for (testCell testCell : list){
//            SubsectionInfo su = new SubsectionInfo();
//            su.setSourceType(testCell.getSourceType());
//            su.setContent(testCell.getContent());
//            su.setTitle(testCell.getTitle());
//            su.setWebsite(testCell.getWebsite());
//            subList.add(su);
//        }
//
//        System.out.println(subList);
//        boolean saveBatch = subsectionInfoService.saveBatch(subList);
//        if (saveBatch){
//            DataImportLog da = new DataImportLog();
//            da.setDataName("新生录取数据");
//            da.setDataDept("招生办");
//            da.setDataCount(list.size());
//            dataImportLogService.saveOrUpdate(da);
//            return Response.success("数据添加成功，数据量"+list.size());
//        }
//        return Response.error("数据添加失败！");
//    }
//
//
//    @GetMapping("/downFile")
//    public void write(HttpServletResponse response) throws IOException {
//        ServletOutputStream out = response.getOutputStream();
//        SubsectionInfo su = new SubsectionInfo();
//        su.setWebsite("ww");
//        su.setContent("ww2");
//        su.setTitle("ww3");
//        su.setSourceType("ww3");
//        List<SubsectionInfo> list = new ArrayList<>();
//        list.add(su);
//        ExcelWriter writer = new ExcelWriter(out, ExcelTypeEnum.XLSX, true);
//        String fileName = "测试exportExcel";
//        Sheet sheet = new Sheet(1, 0,testCell.class);
//        //设置自适应宽度
//        sheet.setAutoWidth(Boolean.TRUE);
//        // 第一个 sheet 名称
//        sheet.setSheetName("第一个sheet");
//        writer.write(list, sheet);
//        //通知浏览器以附件的形式下载处理，设置返回头要注意文件名有中文
//        response.setHeader("Content-disposition", "attachment;filename=" + new String( fileName.getBytes("gb2312"), "ISO8859-1" ) + ".xlsx");
//        writer.finish();
//        response.setContentType("multipart/form-data");
//        response.setCharacterEncoding("utf-8");
//        out.flush();
//    }

}
