package co.toright.enroll.controller;


import co.toright.enroll.entity.Department;
import co.toright.enroll.service.DepartmentService;
import co.toright.enroll.util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 谭永健
 * @since 2021-01-27
 */
@Controller
@ResponseBody
@RequestMapping("/department")
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @RequestMapping("/list")
    public Response<List<Department>> list(){
        List<Department> list = departmentService.list();
        return Response.success(list, "查询成功");
    }
}

