package co.toright.enroll.events;

import co.toright.enroll.constant.ResultEnum;
import co.toright.enroll.entity.InitialStudentData;
import co.toright.enroll.entity.SchoolRoll;
import co.toright.enroll.exception.StuRegisterException;
import co.toright.enroll.service.InitialStudentDataService;
import co.toright.enroll.service.SchoolRollService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * 注解方式 @EventListener
 * @author Summerday
 */
@Service
@Slf4j
public class CreatSchoolRollService implements ApplicationListener<NewStuRegisterEvent> {

    @Autowired
    SchoolRollService schoolRollService;

    /**
     * 监听用户注册事件,完成新生注册后，建立新生学籍
     */

    @Override
    @Async
    @Transactional(rollbackFor = {StuRegisterException.class,Exception.class})
    public void onApplicationEvent(NewStuRegisterEvent event){

        SchoolRoll schoolRoll = new SchoolRoll();
        //转换为学籍信息
        schoolRoll.setStudentId(event.getSno());
        schoolRoll.setNativePlace(event.getStu().getNativePlace());

        schoolRoll.setStudentNo(event.getSno());

        schoolRoll.setStudentName(event.getStu().getNewStuName());

        schoolRoll.setIdCard(event.getStu().getNewStuIdCard());

        //1、招生类别大体分为:普通统考类、艺术类、体育类和特殊类型招生。
        // 2、在特殊类型招生中包括:自主招生、国防生、保送生、高水平艺术团招生、高水平运动队招生、
        // 定向就业生、少数民族预科生等招生类别。
        schoolRoll.setAdmissionCategory("普通统考");
        schoolRoll.setForeignLanguage("英语");

        //TODO 培养形式统招、定向或委托培
        schoolRoll.setTrainningForm("统招");

        //学习形式：全日制学习还是函授学习
        schoolRoll.setLearnForm("全日制");

        //学位类别分为三类：学士学位，硕士学位，博士学位。
        schoolRoll.setDegreeCategory("学士学位");

        schoolRoll.setTrainingPlanId("暂无");

        schoolRoll.setHaveCredit(0);

        schoolRoll.setIsGraduated(0);

        schoolRoll.setGender(event.getStu().getGender());

        schoolRollService.save(schoolRoll);
        log.info("给新生[{}]注册学籍", event.getStu().getNewStuName());
    }
}