package co.toright.enroll.events;

import co.toright.enroll.entity.InitialStudentData;
import org.springframework.context.ApplicationEvent;


public class NewStuRegisterEvent extends ApplicationEvent {

    private InitialStudentData stu;

    private String sno;

    private Integer classId;

    public NewStuRegisterEvent(Object source) {
        super(source);
    }

    public NewStuRegisterEvent(Object source, InitialStudentData stu,String sno, Integer classId) {
        super(source);
        this.stu = stu;
        this.sno = sno;
        this.classId = classId;
    }

    public InitialStudentData getStu() {
        return stu;
    }

    public String getSno() {
        return sno;
    }

    public Integer getClassId() {
        return classId;
    }
}