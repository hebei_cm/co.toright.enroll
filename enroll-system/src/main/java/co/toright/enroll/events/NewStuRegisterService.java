package co.toright.enroll.events;

import co.toright.enroll.entity.*;
import co.toright.enroll.exception.StuRegisterException;
import co.toright.enroll.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Slf4j
public class NewStuRegisterService implements ApplicationEventPublisherAware {

    @Autowired
    SchoolRollService schoolRollService;

    @Autowired
    RoleService roleService;

    @Autowired
    SysUserRoleService userRoleService;

    @Autowired
    UserService userService;

    @Autowired
    StuStatusService stuStatusService;

    @Autowired
    ClassesInfoService classService;

    @Autowired
    StudentInfoService studentInfoService;

    @Autowired
    PayItemService payItemService;

    @Autowired
    StudentPayItemService stuPayItemService;

    @Autowired
    InitialStudentDataService initialStudentDataService;

    // 注入事件发布者
    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * 发布事件
     */
    @Transactional(rollbackFor = {StuRegisterException.class,Exception.class})
    public boolean register(InitialStudentData one, String sno, Integer classId) {
        log.info("执行用户[{}]的注册逻辑", one.getNewStuName());

        int count1 = schoolRollService.count(new QueryWrapper<SchoolRoll>()
                .eq("id_card", one.getNewStuIdCard()));

        int count2 = userService.count(new QueryWrapper<User>()
                .eq("username", sno));

        int count3 = stuStatusService.count(new QueryWrapper<StuStatus>()
                .eq("id_card", one.getNewStuIdCard()));

        int count4 = studentInfoService.count(
                new QueryWrapper<StudentInfo>().eq("stu_no", sno));

        int count5 = stuPayItemService.count(new QueryWrapper<StudentPayItem>()
                .eq("id_card", one.getNewStuIdCard()));
        if ((count1+count2+count3+count4+count5)!=0){
            return false;
        }
        //one-初始学生表，sno学号，classId班级号
        applicationEventPublisher.publishEvent(new NewStuRegisterEvent(this,one ,sno,classId));
        return true;
    }
}
