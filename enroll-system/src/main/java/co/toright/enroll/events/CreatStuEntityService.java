package co.toright.enroll.events;

import co.toright.enroll.constant.ResultEnum;
import co.toright.enroll.entity.ClassesInfo;
import co.toright.enroll.entity.InitialStudentData;
import co.toright.enroll.entity.StudentInfo;
import co.toright.enroll.exception.StuRegisterException;
import co.toright.enroll.service.ClassesInfoService;
import co.toright.enroll.service.InitialStudentDataService;
import co.toright.enroll.service.SpecialityInfoService;
import co.toright.enroll.service.StudentInfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Service
@Slf4j
public class CreatStuEntityService implements ApplicationListener<NewStuRegisterEvent> {
    @Autowired
    StudentInfoService studentInfoService;

    @Autowired
    ClassesInfoService classService;

    @Autowired
    SpecialityInfoService specService;



    @Override
    @Async
    @Transactional(rollbackFor = {StuRegisterException.class,Exception.class})
    //创建学生实体
    public void onApplicationEvent(NewStuRegisterEvent event) {
        ClassesInfo classesInfo = classService.getById(event.getClassId());

        StudentInfo studentInfo = new StudentInfo();
        studentInfo.setStuNo(event.getSno());
        studentInfo.setIdCard(event.getStu().getNewStuIdCard());
        studentInfo.setStuName(event.getStu().getNewStuName());
        studentInfo.setBelongClassId(event.getClassId());
        studentInfo.setBelongSpecId(classesInfo.getBelongSpecId());
        studentInfo.setGender(event.getStu().getGender());
        studentInfoService.save(studentInfo);
        log.info("给新生[{}]建立完学生实体", event.getStu().getNewStuName());
    }
}
