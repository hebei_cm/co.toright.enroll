package co.toright.enroll.events;

import co.toright.enroll.entity.*;
import co.toright.enroll.exception.StuRegisterException;
import co.toright.enroll.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.One;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import java.util.List;

/**
 * 注解方式 @EventListener
 * @author Summerday
 */
@Service
@Slf4j
//为学生建立收费项
public class CreatStuFeeItemService implements ApplicationListener<NewStuRegisterEvent> {

    @Autowired
    PayItemService payItemService;

    @Autowired
    StudentPayItemService stuPayItemService;

    @Autowired
    ClassesInfoService classService;


    @Override
    @Async
    @Transactional(rollbackFor = {StuRegisterException.class,Exception.class})
    public void onApplicationEvent(NewStuRegisterEvent event) {
        ClassesInfo classesInfo = classService.getOne(new QueryWrapper<ClassesInfo>()
                .select("id","belong_spec_id")
                .eq("id", event.getClassId()));
        //TODO 将此处audit_statuc设置为枚举类型
        List<PayItem> list = payItemService.list(new QueryWrapper<PayItem>()
                .eq("charged_spec_id", classesInfo.getBelongSpecId())
                .eq("audit_status", 2).eq("enabled", 0));
        if (list.size()>0){
            for (PayItem payItem : list){
                StudentPayItem studentPayItem = new StudentPayItem();
                studentPayItem.setPayItemId(payItem.getId());
                studentPayItem.setStudentNo(event.getSno());
                studentPayItem.setIdCard(event.getStu().getNewStuIdCard());
                studentPayItem.setAmountReceived(payItem.getChargeAmount());
                stuPayItemService.save(studentPayItem);
            }
            log.info("给新生[{}]完善收费项成功", event.getStu().getNewStuName());
        }
        log.info("新生[{}]无需补充收费项",event.getStu().getNewStuName());
    }

}