package co.toright.enroll.events;

import co.toright.enroll.constant.ResultEnum;
import co.toright.enroll.entity.*;
import co.toright.enroll.exception.StuRegisterException;
import co.toright.enroll.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
//为新生建立新生账号
public class CreatStuAccountService implements ApplicationListener<NewStuRegisterEvent> {
    @Autowired
    RoleService roleService;

    @Autowired
    SysUserRoleService userRoleService;

    @Autowired
    UserService userService;




    @Override
    @Async
    @Transactional(rollbackFor = {StuRegisterException.class,Exception.class})
    public void onApplicationEvent(NewStuRegisterEvent event) {
        Role role = roleService.getOne(new QueryWrapper<Role>().select("id")
                .eq("role_desc", "新生"));

        User user = new User();
        user.setUsername(event.getSno());
        Md5Hash md5Hash = new Md5Hash(getId(event.getStu().getNewStuIdCard()));
        user.setPassword(md5Hash.toHex());
        user.setRealname(event.getStu().getNewStuName());
        userService.save(user);

        UserRole userRole = new UserRole();
        userRole.setRoleId(role.getId());
        userRole.setUserId(user.getId());
        userRoleService.save(userRole);

        log.info("给新生[{}]创建了账号", event.getStu().getNewStuName());
    }


    //截取身份证后6位
    public static String getId(String id){

        Pattern p = Pattern.compile("[0-9]+[X|x]{1}");

        Matcher m = p.matcher(id);

        boolean b = m.matches();

        if(b){

            id = id.substring(id.length()-7,id.length()-1);

        }else{

            id = id.substring(id.length()-6);

        }

        return id;

    }
}
