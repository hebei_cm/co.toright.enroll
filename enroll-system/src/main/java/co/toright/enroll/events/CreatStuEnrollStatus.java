package co.toright.enroll.events;

import co.toright.enroll.constant.ResultEnum;
import co.toright.enroll.entity.*;
import co.toright.enroll.exception.StuRegisterException;
import co.toright.enroll.service.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * 注解方式 @EventListener
 * @author Summerday
 */
@Service
@Slf4j
//为学生建立迎新过程状态表
public class CreatStuEnrollStatus implements ApplicationListener<NewStuRegisterEvent> {

    @Autowired
    StuStatusService stuStatusService;

    @Autowired
    ClassesInfoService classService;




    @Override
    @Async
    @Transactional(rollbackFor = {StuRegisterException.class,Exception.class})
    public void onApplicationEvent(NewStuRegisterEvent event) {
        ClassesInfo classesInfo = classService.getById(event.getClassId());
        StuStatus stuStatus = new StuStatus();
        stuStatus.setStuNo(event.getSno());
        stuStatus.setIdCard(event.getStu().getNewStuIdCard());
        stuStatus.setBelongClassId(classesInfo.getId());
        stuStatus.setBelongSpecId(classesInfo.getBelongSpecId());
        stuStatusService.save(stuStatus);
        log.info("给新生[{}]创建迎新状态表", event.getStu().getNewStuName());
    }

}