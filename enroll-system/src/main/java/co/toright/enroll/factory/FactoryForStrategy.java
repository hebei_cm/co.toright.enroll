package co.toright.enroll.factory;

import co.toright.enroll.service.strategyService.DivideClassStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class FactoryForStrategy {

    @Autowired
    Map<String, DivideClassStrategy> strategys = new ConcurrentHashMap<>(3);

    public DivideClassStrategy getDivideClassStrategy(String component) throws Exception{
        DivideClassStrategy strategy = strategys.get(component);
        if(strategy == null) {
            throw new RuntimeException("暂无此策略！");
        }
        return strategy;
    }

}