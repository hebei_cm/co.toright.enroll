$(document).ready(function() {
    $("#datatable-specMgr").dataTable().fnDestroy();
    initTable();
} );

$.fn.dataTable.defaults.oLanguage = {
    "sProcessing": "处理中...",
    "sLengthMenu": "显示 _MENU_ 项结果",
    "sZeroRecords": "没有匹配结果",
    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
    "sInfoPostFix": "",
    "sSearch": "搜索：",
    "sUrl": "",
    "sEmptyTable": "表中数据为空",
    "sLoadingRecords": "载入中...",
    "sInfoThousands": ",",
    "oPaginate": {
        "sFirst": "首页",
        "sPrevious": "上页",
        "sNext": "下页",
        "sLast": "末页"
    },
    "oAria": {
        "sSortAscending": ": 以升序排列此列",
        "sSortDescending": ": 以降序排列此列"
    }
};

function initTable() {
    var table = $('#datatable-specMgr').DataTable({
        lengthChange: false,
        processing:true,
        ajax:"/specMgr/findAllSpec",
        columns: [
            { "data": "specId" },
            { "data": "specCode" },
            { "data": "specName" },
            { "data": "categoryCode"},
            { "data": "categoryName"},
            { "data": "belongCollegeId" },
        ],

        "columnDefs": [
            {
                "targets" : [6],
                "data" :  "specId" ,
                "render" :  function (data, type, full) {  // 返回自定义内容
                    return  "<a href='#' id='check_classes'>查看班级</a><br/>" +
                        "<a href='#' id='create_class'>生成班级</a>";
                }
            },
            {
                "targets" : [7],  // 目标列位置，下标从0开始
                "data" :  "collegeId" ,  // 数据列名
                "render" :  function (data, type, full) {  // 返回自定义内容
                    //class='btn-sm btn-primary m-1'
                    //class='btn-sm btn-danger m-1'
                    return  "<a id='btn_edit' style='text-decoration:underline;' href='#'>编辑</a></br>" +
                        "<a id='btn_delete' style='text-decoration:underline;' href='#'>删除</a>" ;
                }
            }],
        // "dom": 'Bfrtip',//表示在Buttons在filter左上方显示
        "lengthChange":false,//表示取消用户自选每页显示信息
        //表格初始化完成后的回调函数
        "fnInitComplete": function (oSettings, json) {
        }

    });

    //初始化导出工具按钮组
    new $.fn.dataTable.Buttons( table, {
        "buttons": [{
                extend: 'excelHtml5',
                text:'下载Excel',
                customize: function( xlsx ) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    $('row c[r^="C"]', sheet).attr( 's', '2' );
                }
            },
            {
                extend: 'pdfHtml5',
                text:'导出PDF',
            },
            {extend: 'print',
                text: "打印",
                customize: function(win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            },
            {extend: "colvis",
                text: '显示/隐藏列'}],
    } );

    table.buttons().container()
        .appendTo($('#datatable-specMgr_wrapper .col-md-6:eq(0)'));

    $("#datatable-specMgr tbody").on("click","#create_class",function () {
        var data = table.row( $(this).parents('tr') ).data();
        // alert(data.specId);
        creatClass(data);
    })
}

