$(document).ready(function() {
    $("#datatable_specMgr").dataTable().fnDestroy();
    initbuildTable();
} );


$.fn.dataTable.defaults.oLanguage = {
    "sProcessing": "处理中...",
    "sLengthMenu": "显示 _MENU_ 项结果",
    "sZeroRecords": "没有匹配结果",
    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
    "sInfoPostFix": "",
    "sSearch": "搜索：",
    "sUrl": "",
    "sEmptyTable": "表中数据为空",
    "sLoadingRecords": "载入中...",
    "sInfoThousands": ",",
    "oPaginate": {
        "sFirst": "首页",
        "sPrevious": "上页",
        "sNext": "下页",
        "sLast": "末页"
    },
    "oAria": {
        "sSortAscending": ": 以升序排列此列",
        "sSortDescending": ": 以降序排列此列"
    }
};

function initbuildTable() {
    var table = $('#datatable_buildMgr').DataTable({
        lengthChange: false,
        processing:true,
        defaultContent  :null,
        pageLength:5,
        scrollX:true,
        ajax:"/build/getBuilds",
        columns: [
            {"data":"buildId",visible:false},
            {"data":"belongArea","defaultContent":"-"},
            {"data":"buildName"},
            { "data": "houseMaster",
                "defaultContent":"-" ,
                visible:false,
            },
            { "data": "buildType","defaultContent":"-"},
            { "data": "houseTel" ,"defaultContent":"-",
                visible:false,},
            { "data": "floorCount","defaultContent":"-"},
            { "data": "roomCount","defaultContent":"-"},
        ],
        "columnDefs": [
            {
                targets: [4],
                data: "buildType",
                render:function (data,type,full) {
                    switch (data) {
                        case 1:
                            return "<label class=\"text-primary\">男生宿舍</label>";
                        case 2:
                            return "<label class='text-success'>女生宿舍</label>";
                        default:
                            return "<label class='text-warning'>暂未定义</label>";
                    }
                }
            },
            {
                "targets" : [8],
                "data" :  null ,
                "render" :  function (data, type, full) {  // 返回自定义内容
                    return  '<button type="button" id="btn_edit_build" data-toggle="tooltip" title="修改信息" class="btn-sm mdi btn-warning m-1 mdi-table-edit"></button>\n' +
                        '<button type="button" id="btn_check_build" data-toggle="tooltip" title="查看公寓" class="btn-sm mdi btn-info ion-eye  m-1"></button>' +
                        '<button type="button" id="btn_delete_build" data-toggle="tooltip" title="删除" class="btn-sm m-1 btn-danger mdi mdi-delete-forever "></button>';
                }
            }],
        // "dom": 'Bfrtip',//表示在Buttons在filter左上方显示
        "lengthChange":false,//表示取消用户自选每页显示信息
        order:[[4,"asc"]],
        //表格初始化完成后的回调函数
        "fnInitComplete": function (oSettings, json) {

        }

    });

    //初始化导出工具按钮组
    new $.fn.dataTable.Buttons( table, {
        "buttons": [{
            extend: 'excelHtml5',
            text:'下载Excel',
            customize: function( xlsx ) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                $('row c[r^="C"]', sheet).attr( 's', '2' );
            }
        },
            {extend: 'print',
                text: "打印",
                customize: function(win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            },
            {extend: "colvis",
                text: '显示/隐藏列'}],
    } );

    table.buttons().container()
        .appendTo($('#datatable_buildMgr_wrapper .col-md-6:eq(0)'));

    //查看楼栋按钮点击事件
    $('#datatable_buildMgr tbody').on( 'click', '#btn_check_build', function () {
        var data = table.row( $(this).parents('tr') ).data();
        // alert(data.specialityInfoList[2].specName);
        $("#nav_floorMgr").click();
        // console.log(data);
        searchBuild(data.belongAreaId,data.buildId);
        event.stopImmediatePropagation();
    });
    //编辑楼栋按钮点击事件
    $('#datatable_buildMgr tbody').on( 'click', '#btn_edit_build', function () {
        var data = table.row( $(this).parents('tr') ).data();
        // alert(data.specialityInfoList[2].specName);
        console.log(data.buildId);
        event.stopImmediatePropagation();
    });
    //删除楼栋按钮点击事件
    $('#datatable_buildMgr tbody').on( 'click', '#btn_delete_build', function () {
        var data = table.row( $(this).parents('tr') ).data();
        // alert(data.specialityInfoList[2].specName);
        console.log(data.buildId);
        event.stopImmediatePropagation();
    });
}

$("#select_area").off().click(function () {
//获取被选中的option标签的value
    var value = parseInt($(this).children('option:selected').val());
    if (typeof value ==='number' && !isNaN(value) && value!=0){
        $.ajax({
            url:"/buildCollegeRelation/getBuildList/"+value,
            type:'get',
            success:function (result) {
                //console.log(result);
                $("#select_build").empty();
                $("#select_build").append("<option value='0'>--请选择公寓--</option>");
                $.each(result.data,function (i,item) {
                    var $opti = $("<option value='"+item.buildId+"'>"+item.buildName+"</option>");
                    $opti.appendTo($("#select_build"));
                })

            }
        })
    }else {
        $("#select_build").empty();
        $("#select_build").append("<option value='0'>--请选择公寓--</option>");
    }
    $("#select_area option:eq(1)").prop("selected",true)
    // console.log()
    // TODO通过校区id获取该校区下公寓
});

//获取校区信息，填充到select中
function getAresList() {
    $.ajax({
        url:"/buildCollegeRelation/getAreaList",
        type:'get',
        dataType:'json',
        success:function (result) {
            // console.log(result)
            $("#select_area").empty();
            $("#select_area").append("<option value='0'>--请选择校区--</option>");
            $.each(result.data,function (i,item) {
                var $opti = $("<option value='"+item.areaId+"'>"+item.areaName+"</option>");
                $opti.appendTo($("#select_area"))
            })
            // .click();
            // $("#select_build option:eq(1)").prop("selected",true);
        }
    })
}

function initShowBuildInfo() {
    $("#show_build_name").val("");
    $("#show_build_type").val("");
    $("#show_build_floorCount").val("");
    $("#btn_add_floor").off();
    $("#btn_delete_floor").off();
    $("#btn_edit_floor").off();
}

//查询
function searchBuild(areaId,buildId) {
    if (areaId>0 && buildId>0){
        var val = areaId;
        var val2 = buildId;
    } else {
        var val = parseInt($("#select_area").val());
        var val2 = parseInt($("#select_build").val());
    }
    if (val>0 && val2>0){
        // alert("校区Id:"+val+","+"楼栋Id:"+val2);
        var parm = {"areaId":val,"buildId":val2}
        $.ajax({
            url:"/floor/listById",
            type:'post',
            data:JSON.stringify(parm),
            dataType:'json',
            contentType:"application/json;charset=utf-8",
            success:function (result) {
                // console.log(result);
                var data  = result.data;
                initShowBuildInfo();
                $("#show_build_name").val(data.buildName);
                switch (data.buildType) {
                    case 1:
                        $("#show_build_type").val("男生宿舍");
                        break;
                    case 2:
                        $("#show_build_type").val("女生宿舍");
                        break;
                    default:
                        $("#show_build_type").val("暂未定义");
                }
                $("#show_build_floorCount").val(data.floorCount);
                $("#btn_add_floor").off().attr("onClick",'').click(eval(function () {addFloor(data.buildId)}));
                $("#btn_edit_floor").off().attr("onClick",'').click(eval(function () {editFloor(data.buildId)}));
                $("#datatable_floorMgr tbody").empty();
                $.each(data.floorInfos,function (i,item) {
                    var td1 = $("<td hidden></td>").append(item.floorId);
                    var td2 = $("<td></td>").append("【"+item.floorNo+"】层");
                    if (item.dormInfos!=null){
                        var td3 = $("<td></td>").append(item.dormInfos.length+"间");
                    }else{
                        var td3 = $("<td></td>").append("-间");
                    }
                    var td4 = $("<td colspan='5'></td>")
                    $.each(item.dormInfos,function (i2,item2) {
                        var $div =  $('<div class="btn-group m-1"></div>');
                        var $button = $('<button class="btn btn-sm  dropdown-toggle" ' +
                            'type="button" data-toggle="dropdown" ' +
                            'aria-haspopup="true" aria-expanded="false"></button>')
                            .append(item.floorNo+"-"+item2.dormNo+"室");
                        switch (item2.alreadyLived) {
                            case 0:
                                $button.addClass("btn-success");
                                break;
                            case item2.dormCapacity:
                                $button.addClass("btn-danger");
                                break;
                            default:
                                $button.addClass("btn-warning");
                                break;
                        }
                        var $dropdown = $('<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 32px, 0px); top: 0px; left: 0px; will-change: transform;"></div>')
                        var a1 = $('<a class="dropdown-item" href="#"></a>').append("已住人数："+item2.alreadyLived);
                        var a2 = $('<a class="dropdown-item" href="#"></a>');
                        var free = item2.dormCapacity - item2.alreadyLived;
                        if (free>0) {
                            a2.append("空闲床位："+free);
                        }else {
                            a2.append("暂无空闲床位！");
                        }
                        var a3 = $('<a class="dropdown-item text-primary" onclick="checkDormDetail('+item2.dormId+')" href="javascript:void(0)">查看详情</a>');
                        $dropdown.append(a1).append(a2).append(a3);
                        $div.append($button).append($dropdown);
                        $div.appendTo(td4);

                    });
                    var td5 = $("<td></td>").append("<a href='#'>新增宿舍</a>")
                    $("<tr></tr>").append(td1).append(td2).append(td3).append(td4).append(td5)
                        .appendTo($("#datatable_floorMgr tbody"))
                })
            }
        })
    }else {
        alert("请选择查询条件");
        return false;
    }
}

/**
 *新增floor
 * @param id
 */
//TODO 新增floor
function addFloor(buildId) {
    $("#modal_floor_info").on('show.bs.modal',function () {
        $("#modla_title_floor_info").text("添加楼层信息");
        $("#modal_btn_comfirm").off().click(eval(function () {alert("添加成功")}))

    }).modal('toggle');
}

/**
 * 删除floor
 */
//TODO 删除floor
function editFloor(buildId) {
    $("#modal_floor_info").on('show.bs.modal',function () {
        $("#modla_title_floor_info").text("修改楼层信息");
        $("#modal_btn_comfirm").off().click(eval(function () {alert("修改成功")}))


    }).modal('toggle');

}

function resetSearch() {
    initShowBuildInfo();
    $("#select_area").prop("value",0);
    $("#select_build").empty();
    $("#select_build").append("<option value='0'>--请选择公寓--</option>");
    $("#select_build").prop("value",0);
}

/**
 * 查看宿舍详情
 */
function checkDormDetail(dormId) {
    // alert(dormId)
    $("#modal_dorm_info").off().on('show.bs.modal',function () {
        $("#beds_container").empty();
        $.ajax({
            url:"/dorm/getbedsInfo/"+dormId,
            type:'get',
            dataType:'json',
            contentText:'application/json;charset=utf-8',
            success:function (result) {
                var bedInfo = result.data.bedsInfo;
                var dormInfo = result.data.dormInfo
                $("#show_dorm_no").text(dormInfo.dormNo+"室");
                $("#show_dorm_capacity").text(dormInfo.dormCapacity+"人间");
                var nums = dormInfo.dormCapacity-dormInfo.alreadyLived;
                if (nums>0){
                    $("#show_dorm_empty").addClass("text-success").text(nums+"张");
                }else{
                    $("#show_dorm_empty").addClass("text-danger").text("无空闲床位");
                }
                $("#show_dorm_fee").text(dormInfo.dormFee+"元");

                $.each(bedInfo,function (i,item) {
                    var $div = $("<div class=\"card p-0 m-2\" ></div>").append();
                    var $body = $("<div class=\"card-body text-center flex-column\"></div>");
                    var span1 = $("<span></span>");
                    var i1 = $("<i></i>").append(item.bedNo+"号床");
                    var span2 = $("<span>床位号：</span>").append(i1)

                    if (item.belongSpecName!=null){
                        span1.addClass("fa fa-bed fa-5x");
                        if (item.belongStuName!=null){
                            var i2 = $("<a href='#' class=\"text-primary\"></a>").append(item.belongStuName);
                        }else{
                            var i2 = $("<a href='#' class=\"text-success\"></a>").append("分配人选");
                        }
                        var span3 = $("<span>所属学生：</span>").append(i2);
                        var i3 = $("<a href='#' class=\"text-primary\"></a>").append(item.belongSpecName);
                        var span4 = $("<span>所属专业：</span>").append(i3);
                        $body.append(span1).append("<br>").append(span2).append("<br>").append(span3).append("<br>").append(span4);
                    }else{
                        span1.addClass("fa fa-plus fa-5x");
                        var i3 = $("<a href='/dorm/batchAllocationDorm' class=\"text-success\"></a>").append("分配床位");
                        var span3 = $("<span>操作：</span>").append(i3);
                        $body.append(span1).append("<br>").append(span2).append("<br>").append(span3).append("<br>");
                    }
                     $div.append($body)
                    $("#beds_container").append($div);
                })
            }
        })


    }).modal('toggle');


}