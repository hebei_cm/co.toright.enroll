
$(function () {
    //接受新通知
    getNotice();

    getWebsocket();

})

var username = $("#online_user_name").text();
var roleId = $("#now_role_id").attr("role-id");
var userId = $("#online_user_name").attr("user-id");
var roleName = $("#now_role_id").attr("role-name");
var parm = username+"&"+roleId+"&"+roleName+"&"+userId;


function getNotice() {
    //接受新的

    $.ajax({
        url:'/notice/getAllNotice',
        dataType:'json',
        type:"get",
        success:function (result) {
            $(".fee-notice").empty().append(result.data.unAuditFeeItem+"条");
            $(".be-proccessed-stu").empty().append(result.data.beProcessedStu+"条");
        }
    })
}

function getWebsocket() {

        console.log(username+","+roleId+","+roleName);

        if ('WebSocket' in window){
            ws = new WebSocket("ws://127.0.0.1:8080/socketServer/"+parm);
        }
        else if ('MozWebSocket' in window){
            ws = new MozWebSocket("ws://127.0.0.1:8086/socketServer/"+parm);
        }
        else{
            alert("该浏览器不支持websocket");
        }

        //打开连接时触发
        ws.onopen = function(evt) {
            $("#unReaded_notice").html(evt.data);
        };

        ws.onmessage = function(evt) {
            //一收到消息就刷新未读条数
            refreshUnreadItems();
        };
        // ws.onclose = function(evt) {
        //     var content = $("#ppppp").html();
        //     $("#ppppp").html(content+'<div style="margin-bottom: 8px">\n' +
        //         '                        <p><q style="color: coral">客户端:</q><span>连接中断</span></p>\n' +
        //         '                    </div>\n' +
        //         '                    <br/>');
        // };
}

//收到消息后刷新未读消息条数
function refreshUnreadItems() {
    var id = parseInt(userId);
    $.ajax({
        url:"/notice/refreshUnreadItems/"+id,
        type:'get',
        dataType:'json',
        success:function (result) {
            $("#unReaded_notice").html(result.data);
        }
    })

}

function showUnReadItem() {
    $("#unRead_item_container").empty();
    var id = parseInt(userId);
    $.ajax({
        url:'/notice/getMyUnreadNotice/'+id,
        type:'get',
        dataType:"json",
        success:function (result) {
            // console.log(result.data);
            // alert(result.success);
            $.each(result.data,function (i,item) {
                var $i = $("<i class='mdi mdi-cart-outline'></i>");
                var $div = $("<div class='notify-icon bg-primary'></div>").append($i);

                //通知标题
                var $b = $("<b></b>").append(item.msgTitle);
                //通知内容
                var $small = $("<small class='text-muted'></small>").append(item.msgContent);
                var $p = $("<p class='notify-details'></p>");
                var btn_confirm = $("<a href='javascript:void(0);' onclick='comfirmHavingRead("+item.id+")' class='btn'>" +
                    "<i class='m-1 fa fa-check-circle font-20'></i>确认</a>");

                var btn_refuse = $("<a href='javascript:void(0);' class='btn'><i class='m-1 ion-close-circled'></i></a>");

                $p.append($b).append($small).append(btn_confirm).append(btn_refuse);

                var $a = $("<a href='javascript:void(0);' class='dropdown-item notify-item'></a>");
                $a.append($div).append($p).appendTo($("#unRead_item_container"));
            })
        }
    })
}

//确认用户已读
function comfirmHavingRead(itemId) {
    $.ajax({
        url:'/notice/comfirmHavingRead/'+parseInt(itemId),
        type:'get',
        dataType:'json',
        success:function (result) {
            //alert(result.msg);
            refreshUnreadItems();
        }
    })


}

function send() {
    var username = $("#online_user_name").text();
    // var roleId = $("#now_role_id").prop("role-id");
    // var roleName = $("#now_role_id").prop("role-name");
    ws.send("测试名");

}