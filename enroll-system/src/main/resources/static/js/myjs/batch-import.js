$(function () {
    getAreaInfo();
    getCollegeInfo();
})
/**
 * 学院信息上传
 */
$("#btn_upload_college").click(function () {
    var file = $('#collegefile')[0].files[0];
    var url = "uploadCollege";
    uploadBatch(file, url);
});
/**
 * 上传专业信息
 */
$("#btn_upload_spec").click(function () {
    var file = $('#specfile')[0].files[0];
    var url = "uploadSpec";
    uploadBatch(file, url);
});

/**
 * 上传新生信息
 */
$("#btn_upload_newStu").click(function () {
    var file = $('#newStufile')[0].files[0];
    var url = "uploadNewStu";
    uploadBatch(file, url);
});

/**
 * 上传楼栋信息
 */
$("#btn_upload_build").click(function () {
    var file = $('#build_file')[0].files[0];
    var url = "uploadBuild";
    uploadBatch(file, url);
});

/**
 * 上传宿舍信息
 */
$("#btn_upload_dorm").click(function () {
    var file = $('#dorm_file')[0].files[0];
    var url = "uploadDorm";
    uploadBatch(file, url);
});

/**
 * 批量上传
 * file为上传文件
 * url为地址
 * var file = $('#collegefile')[0].files[0];
 * var url = "uploadCollege";
 */
function uploadBatch(file, url) {
    var formData = new FormData();
    formData.append("file", file);
    $.ajax({
        url: '/sysInit/' + url,
        dataType: 'json',
        type: 'POST',
        async: false,
        data: formData,
        processData: false, // 使数据不做处理
        contentType: false, // 不要设置Content-Type请求头
        success: function (data) {
            console.log(data);
            if (data.success) {
                alert(data.msg);
            }
            if (!data.success) {
                alert(data.msg);
            }
        }
    });
}


/**
 * 步骤二获取学校校区信息
 * @type {SetStep}
 */
function getAreaInfo() {
    $("#area_info_tbody").empty();
    $.ajax({
        url: "/sysInit/getAreaInfo",
        success: function (result) {
            if (result.success) {
                $.each(result.data, function (index, item) {
                    var td1 = $("<td></td>").append(item.areaId);
                    var td2 = $("<td></td>").append(item.areaName);
                    var td3 = $("<td></td>").append(item.areaAddress);
                    var td4 = $("<td></td>");
                    $.each(item.collegeInfoList, function (i, item2) {
                        td4.append("<a href='#' id='" + item2.id + "'>" + item2.collegeName + "</a>").append("<br>")
                    })
                    var td5 = $("<td></td>");
                    $.each(item.buildInfoList, function (i, item) {
                        td5.append("<a href='#' id='" + item.id + "'>" + item.buildName + "</a>").append("<br>")
                    })
                    //将学院关联到校区
                    var contactCollege = $("<button class='btn btn-outline-success m-1' onclick='contactCollege(" + item.areaId + ")'></button>").append("关联学院")
                    var contactBuild = $("<button class='btn btn-outline-success m-1' onclick='contactBuild(" + item.areaId + ")'></button>").append("关联楼栋")
                    var btnDelte = $("<button id='" + item.areaId + "' class='btn btn-outline-danger' onclick='deleteAreaInfo(this)'>删除</button>")
                    var td6 = $("<td></td>").append(contactCollege).append(contactBuild).append(btnDelte);
                    $("<tr></tr>").append(td1).append(td2).append(td3).append(td4).append(td5).append(td6).appendTo($("#area_info_tbody"));
                })
            }
        }
    });
}

/**
 *关联校区学院
 */
function contactCollege(id) {
    $("#modal_contact_area").on('show.bs.modal', function () {
        //添加标签
    }).modal('show');
    //未分配校区的学院/包含本校区所有学院
    $.ajax({
        url: "/college/Unallocated/" + id,
        success: function (result) {
            $("#choose_college_table tbody").empty();
            $.each(result.data, function (i, item) {
                if (item.checked) {
                    var $input = $("<input type='checkbox' checked class='check_item'/>");
                } else {
                    var $input = $("<input type='checkbox' class='check_item'/>");
                }
                var td1 = $("<td></td>").append($input);
                var td2 = $("<td></td>").append(item.collegeId);
                var td3 = $("<td></td>").append(item.collegeName);
                var tr = $("<tr></tr>")
                tr.append(td1).append(td2).append(td3).appendTo($("#choose_college_table tbody"));
            })
        }
    });

    //点击确认按钮后获取所有选中的checkbox取值
    $("#submit_checked_college").off().on('click', function () {
        var itemChcked = $(".check_item:checked").parents("tr").find("td:eq(1)");
        var arr = new Array();
        $.each(itemChcked, function (index, item) {
            arr.push(item.innerText);
        })
        // console.log(arr);
        parm = {"ids": arr, "areaId": id}
        $.ajax({
            url: "/sysInit/saveAreaCollegeRelation",
            type: "post",
            data: JSON.stringify(parm),
            contentType: "application/json",
            success: function (result) {
                alert(result.msg);
                getAreaInfo();
                getCollegeInfo();
            }
        })
    })
}

/**
 * 关联楼栋到校区
 */
function contactBuild(areaId) {
    $("#modal_contact_build").on('show.bs.modal', function () {
    }).modal('show');
    $.ajax({
        url: "/sysInit/contactBuildToArea/" + areaId,
        type: 'get',
        success: function (result) {
            $("#choose_build_table tbody").empty();
            $.each(result.data, function (i, item) {
                // console.log(item.checked)
                if (item.checked) {
                    var $input = $("<input type='checkbox' checked class='check_build_item'/>");
                } else {
                    var $input = $("<input type='checkbox' class='check_build_item'/>");
                }
                var td1 = $("<td></td>").append($input);
                var td2 = $("<td></td>").append(item.buildId);
                var td3 = $("<td></td>").append(item.buildName);
                var td4 = $("<td></td>");
                switch (item.buildType) {
                    case 1:
                        td4.append("男生宿舍楼")
                        break;
                    case 2:
                        td4.append("女生宿舍楼")
                        break;
                    default:
                        td4.append("还未设置类型");
                }
                var tr = $("<tr></tr>");
                tr.append(td1).append(td2).append(td3).append(td4).appendTo($("#choose_build_table tbody"));
            })
        }
    })
    //点击确认按钮后获取所有选中的checkbox取值
    $("#submit_checked_build").off().on('click', function () {
        var itemChcked = $(".check_build_item:checked").parents("tr").find("td:eq(1)");
        var arr = new Array();
        $.each(itemChcked, function (index, item) {
            arr.push(item.innerText);
        })
        // console.log(arr);
        parm = {"ids": arr, "areaId": areaId}
        $.ajax({
            url: "/sysInit/saveAreaBuildRelation",
            type: "post",
            data: JSON.stringify(parm),
            contentType: "application/json",
            success: function (result) {
                alert(result.msg);
                getAreaInfo();
            }
        })
    })
}

//学院全选按钮
$("#check_all").click(function () {
    $(".check_item").prop("checked", $(this).prop("checked")).change();
});

//楼栋全选按钮
$("#check_build_all").click(function () {
    $(".check_build_item").prop("checked", $(this).prop("checked")).change();
});


/**
 * 步骤三，将专业、楼栋关联到学院
 */
function getCollegeInfo() {
    $.ajax({
        url: "/college/getAllCollegeOnArea",
        type: 'get',
        dataType: 'json',
        success: function (result) {
            // console.log(result.data);
            $("#college_spec_build_relation_tbody").empty()
            $.each(result.data, function (i, item) {
                var areaTR = $("<tr class='card-header'></tr>").append("<td class='border-dark' rowspan='" + (item.length + 1) + "'>" + i + "</td>");
                $("#college_spec_build_relation_tbody").append(areaTR);
                $.each(item, function (i2, item2) {
                    var tr = $("<tr></tr>");
                    var td1 = $("<td></td>").append(item2.collegeName);
                    var td2 = $("<td></td>");
                    $.each(item2.buildVos, function (i3, item3) {
                        var a = $("<a href='#'>" + item3.buildName + "</a>").append("<br>");
                        a.appendTo(td2);
                    })
                    var btncontactBuild = $('<button data-toggle="tooltip" title="关联楼栋" onclick="contactBuildToCollege(' + item2.collegeId + ')" class="m-1 btn btn-outline-secondary mdi mdi-arrange-bring-to-front">关联楼栋</button>')
                    var btncontactSpec = $('<button data-toggle="tooltip" title="关联专业" onclick="contactSpecToCollege(' + item2.collegeId + ')" class="m-1 btn btn-outline-success mdi mdi-arrange-bring-to-front ">关联专业</button>')
                    var td3 = $('<td></td>');

                    var td4 = $("<td></td>");
                    $.each(item2.specVos, function (i4, item4) {
                        var a = $("<a  href='#'>" + item4.specName + "</a>").append("<br>");
                        td4.append(a);
                    })
                    td3.append(btncontactSpec).append(btncontactBuild);
                    tr.append(td1).append(td4).append(td2).append(td3).appendTo($("#college_spec_build_relation_tbody"));
                })
            })
        }
    })
}

/**
 * 关联楼栋到学院下
 */
function contactBuildToCollege(collegeId) {
    $("#modal_step3").off().on('show.bs.modal', function () {
        $("#modal_header_step3").text('将楼栋关联到学院');
        //插入数据到模态框内容体中
        $("#modal_container_step3").html('<table id="contact_build_toCollege_table" class="table text-center">\n' +
            '                                        <thead class="thead-light">\n' +
            '                                        <tr>\n' +
            '                                            <th><input type="checkbox" id="check_build_all"></th>\n' +
            '                                            <th>楼栋编号</th>\n' +
            '                                            <th>楼栋名称</th>\n' +
            '                                            <th>楼栋类型</th>\n' +
            '                                        </tr>\n' +
            '                                        </thead>\n' +
            '                                        <tbody></tbody>\n' +
            '                                    </table>');
        $("#modal_container_step3 tbody").empty();
        $("#modal_foot_step3").empty();
        $.ajax({
            url: '/college/getBuildsByCollegeId/' + collegeId,
            type: 'get',
            dataType: 'json',
            success: function (result) {
                // console.log(result)
                $.each(result.data, function (i, item) {
                    var td1 = $("<td></td>");
                    if (item.checked) {
                        td1.append("<input type='checkbox' checked class='check_build_item'/>");
                    } else {
                        td1.append("<input type='checkbox' class='check_build_item'/>");
                    }
                    var td2 = $("<td></td>").append(item.buildId);
                    var td3 = $("<td></td>").append(item.buildName);
                    var td4 = $("<td></td>");
                    switch (item.buildType) {
                        case 1:
                            td4.append("<label class='text-primary'>男生宿舍</label>");
                            break;
                        case 2:
                            td4.append("<label class='text-success'>女生宿舍</label>");
                            break;
                        default:
                            td4.append("<label class='text-warning'>暂未定义</label>");
                    }
                    $("<tr></tr>").append(td1).append(td2).append(td3).append(td4).appendTo($("#contact_build_toCollege_table"))
                })
            }
        })
        //模态框底部
        var $button = $('<button type="button" onclick="submitBuildToCollege(' + collegeId + ')" class="btn btn-success" data-dismiss="modal">确认选中</button>');
        var $close = $('<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>');
        $("#modal_foot_step3").append($button).append($close);
    }).modal('toggle');
}

/**
 * 提交学院-楼栋关系
 */
function submitBuildToCollege(collegeId) {
    var itemChcked = $(".check_build_item:checked").parents("tr").find("td:eq(1)");
    var arr = new Array();
    $.each(itemChcked, function (i, item) {
        arr.push(item.innerText)
    })
    var parm = {
        "ids": arr,
        "collegeId": collegeId,
    };
    $.ajax({
        url: '/buildCollegeRelation/saveRelation',
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(parm),
        contentType: "application/json;charset=utf-8",
        success: function (result) {
            // console.log(result);
            getCollegeInfo();
            alert(result.msg);
        }
    })
    // console.log(arr)
    // console.log(itemChcked[0].innerText);
}

/**
 * 关联专业到学院下
 */
function contactSpecToCollege(collegeId) {
    $("#modal_step3").off().on('show.bs.modal', function () {
        $("#modal_header_step3").text('将专业关联到学院');
        //插入数据到模态框内容体中
        $("#modal_container_step3").html('<table id="contact_build_toCollege_table" class="table text-center">\n' +
            '                                        <thead class="thead-light">\n' +
            '                                        <tr>\n' +
            '                                            <th><input type="checkbox" id="check_spec_all"></th>\n' +
            '                                            <th>专业编号</th>\n' +
            '                                            <th>专业名称</th>\n' +
            '                                            <th>门类名称</th>\n' +
            '                                        </tr>\n' +
            '                                        </thead>\n' +
            '                                        <tbody></tbody>\n' +
            '                                    </table>');
        $("#modal_container_step3 tbody").empty();
        $("#modal_foot_step3").empty();
        $.ajax({
            url: '/specMgr/getSpecsByCollegeId/' + collegeId,
            type: 'get',
            dataType: 'json',
            success: function (result) {
                console.log(result)
                $.each(result.data, function (i, item) {
                    var td1 = $("<td></td>");
                    if (item.checked) {
                        td1.append("<input type='checkbox' checked class='check_spec_item'/>");
                    } else {
                        td1.append("<input type='checkbox' class='check_spec_item'/>");
                    }
                    var td2 = $("<td></td>").append(item.specId);
                    var td3 = $("<td></td>").append(item.specName);
                    var td4 = $("<td></td>").append(item.categoryName);
                    $("<tr></tr>").append(td1).append(td2).append(td3).append(td4).appendTo($("#contact_build_toCollege_table"))
                })
            }
        })
        //模态框底部
        var $button = $('<button type="button" onclick="submitSpecToCollege(' + collegeId + ')" class="btn btn-success" data-dismiss="modal">确认选中</button>');
        var $close = $('<button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>');
        $("#modal_foot_step3").append($button).append($close);
    }).modal('toggle');
}

/**
 * 提交学院专业关系
 */
function submitSpecToCollege(collegeId) {
    var itemChcked = $(".check_spec_item:checked").parents("tr").find("td:eq(1)");
    var arr = new Array();
    $.each(itemChcked, function (i, item) {
        arr.push(item.innerText);
    });
    console.log(arr);
    var parm = {"ids": arr, "collegeId": collegeId};
    $.ajax({
        url: "/specMgr/submitSpecToCollege",
        type: 'post',
        contentType: "application/json;charset=utf8",
        data: JSON.stringify(parm),
        success: function (result) {
            getCollegeInfo();
            alert(result.msg);
        }
    })

}

/**
 * 获取数据日志
 */
function getDataLog() {
    $("#datalog_body").empty();
    $.ajax({
        url: "/dataLog/importLog",
        type: "get",
        success: function (result) {
            $.each(result.data, function (i, item) {
                var td1 = $("<td></td>").append(item.id);
                var td2 = $("<td></td>").append(item.dataName);
                var td3 = $("<td></td>").append(item.dataDept);
                var td4 = $("<td></td>").append(item.updateTime);
                var td5 = $("<td></td>").append(item.dataCount);
                $("<tr></tr>").append(td1).append(td2).append(td3).append(td4).append(td5).appendTo($("#datalog_body"));
            });
        }
    })
}

/**
 * 新增校区信息
 * @type {SetStep}
 */
function addAreaInfo() {
    var areaName = $("#input_area_name").val();
    var areaAddress = $("#input_area_address").val();
    var parm = {"areaName": areaName, "areaAddress": areaAddress};
    $.ajax({
        url: "/sysInit/saveAreaInfo",
        type: "post",
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(parm),
        success: function (result) {
            $("#input_area_name").val("");
            $("#input_area_address").val("");
            alert(result.msg);
        }
    })
}

/**
 * 删除校区信息
 * @type {SetStep}
 */
function deleteAreaInfo(obj) {
    alert("删除前请先确认当前校区下无关联学院！")
    $.ajax({
        url: "/sysInit/removeArea/" + obj.id,
        type: "get",
        success: function (result) {
            alert(result.msg);
        }
    })
}


var step1 = new SetStep({
    skin: 1,
    content: '.stepCont1',
    showBtn: false,
    descriptionHeader: ['步骤一', '步骤二', '步骤三', '步骤四', '步骤五'],
    description: ['导入学院、专业、新生、宿舍信息', '完善校区信息，关联校区学院专业宿舍信息', '将专业、楼栋关联到学院', '设置网站基础信息', '数据总览'],
    showBtn: false,
})
