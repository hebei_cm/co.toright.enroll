$(function () {
    initTable()
})

function initTable() {
    $("#charge_tbody").empty();
    $.ajax({
        url:'/payType/typesList',
        type:'get',
        dataType:'json',
        success:function (result) {
            //console.log(result);
            $.each(result.data,function (i,item) {
                var td1 = $("<td></td>").append(item.id);
                var td2 = $("<td></td>").append(item.payTypeName);
                var td3 = $("<td></td>").append(item.releasePeopleName);
                var td4 = $("<td></td>").append(item.creatTime);
                var btn1 = $("<button class='btn btn-primary' onclick='seeChildCharge("+item.id+")'>查看收费子项</button>")
                var td5 = $("<td></td>").append(btn1);
                var td6 = $("<td></td>")
                switch (item.enabled) {
                    case 0:
                        td6.append("可用");
                        break;
                    case 1:
                        td6.append("不可用");
                        break;
                    default:
                        td6.append("暂未定义");
                        break;
                }
                var td7 = $("<td></td>").append(item.paymentRemark);
                var btn2 = $("<button class='btn btn-success'>启用</button>");
                //TODO 完善点击事件
                var btn3 = $("<button class='btn btn-warning'>禁用</button>");
                var btn4 = $("<button class='btn btn-danger'>删除</button>");
                var td8 = $("<td></td>").append(btn2).append(btn3).append(btn4);
                $("<tr></tr>").append(td1).append(td2).append(td3).append(td4)
                    .append(td5).append(td6).append(td7).append(td8).appendTo($("#charge_tbody"));
            })

        }
    })
}

function seeChildCharge(itemId) {
    alert(itemId);
}