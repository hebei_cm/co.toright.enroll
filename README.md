# 高校数字迎新系统

### 介绍
高校数字迎新系统，用于提供入学季，高校缓解大量新生集中报到压力，学生方门户网浏览学校简介，校园生活，问题咨询，迎新公告，过线上预报到，学生在线个人认证，选 宿，办理医保，物资确认，绿色通道申请，缴费等，学校端可以通过系统，根据学生特征分配班级生成学号，分配宿舍， 发布收费，迎新过程监控，领导迎新过程数据可视化，对迎新过程中每个环节可以查看实时滞留情况，并具体到学生个人。

### 软件架构
前台：bootstrap4、Ajax、Thymeleaf
后台：Springboot

### 关键系统功能截图
#### 1、批量分宿
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/112015_dde87590_5379512.png "屏幕截图.png")

#### 2、按学生特征分班
> *根据学生不同特征，例如（籍贯、生源地、性别、分数）等，保证在所分配班级中，相同特征的学生在各班中均衡分布*

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/112047_cc81c310_5379512.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/112332_1cac0e91_5379512.png "屏幕截图.png")

#### 3、公寓管理
> *可以查看学校所有公寓情况，并对于每栋楼可以查看所有楼层，及楼层下所有宿舍状态，绿色为无人住，黄色为未住满，红色为住满*

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/112504_ddf9471e_5379512.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/112854_81334e51_5379512.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113741_98df191f_5379512.png "屏幕截图.png")

#### 4、发布收费请求
> *学校对于学生的收费类型由财务处制定，具体的收费单由各专业负责人制定，例如学费、书本费、军训费*
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113117_fb1e312c_5379512.png "屏幕截图.png")
> *审核收费项*
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113236_a64edbdb_5379512.png "屏幕截图.png")

#### 5、迎新过程监控
> * 可以直观查看各环节完成情况，以及各学院完成度*
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113338_f3f23067_5379512.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113409_52546184_5379512.png "屏幕截图.png")

#### 6、新生选宿、查看缴费清单、
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113509_45455d4a_5379512.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113533_1d6a0773_5379512.png "屏幕截图.png")

#### 7、其他截图
>专业列表

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113820_31ba9cae_5379512.png "屏幕截图.png")

>班级列表

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113838_62fd0288_5379512.png "屏幕截图.png")

>角色权限

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113855_13e77da6_5379512.png "屏幕截图.png")

>批量导入

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/113954_822fe302_5379512.png "屏幕截图.png")

>关联学院域专业

![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/114029_96fa0237_5379512.png "屏幕截图.png")

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
