/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50729
Source Host           : 127.0.0.1:3306
Source Database       : enroll_system

Target Server Type    : MYSQL
Target Server Version : 50729
File Encoding         : 65001

Date: 2021-04-18 20:41:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for area_info
-- ----------------------------
DROP TABLE IF EXISTS `area_info`;
CREATE TABLE `area_info` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(255) DEFAULT NULL COMMENT '区域名',
  `area_address` varchar(255) DEFAULT NULL COMMENT '区域信息',
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of area_info
-- ----------------------------
INSERT INTO `area_info` VALUES ('1', '主校区', '万柏林区66号');
INSERT INTO `area_info` VALUES ('2', '西校区', '南社街');
INSERT INTO `area_info` VALUES ('3', '南校区', '晋源区');

-- ----------------------------
-- Table structure for bed_info
-- ----------------------------
DROP TABLE IF EXISTS `bed_info`;
CREATE TABLE `bed_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `bed_no` int(20) DEFAULT NULL COMMENT '床位号',
  `belong_dorm_id` int(20) DEFAULT NULL,
  `belong_stu_id` int(20) DEFAULT '0',
  `belong_spec_id` int(20) DEFAULT '0',
  `belong_floor_id` int(20) DEFAULT NULL,
  `belong_build_id` int(20) DEFAULT NULL,
  `bed_type` int(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1321 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bed_info
-- ----------------------------
INSERT INTO `bed_info` VALUES ('1', '1', '9', '0', '20', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('2', '2', '9', '0', '20', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('3', '3', '9', '0', '20', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('4', '4', '9', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('5', '1', '10', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('6', '2', '10', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('7', '3', '10', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('8', '4', '10', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('9', '1', '11', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('10', '2', '11', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('11', '3', '11', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('12', '4', '11', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('13', '1', '12', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('14', '2', '12', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('15', '3', '12', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('16', '4', '12', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('17', '5', '12', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('18', '6', '12', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('19', '1', '13', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('20', '2', '13', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('21', '3', '13', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('22', '4', '13', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('23', '5', '13', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('24', '6', '13', '0', '0', '1', '1', '2');
INSERT INTO `bed_info` VALUES ('25', '1', '14', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('26', '2', '14', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('27', '3', '14', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('28', '4', '14', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('29', '1', '15', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('30', '2', '15', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('31', '3', '15', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('32', '4', '15', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('33', '1', '16', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('34', '2', '16', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('35', '3', '16', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('36', '4', '16', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('37', '1', '17', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('38', '2', '17', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('39', '3', '17', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('40', '4', '17', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('41', '5', '17', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('42', '6', '17', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('43', '1', '18', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('44', '2', '18', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('45', '3', '18', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('46', '4', '18', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('47', '5', '18', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('48', '6', '18', '0', '0', '3', '1', '2');
INSERT INTO `bed_info` VALUES ('49', '1', '19', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('50', '2', '19', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('51', '3', '19', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('52', '4', '19', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('53', '1', '20', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('54', '2', '20', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('55', '3', '20', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('56', '4', '20', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('57', '1', '21', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('58', '2', '21', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('59', '3', '21', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('60', '4', '21', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('61', '1', '22', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('62', '2', '22', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('63', '3', '22', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('64', '4', '22', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('65', '5', '22', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('66', '6', '22', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('67', '1', '23', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('68', '2', '23', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('69', '3', '23', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('70', '4', '23', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('71', '5', '23', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('72', '6', '23', '0', '0', '4', '1', '2');
INSERT INTO `bed_info` VALUES ('73', '1', '24', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('74', '2', '24', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('75', '3', '24', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('76', '4', '24', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('77', '1', '25', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('78', '2', '25', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('79', '3', '25', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('80', '4', '25', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('81', '1', '26', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('82', '2', '26', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('83', '3', '26', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('84', '4', '26', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('85', '1', '27', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('86', '2', '27', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('87', '3', '27', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('88', '4', '27', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('89', '5', '27', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('90', '6', '27', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('91', '1', '28', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('92', '2', '28', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('93', '3', '28', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('94', '4', '28', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('95', '5', '28', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('96', '6', '28', '0', '0', '5', '1', '2');
INSERT INTO `bed_info` VALUES ('97', '1', '29', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('98', '2', '29', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('99', '3', '29', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('100', '4', '29', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('101', '1', '30', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('102', '2', '30', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('103', '3', '30', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('104', '4', '30', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('105', '1', '31', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('106', '2', '31', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('107', '3', '31', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('108', '4', '31', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('109', '1', '32', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('110', '2', '32', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('111', '3', '32', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('112', '4', '32', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('113', '5', '32', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('114', '6', '32', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('115', '1', '33', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('116', '2', '33', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('117', '3', '33', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('118', '4', '33', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('119', '5', '33', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('120', '6', '33', '0', '0', '6', '1', '2');
INSERT INTO `bed_info` VALUES ('121', '1', '34', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('122', '2', '34', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('123', '3', '34', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('124', '4', '34', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('125', '1', '35', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('126', '2', '35', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('127', '3', '35', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('128', '4', '35', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('129', '1', '36', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('130', '2', '36', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('131', '3', '36', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('132', '4', '36', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('133', '1', '37', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('134', '2', '37', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('135', '3', '37', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('136', '4', '37', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('137', '5', '37', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('138', '6', '37', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('139', '1', '38', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('140', '2', '38', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('141', '3', '38', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('142', '4', '38', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('143', '5', '38', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('144', '6', '38', '0', '0', '7', '2', '2');
INSERT INTO `bed_info` VALUES ('145', '1', '39', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('146', '2', '39', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('147', '3', '39', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('148', '4', '39', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('149', '1', '40', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('150', '2', '40', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('151', '3', '40', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('152', '4', '40', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('153', '1', '41', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('154', '2', '41', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('155', '3', '41', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('156', '4', '41', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('157', '1', '42', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('158', '2', '42', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('159', '3', '42', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('160', '4', '42', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('161', '5', '42', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('162', '6', '42', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('163', '1', '43', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('164', '2', '43', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('165', '3', '43', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('166', '4', '43', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('167', '5', '43', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('168', '6', '43', '0', '0', '8', '2', '2');
INSERT INTO `bed_info` VALUES ('169', '1', '44', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('170', '2', '44', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('171', '3', '44', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('172', '4', '44', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('173', '1', '45', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('174', '2', '45', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('175', '3', '45', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('176', '4', '45', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('177', '1', '46', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('178', '2', '46', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('179', '3', '46', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('180', '4', '46', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('181', '1', '47', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('182', '2', '47', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('183', '3', '47', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('184', '4', '47', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('185', '5', '47', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('186', '6', '47', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('187', '1', '48', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('188', '2', '48', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('189', '3', '48', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('190', '4', '48', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('191', '5', '48', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('192', '6', '48', '0', '0', '9', '2', '2');
INSERT INTO `bed_info` VALUES ('193', '1', '49', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('194', '2', '49', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('195', '3', '49', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('196', '4', '49', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('197', '1', '50', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('198', '2', '50', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('199', '3', '50', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('200', '4', '50', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('201', '1', '51', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('202', '2', '51', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('203', '3', '51', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('204', '4', '51', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('205', '1', '52', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('206', '2', '52', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('207', '3', '52', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('208', '4', '52', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('209', '5', '52', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('210', '6', '52', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('211', '1', '53', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('212', '2', '53', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('213', '3', '53', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('214', '4', '53', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('215', '5', '53', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('216', '6', '53', '0', '0', '10', '2', '2');
INSERT INTO `bed_info` VALUES ('217', '1', '54', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('218', '2', '54', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('219', '3', '54', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('220', '4', '54', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('221', '1', '55', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('222', '2', '55', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('223', '3', '55', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('224', '4', '55', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('225', '1', '56', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('226', '2', '56', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('227', '3', '56', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('228', '4', '56', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('229', '1', '57', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('230', '2', '57', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('231', '3', '57', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('232', '4', '57', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('233', '5', '57', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('234', '6', '57', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('235', '1', '58', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('236', '2', '58', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('237', '3', '58', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('238', '4', '58', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('239', '5', '58', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('240', '6', '58', '0', '0', '11', '2', '2');
INSERT INTO `bed_info` VALUES ('241', '1', '59', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('242', '2', '59', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('243', '3', '59', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('244', '4', '59', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('245', '1', '60', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('246', '2', '60', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('247', '3', '60', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('248', '4', '60', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('249', '1', '61', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('250', '2', '61', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('251', '3', '61', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('252', '4', '61', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('253', '1', '62', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('254', '2', '62', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('255', '3', '62', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('256', '4', '62', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('257', '5', '62', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('258', '6', '62', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('259', '1', '63', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('260', '2', '63', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('261', '3', '63', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('262', '4', '63', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('263', '5', '63', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('264', '6', '63', '0', '0', '12', '3', '1');
INSERT INTO `bed_info` VALUES ('265', '1', '64', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('266', '2', '64', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('267', '3', '64', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('268', '4', '64', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('269', '1', '65', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('270', '2', '65', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('271', '3', '65', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('272', '4', '65', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('273', '1', '66', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('274', '2', '66', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('275', '3', '66', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('276', '4', '66', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('277', '1', '67', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('278', '2', '67', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('279', '3', '67', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('280', '4', '67', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('281', '5', '67', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('282', '6', '67', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('283', '1', '68', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('284', '2', '68', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('285', '3', '68', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('286', '4', '68', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('287', '5', '68', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('288', '6', '68', '0', '0', '13', '3', '1');
INSERT INTO `bed_info` VALUES ('289', '1', '69', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('290', '2', '69', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('291', '3', '69', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('292', '4', '69', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('293', '1', '70', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('294', '2', '70', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('295', '3', '70', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('296', '4', '70', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('297', '1', '71', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('298', '2', '71', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('299', '3', '71', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('300', '4', '71', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('301', '1', '72', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('302', '2', '72', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('303', '3', '72', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('304', '4', '72', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('305', '5', '72', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('306', '6', '72', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('307', '1', '73', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('308', '2', '73', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('309', '3', '73', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('310', '4', '73', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('311', '5', '73', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('312', '6', '73', '0', '0', '14', '3', '1');
INSERT INTO `bed_info` VALUES ('313', '1', '74', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('314', '2', '74', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('315', '3', '74', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('316', '4', '74', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('317', '1', '75', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('318', '2', '75', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('319', '3', '75', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('320', '4', '75', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('321', '1', '76', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('322', '2', '76', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('323', '3', '76', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('324', '4', '76', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('325', '1', '77', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('326', '2', '77', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('327', '3', '77', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('328', '4', '77', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('329', '5', '77', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('330', '6', '77', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('331', '1', '78', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('332', '2', '78', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('333', '3', '78', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('334', '4', '78', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('335', '5', '78', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('336', '6', '78', '0', '0', '15', '3', '1');
INSERT INTO `bed_info` VALUES ('337', '1', '79', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('338', '2', '79', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('339', '3', '79', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('340', '4', '79', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('341', '1', '80', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('342', '2', '80', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('343', '3', '80', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('344', '4', '80', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('345', '1', '81', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('346', '2', '81', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('347', '3', '81', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('348', '4', '81', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('349', '1', '82', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('350', '2', '82', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('351', '3', '82', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('352', '4', '82', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('353', '5', '82', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('354', '6', '82', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('355', '1', '83', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('356', '2', '83', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('357', '3', '83', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('358', '4', '83', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('359', '5', '83', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('360', '6', '83', '0', '0', '16', '3', '1');
INSERT INTO `bed_info` VALUES ('361', '1', '84', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('362', '2', '84', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('363', '3', '84', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('364', '4', '84', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('365', '1', '85', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('366', '2', '85', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('367', '3', '85', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('368', '4', '85', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('369', '1', '86', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('370', '2', '86', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('371', '3', '86', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('372', '4', '86', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('373', '1', '87', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('374', '2', '87', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('375', '3', '87', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('376', '4', '87', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('377', '5', '87', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('378', '6', '87', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('379', '1', '88', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('380', '2', '88', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('381', '3', '88', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('382', '4', '88', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('383', '5', '88', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('384', '6', '88', '0', '0', '17', '4', '2');
INSERT INTO `bed_info` VALUES ('385', '1', '89', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('386', '2', '89', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('387', '3', '89', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('388', '4', '89', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('389', '1', '90', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('390', '2', '90', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('391', '3', '90', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('392', '4', '90', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('393', '1', '91', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('394', '2', '91', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('395', '3', '91', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('396', '4', '91', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('397', '1', '92', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('398', '2', '92', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('399', '3', '92', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('400', '4', '92', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('401', '5', '92', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('402', '6', '92', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('403', '1', '93', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('404', '2', '93', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('405', '3', '93', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('406', '4', '93', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('407', '5', '93', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('408', '6', '93', '0', '0', '18', '4', '2');
INSERT INTO `bed_info` VALUES ('409', '1', '94', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('410', '2', '94', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('411', '3', '94', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('412', '4', '94', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('413', '1', '95', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('414', '2', '95', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('415', '3', '95', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('416', '4', '95', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('417', '1', '96', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('418', '2', '96', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('419', '3', '96', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('420', '4', '96', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('421', '1', '97', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('422', '2', '97', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('423', '3', '97', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('424', '4', '97', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('425', '5', '97', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('426', '6', '97', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('427', '1', '98', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('428', '2', '98', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('429', '3', '98', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('430', '4', '98', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('431', '5', '98', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('432', '6', '98', '0', '0', '19', '4', '2');
INSERT INTO `bed_info` VALUES ('433', '1', '99', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('434', '2', '99', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('435', '3', '99', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('436', '4', '99', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('437', '1', '100', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('438', '2', '100', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('439', '3', '100', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('440', '4', '100', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('441', '1', '101', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('442', '2', '101', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('443', '3', '101', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('444', '4', '101', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('445', '1', '102', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('446', '2', '102', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('447', '3', '102', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('448', '4', '102', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('449', '5', '102', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('450', '6', '102', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('451', '1', '103', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('452', '2', '103', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('453', '3', '103', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('454', '4', '103', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('455', '5', '103', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('456', '6', '103', '0', '0', '20', '4', '2');
INSERT INTO `bed_info` VALUES ('457', '1', '104', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('458', '2', '104', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('459', '3', '104', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('460', '4', '104', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('461', '1', '105', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('462', '2', '105', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('463', '3', '105', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('464', '4', '105', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('465', '1', '106', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('466', '2', '106', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('467', '3', '106', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('468', '4', '106', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('469', '1', '107', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('470', '2', '107', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('471', '3', '107', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('472', '4', '107', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('473', '5', '107', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('474', '6', '107', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('475', '1', '108', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('476', '2', '108', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('477', '3', '108', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('478', '4', '108', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('479', '5', '108', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('480', '6', '108', '0', '0', '21', '4', '2');
INSERT INTO `bed_info` VALUES ('481', '1', '109', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('482', '2', '109', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('483', '3', '109', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('484', '4', '109', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('485', '1', '110', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('486', '2', '110', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('487', '3', '110', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('488', '4', '110', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('489', '1', '111', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('490', '2', '111', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('491', '3', '111', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('492', '4', '111', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('493', '1', '112', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('494', '2', '112', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('495', '3', '112', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('496', '4', '112', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('497', '5', '112', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('498', '6', '112', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('499', '1', '113', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('500', '2', '113', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('501', '3', '113', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('502', '4', '113', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('503', '5', '113', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('504', '6', '113', '0', '0', '22', '5', '1');
INSERT INTO `bed_info` VALUES ('505', '1', '114', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('506', '2', '114', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('507', '3', '114', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('508', '4', '114', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('509', '1', '115', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('510', '2', '115', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('511', '3', '115', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('512', '4', '115', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('513', '1', '116', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('514', '2', '116', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('515', '3', '116', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('516', '4', '116', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('517', '1', '117', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('518', '2', '117', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('519', '3', '117', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('520', '4', '117', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('521', '5', '117', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('522', '6', '117', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('523', '1', '118', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('524', '2', '118', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('525', '3', '118', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('526', '4', '118', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('527', '5', '118', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('528', '6', '118', '0', '0', '23', '5', '1');
INSERT INTO `bed_info` VALUES ('529', '1', '119', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('530', '2', '119', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('531', '3', '119', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('532', '4', '119', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('533', '1', '120', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('534', '2', '120', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('535', '3', '120', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('536', '4', '120', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('537', '1', '121', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('538', '2', '121', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('539', '3', '121', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('540', '4', '121', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('541', '1', '122', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('542', '2', '122', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('543', '3', '122', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('544', '4', '122', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('545', '5', '122', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('546', '6', '122', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('547', '1', '123', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('548', '2', '123', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('549', '3', '123', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('550', '4', '123', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('551', '5', '123', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('552', '6', '123', '0', '0', '24', '5', '1');
INSERT INTO `bed_info` VALUES ('553', '1', '124', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('554', '2', '124', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('555', '3', '124', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('556', '4', '124', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('557', '1', '125', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('558', '2', '125', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('559', '3', '125', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('560', '4', '125', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('561', '1', '126', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('562', '2', '126', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('563', '3', '126', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('564', '4', '126', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('565', '1', '127', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('566', '2', '127', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('567', '3', '127', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('568', '4', '127', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('569', '5', '127', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('570', '6', '127', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('571', '1', '128', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('572', '2', '128', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('573', '3', '128', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('574', '4', '128', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('575', '5', '128', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('576', '6', '128', '0', '0', '25', '5', '1');
INSERT INTO `bed_info` VALUES ('577', '1', '129', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('578', '2', '129', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('579', '3', '129', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('580', '4', '129', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('581', '1', '130', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('582', '2', '130', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('583', '3', '130', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('584', '4', '130', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('585', '1', '131', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('586', '2', '131', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('587', '3', '131', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('588', '4', '131', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('589', '1', '132', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('590', '2', '132', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('591', '3', '132', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('592', '4', '132', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('593', '5', '132', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('594', '6', '132', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('595', '1', '133', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('596', '2', '133', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('597', '3', '133', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('598', '4', '133', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('599', '5', '133', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('600', '6', '133', '0', '0', '26', '5', '1');
INSERT INTO `bed_info` VALUES ('601', '1', '134', '0', '30', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('602', '2', '134', '0', '32', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('603', '3', '134', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('604', '4', '134', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('605', '1', '135', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('606', '2', '135', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('607', '3', '135', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('608', '4', '135', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('609', '1', '136', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('610', '2', '136', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('611', '3', '136', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('612', '4', '136', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('613', '1', '137', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('614', '2', '137', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('615', '3', '137', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('616', '4', '137', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('617', '5', '137', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('618', '6', '137', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('619', '1', '138', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('620', '2', '138', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('621', '3', '138', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('622', '4', '138', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('623', '5', '138', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('624', '6', '138', '0', '0', '27', '6', '1');
INSERT INTO `bed_info` VALUES ('625', '1', '139', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('626', '2', '139', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('627', '3', '139', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('628', '4', '139', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('629', '1', '140', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('630', '2', '140', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('631', '3', '140', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('632', '4', '140', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('633', '1', '141', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('634', '2', '141', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('635', '3', '141', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('636', '4', '141', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('637', '1', '142', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('638', '2', '142', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('639', '3', '142', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('640', '4', '142', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('641', '5', '142', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('642', '6', '142', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('643', '1', '143', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('644', '2', '143', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('645', '3', '143', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('646', '4', '143', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('647', '5', '143', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('648', '6', '143', '0', '0', '28', '6', '1');
INSERT INTO `bed_info` VALUES ('649', '1', '144', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('650', '2', '144', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('651', '3', '144', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('652', '4', '144', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('653', '1', '145', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('654', '2', '145', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('655', '3', '145', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('656', '4', '145', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('657', '1', '146', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('658', '2', '146', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('659', '3', '146', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('660', '4', '146', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('661', '1', '147', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('662', '2', '147', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('663', '3', '147', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('664', '4', '147', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('665', '5', '147', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('666', '6', '147', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('667', '1', '148', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('668', '2', '148', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('669', '3', '148', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('670', '4', '148', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('671', '5', '148', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('672', '6', '148', '0', '0', '29', '6', '1');
INSERT INTO `bed_info` VALUES ('673', '1', '149', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('674', '2', '149', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('675', '3', '149', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('676', '4', '149', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('677', '1', '150', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('678', '2', '150', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('679', '3', '150', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('680', '4', '150', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('681', '1', '151', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('682', '2', '151', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('683', '3', '151', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('684', '4', '151', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('685', '1', '152', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('686', '2', '152', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('687', '3', '152', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('688', '4', '152', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('689', '5', '152', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('690', '6', '152', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('691', '1', '153', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('692', '2', '153', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('693', '3', '153', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('694', '4', '153', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('695', '5', '153', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('696', '6', '153', '0', '0', '30', '6', '1');
INSERT INTO `bed_info` VALUES ('697', '1', '154', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('698', '2', '154', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('699', '3', '154', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('700', '4', '154', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('701', '1', '155', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('702', '2', '155', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('703', '3', '155', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('704', '4', '155', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('705', '1', '156', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('706', '2', '156', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('707', '3', '156', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('708', '4', '156', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('709', '1', '157', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('710', '2', '157', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('711', '3', '157', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('712', '4', '157', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('713', '5', '157', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('714', '6', '157', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('715', '1', '158', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('716', '2', '158', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('717', '3', '158', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('718', '4', '158', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('719', '5', '158', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('720', '6', '158', '0', '0', '31', '6', '1');
INSERT INTO `bed_info` VALUES ('721', '1', '159', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('722', '2', '159', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('723', '3', '159', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('724', '4', '159', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('725', '1', '160', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('726', '2', '160', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('727', '3', '160', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('728', '4', '160', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('729', '1', '161', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('730', '2', '161', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('731', '3', '161', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('732', '4', '161', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('733', '1', '162', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('734', '2', '162', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('735', '3', '162', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('736', '4', '162', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('737', '5', '162', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('738', '6', '162', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('739', '1', '163', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('740', '2', '163', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('741', '3', '163', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('742', '4', '163', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('743', '5', '163', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('744', '6', '163', '0', '0', '32', '7', '1');
INSERT INTO `bed_info` VALUES ('745', '1', '164', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('746', '2', '164', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('747', '3', '164', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('748', '4', '164', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('749', '1', '165', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('750', '2', '165', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('751', '3', '165', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('752', '4', '165', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('753', '1', '166', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('754', '2', '166', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('755', '3', '166', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('756', '4', '166', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('757', '1', '167', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('758', '2', '167', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('759', '3', '167', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('760', '4', '167', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('761', '5', '167', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('762', '6', '167', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('763', '1', '168', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('764', '2', '168', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('765', '3', '168', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('766', '4', '168', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('767', '5', '168', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('768', '6', '168', '0', '0', '33', '7', '1');
INSERT INTO `bed_info` VALUES ('769', '1', '169', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('770', '2', '169', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('771', '3', '169', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('772', '4', '169', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('773', '1', '170', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('774', '2', '170', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('775', '3', '170', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('776', '4', '170', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('777', '1', '171', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('778', '2', '171', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('779', '3', '171', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('780', '4', '171', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('781', '1', '172', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('782', '2', '172', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('783', '3', '172', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('784', '4', '172', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('785', '5', '172', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('786', '6', '172', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('787', '1', '173', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('788', '2', '173', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('789', '3', '173', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('790', '4', '173', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('791', '5', '173', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('792', '6', '173', '0', '0', '34', '7', '1');
INSERT INTO `bed_info` VALUES ('793', '1', '174', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('794', '2', '174', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('795', '3', '174', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('796', '4', '174', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('797', '1', '175', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('798', '2', '175', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('799', '3', '175', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('800', '4', '175', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('801', '1', '176', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('802', '2', '176', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('803', '3', '176', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('804', '4', '176', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('805', '1', '177', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('806', '2', '177', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('807', '3', '177', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('808', '4', '177', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('809', '5', '177', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('810', '6', '177', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('811', '1', '178', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('812', '2', '178', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('813', '3', '178', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('814', '4', '178', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('815', '5', '178', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('816', '6', '178', '0', '0', '35', '7', '1');
INSERT INTO `bed_info` VALUES ('817', '1', '179', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('818', '2', '179', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('819', '3', '179', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('820', '4', '179', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('821', '1', '180', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('822', '2', '180', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('823', '3', '180', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('824', '4', '180', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('825', '1', '181', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('826', '2', '181', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('827', '3', '181', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('828', '4', '181', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('829', '1', '182', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('830', '2', '182', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('831', '3', '182', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('832', '4', '182', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('833', '5', '182', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('834', '6', '182', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('835', '1', '183', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('836', '2', '183', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('837', '3', '183', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('838', '4', '183', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('839', '5', '183', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('840', '6', '183', '0', '0', '36', '7', '1');
INSERT INTO `bed_info` VALUES ('841', '1', '184', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('842', '2', '184', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('843', '3', '184', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('844', '4', '184', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('845', '1', '185', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('846', '2', '185', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('847', '3', '185', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('848', '4', '185', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('849', '1', '186', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('850', '2', '186', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('851', '3', '186', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('852', '4', '186', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('853', '1', '187', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('854', '2', '187', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('855', '3', '187', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('856', '4', '187', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('857', '5', '187', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('858', '6', '187', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('859', '1', '188', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('860', '2', '188', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('861', '3', '188', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('862', '4', '188', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('863', '5', '188', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('864', '6', '188', '0', '0', '37', '8', '2');
INSERT INTO `bed_info` VALUES ('865', '1', '189', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('866', '2', '189', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('867', '3', '189', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('868', '4', '189', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('869', '1', '190', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('870', '2', '190', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('871', '3', '190', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('872', '4', '190', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('873', '1', '191', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('874', '2', '191', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('875', '3', '191', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('876', '4', '191', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('877', '1', '192', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('878', '2', '192', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('879', '3', '192', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('880', '4', '192', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('881', '5', '192', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('882', '6', '192', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('883', '1', '193', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('884', '2', '193', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('885', '3', '193', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('886', '4', '193', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('887', '5', '193', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('888', '6', '193', '0', '0', '38', '8', '2');
INSERT INTO `bed_info` VALUES ('889', '1', '194', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('890', '2', '194', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('891', '3', '194', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('892', '4', '194', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('893', '1', '195', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('894', '2', '195', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('895', '3', '195', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('896', '4', '195', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('897', '1', '196', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('898', '2', '196', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('899', '3', '196', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('900', '4', '196', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('901', '1', '197', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('902', '2', '197', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('903', '3', '197', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('904', '4', '197', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('905', '5', '197', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('906', '6', '197', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('907', '1', '198', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('908', '2', '198', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('909', '3', '198', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('910', '4', '198', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('911', '5', '198', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('912', '6', '198', '0', '0', '39', '8', '2');
INSERT INTO `bed_info` VALUES ('913', '1', '199', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('914', '2', '199', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('915', '3', '199', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('916', '4', '199', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('917', '1', '200', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('918', '2', '200', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('919', '3', '200', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('920', '4', '200', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('921', '1', '201', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('922', '2', '201', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('923', '3', '201', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('924', '4', '201', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('925', '1', '202', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('926', '2', '202', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('927', '3', '202', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('928', '4', '202', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('929', '5', '202', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('930', '6', '202', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('931', '1', '203', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('932', '2', '203', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('933', '3', '203', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('934', '4', '203', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('935', '5', '203', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('936', '6', '203', '0', '0', '40', '8', '2');
INSERT INTO `bed_info` VALUES ('937', '1', '204', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('938', '2', '204', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('939', '3', '204', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('940', '4', '204', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('941', '1', '205', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('942', '2', '205', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('943', '3', '205', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('944', '4', '205', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('945', '1', '206', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('946', '2', '206', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('947', '3', '206', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('948', '4', '206', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('949', '1', '207', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('950', '2', '207', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('951', '3', '207', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('952', '4', '207', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('953', '5', '207', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('954', '6', '207', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('955', '1', '208', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('956', '2', '208', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('957', '3', '208', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('958', '4', '208', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('959', '5', '208', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('960', '6', '208', '0', '0', '41', '8', '2');
INSERT INTO `bed_info` VALUES ('961', '1', '209', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('962', '2', '209', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('963', '3', '209', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('964', '4', '209', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('965', '1', '210', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('966', '2', '210', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('967', '3', '210', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('968', '4', '210', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('969', '1', '211', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('970', '2', '211', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('971', '3', '211', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('972', '4', '211', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('973', '1', '212', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('974', '2', '212', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('975', '3', '212', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('976', '4', '212', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('977', '5', '212', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('978', '6', '212', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('979', '1', '213', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('980', '2', '213', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('981', '3', '213', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('982', '4', '213', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('983', '5', '213', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('984', '6', '213', '0', '0', '42', '9', '2');
INSERT INTO `bed_info` VALUES ('985', '1', '214', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('986', '2', '214', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('987', '3', '214', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('988', '4', '214', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('989', '1', '215', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('990', '2', '215', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('991', '3', '215', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('992', '4', '215', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('993', '1', '216', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('994', '2', '216', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('995', '3', '216', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('996', '4', '216', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('997', '1', '217', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('998', '2', '217', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('999', '3', '217', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('1000', '4', '217', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('1001', '5', '217', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('1002', '6', '217', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('1003', '1', '218', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('1004', '2', '218', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('1005', '3', '218', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('1006', '4', '218', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('1007', '5', '218', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('1008', '6', '218', '0', '0', '43', '9', '2');
INSERT INTO `bed_info` VALUES ('1009', '1', '219', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1010', '2', '219', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1011', '3', '219', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1012', '4', '219', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1013', '1', '220', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1014', '2', '220', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1015', '3', '220', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1016', '4', '220', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1017', '1', '221', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1018', '2', '221', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1019', '3', '221', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1020', '4', '221', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1021', '1', '222', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1022', '2', '222', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1023', '3', '222', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1024', '4', '222', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1025', '5', '222', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1026', '6', '222', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1027', '1', '223', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1028', '2', '223', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1029', '3', '223', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1030', '4', '223', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1031', '5', '223', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1032', '6', '223', '0', '0', '44', '9', '2');
INSERT INTO `bed_info` VALUES ('1033', '1', '224', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1034', '2', '224', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1035', '3', '224', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1036', '4', '224', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1037', '1', '225', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1038', '2', '225', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1039', '3', '225', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1040', '4', '225', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1041', '1', '226', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1042', '2', '226', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1043', '3', '226', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1044', '4', '226', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1045', '1', '227', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1046', '2', '227', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1047', '3', '227', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1048', '4', '227', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1049', '5', '227', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1050', '6', '227', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1051', '1', '228', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1052', '2', '228', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1053', '3', '228', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1054', '4', '228', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1055', '5', '228', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1056', '6', '228', '0', '0', '45', '9', '2');
INSERT INTO `bed_info` VALUES ('1057', '1', '229', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1058', '2', '229', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1059', '3', '229', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1060', '4', '229', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1061', '1', '230', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1062', '2', '230', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1063', '3', '230', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1064', '4', '230', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1065', '1', '231', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1066', '2', '231', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1067', '3', '231', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1068', '4', '231', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1069', '1', '232', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1070', '2', '232', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1071', '3', '232', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1072', '4', '232', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1073', '5', '232', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1074', '6', '232', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1075', '1', '233', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1076', '2', '233', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1077', '3', '233', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1078', '4', '233', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1079', '5', '233', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1080', '6', '233', '0', '0', '46', '9', '2');
INSERT INTO `bed_info` VALUES ('1081', '1', '234', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1082', '2', '234', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1083', '3', '234', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1084', '4', '234', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1085', '1', '235', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1086', '2', '235', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1087', '3', '235', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1088', '4', '235', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1089', '1', '236', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1090', '2', '236', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1091', '3', '236', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1092', '4', '236', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1093', '1', '237', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1094', '2', '237', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1095', '3', '237', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1096', '4', '237', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1097', '5', '237', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1098', '6', '237', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1099', '1', '238', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1100', '2', '238', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1101', '3', '238', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1102', '4', '238', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1103', '5', '238', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1104', '6', '238', '0', '0', '47', '10', '2');
INSERT INTO `bed_info` VALUES ('1105', '1', '239', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1106', '2', '239', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1107', '3', '239', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1108', '4', '239', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1109', '1', '240', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1110', '2', '240', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1111', '3', '240', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1112', '4', '240', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1113', '1', '241', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1114', '2', '241', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1115', '3', '241', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1116', '4', '241', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1117', '1', '242', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1118', '2', '242', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1119', '3', '242', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1120', '4', '242', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1121', '5', '242', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1122', '6', '242', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1123', '1', '243', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1124', '2', '243', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1125', '3', '243', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1126', '4', '243', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1127', '5', '243', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1128', '6', '243', '0', '0', '48', '10', '2');
INSERT INTO `bed_info` VALUES ('1129', '1', '244', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1130', '2', '244', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1131', '3', '244', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1132', '4', '244', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1133', '1', '245', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1134', '2', '245', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1135', '3', '245', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1136', '4', '245', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1137', '1', '246', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1138', '2', '246', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1139', '3', '246', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1140', '4', '246', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1141', '1', '247', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1142', '2', '247', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1143', '3', '247', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1144', '4', '247', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1145', '5', '247', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1146', '6', '247', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1147', '1', '248', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1148', '2', '248', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1149', '3', '248', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1150', '4', '248', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1151', '5', '248', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1152', '6', '248', '0', '0', '49', '10', '2');
INSERT INTO `bed_info` VALUES ('1153', '1', '249', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1154', '2', '249', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1155', '3', '249', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1156', '4', '249', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1157', '1', '250', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1158', '2', '250', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1159', '3', '250', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1160', '4', '250', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1161', '1', '251', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1162', '2', '251', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1163', '3', '251', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1164', '4', '251', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1165', '1', '252', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1166', '2', '252', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1167', '3', '252', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1168', '4', '252', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1169', '5', '252', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1170', '6', '252', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1171', '1', '253', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1172', '2', '253', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1173', '3', '253', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1174', '4', '253', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1175', '5', '253', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1176', '6', '253', '0', '0', '50', '10', '2');
INSERT INTO `bed_info` VALUES ('1177', '1', '254', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1178', '2', '254', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1179', '3', '254', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1180', '4', '254', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1181', '1', '255', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1182', '2', '255', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1183', '3', '255', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1184', '4', '255', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1185', '1', '256', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1186', '2', '256', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1187', '3', '256', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1188', '4', '256', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1189', '1', '257', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1190', '2', '257', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1191', '3', '257', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1192', '4', '257', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1193', '5', '257', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1194', '6', '257', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1195', '1', '258', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1196', '2', '258', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1197', '3', '258', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1198', '4', '258', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1199', '5', '258', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1200', '6', '258', '0', '0', '51', '10', '2');
INSERT INTO `bed_info` VALUES ('1201', '1', '259', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1202', '2', '259', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1203', '3', '259', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1204', '4', '259', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1205', '1', '260', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1206', '2', '260', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1207', '3', '260', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1208', '4', '260', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1209', '1', '261', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1210', '2', '261', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1211', '3', '261', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1212', '4', '261', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1213', '1', '262', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1214', '2', '262', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1215', '3', '262', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1216', '4', '262', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1217', '5', '262', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1218', '6', '262', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1219', '1', '263', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1220', '2', '263', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1221', '3', '263', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1222', '4', '263', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1223', '5', '263', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1224', '6', '263', '0', '0', '52', '11', '1');
INSERT INTO `bed_info` VALUES ('1225', '1', '264', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1226', '2', '264', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1227', '3', '264', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1228', '4', '264', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1229', '1', '265', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1230', '2', '265', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1231', '3', '265', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1232', '4', '265', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1233', '1', '266', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1234', '2', '266', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1235', '3', '266', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1236', '4', '266', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1237', '1', '267', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1238', '2', '267', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1239', '3', '267', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1240', '4', '267', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1241', '5', '267', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1242', '6', '267', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1243', '1', '268', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1244', '2', '268', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1245', '3', '268', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1246', '4', '268', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1247', '5', '268', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1248', '6', '268', '0', '0', '53', '11', '1');
INSERT INTO `bed_info` VALUES ('1249', '1', '269', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1250', '2', '269', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1251', '3', '269', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1252', '4', '269', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1253', '1', '270', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1254', '2', '270', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1255', '3', '270', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1256', '4', '270', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1257', '1', '271', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1258', '2', '271', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1259', '3', '271', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1260', '4', '271', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1261', '1', '272', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1262', '2', '272', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1263', '3', '272', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1264', '4', '272', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1265', '5', '272', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1266', '6', '272', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1267', '1', '273', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1268', '2', '273', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1269', '3', '273', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1270', '4', '273', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1271', '5', '273', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1272', '6', '273', '0', '0', '54', '11', '1');
INSERT INTO `bed_info` VALUES ('1273', '1', '274', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1274', '2', '274', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1275', '3', '274', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1276', '4', '274', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1277', '1', '275', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1278', '2', '275', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1279', '3', '275', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1280', '4', '275', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1281', '1', '276', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1282', '2', '276', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1283', '3', '276', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1284', '4', '276', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1285', '1', '277', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1286', '2', '277', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1287', '3', '277', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1288', '4', '277', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1289', '5', '277', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1290', '6', '277', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1291', '1', '278', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1292', '2', '278', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1293', '3', '278', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1294', '4', '278', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1295', '5', '278', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1296', '6', '278', '0', '0', '55', '11', '1');
INSERT INTO `bed_info` VALUES ('1297', '1', '279', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1298', '2', '279', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1299', '3', '279', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1300', '4', '279', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1301', '1', '280', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1302', '2', '280', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1303', '3', '280', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1304', '4', '280', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1305', '1', '281', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1306', '2', '281', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1307', '3', '281', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1308', '4', '281', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1309', '1', '282', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1310', '2', '282', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1311', '3', '282', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1312', '4', '282', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1313', '5', '282', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1314', '6', '282', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1315', '1', '283', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1316', '2', '283', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1317', '3', '283', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1318', '4', '283', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1319', '5', '283', '0', '0', '56', '11', '1');
INSERT INTO `bed_info` VALUES ('1320', '6', '283', '0', '0', '56', '11', '1');

-- ----------------------------
-- Table structure for build_college_relation
-- ----------------------------
DROP TABLE IF EXISTS `build_college_relation`;
CREATE TABLE `build_college_relation` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `college_id` int(20) DEFAULT NULL,
  `build_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of build_college_relation
-- ----------------------------
INSERT INTO `build_college_relation` VALUES ('24', '41', '8');
INSERT INTO `build_college_relation` VALUES ('25', '45', '9');
INSERT INTO `build_college_relation` VALUES ('26', '52', '10');
INSERT INTO `build_college_relation` VALUES ('27', '46', '8');
INSERT INTO `build_college_relation` VALUES ('28', '46', '9');
INSERT INTO `build_college_relation` VALUES ('29', '48', '8');
INSERT INTO `build_college_relation` VALUES ('30', '48', '9');
INSERT INTO `build_college_relation` VALUES ('31', '47', '8');
INSERT INTO `build_college_relation` VALUES ('32', '47', '9');
INSERT INTO `build_college_relation` VALUES ('33', '41', '3');
INSERT INTO `build_college_relation` VALUES ('34', '50', '3');
INSERT INTO `build_college_relation` VALUES ('35', '50', '4');

-- ----------------------------
-- Table structure for build_info
-- ----------------------------
DROP TABLE IF EXISTS `build_info`;
CREATE TABLE `build_info` (
  `build_id` int(10) NOT NULL AUTO_INCREMENT,
  `build_name` varchar(255) DEFAULT NULL,
  `house_master` varchar(255) DEFAULT NULL COMMENT '宿舍管理员',
  `build_type` int(20) DEFAULT '0' COMMENT '1男生宿舍或2女生宿舍，0未定义',
  `house_tel` varchar(255) DEFAULT NULL COMMENT '楼栋联系电话',
  `belong_area_id` int(20) DEFAULT '0',
  PRIMARY KEY (`build_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of build_info
-- ----------------------------
INSERT INTO `build_info` VALUES ('1', '新一号楼', null, '2', null, '2');
INSERT INTO `build_info` VALUES ('2', '新二号楼', null, '2', null, '2');
INSERT INTO `build_info` VALUES ('3', '新三号楼', null, '1', null, '2');
INSERT INTO `build_info` VALUES ('4', '新四号楼', null, '2', null, '2');
INSERT INTO `build_info` VALUES ('5', '主一号楼', null, '1', null, '1');
INSERT INTO `build_info` VALUES ('6', '主二号楼', null, '1', null, '1');
INSERT INTO `build_info` VALUES ('7', '主三号楼', null, '1', null, '1');
INSERT INTO `build_info` VALUES ('8', '南一号楼', null, '2', null, '3');
INSERT INTO `build_info` VALUES ('9', '南二号楼', null, '2', null, '3');
INSERT INTO `build_info` VALUES ('10', '南三号楼', null, '2', null, '3');
INSERT INTO `build_info` VALUES ('11', '德馨楼', '刘志气', '1', '12847394739', '3');

-- ----------------------------
-- Table structure for classes_info
-- ----------------------------
DROP TABLE IF EXISTS `classes_info`;
CREATE TABLE `classes_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_code` int(2) unsigned zerofill NOT NULL COMMENT '班级代码',
  `counselor_id` int(11) DEFAULT NULL COMMENT '辅导员id',
  `counselor_name` varchar(255) DEFAULT NULL COMMENT '辅导员姓名',
  `class_name` varchar(255) DEFAULT NULL COMMENT '班级名称',
  `stu_numbers` int(50) DEFAULT NULL COMMENT '学生数量',
  `belong_spec_id` int(20) DEFAULT NULL COMMENT '所属专业id',
  `belong_spec_name` varchar(255) DEFAULT NULL,
  `belong_college_id` int(20) DEFAULT NULL COMMENT '所属学院id',
  `belong_college_name` varchar(255) DEFAULT NULL,
  `grade` varchar(255) DEFAULT NULL COMMENT '年级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=444 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of classes_info
-- ----------------------------
INSERT INTO `classes_info` VALUES ('1', '04', '12154', '王老师', '软件工程一班级', '100', '20', '软件工程', '50', '计算机科学与技术学院', '2017');
INSERT INTO `classes_info` VALUES ('343', '02', null, null, '哲学2班', null, '6', '哲学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('344', '03', null, null, '哲学3班', null, '6', '哲学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('345', '01', null, null, '马克思主义理论1班', null, '6', '马克思主义理论', null, null, '2021');
INSERT INTO `classes_info` VALUES ('346', '02', null, null, '马克思主义理论2班', null, '6', '马克思主义理论', null, null, '2021');
INSERT INTO `classes_info` VALUES ('347', '03', null, null, '马克思主义理论3班', null, '6', '马克思主义理论', null, null, '2021');
INSERT INTO `classes_info` VALUES ('348', '01', null, null, '诉讼法学1班', null, '6', '诉讼法学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('349', '02', null, null, '诉讼法学2班', null, '3', '诉讼法学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('350', '03', null, null, '诉讼法学3班', null, '6', '诉讼法学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('351', '01', null, null, '社会工作硕士1班', null, '5', '社会工作硕士', null, null, '2021');
INSERT INTO `classes_info` VALUES ('352', '02', null, null, '社会工作硕士2班', null, '5', '社会工作硕士', null, null, '2021');
INSERT INTO `classes_info` VALUES ('353', '03', null, null, '社会工作硕士3班', null, '5', '社会工作硕士', null, null, '2021');
INSERT INTO `classes_info` VALUES ('354', '01', null, null, '数学1班', null, '6', '数学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('355', '02', null, null, '数学2班', null, '6', '数学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('356', '03', null, null, '数学3班', null, '6', '数学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('357', '01', null, null, '力学1班', null, '7', '力学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('358', '02', null, null, '力学2班', null, '7', '力学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('359', '03', null, null, '力学3班', null, '7', '力学', null, null, '2021');
INSERT INTO `classes_info` VALUES ('360', '01', null, null, '光学工程1班', null, '8', '光学工程', '47', null, '2021');
INSERT INTO `classes_info` VALUES ('361', '02', null, null, '光学工程2班', null, '8', '光学工程', '47', null, '2021');
INSERT INTO `classes_info` VALUES ('362', '03', null, null, '光学工程3班', null, '8', '光学工程', '47', null, '2021');
INSERT INTO `classes_info` VALUES ('363', '01', null, null, '车辆工程1班', null, '10', '车辆工程', '41', null, '2021');
INSERT INTO `classes_info` VALUES ('364', '02', null, null, '车辆工程2班', null, '10', '车辆工程', '41', null, '2021');
INSERT INTO `classes_info` VALUES ('365', '03', null, null, '车辆工程3班', null, '10', '车辆工程', '41', null, '2021');
INSERT INTO `classes_info` VALUES ('366', '01', null, null, '材料科学与工程1班', null, '11', '材料科学与工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('367', '02', null, null, '材料科学与工程2班', null, '11', '材料科学与工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('368', '03', null, null, '材料科学与工程3班', null, '11', '材料科学与工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('369', '01', null, null, '材料工程1班', null, '12', '材料工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('370', '02', null, null, '材料工程2班', null, '12', '材料工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('371', '03', null, null, '材料工程3班', null, '12', '材料工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('372', '01', null, null, '冶金工程1班', null, '13', '冶金工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('373', '02', null, null, '冶金工程2班', null, '13', '冶金工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('374', '03', null, null, '冶金工程3班', null, '13', '冶金工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('375', '01', null, null, '电力电子与动力传动1班', null, '14', '电力电子与动力传动', null, null, '2021');
INSERT INTO `classes_info` VALUES ('376', '02', null, null, '电力电子与动力传动2班', null, '14', '电力电子与动力传动', null, null, '2021');
INSERT INTO `classes_info` VALUES ('377', '03', null, null, '电力电子与动力传动3班', null, '14', '电力电子与动力传动', null, null, '2021');
INSERT INTO `classes_info` VALUES ('378', '01', null, null, '控制科学与工程1班', null, '15', '控制科学与工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('379', '02', null, null, '控制科学与工程2班', null, '15', '控制科学与工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('380', '03', null, null, '控制科学与工程3班', null, '15', '控制科学与工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('381', '01', null, null, '电气工程1班', null, '16', '电气工程', '47', null, '2021');
INSERT INTO `classes_info` VALUES ('382', '02', null, null, '电气工程2班', null, '16', '电气工程', '47', null, '2021');
INSERT INTO `classes_info` VALUES ('383', '03', null, null, '电气工程3班', null, '16', '电气工程', '47', null, '2021');
INSERT INTO `classes_info` VALUES ('384', '01', null, null, '电子与通信工程1班', null, '17', '电子与通信工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('385', '02', null, null, '电子与通信工程2班', null, '17', '电子与通信工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('386', '03', null, null, '电子与通信工程3班', null, '17', '电子与通信工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('387', '01', null, null, '控制工程1班', null, '18', '控制工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('388', '02', null, null, '控制工程2班', null, '18', '控制工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('389', '03', null, null, '控制工程3班', null, '18', '控制工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('390', '01', null, null, '计算机科学与技术1班', null, '19', '计算机科学与技术', null, null, '2021');
INSERT INTO `classes_info` VALUES ('391', '02', null, null, '计算机科学与技术2班', null, '19', '计算机科学与技术', null, null, '2021');
INSERT INTO `classes_info` VALUES ('392', '03', null, null, '计算机科学与技术3班', null, '19', '计算机科学与技术', null, null, '2021');
INSERT INTO `classes_info` VALUES ('393', '01', null, null, '软件工程1班', null, '20', '软件工程', '50', null, '2021');
INSERT INTO `classes_info` VALUES ('394', '02', null, null, '软件工程2班', null, '20', '软件工程', '50', null, '2021');
INSERT INTO `classes_info` VALUES ('395', '03', null, null, '软件工程3班', null, '20', '软件工程', '50', null, '2021');
INSERT INTO `classes_info` VALUES ('396', '01', null, null, '交通运输工程1班', null, '21', '交通运输工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('397', '02', null, null, '交通运输工程2班', null, '21', '交通运输工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('398', '03', null, null, '交通运输工程3班', null, '21', '交通运输工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('399', '01', null, null, '物流工程1班', null, '22', '物流工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('400', '02', null, null, '物流工程2班', null, '22', '物流工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('401', '03', null, null, '物流工程3班', null, '22', '物流工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('402', '01', null, null, '环境科学与工程1班', null, '23', '环境科学与工程', '46', null, '2021');
INSERT INTO `classes_info` VALUES ('403', '02', null, null, '环境科学与工程2班', null, '23', '环境科学与工程', '46', null, '2021');
INSERT INTO `classes_info` VALUES ('404', '03', null, null, '环境科学与工程3班', null, '23', '环境科学与工程', '46', null, '2021');
INSERT INTO `classes_info` VALUES ('405', '01', null, null, '安全工程1班', null, '24', '安全工程', '46', null, '2021');
INSERT INTO `classes_info` VALUES ('406', '02', null, null, '安全工程2班', null, '24', '安全工程', '46', null, '2021');
INSERT INTO `classes_info` VALUES ('407', '03', null, null, '安全工程3班', null, '24', '安全工程', '46', null, '2021');
INSERT INTO `classes_info` VALUES ('408', '01', null, null, '理论经济学1班', null, '25', '理论经济学', '49', null, '2021');
INSERT INTO `classes_info` VALUES ('409', '02', null, null, '理论经济学2班', null, '25', '理论经济学', '49', null, '2021');
INSERT INTO `classes_info` VALUES ('410', '03', null, null, '理论经济学3班', null, '25', '理论经济学', '49', null, '2021');
INSERT INTO `classes_info` VALUES ('411', '01', null, null, '工业工程1班', null, '26', '工业工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('412', '02', null, null, '工业工程2班', null, '26', '工业工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('413', '03', null, null, '工业工程3班', null, '26', '工业工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('414', '01', null, null, '管理科学与工程1班', null, '27', '管理科学与工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('415', '02', null, null, '管理科学与工程2班', null, '27', '管理科学与工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('416', '03', null, null, '管理科学与工程3班', null, '27', '管理科学与工程', null, null, '2021');
INSERT INTO `classes_info` VALUES ('417', '01', null, null, '工商管理1班', null, '28', '工商管理', null, null, '2021');
INSERT INTO `classes_info` VALUES ('418', '02', null, null, '工商管理2班', null, '28', '工商管理', null, null, '2021');
INSERT INTO `classes_info` VALUES ('419', '03', null, null, '工商管理3班', null, '28', '工商管理', null, null, '2021');
INSERT INTO `classes_info` VALUES ('420', '01', null, null, '化学工程1班', null, '29', '化学工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('421', '02', null, null, '化学工程2班', null, '29', '化学工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('422', '03', null, null, '化学工程3班', null, '29', '化学工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('423', '01', null, null, '机械设计制造及其自动化1班', null, '30', '机械设计制造及其自动化', '41', null, '2021');
INSERT INTO `classes_info` VALUES ('424', '02', null, null, '机械设计制造及其自动化2班', null, '30', '机械设计制造及其自动化', '41', null, '2021');
INSERT INTO `classes_info` VALUES ('425', '03', null, null, '机械设计制造及其自动化3班', null, '30', '机械设计制造及其自动化', '41', null, '2021');
INSERT INTO `classes_info` VALUES ('426', '01', null, null, '机械电子工程 1班', null, '32', '机械电子工程 ', '41', null, '2021');
INSERT INTO `classes_info` VALUES ('427', '02', null, null, '机械电子工程 2班', null, '32', '机械电子工程 ', '41', null, '2021');
INSERT INTO `classes_info` VALUES ('428', '03', null, null, '机械电子工程 3班', null, '32', '机械电子工程 ', '41', null, '2021');
INSERT INTO `classes_info` VALUES ('429', '01', null, null, '国际经济与贸易1班', null, '33', '国际经济与贸易', '49', null, '2021');
INSERT INTO `classes_info` VALUES ('430', '02', null, null, '国际经济与贸易2班', null, '33', '国际经济与贸易', '49', null, '2021');
INSERT INTO `classes_info` VALUES ('431', '03', null, null, '国际经济与贸易3班', null, '33', '国际经济与贸易', '49', null, '2021');
INSERT INTO `classes_info` VALUES ('432', '01', null, null, '过程装备与控制工程1班', null, '34', '过程装备与控制工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('433', '02', null, null, '过程装备与控制工程2班', null, '34', '过程装备与控制工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('434', '03', null, null, '过程装备与控制工程3班', null, '34', '过程装备与控制工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('435', '01', null, null, '化学工程与工艺1班', null, '35', '化学工程与工艺', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('436', '02', null, null, '化学工程与工艺2班', null, '35', '化学工程与工艺', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('437', '03', null, null, '化学工程与工艺3班', null, '35', '化学工程与工艺', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('438', '01', null, null, '能源化学工程1班', null, '36', '能源化学工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('439', '02', null, null, '能源化学工程2班', null, '36', '能源化学工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('440', '03', null, null, '能源化学工程3班', null, '36', '能源化学工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('441', '01', null, null, '油气储运工程1班', null, '37', '油气储运工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('442', '02', null, null, '油气储运工程2班', null, '37', '油气储运工程', '42', null, '2021');
INSERT INTO `classes_info` VALUES ('443', '03', null, null, '油气储运工程3班', null, '37', '油气储运工程', '42', null, '2021');

-- ----------------------------
-- Table structure for college_info
-- ----------------------------
DROP TABLE IF EXISTS `college_info`;
CREATE TABLE `college_info` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `college_code` int(4) DEFAULT NULL COMMENT '学院代码',
  `college_name` varchar(50) NOT NULL COMMENT '学院名',
  `president` varchar(50) DEFAULT NULL COMMENT '院长',
  `vice_president` varchar(50) DEFAULT NULL COMMENT '副院长',
  `college_address` varchar(200) DEFAULT NULL COMMENT '学院地址\r\n            ',
  `college_tel` varchar(20) DEFAULT NULL COMMENT '学院电话',
  `zip_code` varchar(20) DEFAULT NULL COMMENT '学院邮政编码',
  `college_net_address` varchar(100) DEFAULT '' COMMENT '学院网址',
  `description` varchar(4000) DEFAULT NULL COMMENT '学院简介',
  `belong_area_id` int(20) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `college_code` (`college_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of college_info
-- ----------------------------
INSERT INTO `college_info` VALUES ('41', '1', '机械工程学院', '马立峰', '兰国生', '太原科技大学11号教学楼三层', '0351-6998115', '030024', 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学（原太原重型机械学院，2004年更名）1952年建校，是我国重大技术装备领域重要的人才培养和科技研发基地。机械工程学院作为学校历史最悠久、规模最大、教学科研实力最为雄厚的学院之一，为我国重大技术装备制造业，特别是为起重运输机械、工程机械、冶金机械和矿山机械行业培养了大批高级工程技术人才，专业特色鲜明，在重型机械行业享有盛誉。\r\n\r\n学院现有机械工程一级学科博士后科研流动站，机械工程一级学科博士学位授权点（涵盖机械设计及理论、机械制造及其自动化、机械电子工程、车辆工程和自设的“重型装备控制理论与工程”与“工业与工程”6个二级学科）。现有“重型机械装备省部共建协同创新中心”、“山西省冶金设备设计理论与技术重点实验室—省部共建国家重点实验室培育基地”、“国家级机械实验教学示范中心”、“山西省冶金装备院士工作站”、“山西省重科博远众创空间”等国家级和省部级科研教学平台23个，省级重点科技创新团队两个。机械工程学科2017年中国高校第四轮学科评估中排名B级，同年该学科被评为山西省“1331工程”机械工程优势特色学科。2019年，学院获“全国教育系统先进集体”称号。\r\n\r\n学院开设本科专业5个：“机械设计制造及其自动化”、“机械电子工程”、“车辆工程”、“工业设计”、“机器人工程”。开设本科专业方向11个，其中机械设计制造及其自动化专业（包括起重运输机械、工程机械、冶金机械、矿山机械、机械制造工艺与设备、流体传动与控制6个方向）为“国家管理的专业点”和“国家级特色专业建设点”，2019年获批国家一流专业，该专业从2011年始连续三次通过国际工程教育专业认证。机械电子工程专业2018年通过国际工程教育专业认证，2019年被评为山西省一流专业。\r\n\r\n学院现有教职员工159人，其中专任教师143人（教授35名，副教授56名，高级职称占比63.6%），工程院院士1名（学科首席负责人黄庆学教授2017年当选中国工程院院士），双聘院士2名，国家“千百万人才”1名，其他省部级人才20余名，博导13名，硕导78名。现有在校生3600余名，包括博士研究生90余名，硕士研究生450余名，本科生3070余名，毕业生就业率连续十多年居同行高校前列。\r\n\r\n学院在重型机械装备研发方面的科研成果显著，获得国家科技奖励5项，何梁何利基金奖励1项，中国专利优秀奖2项，省部级奖励50余项。近几年主持和参加国家高技术及重点研发计划项目10余项、国家自然科学基金45项；获国家发明专利200余项；发表学术论文2000余篇，其中SCI、EI收录550多篇。获批20余项国家、省部级大学生创新创业项目，获大学生创新创业国家级奖励百余项，其中包括全国第八届中国青少年科技创新奖、全球ABB电气集团中国第二届大学生创新大赛一等奖，徐工杯创新大赛一等奖等。“重大装备设计关键技术应用研究团队”在2014年荣获“小平科技创新团队”称号。\r\n\r\n学院科研设施完备，包含机器人实验室，机械原理零件实验室，机器人自动化生产线，液压实验台等专用实验室和 “人机工程综合设备与系统”、Polytec激光测试仪、岩石全应力三轴试验仪及配套设备等高精密检测仪器，设备总价值2亿余元人民币。\r\n\r\n学院产学研合作成果显著，2001年联合太重集团、太钢集团、柳工集团、徐工集团、山推集团等全国40多家机械制造骨干企业成立产学研董事会，先后与三一重工、中信重工、中联重工、徐工、柳工、洛阳轴承等全国两百多家企业建立了产学研战略合作（联盟）关系，并与江苏海安县合作建设海安锻压装备产业研究院、海安太原科大高端装备及轨道交通研究院。\r\n\r\n学院倡导国际化办学理念，先后与美国奥本大学、北卡罗来纳大学、澳大利亚卧龙岗大学、荷兰代尔夫特大学和日本冈山大学等国外高校建立了友好合作关系，联合开展国际合作培养项目及教师互访交流。\r\n\r\n学院秉承“明德 弘志 厚重 博远”的院训精神，坚守根植重工，务实开拓的文化底蕴，继往开来，聚焦立德树人根本任务，坚持特色发展之路，坚定不移地推进综合改革，推进优势学科攀升，推进内涵式发展，奋力书写新时代学院发展新篇章。', '3');
INSERT INTO `college_info` VALUES ('42', '2', '化学与生物工程学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学化学与生物工程学院，是太原科技大学下属的二级学院，其前身是山西省太原化学工业学校。学院创办于1958年，2003年并入太原科技大学。学院位于太原市晋祠路二段264号。化学工程与工艺、能源化学工程、过程装备与控制工程、生物工程、制药工程、油气储运工程6个本科专业和应用化工技术等13个专科专业；在校生近3000人；学院主要招收四年制本科生和硕士研究生。', '2');
INSERT INTO `college_info` VALUES ('45', '3', '材料科学与工程学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学材料科学与工程学院组建于2001年3月。学院本科设有材料类和机械类等14个专业(方向)，包括山西省品牌专业2个，校级品牌专业3个。研究生培养设有“材料加工工程”博士点，“材料科学与工程”一级学科硕士点，“材料学”、“材料物理与化学”和“钢铁冶金”等二级学科硕士点，还具有材料工程领域的工程硕士授予权。\r\n材料科学与工程学院下设3个系，14个专业教研室。现有教职员工152人，包括教师127人，实验人员21人，专职党政管理干部4人；教授28人，副教授32人，具有博士学位的教师20人，博士生导师6人，硕士生导师35人，享受政府津贴专家5人。学院在校本科生2217人，硕士研究生105人。\r\n学院现有2个省级工程中心、2个校级研究中心、3个实验中心，占地面积5000m2。拥有先进的现代材料加工设备及仪器、基于网络CAD/CAE/CAM系统、先进材料制备及分析测试设备与仪器、废水', '3');
INSERT INTO `college_info` VALUES ('46', '4', '环境与安全学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学环境与安全学院成立于2009年5月，现有教职工62人，其中教授6人，副教授20人，2人为山西省引进海外杰出人才“百人计划”特聘教授，1人为山西省学术技术带头人，1人为山西省青年学术带头人。具有博士学位的教师30人，在读博士5人。\r\n学院形成了本科生和研究生互补的人才培养体系。现有环境科学、环境工程、环保设备工程、安全工程4个本科专业和太原科技大学与美国奥本大学合作举办环境工程专业本科教育项目；环境科学与工程一级学科硕士学位和安全工程专业硕士学位授予权。环境科学专业2007年被山西省教育厅评为山西省品牌专业、2011年获批山西省特色专业建设项目。通过严格教学质量监控，人才培养质量得到明显提升,毕业生遍布全国的环保部门、安监、科研机构和高等院校，受到普遍好评。\r\n学院以“立足行业、服务地方”为目标，结合我国环境、安全领域热点和重点问题，致力于污染物排放及控制、污染降解和生态修复、城市大气污染及控制和矿山安全等方面的研究，包括：核废物处置与水土资源环境、区域环境污染防治、大气污染排放及控制、水处理技术、环境生物技术、环境生态工程和矿山瓦斯、粉尘防治和顶板围岩工程等，相关成果已广泛应用于工程实践。近年来，学院获得国家自然科学基金-重点项目、面上、青年基金项目和山西省科技重大专项、山西煤基重点科技攻关项目等资助,发表论文200余篇，获山西省科技进步二等奖1项，专著/编著15部，专利10项。\r\n红色思想立德，绿色情怀树人。我们将以山西省“1331工程”建设和我校“十三五规划”为指引，稳步推进学院学科科研平台建设，提升人才队伍和教育教学质量水平，促进重点实验室、博士点和博士后流动站建设，为地区输送高水平环保和安全类专业人才。', '1');
INSERT INTO `college_info` VALUES ('47', '5', '应用科学学院', '\r\n王希云', null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学应用科学学院是以数学、力学、物理三个学科为基础，历经了数力系、数理系、应用科学分院三个办学阶段，2004年随着学校更名为太原科技大学更名为应用科学学院。学院现有三个系：应用数学系、工程力学系和应用物理系。六个硕士点：力学一级硕士授权点（工程力学、流体力学、固体力学、一般力学与力学基础），应用数学和光学二级硕士授权点。五个专业：信息与计算科学、工程力学、光信息科学与技术、应用统计、数据计算。其中工程力学专业为山西省品牌专业，信息与计算科学专业为山西省优秀专业', '1');
INSERT INTO `college_info` VALUES ('48', '6', '电子信息工程学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学电子信息工程学院成立于2001年3月。学院现有自动化和电子信息两个系，设有自动化、电气工程及其自动化、电子信息工程、通信工程四个专业，其中最早的专业是自动化专业，成立于1978年。', '1');
INSERT INTO `college_info` VALUES ('49', '7', '经济与管理学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学经济与管理学院始创于1983年，前身为太原重型机械学院机械三系管理工程教研室，1983 年开设企业管理干部培训班，为山西省机械工业企业培养管理干部。1985年正式建系，1986年招收第一届工业经济专业本科生，多年来共为国家培养本、专科学生及各类管理干部5000多人。 [1]  经过二十多年的建设，经济与管理学院逐步发展壮大，“人轻知责善，业重晓勤勇”是经管精神的浓缩。在长期的教学过程中，在所有经管人不懈的努力下，学院形成了“经管理工交融、四框三线并重、内控外审结合”完善的教学体系和“责善勤勇协同”独特的经管人文环境。', '2');
INSERT INTO `college_info` VALUES ('50', '8', '计算机科学与技术学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学计算机科学与技术学院的前身为太原重型机械学院计算机科学与工程系，成立于1996年。2003年8月成立计算机科学与技术分院，2004年5月随着太原重型机械学院更名为太原科技大学，计算机科学与技术分院随之更名为计算机科学与技术学院。', '2');
INSERT INTO `college_info` VALUES ('51', '9', '法学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '\r\n太原科技大学法学院，成立于1997年9月，1998年开始招收本科生，2004年开始招收硕士研究生。\r\n历经近二十年的勤奋耕耘和持续发展，积聚了雄厚的实力和丰富的办学经验，成为一个具有广泛影响、享有良好声誉的人才培养基地。法学院现有专职教师35人，其中教授3人，副教授11人，博士5人（在读博士生8人），博士后1人，研究生导师13人，兼职导师20余人，在校学生952人，其中本科生882人，硕士研究生70人。法学院有着高质量的本科学士学位教育，并且是诉讼法学硕士学位授予单位。面向全国招收本科生、硕士研究生。本科教育，按通才培养理念，只设法学专业。在本科教学中，树立了牢固的“以学生为本”，“以学生为主体，教师为主导”的本科教学观念，形成了“突出基础、突出实践”的办学特色，立足山西，放眼全国，建立具有工科院校特色的法学专业，着重培养学生的创新精神和实践能力。\r\n', '2');
INSERT INTO `college_info` VALUES ('52', '10', '人文社科学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学人文社科学院始建于1952年，属于太原科技大学的分院之一。目前学院还发起设立并运作了山西省第一家民办社工机构山西久善社工服务中心。', '3');
INSERT INTO `college_info` VALUES ('53', '11', '外国语学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学外国语学院成立于1999年7月，现有教职员工95人，外国语学院现下设3个系（部），学院开设有英语、德语、日语、法语四个语种的课程，全体教师的科研能力和学术水平普遍较强，在各种学术刊物及学术会议上发表论文百余篇。学院学生在教师们的培养下表现优秀，多次获得国家级/省级奖项。', '1');
INSERT INTO `college_info` VALUES ('54', '12', '艺术学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学艺术学院工业设计专业成立于1996年，现设有工业设计、艺术设计、绘画3个本科专业，6个专业方向，在校学生500余人。', '1');
INSERT INTO `college_info` VALUES ('55', '13', '体育学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '体育学院拥有室外400米标准塑胶田径场1座。塑胶灯光网球场2块，篮球场26块，排球场5块，乒乓球台16张，羽毛球场地4块。体育馆一座，建筑面积7000多平方米，馆内设施先进，功能齐备，主场馆设座位1955个，配置有高级灯光、音响和彩色显示屏等设备，可用于平日教学、比赛、大型文艺演出等活动。体育馆内建羽毛球馆一个，占地380平米；一个跆拳道馆，面积380平米；一个体操房，面积180平米；一个乒乓球厅，面积500平米；多功能厅，可进行100人左右的会议、授课、卡拉ok、舞会活动。另外，体育馆还设有办公室，体质测试室，研究所，运动员休息室，图书馆等。', '1');
INSERT INTO `college_info` VALUES ('56', '14', '交通与物流学院', null, null, '太原科技大学11号教学楼三层', '0351-6998115', null, 'https://jx.tyust.edu.cn/xygk/jgsz/dzbgs.htm', '太原科技大学交通与物流学院坚持以“工”为主，以“管”为辅的专业培养体系，注重知识结构的设计与建设，教学中以学科基础与专业知识为载体，提高学生跨学科应用研究的实践能力和创新能力。属于太原科技大学的分院之一。', '1');

-- ----------------------------
-- Table structure for data_import_log
-- ----------------------------
DROP TABLE IF EXISTS `data_import_log`;
CREATE TABLE `data_import_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_name` varchar(255) DEFAULT NULL COMMENT '数据名称',
  `data_dept` varchar(255) DEFAULT '' COMMENT '数据上传部门',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `data_count` int(20) DEFAULT NULL COMMENT '上传总数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of data_import_log
-- ----------------------------
INSERT INTO `data_import_log` VALUES ('6', '新生录取数据', '招生办', '2021-02-07 13:32:59', '3');
INSERT INTO `data_import_log` VALUES ('7', '学院信息', '系统管理员', '2021-02-20 22:14:59', '2');
INSERT INTO `data_import_log` VALUES ('8', '专业信息', 'admin', '2021-02-21 09:58:26', '1');
INSERT INTO `data_import_log` VALUES ('9', '专业信息', 'admin', '2021-02-21 10:02:30', '1');
INSERT INTO `data_import_log` VALUES ('10', '专业信息', 'admin', '2021-02-21 10:46:08', '1');
INSERT INTO `data_import_log` VALUES ('11', '学院信息', '系统管理员', '2021-02-21 11:39:56', '1');
INSERT INTO `data_import_log` VALUES ('12', '学院信息', '系统管理员', '2021-02-21 11:42:26', '1');
INSERT INTO `data_import_log` VALUES ('13', '楼栋信息', 'admin', '2021-02-21 13:43:20', '1');
INSERT INTO `data_import_log` VALUES ('14', '宿舍信息', 'admin', '2021-02-21 13:50:37', '1');
INSERT INTO `data_import_log` VALUES ('15', '学院信息', '系统管理员', '2021-03-09 00:08:02', '6');
INSERT INTO `data_import_log` VALUES ('16', '学院信息', '系统管理员', '2021-03-09 00:08:38', '6');
INSERT INTO `data_import_log` VALUES ('17', '学院信息', '系统管理员', '2021-03-09 00:08:39', '6');

-- ----------------------------
-- Table structure for dorm_info
-- ----------------------------
DROP TABLE IF EXISTS `dorm_info`;
CREATE TABLE `dorm_info` (
  `dorm_id` int(11) NOT NULL AUTO_INCREMENT,
  `dorm_no` int(20) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `already_lived` int(20) DEFAULT '0' COMMENT '已经居住人数',
  `avaliable` varchar(5) DEFAULT '0' COMMENT '0可用，1不可用',
  `dorm_capacity` int(20) DEFAULT NULL COMMENT '宿舍可容纳人数',
  `dorm_fee` double(255,0) DEFAULT NULL COMMENT '宿舍费用',
  `belong_floor_id` int(50) NOT NULL,
  `dorm_type_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`dorm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=291 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dorm_info
-- ----------------------------
INSERT INTO `dorm_info` VALUES ('9', '101', null, '2', '0', '4', '1000', '1', null);
INSERT INTO `dorm_info` VALUES ('10', '102', null, '4', '0', '4', '1000', '1', null);
INSERT INTO `dorm_info` VALUES ('11', '103', null, '0', '0', '4', '1000', '1', null);
INSERT INTO `dorm_info` VALUES ('12', '104', null, '0', '0', '6', '800', '1', null);
INSERT INTO `dorm_info` VALUES ('13', '105', null, '0', '0', '6', '800', '1', null);
INSERT INTO `dorm_info` VALUES ('14', '201', null, '0', '0', '4', '1000', '3', null);
INSERT INTO `dorm_info` VALUES ('15', '202', null, '0', '0', '4', '1000', '3', null);
INSERT INTO `dorm_info` VALUES ('16', '203', null, '0', '0', '4', '1000', '3', null);
INSERT INTO `dorm_info` VALUES ('17', '204', null, '0', '0', '6', '800', '3', null);
INSERT INTO `dorm_info` VALUES ('18', '205', null, '0', '0', '6', '800', '3', null);
INSERT INTO `dorm_info` VALUES ('19', '301', null, '0', '0', '4', '1000', '4', null);
INSERT INTO `dorm_info` VALUES ('20', '302', null, '0', '0', '4', '1000', '4', null);
INSERT INTO `dorm_info` VALUES ('21', '303', null, '0', '0', '4', '1000', '4', null);
INSERT INTO `dorm_info` VALUES ('22', '304', null, '0', '0', '6', '800', '4', null);
INSERT INTO `dorm_info` VALUES ('23', '305', null, '0', '0', '6', '800', '4', null);
INSERT INTO `dorm_info` VALUES ('24', '401', null, '0', '0', '4', '1000', '5', null);
INSERT INTO `dorm_info` VALUES ('25', '402', null, '0', '0', '4', '1000', '5', null);
INSERT INTO `dorm_info` VALUES ('26', '403', null, '0', '0', '4', '1000', '5', null);
INSERT INTO `dorm_info` VALUES ('27', '404', null, '0', '0', '6', '800', '5', null);
INSERT INTO `dorm_info` VALUES ('28', '405', null, '0', '0', '6', '800', '5', null);
INSERT INTO `dorm_info` VALUES ('29', '501', null, '0', '0', '4', '1000', '6', null);
INSERT INTO `dorm_info` VALUES ('30', '502', null, '0', '0', '4', '1000', '6', null);
INSERT INTO `dorm_info` VALUES ('31', '503', null, '0', '0', '4', '1000', '6', null);
INSERT INTO `dorm_info` VALUES ('32', '504', null, '0', '0', '6', '800', '6', null);
INSERT INTO `dorm_info` VALUES ('33', '505', null, '0', '0', '6', '800', '6', null);
INSERT INTO `dorm_info` VALUES ('34', '101', null, '0', '0', '4', '1000', '7', null);
INSERT INTO `dorm_info` VALUES ('35', '102', null, '0', '0', '4', '1000', '7', null);
INSERT INTO `dorm_info` VALUES ('36', '103', null, '0', '0', '4', '1000', '7', null);
INSERT INTO `dorm_info` VALUES ('37', '104', null, '0', '0', '6', '800', '7', null);
INSERT INTO `dorm_info` VALUES ('38', '105', null, '0', '0', '6', '800', '7', null);
INSERT INTO `dorm_info` VALUES ('39', '201', null, '0', '0', '4', '1000', '8', null);
INSERT INTO `dorm_info` VALUES ('40', '202', null, '0', '0', '4', '1000', '8', null);
INSERT INTO `dorm_info` VALUES ('41', '203', null, '0', '0', '4', '1000', '8', null);
INSERT INTO `dorm_info` VALUES ('42', '204', null, '0', '0', '6', '800', '8', null);
INSERT INTO `dorm_info` VALUES ('43', '205', null, '0', '0', '6', '800', '8', null);
INSERT INTO `dorm_info` VALUES ('44', '301', null, '0', '0', '4', '1000', '9', null);
INSERT INTO `dorm_info` VALUES ('45', '302', null, '0', '0', '4', '1000', '9', null);
INSERT INTO `dorm_info` VALUES ('46', '303', null, '0', '0', '4', '1000', '9', null);
INSERT INTO `dorm_info` VALUES ('47', '304', null, '0', '0', '6', '800', '9', null);
INSERT INTO `dorm_info` VALUES ('48', '305', null, '0', '0', '6', '800', '9', null);
INSERT INTO `dorm_info` VALUES ('49', '401', null, '0', '0', '4', '1000', '10', null);
INSERT INTO `dorm_info` VALUES ('50', '402', null, '0', '0', '4', '1000', '10', null);
INSERT INTO `dorm_info` VALUES ('51', '403', null, '0', '0', '4', '1000', '10', null);
INSERT INTO `dorm_info` VALUES ('52', '404', null, '0', '0', '6', '800', '10', null);
INSERT INTO `dorm_info` VALUES ('53', '405', null, '0', '0', '6', '800', '10', null);
INSERT INTO `dorm_info` VALUES ('54', '501', null, '0', '0', '4', '1000', '11', null);
INSERT INTO `dorm_info` VALUES ('55', '502', null, '0', '0', '4', '1000', '11', null);
INSERT INTO `dorm_info` VALUES ('56', '503', null, '0', '0', '4', '1000', '11', null);
INSERT INTO `dorm_info` VALUES ('57', '504', null, '0', '0', '6', '800', '11', null);
INSERT INTO `dorm_info` VALUES ('58', '505', null, '0', '0', '6', '800', '11', null);
INSERT INTO `dorm_info` VALUES ('59', '101', null, '0', '0', '4', '1000', '12', null);
INSERT INTO `dorm_info` VALUES ('60', '102', null, '0', '0', '4', '1000', '12', null);
INSERT INTO `dorm_info` VALUES ('61', '103', null, '0', '0', '4', '1000', '12', null);
INSERT INTO `dorm_info` VALUES ('62', '104', null, '0', '0', '6', '800', '12', null);
INSERT INTO `dorm_info` VALUES ('63', '105', null, '0', '0', '6', '800', '12', null);
INSERT INTO `dorm_info` VALUES ('64', '201', null, '0', '0', '4', '1000', '13', null);
INSERT INTO `dorm_info` VALUES ('65', '202', null, '0', '0', '4', '1000', '13', null);
INSERT INTO `dorm_info` VALUES ('66', '203', null, '0', '0', '4', '1000', '13', null);
INSERT INTO `dorm_info` VALUES ('67', '204', null, '0', '0', '6', '800', '13', null);
INSERT INTO `dorm_info` VALUES ('68', '205', null, '0', '0', '6', '800', '13', null);
INSERT INTO `dorm_info` VALUES ('69', '301', null, '0', '0', '4', '1000', '14', null);
INSERT INTO `dorm_info` VALUES ('70', '302', null, '0', '0', '4', '1000', '14', null);
INSERT INTO `dorm_info` VALUES ('71', '303', null, '0', '0', '4', '1000', '14', null);
INSERT INTO `dorm_info` VALUES ('72', '304', null, '0', '0', '6', '800', '14', null);
INSERT INTO `dorm_info` VALUES ('73', '305', null, '0', '0', '6', '800', '14', null);
INSERT INTO `dorm_info` VALUES ('74', '401', null, '0', '0', '4', '1000', '15', null);
INSERT INTO `dorm_info` VALUES ('75', '402', null, '0', '0', '4', '1000', '15', null);
INSERT INTO `dorm_info` VALUES ('76', '403', null, '0', '0', '4', '1000', '15', null);
INSERT INTO `dorm_info` VALUES ('77', '404', null, '0', '0', '6', '800', '15', null);
INSERT INTO `dorm_info` VALUES ('78', '405', null, '0', '0', '6', '800', '15', null);
INSERT INTO `dorm_info` VALUES ('79', '501', null, '0', '0', '4', '1000', '16', null);
INSERT INTO `dorm_info` VALUES ('80', '502', null, '0', '0', '4', '1000', '16', null);
INSERT INTO `dorm_info` VALUES ('81', '503', null, '0', '0', '4', '1000', '16', null);
INSERT INTO `dorm_info` VALUES ('82', '504', null, '0', '0', '6', '800', '16', null);
INSERT INTO `dorm_info` VALUES ('83', '505', null, '0', '0', '6', '800', '16', null);
INSERT INTO `dorm_info` VALUES ('84', '101', null, '0', '0', '4', '1000', '17', null);
INSERT INTO `dorm_info` VALUES ('85', '102', null, '0', '0', '4', '1000', '17', null);
INSERT INTO `dorm_info` VALUES ('86', '103', null, '0', '0', '4', '1000', '17', null);
INSERT INTO `dorm_info` VALUES ('87', '104', null, '0', '0', '6', '800', '17', null);
INSERT INTO `dorm_info` VALUES ('88', '105', null, '0', '0', '6', '800', '17', null);
INSERT INTO `dorm_info` VALUES ('89', '201', null, '0', '0', '4', '1000', '18', null);
INSERT INTO `dorm_info` VALUES ('90', '202', null, '0', '0', '4', '1000', '18', null);
INSERT INTO `dorm_info` VALUES ('91', '203', null, '0', '0', '4', '1000', '18', null);
INSERT INTO `dorm_info` VALUES ('92', '204', null, '0', '0', '6', '800', '18', null);
INSERT INTO `dorm_info` VALUES ('93', '205', null, '0', '0', '6', '800', '18', null);
INSERT INTO `dorm_info` VALUES ('94', '301', null, '0', '0', '4', '1000', '19', null);
INSERT INTO `dorm_info` VALUES ('95', '302', null, '0', '0', '4', '1000', '19', null);
INSERT INTO `dorm_info` VALUES ('96', '303', null, '0', '0', '4', '1000', '19', null);
INSERT INTO `dorm_info` VALUES ('97', '304', null, '0', '0', '6', '800', '19', null);
INSERT INTO `dorm_info` VALUES ('98', '305', null, '0', '0', '6', '800', '19', null);
INSERT INTO `dorm_info` VALUES ('99', '401', null, '0', '0', '4', '1000', '20', null);
INSERT INTO `dorm_info` VALUES ('100', '402', null, '0', '0', '4', '1000', '20', null);
INSERT INTO `dorm_info` VALUES ('101', '403', null, '0', '0', '4', '1000', '20', null);
INSERT INTO `dorm_info` VALUES ('102', '404', null, '0', '0', '6', '800', '20', null);
INSERT INTO `dorm_info` VALUES ('103', '405', null, '0', '0', '6', '800', '20', null);
INSERT INTO `dorm_info` VALUES ('104', '501', null, '0', '0', '4', '1000', '21', null);
INSERT INTO `dorm_info` VALUES ('105', '502', null, '0', '0', '4', '1000', '21', null);
INSERT INTO `dorm_info` VALUES ('106', '503', null, '0', '0', '4', '1000', '21', null);
INSERT INTO `dorm_info` VALUES ('107', '504', null, '0', '0', '6', '800', '21', null);
INSERT INTO `dorm_info` VALUES ('108', '505', null, '0', '0', '6', '800', '21', null);
INSERT INTO `dorm_info` VALUES ('109', '101', null, '0', '0', '4', '1000', '22', null);
INSERT INTO `dorm_info` VALUES ('110', '102', null, '0', '0', '4', '1000', '22', null);
INSERT INTO `dorm_info` VALUES ('111', '103', null, '0', '0', '4', '1000', '22', null);
INSERT INTO `dorm_info` VALUES ('112', '104', null, '0', '0', '6', '800', '22', null);
INSERT INTO `dorm_info` VALUES ('113', '105', null, '0', '0', '6', '800', '22', null);
INSERT INTO `dorm_info` VALUES ('114', '201', null, '0', '0', '4', '1000', '23', null);
INSERT INTO `dorm_info` VALUES ('115', '202', null, '0', '0', '4', '1000', '23', null);
INSERT INTO `dorm_info` VALUES ('116', '203', null, '0', '0', '4', '1000', '23', null);
INSERT INTO `dorm_info` VALUES ('117', '204', null, '0', '0', '6', '800', '23', null);
INSERT INTO `dorm_info` VALUES ('118', '205', null, '0', '0', '6', '800', '23', null);
INSERT INTO `dorm_info` VALUES ('119', '301', null, '0', '0', '4', '1000', '24', null);
INSERT INTO `dorm_info` VALUES ('120', '302', null, '0', '0', '4', '1000', '24', null);
INSERT INTO `dorm_info` VALUES ('121', '303', null, '0', '0', '4', '1000', '24', null);
INSERT INTO `dorm_info` VALUES ('122', '304', null, '0', '0', '6', '800', '24', null);
INSERT INTO `dorm_info` VALUES ('123', '305', null, '0', '0', '6', '800', '24', null);
INSERT INTO `dorm_info` VALUES ('124', '401', null, '0', '0', '4', '1000', '25', null);
INSERT INTO `dorm_info` VALUES ('125', '402', null, '0', '0', '4', '1000', '25', null);
INSERT INTO `dorm_info` VALUES ('126', '403', null, '0', '0', '4', '1000', '25', null);
INSERT INTO `dorm_info` VALUES ('127', '404', null, '0', '0', '6', '800', '25', null);
INSERT INTO `dorm_info` VALUES ('128', '405', null, '0', '0', '6', '800', '25', null);
INSERT INTO `dorm_info` VALUES ('129', '501', null, '0', '0', '4', '1000', '26', null);
INSERT INTO `dorm_info` VALUES ('130', '502', null, '0', '0', '4', '1000', '26', null);
INSERT INTO `dorm_info` VALUES ('131', '503', null, '0', '0', '4', '1000', '26', null);
INSERT INTO `dorm_info` VALUES ('132', '504', null, '0', '0', '6', '800', '26', null);
INSERT INTO `dorm_info` VALUES ('133', '505', null, '0', '0', '6', '800', '26', null);
INSERT INTO `dorm_info` VALUES ('134', '101', null, '0', '0', '4', '1000', '27', null);
INSERT INTO `dorm_info` VALUES ('135', '102', null, '0', '0', '4', '1000', '27', null);
INSERT INTO `dorm_info` VALUES ('136', '103', null, '0', '0', '4', '1000', '27', null);
INSERT INTO `dorm_info` VALUES ('137', '104', null, '0', '0', '6', '800', '27', null);
INSERT INTO `dorm_info` VALUES ('138', '105', null, '0', '0', '6', '800', '27', null);
INSERT INTO `dorm_info` VALUES ('139', '201', null, '0', '0', '4', '1000', '28', null);
INSERT INTO `dorm_info` VALUES ('140', '202', null, '0', '0', '4', '1000', '28', null);
INSERT INTO `dorm_info` VALUES ('141', '203', null, '0', '0', '4', '1000', '28', null);
INSERT INTO `dorm_info` VALUES ('142', '204', null, '0', '0', '6', '800', '28', null);
INSERT INTO `dorm_info` VALUES ('143', '205', null, '0', '0', '6', '800', '28', null);
INSERT INTO `dorm_info` VALUES ('144', '301', null, '0', '0', '4', '1000', '29', null);
INSERT INTO `dorm_info` VALUES ('145', '302', null, '0', '0', '4', '1000', '29', null);
INSERT INTO `dorm_info` VALUES ('146', '303', null, '0', '0', '4', '1000', '29', null);
INSERT INTO `dorm_info` VALUES ('147', '304', null, '0', '0', '6', '800', '29', null);
INSERT INTO `dorm_info` VALUES ('148', '305', null, '0', '0', '6', '800', '29', null);
INSERT INTO `dorm_info` VALUES ('149', '401', null, '0', '0', '4', '1000', '30', null);
INSERT INTO `dorm_info` VALUES ('150', '402', null, '0', '0', '4', '1000', '30', null);
INSERT INTO `dorm_info` VALUES ('151', '403', null, '0', '0', '4', '1000', '30', null);
INSERT INTO `dorm_info` VALUES ('152', '404', null, '0', '0', '6', '800', '30', null);
INSERT INTO `dorm_info` VALUES ('153', '405', null, '0', '0', '6', '800', '30', null);
INSERT INTO `dorm_info` VALUES ('154', '501', null, '0', '0', '4', '1000', '31', null);
INSERT INTO `dorm_info` VALUES ('155', '502', null, '0', '0', '4', '1000', '31', null);
INSERT INTO `dorm_info` VALUES ('156', '503', null, '0', '0', '4', '1000', '31', null);
INSERT INTO `dorm_info` VALUES ('157', '504', null, '0', '0', '6', '800', '31', null);
INSERT INTO `dorm_info` VALUES ('158', '505', null, '0', '0', '6', '800', '31', null);
INSERT INTO `dorm_info` VALUES ('159', '101', null, '0', '0', '4', '1000', '32', null);
INSERT INTO `dorm_info` VALUES ('160', '102', null, '0', '0', '4', '1000', '32', null);
INSERT INTO `dorm_info` VALUES ('161', '103', null, '0', '0', '4', '1000', '32', null);
INSERT INTO `dorm_info` VALUES ('162', '104', null, '0', '0', '6', '800', '32', null);
INSERT INTO `dorm_info` VALUES ('163', '105', null, '0', '0', '6', '800', '32', null);
INSERT INTO `dorm_info` VALUES ('164', '201', null, '0', '0', '4', '1000', '33', null);
INSERT INTO `dorm_info` VALUES ('165', '202', null, '0', '0', '4', '1000', '33', null);
INSERT INTO `dorm_info` VALUES ('166', '203', null, '0', '0', '4', '1000', '33', null);
INSERT INTO `dorm_info` VALUES ('167', '204', null, '0', '0', '6', '800', '33', null);
INSERT INTO `dorm_info` VALUES ('168', '205', null, '0', '0', '6', '800', '33', null);
INSERT INTO `dorm_info` VALUES ('169', '301', null, '0', '0', '4', '1000', '34', null);
INSERT INTO `dorm_info` VALUES ('170', '302', null, '0', '0', '4', '1000', '34', null);
INSERT INTO `dorm_info` VALUES ('171', '303', null, '0', '0', '4', '1000', '34', null);
INSERT INTO `dorm_info` VALUES ('172', '304', null, '0', '0', '6', '800', '34', null);
INSERT INTO `dorm_info` VALUES ('173', '305', null, '0', '0', '6', '800', '34', null);
INSERT INTO `dorm_info` VALUES ('174', '401', null, '0', '0', '4', '1000', '35', null);
INSERT INTO `dorm_info` VALUES ('175', '402', null, '0', '0', '4', '1000', '35', null);
INSERT INTO `dorm_info` VALUES ('176', '403', null, '0', '0', '4', '1000', '35', null);
INSERT INTO `dorm_info` VALUES ('177', '404', null, '0', '0', '6', '800', '35', null);
INSERT INTO `dorm_info` VALUES ('178', '405', null, '0', '0', '6', '800', '35', null);
INSERT INTO `dorm_info` VALUES ('179', '501', null, '0', '0', '4', '1000', '36', null);
INSERT INTO `dorm_info` VALUES ('180', '502', null, '0', '0', '4', '1000', '36', null);
INSERT INTO `dorm_info` VALUES ('181', '503', null, '0', '0', '4', '1000', '36', null);
INSERT INTO `dorm_info` VALUES ('182', '504', null, '0', '0', '6', '800', '36', null);
INSERT INTO `dorm_info` VALUES ('183', '505', null, '0', '0', '6', '800', '36', null);
INSERT INTO `dorm_info` VALUES ('184', '101', null, '0', '0', '4', '1000', '37', null);
INSERT INTO `dorm_info` VALUES ('185', '102', null, '0', '0', '4', '1000', '37', null);
INSERT INTO `dorm_info` VALUES ('186', '103', null, '0', '0', '4', '1000', '37', null);
INSERT INTO `dorm_info` VALUES ('187', '104', null, '0', '0', '6', '800', '37', null);
INSERT INTO `dorm_info` VALUES ('188', '105', null, '0', '0', '6', '800', '37', null);
INSERT INTO `dorm_info` VALUES ('189', '201', null, '0', '0', '4', '1000', '38', null);
INSERT INTO `dorm_info` VALUES ('190', '202', null, '0', '0', '4', '1000', '38', null);
INSERT INTO `dorm_info` VALUES ('191', '203', null, '0', '0', '4', '1000', '38', null);
INSERT INTO `dorm_info` VALUES ('192', '204', null, '0', '0', '6', '800', '38', null);
INSERT INTO `dorm_info` VALUES ('193', '205', null, '0', '0', '6', '800', '38', null);
INSERT INTO `dorm_info` VALUES ('194', '301', null, '0', '0', '4', '1000', '39', null);
INSERT INTO `dorm_info` VALUES ('195', '302', null, '0', '0', '4', '1000', '39', null);
INSERT INTO `dorm_info` VALUES ('196', '303', null, '0', '0', '4', '1000', '39', null);
INSERT INTO `dorm_info` VALUES ('197', '304', null, '0', '0', '6', '800', '39', null);
INSERT INTO `dorm_info` VALUES ('198', '305', null, '0', '0', '6', '800', '39', null);
INSERT INTO `dorm_info` VALUES ('199', '401', null, '0', '0', '4', '1000', '40', null);
INSERT INTO `dorm_info` VALUES ('200', '402', null, '0', '0', '4', '1000', '40', null);
INSERT INTO `dorm_info` VALUES ('201', '403', null, '0', '0', '4', '1000', '40', null);
INSERT INTO `dorm_info` VALUES ('202', '404', null, '0', '0', '6', '800', '40', null);
INSERT INTO `dorm_info` VALUES ('203', '405', null, '0', '0', '6', '800', '40', null);
INSERT INTO `dorm_info` VALUES ('204', '501', null, '0', '0', '4', '1000', '41', null);
INSERT INTO `dorm_info` VALUES ('205', '502', null, '0', '0', '4', '1000', '41', null);
INSERT INTO `dorm_info` VALUES ('206', '503', null, '0', '0', '4', '1000', '41', null);
INSERT INTO `dorm_info` VALUES ('207', '504', null, '0', '0', '6', '800', '41', null);
INSERT INTO `dorm_info` VALUES ('208', '505', null, '0', '0', '6', '800', '41', null);
INSERT INTO `dorm_info` VALUES ('209', '101', null, '0', '0', '4', '1000', '42', null);
INSERT INTO `dorm_info` VALUES ('210', '102', null, '0', '0', '4', '1000', '42', null);
INSERT INTO `dorm_info` VALUES ('211', '103', null, '0', '0', '4', '1000', '42', null);
INSERT INTO `dorm_info` VALUES ('212', '104', null, '0', '0', '6', '800', '42', null);
INSERT INTO `dorm_info` VALUES ('213', '105', null, '0', '0', '6', '800', '42', null);
INSERT INTO `dorm_info` VALUES ('214', '201', null, '0', '0', '4', '1000', '43', null);
INSERT INTO `dorm_info` VALUES ('215', '202', null, '0', '0', '4', '1000', '43', null);
INSERT INTO `dorm_info` VALUES ('216', '203', null, '0', '0', '4', '1000', '43', null);
INSERT INTO `dorm_info` VALUES ('217', '204', null, '0', '0', '6', '800', '43', null);
INSERT INTO `dorm_info` VALUES ('218', '205', null, '0', '0', '6', '800', '43', null);
INSERT INTO `dorm_info` VALUES ('219', '301', null, '0', '0', '4', '1000', '44', null);
INSERT INTO `dorm_info` VALUES ('220', '302', null, '0', '0', '4', '1000', '44', null);
INSERT INTO `dorm_info` VALUES ('221', '303', null, '0', '0', '4', '1000', '44', null);
INSERT INTO `dorm_info` VALUES ('222', '304', null, '0', '0', '6', '800', '44', null);
INSERT INTO `dorm_info` VALUES ('223', '305', null, '0', '0', '6', '800', '44', null);
INSERT INTO `dorm_info` VALUES ('224', '401', null, '0', '0', '4', '1000', '45', null);
INSERT INTO `dorm_info` VALUES ('225', '402', null, '0', '0', '4', '1000', '45', null);
INSERT INTO `dorm_info` VALUES ('226', '403', null, '0', '0', '4', '1000', '45', null);
INSERT INTO `dorm_info` VALUES ('227', '404', null, '0', '0', '6', '800', '45', null);
INSERT INTO `dorm_info` VALUES ('228', '405', null, '0', '0', '6', '800', '45', null);
INSERT INTO `dorm_info` VALUES ('229', '501', null, '0', '0', '4', '1000', '46', null);
INSERT INTO `dorm_info` VALUES ('230', '502', null, '0', '0', '4', '1000', '46', null);
INSERT INTO `dorm_info` VALUES ('231', '503', null, '0', '0', '4', '1000', '46', null);
INSERT INTO `dorm_info` VALUES ('232', '504', null, '0', '0', '6', '800', '46', null);
INSERT INTO `dorm_info` VALUES ('233', '505', null, '0', '0', '6', '800', '46', null);
INSERT INTO `dorm_info` VALUES ('234', '101', null, '0', '0', '4', '1000', '47', null);
INSERT INTO `dorm_info` VALUES ('235', '102', null, '0', '0', '4', '1000', '47', null);
INSERT INTO `dorm_info` VALUES ('236', '103', null, '0', '0', '4', '1000', '47', null);
INSERT INTO `dorm_info` VALUES ('237', '104', null, '0', '0', '6', '800', '47', null);
INSERT INTO `dorm_info` VALUES ('238', '105', null, '0', '0', '6', '800', '47', null);
INSERT INTO `dorm_info` VALUES ('239', '201', null, '0', '0', '4', '1000', '48', null);
INSERT INTO `dorm_info` VALUES ('240', '202', null, '0', '0', '4', '1000', '48', null);
INSERT INTO `dorm_info` VALUES ('241', '203', null, '0', '0', '4', '1000', '48', null);
INSERT INTO `dorm_info` VALUES ('242', '204', null, '0', '0', '6', '800', '48', null);
INSERT INTO `dorm_info` VALUES ('243', '205', null, '0', '0', '6', '800', '48', null);
INSERT INTO `dorm_info` VALUES ('244', '301', null, '0', '0', '4', '1000', '49', null);
INSERT INTO `dorm_info` VALUES ('245', '302', null, '0', '0', '4', '1000', '49', null);
INSERT INTO `dorm_info` VALUES ('246', '303', null, '0', '0', '4', '1000', '49', null);
INSERT INTO `dorm_info` VALUES ('247', '304', null, '0', '0', '6', '800', '49', null);
INSERT INTO `dorm_info` VALUES ('248', '305', null, '0', '0', '6', '800', '49', null);
INSERT INTO `dorm_info` VALUES ('249', '401', null, '0', '0', '4', '1000', '50', null);
INSERT INTO `dorm_info` VALUES ('250', '402', null, '0', '0', '4', '1000', '50', null);
INSERT INTO `dorm_info` VALUES ('251', '403', null, '0', '0', '4', '1000', '50', null);
INSERT INTO `dorm_info` VALUES ('252', '404', null, '0', '0', '6', '800', '50', null);
INSERT INTO `dorm_info` VALUES ('253', '405', null, '0', '0', '6', '800', '50', null);
INSERT INTO `dorm_info` VALUES ('254', '501', null, '0', '0', '4', '1000', '51', null);
INSERT INTO `dorm_info` VALUES ('255', '502', null, '0', '0', '4', '1000', '51', null);
INSERT INTO `dorm_info` VALUES ('256', '503', null, '0', '0', '4', '1000', '51', null);
INSERT INTO `dorm_info` VALUES ('257', '504', null, '0', '0', '6', '800', '51', null);
INSERT INTO `dorm_info` VALUES ('258', '505', null, '0', '0', '6', '800', '51', null);
INSERT INTO `dorm_info` VALUES ('259', '101', null, '0', '0', '4', '1000', '52', null);
INSERT INTO `dorm_info` VALUES ('260', '102', null, '0', '0', '4', '1000', '52', null);
INSERT INTO `dorm_info` VALUES ('261', '103', null, '0', '0', '4', '1000', '52', null);
INSERT INTO `dorm_info` VALUES ('262', '104', null, '0', '0', '6', '800', '52', null);
INSERT INTO `dorm_info` VALUES ('263', '105', null, '0', '0', '6', '800', '52', null);
INSERT INTO `dorm_info` VALUES ('264', '201', null, '0', '0', '4', '1000', '53', null);
INSERT INTO `dorm_info` VALUES ('265', '202', null, '0', '0', '4', '1000', '53', null);
INSERT INTO `dorm_info` VALUES ('266', '203', null, '0', '0', '4', '1000', '53', null);
INSERT INTO `dorm_info` VALUES ('267', '204', null, '0', '0', '6', '800', '53', null);
INSERT INTO `dorm_info` VALUES ('268', '205', null, '0', '0', '6', '800', '53', null);
INSERT INTO `dorm_info` VALUES ('269', '301', null, '0', '0', '4', '1000', '54', null);
INSERT INTO `dorm_info` VALUES ('270', '302', null, '0', '0', '4', '1000', '54', null);
INSERT INTO `dorm_info` VALUES ('271', '303', null, '0', '0', '4', '1000', '54', null);
INSERT INTO `dorm_info` VALUES ('272', '304', null, '0', '0', '6', '800', '54', null);
INSERT INTO `dorm_info` VALUES ('273', '305', null, '0', '0', '6', '800', '54', null);
INSERT INTO `dorm_info` VALUES ('274', '401', null, '0', '0', '4', '1000', '55', null);
INSERT INTO `dorm_info` VALUES ('275', '402', null, '0', '0', '4', '1000', '55', null);
INSERT INTO `dorm_info` VALUES ('276', '403', null, '0', '0', '4', '1000', '55', null);
INSERT INTO `dorm_info` VALUES ('277', '404', null, '0', '0', '6', '800', '55', null);
INSERT INTO `dorm_info` VALUES ('278', '405', null, '0', '0', '6', '800', '55', null);
INSERT INTO `dorm_info` VALUES ('279', '501', null, '0', '0', '4', '1000', '56', null);
INSERT INTO `dorm_info` VALUES ('280', '502', null, '0', '0', '4', '1000', '56', null);
INSERT INTO `dorm_info` VALUES ('281', '503', null, '0', '0', '4', '1000', '56', null);
INSERT INTO `dorm_info` VALUES ('282', '504', null, '0', '0', '6', '800', '56', null);
INSERT INTO `dorm_info` VALUES ('283', '505', null, '0', '0', '6', '800', '56', null);
INSERT INTO `dorm_info` VALUES ('284', '2121', null, '0', '0', '6', '800', '1', null);
INSERT INTO `dorm_info` VALUES ('285', '222', null, '0', '0', '6', '800', '1', null);
INSERT INTO `dorm_info` VALUES ('287', '223', null, '0', '0', '6', '800', '1', null);
INSERT INTO `dorm_info` VALUES ('288', '23', null, '0', '0', '6', '800', '1', null);
INSERT INTO `dorm_info` VALUES ('289', '2321', null, '0', '0', '6', '1000', '1', null);
INSERT INTO `dorm_info` VALUES ('290', '215', null, '0', '0', '6', '1000', '1', null);

-- ----------------------------
-- Table structure for dorm_stu_relation
-- ----------------------------
DROP TABLE IF EXISTS `dorm_stu_relation`;
CREATE TABLE `dorm_stu_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stu_no` int(11) NOT NULL,
  `dorm_id` int(11) NOT NULL,
  `class_id` int(11) DEFAULT NULL,
  `spec_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dorm_stu_relation
-- ----------------------------

-- ----------------------------
-- Table structure for dorm_type
-- ----------------------------
DROP TABLE IF EXISTS `dorm_type`;
CREATE TABLE `dorm_type` (
  `id` int(10) NOT NULL,
  `dorm_type_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `dorm_fee` float(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dorm_type
-- ----------------------------

-- ----------------------------
-- Table structure for floor_info
-- ----------------------------
DROP TABLE IF EXISTS `floor_info`;
CREATE TABLE `floor_info` (
  `floor_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '楼层id',
  `floor_no` int(11) DEFAULT NULL,
  `belong_build_id` int(11) DEFAULT NULL COMMENT '所属楼栋id',
  `dorm_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`floor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of floor_info
-- ----------------------------
INSERT INTO `floor_info` VALUES ('1', '1', '1', '12');
INSERT INTO `floor_info` VALUES ('3', '2', '1', null);
INSERT INTO `floor_info` VALUES ('4', '3', '1', null);
INSERT INTO `floor_info` VALUES ('5', '4', '1', null);
INSERT INTO `floor_info` VALUES ('6', '5', '1', null);
INSERT INTO `floor_info` VALUES ('7', '1', '2', null);
INSERT INTO `floor_info` VALUES ('8', '2', '2', null);
INSERT INTO `floor_info` VALUES ('9', '3', '2', null);
INSERT INTO `floor_info` VALUES ('10', '4', '2', null);
INSERT INTO `floor_info` VALUES ('11', '5', '2', null);
INSERT INTO `floor_info` VALUES ('12', '1', '3', null);
INSERT INTO `floor_info` VALUES ('13', '2', '3', null);
INSERT INTO `floor_info` VALUES ('14', '3', '3', null);
INSERT INTO `floor_info` VALUES ('15', '4', '3', null);
INSERT INTO `floor_info` VALUES ('16', '5', '3', null);
INSERT INTO `floor_info` VALUES ('17', '1', '4', null);
INSERT INTO `floor_info` VALUES ('18', '2', '4', null);
INSERT INTO `floor_info` VALUES ('19', '3', '4', null);
INSERT INTO `floor_info` VALUES ('20', '4', '4', null);
INSERT INTO `floor_info` VALUES ('21', '5', '4', null);
INSERT INTO `floor_info` VALUES ('22', '1', '5', null);
INSERT INTO `floor_info` VALUES ('23', '2', '5', null);
INSERT INTO `floor_info` VALUES ('24', '3', '5', null);
INSERT INTO `floor_info` VALUES ('25', '4', '5', null);
INSERT INTO `floor_info` VALUES ('26', '5', '5', null);
INSERT INTO `floor_info` VALUES ('27', '1', '6', null);
INSERT INTO `floor_info` VALUES ('28', '2', '6', null);
INSERT INTO `floor_info` VALUES ('29', '3', '6', null);
INSERT INTO `floor_info` VALUES ('30', '4', '6', null);
INSERT INTO `floor_info` VALUES ('31', '5', '6', null);
INSERT INTO `floor_info` VALUES ('32', '1', '7', null);
INSERT INTO `floor_info` VALUES ('33', '2', '7', null);
INSERT INTO `floor_info` VALUES ('34', '3', '7', null);
INSERT INTO `floor_info` VALUES ('35', '4', '7', null);
INSERT INTO `floor_info` VALUES ('36', '5', '7', null);
INSERT INTO `floor_info` VALUES ('37', '1', '8', null);
INSERT INTO `floor_info` VALUES ('38', '2', '8', null);
INSERT INTO `floor_info` VALUES ('39', '3', '8', null);
INSERT INTO `floor_info` VALUES ('40', '4', '8', null);
INSERT INTO `floor_info` VALUES ('41', '5', '8', null);
INSERT INTO `floor_info` VALUES ('42', '1', '9', null);
INSERT INTO `floor_info` VALUES ('43', '2', '9', null);
INSERT INTO `floor_info` VALUES ('44', '3', '9', null);
INSERT INTO `floor_info` VALUES ('45', '4', '9', null);
INSERT INTO `floor_info` VALUES ('46', '5', '9', null);
INSERT INTO `floor_info` VALUES ('47', '1', '10', null);
INSERT INTO `floor_info` VALUES ('48', '2', '10', null);
INSERT INTO `floor_info` VALUES ('49', '3', '10', null);
INSERT INTO `floor_info` VALUES ('50', '4', '10', null);
INSERT INTO `floor_info` VALUES ('51', '5', '10', null);
INSERT INTO `floor_info` VALUES ('52', '1', '11', null);
INSERT INTO `floor_info` VALUES ('53', '2', '11', null);
INSERT INTO `floor_info` VALUES ('54', '3', '11', null);
INSERT INTO `floor_info` VALUES ('55', '4', '11', null);
INSERT INTO `floor_info` VALUES ('56', '5', '11', null);

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(32) NOT NULL COMMENT '物资名称',
  `manufactuer_name` varchar(32) NOT NULL COMMENT '厂家名称',
  `factory_address` varchar(255) DEFAULT NULL COMMENT '厂家地址',
  `inventory` int(3) DEFAULT NULL COMMENT '物资库存量',
  `last_purchase_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '上次采购时间',
  `goods_price` float(10,2) NOT NULL COMMENT '物资价格',
  `procurement_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of goods
-- ----------------------------

-- ----------------------------
-- Table structure for goods_dispense_record
-- ----------------------------
DROP TABLE IF EXISTS `goods_dispense_record`;
CREATE TABLE `goods_dispense_record` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_to_receive` int(10) NOT NULL COMMENT '领取人学号',
  `supplie_id` int(10) NOT NULL COMMENT '物资id',
  `receive_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '领取时间',
  PRIMARY KEY (`id`,`id_to_receive`,`supplie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of goods_dispense_record
-- ----------------------------

-- ----------------------------
-- Table structure for initial_student_data
-- ----------------------------
DROP TABLE IF EXISTS `initial_student_data`;
CREATE TABLE `initial_student_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `new_stu_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `new_stu_id_card` char(18) CHARACTER SET utf8 NOT NULL COMMENT '身份证号',
  `origin_of_stu` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '源地生:省/市/地区',
  `native_place` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '新生籍贯',
  `date_of_birth` datetime DEFAULT NULL,
  `applying_major_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `gender` bit(1) NOT NULL DEFAULT b'1' COMMENT '性别，0女，1男',
  `total_score` decimal(4,1) NOT NULL DEFAULT '0.0' COMMENT '高考总分',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` int(2) NOT NULL DEFAULT '0' COMMENT '逻辑删除字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1024 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of initial_student_data
-- ----------------------------
INSERT INTO `initial_student_data` VALUES ('1', '张三', '430524199912043716', '福建省/泉州市', '湖南省/邵阳市', null, '083500', '', '455.5', '2021-04-16 10:32:48', '1');
INSERT INTO `initial_student_data` VALUES ('2', '李四', '451247856932145741', '山西省/太原市', '山西省/太原市', null, '080600', '', '461.0', '2021-04-16 18:31:04', '1');
INSERT INTO `initial_student_data` VALUES ('3', '王五', '745123654789512365', '湖南省/长沙市', '湖南省/长沙市', null, '083500', '', '433.0', '2021-04-16 22:10:50', '1');
INSERT INTO `initial_student_data` VALUES ('34', '巴随席', '522229200303317106', '山东/青岛', '山东/青岛', '2003-03-31 09:27:34', '030106', '\0', '478.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('35', '山姚巨', '130821200307230355', '辽宁', '辽宁', '2003-07-23 16:03:55', '030106', '', '490.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('36', '昝微生牟', '511102200302021348', '重庆', '重庆', '2003-02-02 11:45:27', '030106', '\0', '475.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('37', '叶司空姚', '370828200307155737', '重庆', '重庆', '2003-07-15 15:16:39', '030106', '', '462.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('38', '蔚柏熊', '360728200107217000', '湖南/长沙', '湖南/长沙', '2001-07-21 11:40:11', '030106', '\0', '467.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('39', '哈沃麻', '522325200301180732', '云南/大理', '云南/大理', '2003-01-18 15:09:16', '030106', '', '412.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('40', '蔺年南郭', '321100200107145208', '湖北/武汉', '湖北/武汉', '2001-07-14 10:01:57', '030106', '\0', '413.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('41', '公羊宁叔孙', '610403200307035250', '山东/青岛', '山东/青岛', '2003-07-03 21:18:57', '030106', '', '471.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('42', '温仲都', '410725200302068026', '云南/大理', '云南/大理', '2003-02-06 05:41:59', '030106', '\0', '47.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('43', '林游贾', '610422200202078739', '广西/柳州', '广西/柳州', '2002-02-07 20:55:47', '030106', '', '439.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('44', '羿法辛', '450200200306237889', '北京', '北京', '2003-06-23 20:02:43', '030106', '\0', '420.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('45', '靳何尤', '652122200310188831', '天津', '天津', '2003-10-18 22:39:06', '030106', '', '417.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('46', '袁利微生', '621022200211297061', '山东/青岛', '山东/青岛', '2002-11-29 04:27:53', '030106', '\0', '492.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('47', '沃岑师', '41162120030214499X', '湖南/长沙', '湖南/长沙', '2003-02-14 20:55:02', '030106', '', '471.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('48', '夏郦左丘', '211421200302062427', '重庆', '重庆', '2003-02-06 20:31:48', '030106', '\0', '484.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('49', '苍南鄂', '610423200302277635', '海南', '海南', '2003-02-27 20:50:21', '030106', '', '436.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('50', '靳阿麻', '440785200307104729', '重庆', '重庆', '2003-07-10 03:33:26', '030106', '\0', '426.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('51', '索孟刘', '321081200112119517', '河北/保定', '河北/保定', '2001-12-11 01:55:10', '030106', '', '497.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('52', '老和滕', '361023200305115428', '北京', '北京', '2003-05-11 09:12:12', '030106', '\0', '469.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('53', '阿官班', '220284200403050914', '上海', '上海', '2004-03-05 08:31:07', '030106', '', '493.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('54', '鲜东乡屋庐', '360725200301078041', '重庆', '重庆', '2003-01-07 03:17:44', '030106', '\0', '492.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('55', '辜还东郭', '652827200309199015', '陕西/西安', '陕西/西安', '2003-09-19 16:30:30', '030106', '', '432.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('56', '郇仉寿', '410823200202093761', '上海', '上海', '2002-02-09 19:27:56', '030106', '\0', '461.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('57', '陆储揭', '150823200105189530', '天津', '天津', '2001-05-18 22:44:35', '030106', '', '420.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('58', '上官太叔齐', '360921200108010382', '山东/青岛', '山东/青岛', '2001-08-01 13:15:07', '030106', '\0', '415.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('59', '百里羊舌纪', '500000200110271616', '河北/保定', '河北/保定', '2001-10-27 08:59:14', '030106', '', '426.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('60', '唐木黄', '610923200303276180', '河南、洛阳', '河南、洛阳', '2003-03-27 09:11:06', '030106', '\0', '459.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('61', '漆鲁栾', '131127200111180254', '湖南/长沙', '湖南/长沙', '2001-11-18 20:37:58', '030106', '', '47.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('62', '东乡空史', '654026200202198786', '天津', '天津', '2002-02-19 08:58:44', '030106', '\0', '423.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('63', '殳樊伯', '21110020030713417X', '海南', '海南', '2003-07-13 05:02:41', '030106', '', '415.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('64', '雍门蔚池', '361026200108149185', '天津', '天津', '2001-08-14 20:17:41', '035200', '\0', '439.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('65', '酆西门琴', '41142320030926331X', '江西/宜春', '江西/宜春', '2003-09-26 10:34:50', '035200', '', '49.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('66', '薛乜韶', '513434200205110962', '香港', '香港', '2002-05-11 20:39:06', '035200', '\0', '422.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('67', '郭老米', '141022200312273259', '北京', '北京', '2003-12-27 15:18:55', '035200', '', '452.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('68', '年竺司', '65322720020929614X', '内蒙古', '内蒙古', '2002-09-29 17:54:34', '035200', '\0', '473.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('69', '林佴伏', '320300200110278634', '河北/保定', '河北/保定', '2001-10-27 02:13:33', '035200', '', '477.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('70', '季项铁', '34082420030313054X', '内蒙古', '内蒙古', '2003-03-13 23:20:37', '035200', '\0', '476.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('71', '陶耿殷', '321112200209164819', '湖南/长沙', '湖南/长沙', '2002-09-16 05:04:10', '035200', '', '416.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('72', '况伏彭', '130323200304207489', '湖南/长沙', '湖南/长沙', '2003-04-20 11:41:37', '035200', '\0', '445.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('73', '雍毛楚', '360302200112122035', '江苏/无锡', '江苏/无锡', '2001-12-12 04:24:48', '035200', '', '46.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('74', '封沃殷', '340721200208219509', '上海', '上海', '2002-08-21 12:39:36', '035200', '\0', '448.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('75', '季老闫', '360982200108104875', '湖北/武汉', '湖北/武汉', '2001-08-10 12:14:57', '035200', '', '42.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('76', '呼延潘逯', '522635200401072105', '广西/柳州', '广西/柳州', '2004-01-07 00:11:08', '035200', '\0', '474.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('77', '舒姚夹谷', '653125200307131914', '安徽', '安徽', '2003-07-13 06:21:34', '035200', '', '476.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('78', '暴仇池', '532527200105010942', '陕西/西安', '陕西/西安', '2001-05-01 19:26:28', '035200', '\0', '482.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('79', '李厉詹', '421126200205089139', '重庆', '重庆', '2002-05-08 10:04:03', '035200', '', '419.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('80', '蹇枚熊', '410506200308316641', '天津', '天津', '2003-08-31 16:48:43', '035200', '\0', '420.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('81', '孙火那', '450803200112188478', '广东/深圳', '广东/深圳', '2001-12-18 15:14:32', '035200', '', '422.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('82', '骆沙习', '140623200205231828', '山西/太原', '山西/太原', '2002-05-23 03:15:30', '035200', '\0', '451.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('83', '逯杞曹', '43010020030622427X', '河北/保定', '河北/保定', '2003-06-22 16:23:36', '035200', '', '419.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('84', '余施宇文', '140826200111277024', '四川/成都', '四川/成都', '2001-11-27 22:17:41', '035200', '\0', '448.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('85', '裴郦查', '130400200207291130', '湖南/长沙', '湖南/长沙', '2002-07-29 01:25:59', '035200', '', '44.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('86', '法诸葛越', '654028200305272101', '重庆', '重庆', '2003-05-27 14:25:09', '035200', '\0', '453.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('87', '哈夏滑', '511525200107319196', '湖南/长沙', '湖南/长沙', '2001-07-31 10:08:16', '035200', '', '427.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('88', '东乡阎严', '211382200306100803', '湖北/武汉', '湖北/武汉', '2003-06-10 06:11:05', '035200', '\0', '450.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('89', '盛骆门', '360202200210134173', '内蒙古', '内蒙古', '2002-10-13 04:35:35', '035200', '', '487.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('90', '蔚裘高', '510683200307048640', '重庆', '重庆', '2003-07-04 00:12:15', '035200', '\0', '468.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('91', '宗正水蔡', '22020320010728235X', '内蒙古', '内蒙古', '2001-07-28 00:51:38', '035200', '', '441.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('92', '查那任', '35010520010818570X', '云南/大理', '云南/大理', '2001-08-18 06:13:42', '035200', '\0', '467.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('93', '牛伊公西', '120110200111127357', '安徽', '安徽', '2001-11-12 23:50:47', '035200', '', '448.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('94', '安厉原', '520113200401238366', '安徽', '安徽', '2004-01-23 17:02:49', '070100', '\0', '442.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('95', '篁帅胶', '330211200209134695', '四川/成都', '四川/成都', '2002-09-13 15:49:10', '070100', '', '46.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('96', '夏壤驷潘', '420528200107277463', '广西/柳州', '广西/柳州', '2001-07-27 12:17:48', '070100', '\0', '457.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('97', '折疏公孙', '53332320020606109X', '浙江/舟山', '浙江/舟山', '2002-06-06 05:15:26', '070100', '', '477.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('98', '长孙元别', '150526200304130921', '重庆', '重庆', '2003-04-13 20:23:13', '070100', '\0', '472.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('99', '北宫阚家', '361024200212117455', '山西/太原', '山西/太原', '2002-12-11 02:40:34', '070100', '', '479.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('100', '巴召海', '500229200204071483', '上海', '上海', '2002-04-07 03:35:03', '070100', '\0', '477.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('101', '吉祭邴', '640522200106198619', '陕西/西安', '陕西/西安', '2001-06-19 17:49:15', '070100', '', '448.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('102', '岳孔权', '500232200310263647', '河北/保定', '河北/保定', '2003-10-26 05:54:04', '070100', '\0', '495.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('103', '太史阎真', '500224200112021479', '江苏/无锡', '江苏/无锡', '2001-12-02 00:04:43', '070100', '', '430.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('104', '广吉国', '62292520020514456X', '广西/柳州', '广西/柳州', '2002-05-14 01:48:21', '070100', '\0', '434.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('105', '辜宰父裘', '513336200208310297', '山东/青岛', '山东/青岛', '2002-08-31 07:11:34', '070100', '', '448.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('106', '桂苏狄', '231224200203116743', '山西/太原', '山西/太原', '2002-03-11 21:50:28', '070100', '\0', '469.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('107', '厉火谢', '341822200105264377', '香港', '香港', '2001-05-26 10:20:35', '070100', '', '438.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('108', '易刁荆', '370786200108317981', '湖北/武汉', '湖北/武汉', '2001-08-31 17:06:42', '070100', '\0', '444.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('109', '司空空张', '532328200404198071', '海南', '海南', '2004-04-19 22:22:30', '070100', '', '491.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('110', '风能艾', '542325200108066149', '广东/深圳', '广东/深圳', '2001-08-06 22:12:03', '070100', '\0', '427.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('111', '左丘钦燕', '110112200302203536', '上海', '上海', '2003-02-20 10:26:49', '070100', '', '473.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('112', '方阚劳', '360321200404136147', '重庆', '重庆', '2004-04-13 21:25:19', '070100', '\0', '461.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('113', '谯褚苏', '520330200111172773', '重庆', '重庆', '2001-11-17 04:34:32', '070100', '', '481.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('114', '海卢危', '820000200305234721', '陕西/西安', '陕西/西安', '2003-05-23 04:24:40', '070100', '\0', '451.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('115', '尔朱甄仲长', '411503200107135619', '天津', '天津', '2001-07-13 03:33:30', '070100', '', '480.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('116', '第五冼莫', '522401200303308722', '陕西/西安', '陕西/西安', '2003-03-30 05:55:32', '070100', '\0', '483.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('117', '明冼琴', '350627200305127635', '山东/青岛', '山东/青岛', '2003-05-12 21:27:29', '070100', '', '476.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('118', '姬翟米', '520113200205071925', '福建/泉州', '福建/泉州', '2002-05-07 05:18:33', '070100', '\0', '442.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('119', '房叔孙单于', '65282720020506997X', '福建/泉州', '福建/泉州', '2002-05-06 07:19:04', '070100', '', '439.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('120', '邓蒯须', '420981200111072704', '江苏/无锡', '江苏/无锡', '2001-11-07 05:26:24', '070100', '\0', '484.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('121', '官贲欧', '530828200109069715', '广东/深圳', '广东/深圳', '2001-09-06 00:00:38', '070100', '', '493.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('122', '盛缪南门', '43092220020702774X', '浙江/舟山', '浙江/舟山', '2002-07-02 03:04:36', '070100', '\0', '477.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('123', '危慕虞', '350212200307243131', '浙江/舟山', '浙江/舟山', '2003-07-24 13:05:21', '070100', '', '439.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('124', '毋寇劳', '542330200202189445', '辽宁', '辽宁', '2002-02-18 14:21:51', '077200', '\0', '424.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('125', '熊虞侴', '152201200210273057', '江苏/无锡', '江苏/无锡', '2002-10-27 21:14:13', '077200', '', '48.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('126', '相沃山', '330212200202135667', '河北/保定', '河北/保定', '2002-02-13 06:52:52', '077200', '\0', '496.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('127', '张仰令狐', '513222200401144696', '海南', '海南', '2004-01-14 16:24:06', '077200', '', '446.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('128', '费慕容闻', '511681200203157162', '山东/青岛', '山东/青岛', '2002-03-15 00:56:03', '077200', '\0', '428.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('129', '曹暨敬', '421381200109136172', '湖南/长沙', '湖南/长沙', '2001-09-13 01:54:06', '077200', '', '454.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('130', '公良鲜凌', '231225200202257006', '陕西/西安', '陕西/西安', '2002-02-25 09:55:56', '077200', '\0', '410.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('131', '侯詹迟', '370321200107108838', '辽宁', '辽宁', '2001-07-10 20:48:07', '077200', '', '449.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('132', '裘焦俞', '510700200304091701', '辽宁', '辽宁', '2003-04-09 04:30:36', '077200', '\0', '456.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('133', '公羊季游', '421124200208185959', '福建/泉州', '福建/泉州', '2002-08-18 06:32:36', '077200', '', '451.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('134', '祝篁相里', '450422200303129128', '重庆', '重庆', '2003-03-12 03:40:24', '077200', '\0', '436.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('135', '相苌牛', '130632200204097393', '天津', '天津', '2002-04-09 07:19:54', '077200', '', '471.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('136', '南老萧', '513223200310145868', '海南', '海南', '2003-10-14 06:36:07', '077200', '\0', '461.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('137', '茅第五乜', '450332200201285557', '广东/深圳', '广东/深圳', '2002-01-28 02:58:18', '077200', '', '46.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('138', '聂瞿贝', '130681200110175108', '内蒙古', '内蒙古', '2001-10-17 04:49:34', '077200', '\0', '423.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('139', '蓬农充', '150522200204019710', '湖南/长沙', '湖南/长沙', '2002-04-01 23:51:05', '077200', '', '486.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('140', '干钟离封', '510822200310269725', '浙江/舟山', '浙江/舟山', '2003-10-26 04:21:54', '077200', '\0', '495.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('141', '费禹官', '230804200201114898', '陕西/西安', '陕西/西安', '2002-01-11 04:28:25', '077200', '', '429.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('142', '井常濮', '451281200309088400', '湖南/长沙', '湖南/长沙', '2003-09-08 17:00:34', '077200', '\0', '455.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('143', '权宰父祭', '370725200209232271', '香港', '香港', '2002-09-23 14:25:55', '077200', '', '455.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('144', '巫胶蒋', '610630200109099528', '安徽', '安徽', '2001-09-09 13:16:16', '077200', '\0', '43.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('145', '蔡汝何', '530925200302237991', '湖南/长沙', '湖南/长沙', '2003-02-23 23:56:34', '077200', '', '415.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('146', '薛戈萧', '620723200303309748', '江西/宜春', '江西/宜春', '2003-03-30 10:38:58', '077200', '\0', '432.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('147', '篁庆柳', '420606200309110837', '山东/青岛', '山东/青岛', '2003-09-11 11:01:29', '077200', '', '428.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('148', '贾蔚逄', '37010520010708832X', '安徽', '安徽', '2001-07-08 07:41:03', '077200', '\0', '474.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('149', '纵冀言', '340100200207206891', '湖南/长沙', '湖南/长沙', '2002-07-20 03:08:54', '077200', '', '420.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('150', '巢缪舒', '411082200203140360', '陕西/西安', '陕西/西安', '2002-03-14 22:53:55', '077200', '\0', '460.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('151', '宾翁上官', '52262720030531889X', '安徽', '安徽', '2003-05-31 07:38:21', '077200', '', '49.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('152', '揭苗茹', '513229200302071684', '山东/青岛', '山东/青岛', '2003-02-07 18:03:25', '077200', '\0', '493.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('153', '蒲甘柯', '210905200311097556', '四川/成都', '四川/成都', '2003-11-09 18:22:19', '077200', '', '449.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('154', '法魏百里', '440205200307122901', '陕西/西安', '陕西/西安', '2003-07-12 17:17:14', '080300', '\0', '419.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('155', '邴全风', '532929200301301813', '广东/深圳', '广东/深圳', '2003-01-30 07:14:18', '080300', '', '494.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('156', '余老琴', '500107200305251869', '广东/深圳', '广东/深圳', '2003-05-25 21:27:38', '080300', '\0', '446.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('157', '公仪尤秘', '430481200212050437', '上海', '上海', '2002-12-05 01:37:19', '080300', '', '426.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('158', '祝乐佘', '530121200312145223', '天津', '天津', '2003-12-14 00:48:58', '080300', '\0', '433.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('159', '崔臧司马', '653200200105187213', '内蒙古', '内蒙古', '2001-05-18 05:34:39', '080300', '', '431.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('160', '饶顾乜', '422825200306036784', '安徽', '安徽', '2003-06-03 00:10:45', '080300', '\0', '492.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('161', '杜成公乘', '12010420030429885X', '河南、洛阳', '河南、洛阳', '2003-04-29 10:38:39', '080300', '', '479.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('162', '暨茹祭', '510922200306217207', '山西/太原', '山西/太原', '2003-06-21 02:13:27', '080300', '\0', '47.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('163', '林巫谷梁', '440400200305144110', '重庆', '重庆', '2003-05-14 06:11:47', '080300', '', '490.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('164', '季畅宗正', '340825200305239502', '北京', '北京', '2003-05-23 09:04:20', '080300', '\0', '443.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('165', '阿郑水', '431021200206278454', '天津', '天津', '2002-06-27 10:57:29', '080300', '', '439.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('166', '亢戴盛', '140781200212092327', '重庆', '重庆', '2002-12-09 13:08:00', '080300', '\0', '457.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('167', '段干施元', '610729200301187579', '上海', '上海', '2003-01-18 10:47:45', '080300', '', '466.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('168', '尔朱慎黎', '371200200206211906', '福建/泉州', '福建/泉州', '2002-06-21 14:26:11', '080300', '\0', '487.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('169', '张甄杨', '371428200402249175', '北京', '北京', '2004-02-24 23:07:13', '080300', '', '436.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('170', '丰项习', '440115200307125042', '福建/泉州', '福建/泉州', '2003-07-12 11:48:14', '080300', '\0', '493.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('171', '邵令狐虞', '11010920010715833X', '北京', '北京', '2001-07-15 19:12:10', '080300', '', '432.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('172', '梅广郑', '430300200204271762', '广东/深圳', '广东/深圳', '2002-04-27 05:04:49', '080300', '\0', '446.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('173', '庾祭符', '532931200206209033', '云南/大理', '云南/大理', '2002-06-20 18:27:09', '080300', '', '439.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('174', '璩谢包', '654225200108114309', '北京', '北京', '2001-08-11 15:12:33', '080300', '\0', '470.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('175', '畅云扈', '500107200308151492', '辽宁', '辽宁', '2003-08-15 19:09:26', '080300', '', '413.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('176', '水柏钮', '622927200205295360', '广西/柳州', '广西/柳州', '2002-05-29 06:19:51', '080300', '\0', '444.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('177', '柴老易', '340223200311251473', '海南', '海南', '2003-11-25 20:26:18', '080300', '', '454.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('178', '浦西门狐', '330921200306045708', '福建/泉州', '福建/泉州', '2003-06-04 03:09:00', '080300', '\0', '491.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('179', '任梅闾', '653001200203255852', '陕西/西安', '陕西/西安', '2002-03-25 12:07:43', '080300', '', '470.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('180', '池毛贝', '420000200210120022', '山东/青岛', '山东/青岛', '2002-10-12 05:41:20', '080300', '\0', '414.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('181', '简阚濮阳', '370725200310207855', '江西/宜春', '江西/宜春', '2003-10-20 23:00:58', '080300', '', '474.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('182', '阳冒司寇', '532527200307172181', '陕西/西安', '陕西/西安', '2003-07-17 08:08:08', '080300', '\0', '480.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('183', '岑莘季', '231085200301091414', '广西/柳州', '广西/柳州', '2003-01-09 19:23:06', '080300', '', '474.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('184', '晏南简', '513431200309158646', '福建/泉州', '福建/泉州', '2003-09-15 08:51:26', '085234', '\0', '42.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('185', '乌钭齐', '220106200304194010', '山西/太原', '山西/太原', '2003-04-19 02:58:47', '085234', '', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('186', '揭印钭', '140524200401018603', '云南/大理', '云南/大理', '2004-01-01 10:44:11', '085234', '\0', '463.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('187', '繁琴归', '620523200403016634', '山东/青岛', '山东/青岛', '2004-03-01 06:32:12', '085234', '', '433.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('188', '裴贾经', '441825200105304685', '广西/柳州', '广西/柳州', '2001-05-30 06:51:17', '085234', '\0', '419.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('189', '裴嵇山', '320722200211031358', '四川/成都', '四川/成都', '2002-11-03 09:42:54', '085234', '', '470.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('190', '郭利苍', '540124200108179208', '浙江/舟山', '浙江/舟山', '2001-08-17 18:08:21', '085234', '\0', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('191', '璩咸伊', '530829200212163298', '北京', '北京', '2002-12-16 18:45:56', '085234', '', '419.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('192', '任施牛', '231124200310275867', '广东/深圳', '广东/深圳', '2003-10-27 12:56:27', '085234', '\0', '498.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('193', '松车侴', '411421200208078278', '河北/保定', '河北/保定', '2002-08-07 21:28:54', '085234', '', '478.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('194', '路綦毋闾丘', '522622200311173787', '山西/太原', '山西/太原', '2003-11-17 01:37:10', '085234', '\0', '424.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('195', '皇甫左丘从', '513329200212031414', '内蒙古', '内蒙古', '2002-12-03 20:57:55', '085234', '', '424.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('196', '印木符', '411103200401162608', '湖北/武汉', '湖北/武汉', '2004-01-16 10:03:35', '085234', '\0', '418.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('197', '端是支', '440800200107092593', '四川/成都', '四川/成都', '2001-07-09 19:27:52', '085234', '', '419.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('198', '慕查东门', '330903200204163823', '辽宁', '辽宁', '2002-04-16 10:38:28', '085234', '\0', '462.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('199', '荆罗衡', '140929200107086414', '江苏/无锡', '江苏/无锡', '2001-07-08 11:00:32', '085234', '', '472.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('200', '盛钟离车', '510800200212206589', '湖北/武汉', '湖北/武汉', '2002-12-20 03:32:14', '085234', '\0', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('201', '牛齐来', '360426200209156537', '广西/柳州', '广西/柳州', '2002-09-15 04:48:42', '085234', '', '423.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('202', '万俟卢金', '320102200104298585', '河南、洛阳', '河南、洛阳', '2001-04-29 07:19:26', '085234', '\0', '440.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('203', '戈汪尔朱', '140500200207113075', '湖北/武汉', '湖北/武汉', '2002-07-11 08:21:44', '085234', '', '426.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('204', '尚裴庆', '211303200204010086', '安徽', '安徽', '2002-04-01 07:44:00', '085234', '\0', '476.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('205', '那楚常', '150623200307034675', '内蒙古', '内蒙古', '2003-07-03 13:52:48', '085234', '', '48.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('206', '壤驷宇文单于', '150426200303101568', '福建/泉州', '福建/泉州', '2003-03-10 14:58:10', '085234', '\0', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('207', '许湛沃', '632624200111147750', '广西/柳州', '广西/柳州', '2001-11-14 18:15:45', '085234', '', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('208', '缑虎阙', '530400200307180467', '浙江/舟山', '浙江/舟山', '2003-07-18 05:23:02', '085234', '\0', '454.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('209', '江溥单', '370832200306036239', '云南/大理', '云南/大理', '2003-06-03 12:53:54', '085234', '', '466.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('210', '索郎国', '141100200212099924', '内蒙古', '内蒙古', '2002-12-09 16:23:16', '085234', '\0', '426.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('211', '羿宓陶', '620300200105082558', '广东/深圳', '广东/深圳', '2001-05-08 17:40:48', '085234', '', '491.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('212', '那钟离汝', '141029200210045188', '广东/深圳', '广东/深圳', '2002-10-04 08:20:13', '085234', '\0', '419.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('213', '太叔爱太史', '360726200309276938', '山东/青岛', '山东/青岛', '2003-09-27 03:17:08', '085234', '', '429.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('214', '梅苏查', '211081200312141246', '江西/宜春', '江西/宜春', '2003-12-14 15:43:53', '080500', '\0', '446.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('215', '焦蒙瞿', '34012120030716757X', '湖北/武汉', '湖北/武汉', '2003-07-16 03:16:58', '080500', '', '44.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('216', '风东乡召', '511324200109082949', '山西/太原', '山西/太原', '2001-09-08 06:24:46', '080500', '\0', '464.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('217', '陆于蔚', '520303200306179714', '上海', '上海', '2003-06-17 14:48:58', '080500', '', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('218', '纵郏邴', '451222200107235804', '天津', '天津', '2001-07-23 23:33:27', '080500', '\0', '475.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('219', '伊展皇', '150207200111175335', '香港', '香港', '2001-11-17 15:37:27', '080500', '', '434.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('220', '项端巫', '350583200304138346', '陕西/西安', '陕西/西安', '2003-04-13 17:06:02', '080500', '\0', '43.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('221', '桑邵蔚', '441224200212264333', '安徽', '安徽', '2002-12-26 17:52:40', '080500', '', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('222', '上官宗正堵', '340323200306028803', '香港', '香港', '2003-06-02 06:50:23', '080500', '\0', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('223', '鲁虞戚', '542327200109181555', '辽宁', '辽宁', '2001-09-18 23:45:57', '080500', '', '46.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('224', '佴柯伊', '411524200206218489', '山东/青岛', '山东/青岛', '2002-06-21 10:48:47', '080500', '\0', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('225', '井子车曲', '451123200308144096', '北京', '北京', '2003-08-14 20:01:30', '080500', '', '414.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('226', '杜封鄢', '110117200212121926', '天津', '天津', '2002-12-12 07:38:44', '080500', '\0', '460.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('227', '蓬滕居', '640302200310019255', '上海', '上海', '2003-10-01 04:33:45', '080500', '', '481.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('228', '慕党仇', '532823200303184629', '浙江/舟山', '浙江/舟山', '2003-03-18 08:25:01', '080500', '\0', '469.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('229', '苗焦关', '370832200310183696', '重庆', '重庆', '2003-10-18 06:02:46', '080500', '', '418.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('230', '终邓东方', '451302200211162801', '广东/深圳', '广东/深圳', '2002-11-16 04:11:51', '080500', '\0', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('231', '仲长逄冼', '42050520020313699X', '江西/宜春', '江西/宜春', '2002-03-13 16:49:34', '080500', '', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('232', '怀充红', '440232200303288102', '四川/成都', '四川/成都', '2003-03-28 17:45:23', '080500', '\0', '473.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('233', '赏贡家', '220524200209015357', '福建/泉州', '福建/泉州', '2002-09-01 22:46:24', '080500', '', '462.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('234', '殳弘鲍', '341024200309247009', '湖南/长沙', '湖南/长沙', '2003-09-24 18:02:31', '080500', '\0', '454.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('235', '扈糜公良', '410403200208249010', '云南/大理', '云南/大理', '2002-08-24 16:34:58', '080500', '', '411.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('236', '苏计路', '450300200209118883', '湖北/武汉', '湖北/武汉', '2002-09-11 21:53:34', '080500', '\0', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('237', '堵敬居', '440803200106033179', '河北/保定', '河北/保定', '2001-06-03 07:50:48', '080500', '', '476.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('238', '厉子车文', '450400200111024462', '湖南/长沙', '湖南/长沙', '2001-11-02 05:02:10', '080500', '\0', '428.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('239', '司寇廖许', '41040220020510379X', '广东/深圳', '广东/深圳', '2002-05-10 17:38:38', '080500', '', '423.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('240', '桓慕俞', '513433200307277388', '福建/泉州', '福建/泉州', '2003-07-27 09:46:27', '080500', '\0', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('241', '尤廖惠', '360502200206072332', '香港', '香港', '2002-06-07 11:38:55', '080500', '', '439.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('242', '涂钮于', '511824200302128686', '山东/青岛', '山东/青岛', '2003-02-12 21:52:52', '080500', '\0', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('243', '梅奚闫', '320205200207079291', '香港', '香港', '2002-07-07 19:42:54', '080500', '', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('244', '拓跋法扶', '230714200308211767', '浙江/舟山', '浙江/舟山', '2003-08-21 10:03:22', '085204', '\0', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('245', '劳赖甄', '511126200403103191', '四川/成都', '四川/成都', '2004-03-10 15:38:18', '085204', '', '447.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('246', '赖通臧', '331126200210239064', '浙江/舟山', '浙江/舟山', '2002-10-23 01:54:41', '085204', '\0', '43.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('247', '相里龚纵', '130903200209161838', '内蒙古', '内蒙古', '2002-09-16 08:15:19', '085204', '', '47.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('248', '丰明裴', '440982200105203464', '福建/泉州', '福建/泉州', '2001-05-20 10:47:36', '085204', '\0', '464.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('249', '莫却太史', '410622200207274417', '河北/保定', '河北/保定', '2002-07-27 08:17:22', '085204', '', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('250', '东门亓亢', '150426200111084564', '广西/柳州', '广西/柳州', '2001-11-08 03:51:59', '085204', '\0', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('251', '张鲍郎', '632522200308053637', '河北/保定', '河北/保定', '2003-08-05 12:18:15', '085204', '', '420.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('252', '茹曾司城', '360922200201127127', '山西/太原', '山西/太原', '2002-01-12 01:43:04', '085204', '\0', '436.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('253', '种葛屈', '141102200311015493', '海南', '海南', '2003-11-01 13:52:07', '085204', '', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('254', '佘年齐', '350783200207301923', '内蒙古', '内蒙古', '2002-07-30 15:21:25', '085204', '\0', '426.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('255', '陆鄂陶', '220681200207190437', '天津', '天津', '2002-07-19 08:30:30', '085204', '', '452.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('256', '寇濮丁', '210411200309077142', '海南', '海南', '2003-09-07 09:49:35', '085204', '\0', '410.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('257', '左哈查', '220521200107239676', '山东/青岛', '山东/青岛', '2001-07-23 03:15:09', '085204', '', '483.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('258', '戈夏侯庾', '150724200112142686', '江西/宜春', '江西/宜春', '2001-12-14 18:45:02', '085204', '\0', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('259', '钱龚万', '410222200310102857', '海南', '海南', '2003-10-10 05:32:34', '085204', '', '498.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('260', '陆靳左', '230000200303237865', '山西/太原', '山西/太原', '2003-03-23 16:59:31', '085204', '\0', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('261', '邱蓝谢', '510124200203309175', '江苏/无锡', '江苏/无锡', '2002-03-30 18:01:17', '085204', '', '476.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('262', '敬原糜', '370481200107049585', '湖北/武汉', '湖北/武汉', '2001-07-04 13:11:28', '085204', '\0', '434.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('263', '佘俞宰', '451300200202241871', '广西/柳州', '广西/柳州', '2002-02-24 13:10:23', '085204', '', '43.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('264', '房綦毋綦毋', '500232200206107302', '内蒙古', '内蒙古', '2002-06-10 02:19:11', '085204', '\0', '428.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('265', '金那邝', '440303200205179774', '四川/成都', '四川/成都', '2002-05-17 17:02:05', '085204', '', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('266', '平贝祁', '130230200209129409', '北京', '北京', '2002-09-12 16:28:16', '085204', '\0', '491.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('267', '蔺干胶', '331022200210142472', '湖北/武汉', '湖北/武汉', '2002-10-14 02:09:19', '085204', '', '45.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('268', '叶司徒毛', '341802200107203222', '山西/太原', '山西/太原', '2001-07-20 09:58:11', '085204', '\0', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('269', '文闵夏侯', '510183200303127758', '上海', '上海', '2003-03-12 04:42:57', '085204', '', '492.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('270', '巩璩仰', '340823200205066082', '内蒙古', '内蒙古', '2002-05-06 16:28:52', '085204', '\0', '497.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('271', '展公冶南门', '211005200401259135', '云南/大理', '云南/大理', '2004-01-25 03:12:17', '085204', '', '446.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('272', '阮邰厉', '141129200402038623', '湖南/长沙', '湖南/长沙', '2004-02-03 17:09:24', '085204', '\0', '478.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('273', '郑胥喻', '140300200305195931', '重庆', '重庆', '2003-05-19 10:15:21', '085204', '', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('274', '支蓟墨', '210202200305205007', '河北/保定', '河北/保定', '2003-05-20 03:56:40', '080600', '\0', '47.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('275', '荀谈长孙', '140600200109136834', '重庆', '重庆', '2001-09-13 07:14:43', '080600', '', '466.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('276', '伯慕班', '150723200311046041', '香港', '香港', '2003-11-04 19:03:15', '080600', '\0', '456.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('277', '范谭高', '632821200307054953', '陕西/西安', '陕西/西安', '2003-07-05 08:57:56', '080600', '', '455.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('278', '钮干张', '623022200301193305', '内蒙古', '内蒙古', '2003-01-19 20:10:00', '080600', '\0', '475.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('279', '訾湛却', '150727200212219053', '广西/柳州', '广西/柳州', '2002-12-21 19:09:57', '080600', '', '432.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('280', '畅蓬邵', '131128200303217185', '福建/泉州', '福建/泉州', '2003-03-21 21:19:04', '080600', '\0', '42.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('281', '简曾裘', '361021200211205219', '山东/青岛', '山东/青岛', '2002-11-20 14:59:19', '080600', '', '465.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('282', '蓬东桂', '542428200204305167', '重庆', '重庆', '2002-04-30 03:30:47', '080600', '\0', '415.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('283', '阙勾韩', '140200200204109410', '山东/青岛', '山东/青岛', '2002-04-10 11:17:35', '080600', '', '413.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('284', '况陶仇', '210711200205117484', '山东/青岛', '山东/青岛', '2002-05-11 08:56:45', '080600', '\0', '428.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('285', '召贺祝', '350625200303086937', '北京', '北京', '2003-03-08 16:35:30', '080600', '', '463.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('286', '真于空', '35078320030216906X', '云南/大理', '云南/大理', '2003-02-16 04:20:01', '080600', '\0', '418.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('287', '官鄢薛', '62040320010808465X', '湖北/武汉', '湖北/武汉', '2001-08-08 20:48:21', '080600', '', '468.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('288', '蔺拓跋钟', '451022200310248183', '天津', '天津', '2003-10-24 02:11:53', '080600', '\0', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('289', '侴太叔解', '150000200312139191', '安徽', '安徽', '2003-12-13 19:15:27', '080600', '', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('290', '令狐胥况', '441423200208305440', '广东/深圳', '广东/深圳', '2002-08-30 12:03:12', '080600', '\0', '478.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('291', '从饶屈突', '130324200108043858', '河南、洛阳', '河南、洛阳', '2001-08-04 05:18:40', '080600', '', '478.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('292', '敖密亢', '622921200307206084', '河北/保定', '河北/保定', '2003-07-20 16:17:18', '080600', '\0', '496.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('293', '王常阚', '130433200212104018', '北京', '北京', '2002-12-10 23:47:35', '080600', '', '489.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('294', '计轩辕法', '341421200312158405', '香港', '香港', '2003-12-15 00:09:01', '080600', '\0', '464.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('295', '仓却巴', '210505200304055997', '江苏/无锡', '江苏/无锡', '2003-04-05 00:47:31', '080600', '', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('296', '花毛龙', '331081200209150109', '福建/泉州', '福建/泉州', '2002-09-15 23:34:34', '080600', '\0', '439.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('297', '边潘舒', '350800200211034658', '江西/宜春', '江西/宜春', '2002-11-03 00:40:48', '080600', '', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('298', '夔呼延花', '22060520020511854X', '广东/深圳', '广东/深圳', '2002-05-11 20:44:04', '080600', '\0', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('299', '冷督包', '371428200107219272', '云南/大理', '云南/大理', '2001-07-21 16:09:29', '080600', '', '476.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('300', '竺郦都', '621121200304099549', '河南、洛阳', '河南、洛阳', '2003-04-09 10:58:01', '080600', '\0', '493.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('301', '公羊尔朱詹', '500227200210058258', '云南/大理', '云南/大理', '2002-10-05 11:52:21', '080600', '', '418.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('302', '吴京羊', '130223200108296984', '广西/柳州', '广西/柳州', '2001-08-29 06:27:14', '080600', '\0', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('303', '訾姚钮', '330784200305213452', '湖北/武汉', '湖北/武汉', '2003-05-21 04:36:13', '080600', '', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('304', '苗范万俟', '410882200208269422', '北京', '北京', '2002-08-26 03:14:49', '080804', '\0', '422.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('305', '雍门红司城', '360300200109181250', '海南', '海南', '2001-09-18 01:16:36', '080804', '', '436.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('306', '酆顾步', '450924200301124825', '江西/宜春', '江西/宜春', '2003-01-12 11:26:15', '080804', '\0', '477.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('307', '储门红', '430102200109079811', '上海', '上海', '2001-09-07 06:00:24', '080804', '', '445.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('308', '万严相里', '410811200210283324', '北京', '北京', '2002-10-28 13:04:05', '080804', '\0', '48.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('309', '蒲沙宣', '140211200210278236', '重庆', '重庆', '2002-10-27 17:07:12', '080804', '', '462.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('310', '闵漆雕闻人', '445222200312230609', '河南、洛阳', '河南、洛阳', '2003-12-23 09:00:00', '080804', '\0', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('311', '赏翁梁丘', '341321200209239818', '广东/深圳', '广东/深圳', '2002-09-23 20:34:30', '080804', '', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('312', '檀麦封', '370786200206280609', '福建/泉州', '福建/泉州', '2002-06-28 16:18:14', '080804', '\0', '446.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('313', '阚端木祝', '420626200201014152', '天津', '天津', '2002-01-01 15:25:47', '080804', '', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('314', '督红卓', '510725200212319307', '海南', '海南', '2002-12-31 13:49:45', '080804', '\0', '481.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('315', '双须令狐', '440704200310024297', '山东/青岛', '山东/青岛', '2003-10-02 07:58:05', '080804', '', '472.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('316', '郗别扶', '441481200106062840', '上海', '上海', '2001-06-06 21:29:24', '080804', '\0', '464.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('317', '公仪陶秘', '450602200206088614', '海南', '海南', '2002-06-08 19:50:59', '080804', '', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('318', '常第五厉', '500116200308195109', '陕西/西安', '陕西/西安', '2003-08-19 18:21:47', '080804', '\0', '410.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('319', '谷郈左', '411421200107262711', '四川/成都', '四川/成都', '2001-07-26 16:06:06', '080804', '', '493.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('320', '师闾公', '610522200201077588', '内蒙古', '内蒙古', '2002-01-07 08:04:48', '080804', '\0', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('321', '庾还檀', '320621200312245038', '陕西/西安', '陕西/西安', '2003-12-24 14:59:54', '080804', '', '456.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('322', '樊裴宁', '440500200302089342', '四川/成都', '四川/成都', '2003-02-08 19:05:31', '080804', '\0', '454.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('323', '解赫连索', '210204200209256835', '香港', '香港', '2002-09-25 05:52:20', '080804', '', '40.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('324', '于宰桂', '230822200207257382', '重庆', '重庆', '2002-07-25 06:12:53', '080804', '\0', '466.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('325', '隗严法', '210283200105279093', '山东/青岛', '山东/青岛', '2001-05-27 00:35:56', '080804', '', '488.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('326', '门伍冀', '130629200209248105', '安徽', '安徽', '2002-09-24 18:29:31', '080804', '\0', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('327', '郜魏蔺', '532801200307041053', '河南、洛阳', '河南、洛阳', '2003-07-04 11:59:47', '080804', '', '453.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('328', '有水楼', '61032720011209386X', '浙江/舟山', '浙江/舟山', '2001-12-09 08:44:53', '080804', '\0', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('329', '尹勾眭', '210400200201151495', '香港', '香港', '2002-01-15 03:35:40', '080804', '', '413.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('330', '权范车', '350802200210241703', '香港', '香港', '2002-10-24 02:18:40', '080804', '\0', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('331', '束凌郗', '542327200107310472', '河北/保定', '河北/保定', '2001-07-31 02:12:25', '080804', '', '431.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('332', '熊蹇越', '350624200209057340', '内蒙古', '内蒙古', '2002-09-05 00:13:48', '080804', '\0', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('333', '万丰展', '500224200204255491', '湖南/长沙', '湖南/长沙', '2002-04-25 07:28:24', '080804', '', '46.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('334', '关靳过', '360203200208300183', '江苏/无锡', '江苏/无锡', '2002-08-30 05:55:48', '081100', '\0', '490.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('335', '老百里尤', '211281200305137354', '广东/深圳', '广东/深圳', '2003-05-13 15:00:24', '081100', '', '438.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('336', '燕公冶杞', '140428200306244901', '广西/柳州', '广西/柳州', '2003-06-24 21:34:20', '081100', '\0', '457.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('337', '端王孙越', '620700200402296896', '海南', '海南', '2004-02-29 16:00:39', '081100', '', '430.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('338', '陈爱董', '410803200301119220', '海南', '海南', '2003-01-11 17:30:55', '081100', '\0', '436.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('339', '桂阮伯', '532326200304292797', '福建/泉州', '福建/泉州', '2003-04-29 22:12:36', '081100', '', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('340', '郏邹微生', '510303200210293705', '河北/保定', '河北/保定', '2002-10-29 15:06:52', '081100', '\0', '42.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('341', '仲孙屋庐雍门', '220112200305029057', '四川/成都', '四川/成都', '2003-05-02 06:39:41', '081100', '', '433.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('342', '宗正杨虎', '371081200301048980', '天津', '天津', '2003-01-04 11:40:53', '081100', '\0', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('343', '海秋须', '360923200208145439', '河南、洛阳', '河南、洛阳', '2002-08-14 12:29:10', '081100', '', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('344', '乐寇万', '44148120030220034X', '云南/大理', '云南/大理', '2003-02-20 04:48:55', '081100', '\0', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('345', '后余鲜', '542123200202085270', '重庆', '重庆', '2002-02-08 02:48:26', '081100', '', '429.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('346', '白党姜', '640500200203308260', '海南', '海南', '2002-03-30 17:22:46', '081100', '\0', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('347', '巫马喻景', '652925200301077278', '江苏/无锡', '江苏/无锡', '2003-01-07 18:07:13', '081100', '', '438.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('348', '巢屈潘', '23128120020831608X', '重庆', '重庆', '2002-08-31 08:16:23', '081100', '\0', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('349', '篁郗钭', '513328200311206973', '广西/柳州', '广西/柳州', '2003-11-20 08:05:30', '081100', '', '441.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('350', '孔牛怀', '441825200112277486', '浙江/舟山', '浙江/舟山', '2001-12-27 21:44:25', '081100', '\0', '483.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('351', '邬史闾丘', '341003200212071332', '四川/成都', '四川/成都', '2002-12-07 17:13:16', '081100', '', '447.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('352', '况裘官', '350725200201238843', '福建/泉州', '福建/泉州', '2002-01-23 07:18:41', '081100', '\0', '470.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('353', '江符宰', '652900200110011914', '上海', '上海', '2001-10-01 20:03:04', '081100', '', '456.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('354', '车正怀贡', '445281200205163260', '山东/青岛', '山东/青岛', '2002-05-16 09:34:34', '081100', '\0', '435.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('355', '郭巢相', '450125200112165778', '上海', '上海', '2001-12-16 21:34:51', '081100', '', '418.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('356', '宣甘冀', '650100200205017563', '广东/深圳', '广东/深圳', '2002-05-01 09:50:03', '081100', '\0', '449.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('357', '吴祁隆', '451024200310316476', '北京', '北京', '2003-10-31 22:15:34', '081100', '', '441.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('358', '单单金', '230603200211165827', '天津', '天津', '2002-11-16 00:16:26', '081100', '\0', '419.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('359', '聂邝缪', '370831200304063771', '广东/深圳', '广东/深圳', '2003-04-06 06:25:00', '081100', '', '446.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('360', '徐强皇甫', '230881200107270109', '河南、洛阳', '河南、洛阳', '2001-07-27 13:40:56', '081100', '\0', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('361', '翁庄谷', '530500200403133850', '河北/保定', '河北/保定', '2004-03-13 16:13:07', '081100', '', '490.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('362', '扈宗正空', '370705200111256528', '辽宁', '辽宁', '2001-11-25 09:47:19', '081100', '\0', '498.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('363', '疏苌舜', '220622200310134839', '海南', '海南', '2003-10-13 04:12:19', '081100', '', '420.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('364', '越窦吕', '360323200109238645', '湖北/武汉', '湖北/武汉', '2001-09-23 13:11:48', '085207', '\0', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('365', '尉迟公焦', '511902200307289873', '河南、洛阳', '河南、洛阳', '2003-07-28 04:11:48', '085207', '', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('366', '郎步韩', '23050320031215286X', '内蒙古', '内蒙古', '2003-12-15 03:27:42', '085207', '\0', '421.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('367', '胡巴劳', '220722200201056777', '上海', '上海', '2002-01-05 06:50:07', '085207', '', '462.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('368', '戚侴潘', '622921200404215361', '福建/泉州', '福建/泉州', '2004-04-21 15:13:10', '085207', '\0', '43.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('369', '夏侯挚虞', '141024200201117918', '重庆', '重庆', '2002-01-11 20:14:46', '085207', '', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('370', '璩国胶', '341702200110118429', '河北/保定', '河北/保定', '2001-10-11 19:22:45', '085207', '\0', '492.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('371', '公羊殷龙', '320831200211086270', '湖北/武汉', '湖北/武汉', '2002-11-08 10:17:31', '085207', '', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('372', '蒯解邰', '632623200109084686', '山东/青岛', '山东/青岛', '2001-09-08 01:14:24', '085207', '\0', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('373', '古颛孙闻人', '440184200308056192', '广西/柳州', '广西/柳州', '2003-08-05 12:36:35', '085207', '', '417.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('374', '麻佴仪', '630123200306296582', '湖北/武汉', '湖北/武汉', '2003-06-29 11:59:30', '085207', '\0', '472.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('375', '班崔夏侯', '430923200210221291', '陕西/西安', '陕西/西安', '2002-10-22 02:05:40', '085207', '', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('376', '胡母卞郎', '210905200107163106', '山东/青岛', '山东/青岛', '2001-07-16 06:25:03', '085207', '\0', '446.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('377', '勾柴易', '42011620040408867X', '陕西/西安', '陕西/西安', '2004-04-08 22:44:33', '085207', '', '457.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('378', '平高秦', '131003200107277807', '上海', '上海', '2001-07-27 15:42:45', '085207', '\0', '467.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('379', '隗储居', '110106200206306992', '天津', '天津', '2002-06-30 21:42:12', '085207', '', '45.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('380', '越甘宋', '532928200207047007', '北京', '北京', '2002-07-04 00:11:08', '085207', '\0', '427.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('381', '独孤宰水', '230305200112310757', '重庆', '重庆', '2001-12-31 20:09:49', '085207', '', '478.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('382', '充卜巩', '411327200303140466', '福建/泉州', '福建/泉州', '2003-03-14 22:12:45', '085207', '\0', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('383', '常仓季', '340621200404173477', '湖南/长沙', '湖南/长沙', '2004-04-17 10:07:12', '085207', '', '411.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('384', '艾艾公', '513330200201292980', '湖南/长沙', '湖南/长沙', '2002-01-29 15:09:59', '085207', '\0', '481.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('385', '郭仲孙海', '210711200107282013', '陕西/西安', '陕西/西安', '2001-07-28 18:21:20', '085207', '', '477.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('386', '于裘古', '542225200205165602', '海南', '海南', '2002-05-16 15:08:06', '085207', '\0', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('387', '充谭慕', '150926200107138651', '山西/太原', '山西/太原', '2001-07-13 22:53:34', '085207', '', '414.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('388', '费宰父文', '500229200105250363', '重庆', '重庆', '2001-05-25 05:06:51', '085207', '\0', '480.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('389', '都殷温', '510524200306134795', '江苏/无锡', '江苏/无锡', '2003-06-13 10:55:54', '085207', '', '42.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('390', '独孤戚莘', '152200200209212269', '云南/大理', '云南/大理', '2002-09-21 23:56:28', '085207', '\0', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('391', '巴东乡东', '513323200207235174', '安徽', '安徽', '2002-07-23 06:45:10', '085207', '', '471.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('392', '奚蒲卫', '440106200205118283', '湖南/长沙', '湖南/长沙', '2002-05-11 10:35:50', '085207', '\0', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('393', '金居端木', '130100200201269796', '浙江/舟山', '浙江/舟山', '2002-01-26 05:17:42', '085207', '', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('394', '竺闵亓', '220700200212302525', '上海', '上海', '2002-12-30 04:36:48', '085208', '\0', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('395', '黎松慕容', '321311200108280591', '江苏/无锡', '江苏/无锡', '2001-08-28 14:05:38', '085208', '', '427.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('396', '雍融步', '130628200204203761', '江苏/无锡', '江苏/无锡', '2002-04-20 05:08:50', '085208', '\0', '417.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('397', '从仲终', '431123200201238750', '内蒙古', '内蒙古', '2002-01-23 16:59:39', '085208', '', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('398', '柳党鲍', '433130200108219569', '江西/宜春', '江西/宜春', '2001-08-21 20:55:43', '085208', '\0', '446.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('399', '阚霍匡', '420921200107287511', '湖南/长沙', '湖南/长沙', '2001-07-28 19:31:03', '085208', '', '480.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('400', '邢苗费', '610126200402147988', '安徽', '安徽', '2004-02-14 12:35:20', '085208', '\0', '461.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('401', '卞亓百里', '230503200106162434', '河南、洛阳', '河南、洛阳', '2001-06-16 02:00:45', '085208', '', '439.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('402', '师欧阳武', '610528200211254902', '山东/青岛', '山东/青岛', '2002-11-25 18:01:28', '085208', '\0', '473.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('403', '赏巢易', '411000200402192871', '河北/保定', '河北/保定', '2004-02-19 08:39:51', '085208', '', '453.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('404', '咸诸葛全', '410782200308074424', '福建/泉州', '福建/泉州', '2003-08-07 00:16:19', '085208', '\0', '497.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('405', '阳经揭', '653024200210158596', '湖北/武汉', '湖北/武汉', '2002-10-15 10:11:43', '085208', '', '428.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('406', '毕焦司空', '130700200106178100', '内蒙古', '内蒙古', '2001-06-17 09:58:44', '085208', '\0', '476.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('407', '汲濮慎', '321023200304068351', '北京', '北京', '2003-04-06 19:32:32', '085208', '', '424.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('408', '宁叔孙赵', '220422200304125804', '广东/深圳', '广东/深圳', '2003-04-12 06:55:23', '085208', '\0', '463.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('409', '夏鲜于澹台', '150207200209293372', '山西/太原', '山西/太原', '2002-09-29 21:02:15', '085208', '', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('410', '魏汪楚', '141032200105285006', '内蒙古', '内蒙古', '2001-05-28 09:06:32', '085208', '\0', '438.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('411', '敬檀通', '411121200304284438', '陕西/西安', '陕西/西安', '2003-04-28 02:35:00', '085208', '', '468.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('412', '滑龚都', '320411200403154584', '河北/保定', '河北/保定', '2004-03-15 21:50:01', '085208', '\0', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('413', '栾商钦', '469022200109104611', '云南/大理', '云南/大理', '2001-09-10 21:03:31', '085208', '', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('414', '韶还尤', '542522200302282648', '安徽', '安徽', '2003-02-28 02:43:43', '085208', '\0', '417.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('415', '俞鲜景', '130400200208130435', '北京', '北京', '2002-08-13 07:38:09', '085208', '', '414.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('416', '雷令狐岑', '230712200205134460', '江西/宜春', '江西/宜春', '2002-05-13 14:06:22', '085208', '\0', '441.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('417', '钟离舒殷', '640200200303301414', '广东/深圳', '广东/深圳', '2003-03-30 11:58:05', '085208', '', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('418', '抗于山', '652327200308216007', '重庆', '重庆', '2003-08-21 13:04:07', '085208', '\0', '496.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('419', '左仲孙壤驷', '45122520020702179X', '河北/保定', '河北/保定', '2002-07-02 00:06:45', '085208', '', '453.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('420', '尔朱介沈', '611021200207239688', '天津', '天津', '2002-07-23 00:40:41', '085208', '\0', '448.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('421', '汤第五封', '511681200109222175', '江西/宜春', '江西/宜春', '2001-09-22 16:51:24', '085208', '', '466.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('422', '寇向牧', '430722200109150444', '福建/泉州', '福建/泉州', '2001-09-15 06:04:44', '085208', '\0', '453.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('423', '滕莘刁', '420682200105163431', '上海', '上海', '2001-05-16 18:31:28', '085208', '', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('424', '璩龚舒', '341226200303024908', '山西/太原', '山西/太原', '2003-03-02 11:50:16', '085210', '\0', '428.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('425', '邴雷卓', '371722200403164593', '天津', '天津', '2004-03-16 01:37:11', '085210', '', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('426', '胶俞谯', '632224200110113768', '浙江/舟山', '浙江/舟山', '2001-10-11 08:50:17', '085210', '\0', '447.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('427', '全廉司马', '621121200207229292', '河南、洛阳', '河南、洛阳', '2002-07-22 11:36:08', '085210', '', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('428', '宓景樊', '130582200112204866', '湖南/长沙', '湖南/长沙', '2001-12-20 22:57:13', '085210', '\0', '413.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('429', '曾常陆', '120115200205313275', '辽宁', '辽宁', '2002-05-31 06:50:15', '085210', '', '442.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('430', '公舒桓', '512022200302048023', '安徽', '安徽', '2003-02-04 21:17:07', '085210', '\0', '457.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('431', '夹谷林璩', '141129200210212332', '云南/大理', '云南/大理', '2002-10-21 07:14:18', '085210', '', '466.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('432', '叔孙芮柴', '34012120011220852X', '天津', '天津', '2001-12-20 11:08:23', '085210', '\0', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('433', '虎督穆', '510922200209177899', '山西/太原', '山西/太原', '2002-09-17 11:19:49', '085210', '', '494.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('434', '赏柏广', '542100200201102185', '福建/泉州', '福建/泉州', '2002-01-10 21:59:37', '085210', '\0', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('435', '京贡舒', '542127200312200239', '香港', '香港', '2003-12-20 11:27:43', '085210', '', '495.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('436', '勾仲长郇', '140624200211284746', '四川/成都', '四川/成都', '2002-11-28 01:33:59', '085210', '\0', '456.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('437', '邱秋彭', '320125200401053490', '湖南/长沙', '湖南/长沙', '2004-01-05 02:36:23', '085210', '', '438.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('438', '封柳谯', '370211200206049089', '河南、洛阳', '河南、洛阳', '2002-06-04 01:42:10', '085210', '\0', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('439', '贾胡母宋', '150824200401175738', '江苏/无锡', '江苏/无锡', '2004-01-17 13:39:48', '085210', '', '442.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('440', '郜姚益', '430281200312093089', '湖北/武汉', '湖北/武汉', '2003-12-09 10:08:35', '085210', '\0', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('441', '逄谯丁', '53092120030321573X', '辽宁', '辽宁', '2003-03-21 03:25:15', '085210', '', '482.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('442', '巫马展包', '150422200211164320', '山西/太原', '山西/太原', '2002-11-16 00:19:54', '085210', '\0', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('443', '闾丘荣宁', '130900200208182734', '湖北/武汉', '湖北/武汉', '2002-08-18 12:17:25', '085210', '', '447.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('444', '红充宗正', '610902200108128826', '海南', '海南', '2001-08-12 23:17:34', '085210', '\0', '44.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('445', '哈班畅', '320124200308138813', '湖南/长沙', '湖南/长沙', '2003-08-13 13:39:57', '085210', '', '497.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('446', '邹曹蓬', '623026200206073140', '山西/太原', '山西/太原', '2002-06-07 21:11:19', '085210', '\0', '498.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('447', '訾郏刘', '330922200207083035', '海南', '海南', '2002-07-08 17:14:17', '085210', '', '497.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('448', '贝葛溥', '61102520040224594X', '山东/青岛', '山东/青岛', '2004-02-24 05:31:39', '085210', '\0', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('449', '过长孙太史', '371521200204303856', '广西/柳州', '广西/柳州', '2002-04-30 12:48:37', '085210', '', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('450', '慕谯茅', '42122420040203210X', '香港', '香港', '2004-02-03 02:05:52', '085210', '\0', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('451', '亓郦段', '141125200112233177', '天津', '天津', '2001-12-23 03:08:46', '085210', '', '44.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('452', '向燕壤驷', '331003200208253086', '山西/太原', '山西/太原', '2002-08-25 15:33:09', '085210', '\0', '415.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('453', '曲于乔', '410204200209129358', '湖南/长沙', '湖南/长沙', '2002-09-12 18:06:30', '085210', '', '46.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('454', '柳云南门', '320921200202108784', '福建/泉州', '福建/泉州', '2002-02-10 17:09:42', '081200', '\0', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('455', '华慕褚', '321203200211291170', '广东/深圳', '广东/深圳', '2002-11-29 21:49:07', '081200', '', '428.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('456', '真虎连', '141122200209288589', '四川/成都', '四川/成都', '2002-09-28 01:37:07', '081200', '\0', '494.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('457', '红娄澹台', '532822200204034776', '内蒙古', '内蒙古', '2002-04-03 19:47:33', '081200', '', '417.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('458', '濮邢史', '410000200301054364', '江苏/无锡', '江苏/无锡', '2003-01-05 12:52:29', '081200', '\0', '498.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('459', '安有方', '350425200312196758', '天津', '天津', '2003-12-19 03:59:44', '081200', '', '497.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('460', '溥夔爱', '610404200110192304', '安徽', '安徽', '2001-10-19 16:49:49', '081200', '\0', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('461', '伊左费', '445200200308131650', '内蒙古', '内蒙古', '2003-08-13 16:28:19', '081200', '', '447.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('462', '冒国汤', '510623200110095148', '江苏/无锡', '江苏/无锡', '2001-10-09 08:18:55', '081200', '\0', '410.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('463', '冒恽禄', '37098320020406041X', '香港', '香港', '2002-04-06 22:41:18', '081200', '', '466.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('464', '白哈宁', '511400200109193942', '安徽', '安徽', '2001-09-19 05:08:35', '081200', '\0', '490.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('465', '寿堵敬', '230708200306244517', '江苏/无锡', '江苏/无锡', '2003-06-24 07:57:40', '081200', '', '418.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('466', '卓终怀', '653022200307284529', '云南/大理', '云南/大理', '2003-07-28 03:07:21', '081200', '\0', '414.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('467', '达百里阎', '610204200105119873', '浙江/舟山', '浙江/舟山', '2001-05-11 08:17:55', '081200', '', '467.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('468', '公羊汲万', '610104200311201442', '福建/泉州', '福建/泉州', '2003-11-20 06:45:30', '081200', '\0', '463.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('469', '来娄岳', '542125200207221238', '河北/保定', '河北/保定', '2002-07-22 12:44:34', '081200', '', '422.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('470', '甄武蒯', '431322200202252540', '内蒙古', '内蒙古', '2002-02-25 06:59:19', '081200', '\0', '446.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('471', '窦甄衡', '510800200207310496', '山东/青岛', '山东/青岛', '2002-07-31 11:44:10', '081200', '', '40.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('472', '公孙吴扈', '623022200211166047', '湖北/武汉', '湖北/武汉', '2002-11-16 17:38:13', '081200', '\0', '492.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('473', '綦祝凤', '210105200202097270', '广西/柳州', '广西/柳州', '2002-02-09 03:17:57', '081200', '', '471.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('474', '金晋龚', '370600200308025961', '重庆', '重庆', '2003-08-02 22:22:42', '081200', '\0', '420.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('475', '宾许纪', '420502200210312219', '广东/深圳', '广东/深圳', '2002-10-31 17:45:47', '081200', '', '495.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('476', '安海鲍', '510823200112138800', '江苏/无锡', '江苏/无锡', '2001-12-13 03:51:19', '081200', '\0', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('477', '狄吕谷', '140728200106076970', '辽宁', '辽宁', '2001-06-07 06:31:21', '081200', '', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('478', '毛黄邓', '513222200201143381', '江西/宜春', '江西/宜春', '2002-01-14 22:11:41', '081200', '\0', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('479', '利倪郈', '632521200107250179', '云南/大理', '云南/大理', '2001-07-25 08:46:37', '081200', '', '441.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('480', '左尉迟湛', '410411200205227668', '湖北/武汉', '湖北/武汉', '2002-05-22 13:30:46', '081200', '\0', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('481', '林弓佘', '370800200206114630', '重庆', '重庆', '2002-06-11 10:25:14', '081200', '', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('482', '贲来倪', '522729200310319406', '内蒙古', '内蒙古', '2003-10-31 06:06:49', '081200', '\0', '421.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('483', '赵酆太叔', '150502200206053351', '江西/宜春', '江西/宜春', '2002-06-05 13:14:53', '081200', '', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('484', '冷韩蔚', '511823200304296783', '四川/成都', '四川/成都', '2003-04-29 10:55:15', '083500', '\0', '420.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('485', '李习法', '220282200403269913', '辽宁', '辽宁', '2004-03-26 08:20:49', '083500', '', '417.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('486', '阚贡家', '530524200111190563', '山东/青岛', '山东/青岛', '2001-11-19 21:18:07', '083500', '\0', '498.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('487', '祖温乔', '513323200111224470', '湖南/长沙', '湖南/长沙', '2001-11-22 11:02:30', '083500', '', '432.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('488', '鄢巫马欧', '511602200106299120', '山西/太原', '山西/太原', '2001-06-29 11:46:54', '083500', '\0', '413.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('489', '花綦邝', '652302200403256317', '四川/成都', '四川/成都', '2004-03-25 01:57:54', '083500', '', '463.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('490', '栾任项', '350212200105068849', '广西/柳州', '广西/柳州', '2001-05-06 03:14:34', '083500', '\0', '424.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('491', '姜舒左', '360921200210175191', '北京', '北京', '2002-10-17 17:49:41', '083500', '', '419.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('492', '黎祁简', '361121200202109044', '浙江/舟山', '浙江/舟山', '2002-02-10 12:05:05', '083500', '\0', '488.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('493', '应瞿钦', '440233200204293391', '河南、洛阳', '河南、洛阳', '2002-04-29 18:09:50', '083500', '', '442.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('494', '孟京赫连', '610100200212147289', '天津', '天津', '2002-12-14 06:22:37', '083500', '\0', '439.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('495', '晁仇祁', '33072620010802777X', '香港', '香港', '2001-08-02 16:48:54', '083500', '', '479.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('496', '纵庞申', '500226200111051206', '湖北/武汉', '湖北/武汉', '2001-11-05 13:25:54', '083500', '\0', '410.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('497', '沙程狐', '650104200202151332', '辽宁', '辽宁', '2002-02-15 21:09:20', '083500', '', '490.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('498', '冉荀郝', '140882200107050629', '北京', '北京', '2001-07-05 20:17:36', '083500', '\0', '415.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('499', '饶花漆', '140430200310187016', '湖南/长沙', '湖南/长沙', '2003-10-18 00:00:33', '083500', '', '465.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('500', '南郭韩范', '140602200107078282', '上海', '上海', '2001-07-07 09:30:53', '083500', '\0', '461.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('501', '丁籍挚', '530129200403081211', '重庆', '重庆', '2004-03-08 02:57:41', '083500', '', '461.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('502', '费蹇单', '320482200306162043', '江西/宜春', '江西/宜春', '2003-06-16 01:10:32', '083500', '\0', '466.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('503', '公相里篁', '230833200212099736', '四川/成都', '四川/成都', '2002-12-09 15:13:58', '083500', '', '475.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('504', '林慎柏', '320903200201028886', '江苏/无锡', '江苏/无锡', '2002-01-02 08:27:48', '083500', '\0', '454.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('505', '别钭贾', '441500200111062818', '浙江/舟山', '浙江/舟山', '2001-11-06 01:30:34', '083500', '', '443.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('506', '宇文邝达', '43112120020529188X', '湖北/武汉', '湖北/武汉', '2002-05-29 22:13:42', '083500', '\0', '45.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('507', '祁麦广', '542128200211261557', '天津', '天津', '2002-11-26 13:06:26', '083500', '', '451.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('508', '晋沈是', '630121200308019068', '陕西/西安', '陕西/西安', '2003-08-01 15:46:48', '083500', '\0', '453.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('509', '舜宓郁', '130926200302067058', '陕西/西安', '陕西/西安', '2003-02-06 05:33:47', '083500', '', '468.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('510', '门樊关', '130423200203255225', '海南', '海南', '2002-03-25 08:48:00', '083500', '\0', '489.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('511', '姬连乐', '130721200207145617', '江西/宜春', '江西/宜春', '2002-07-14 02:14:02', '083500', '', '444.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('512', '毋还钟', '522725200207152665', '湖南/长沙', '湖南/长沙', '2002-07-15 16:34:53', '083500', '\0', '472.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('513', '诸虎相里', '410721200307146673', '河北/保定', '河北/保定', '2003-07-14 11:42:08', '083500', '', '473.0', '2021-04-18 11:47:36', '1');
INSERT INTO `initial_student_data` VALUES ('514', '储赏幸', '513233200311309302', '安徽', '安徽', '2003-11-30 05:55:34', '085222', '\0', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('515', '扈楼吴', '431382200212114551', '天津', '天津', '2002-12-11 06:46:30', '085222', '', '480.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('516', '诸葛高皋', '320705200311029988', '浙江/舟山', '浙江/舟山', '2003-11-02 13:52:08', '085222', '\0', '494.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('517', '刁后韦', '131023200104269292', '河北/保定', '河北/保定', '2001-04-26 12:34:03', '085222', '', '48.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('518', '柯王祝', '130582200205251944', '香港', '香港', '2002-05-25 00:00:12', '085222', '\0', '460.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('519', '端木廉覃', '230804200210252934', '上海', '上海', '2002-10-25 10:40:11', '085222', '', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('520', '苌祝亢', '520324200304131946', '内蒙古', '内蒙古', '2003-04-13 05:32:23', '085222', '\0', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('521', '蹇沃禄', '350502200110209312', '江西/宜春', '江西/宜春', '2001-10-20 01:35:38', '085222', '', '411.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('522', '查茹戚', '441826200401225160', '云南/大理', '云南/大理', '2004-01-22 14:19:09', '085222', '\0', '459.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('523', '冒虞芮', '450500200108255097', '内蒙古', '内蒙古', '2001-08-25 18:41:29', '085222', '', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('524', '别逄呼延', '140421200404098261', '四川/成都', '四川/成都', '2004-04-09 16:51:55', '085222', '\0', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('525', '闾鄢余', '41102320020217485X', '陕西/西安', '陕西/西安', '2002-02-17 01:27:08', '085222', '', '430.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('526', '裘东乡闾', '53092420010720416X', '北京', '北京', '2001-07-20 16:09:23', '085222', '\0', '433.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('527', '蔡步栾', '421023200110076197', '湖北/武汉', '湖北/武汉', '2001-10-07 22:34:19', '085222', '', '447.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('528', '亓蓝冼', '510623200303266783', '河南、洛阳', '河南、洛阳', '2003-03-26 11:08:53', '085222', '\0', '442.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('529', '佟任束', '140600200312301719', '安徽', '安徽', '2003-12-30 21:24:27', '085222', '', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('530', '束班康', '371482200203045821', '浙江/舟山', '浙江/舟山', '2002-03-04 07:11:04', '085222', '\0', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('531', '郈眭綦', '222426200307180275', '广东/深圳', '广东/深圳', '2003-07-18 14:24:24', '085222', '', '442.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('532', '慎冒火', '33118120040404530X', '上海', '上海', '2004-04-04 23:16:51', '085222', '\0', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('533', '明习公', '532900200106031050', '河北/保定', '河北/保定', '2001-06-03 10:49:52', '085222', '', '464.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('534', '沈柯眭', '110102200212172243', '海南', '海南', '2002-12-17 14:52:02', '085222', '\0', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('535', '李岑伯', '530322200402161072', '辽宁', '辽宁', '2004-02-16 15:34:11', '085222', '', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('536', '山缪霍', '410823200107289328', '北京', '北京', '2001-07-28 14:55:45', '085222', '\0', '463.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('537', '邱于禹', '22080020020227399X', '山东/青岛', '山东/青岛', '2002-02-27 14:03:50', '085222', '', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('538', '皮左邝', '33010920040401638X', '内蒙古', '内蒙古', '2004-04-01 08:35:37', '085222', '\0', '477.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('539', '汲召揭', '130633200108279572', '海南', '海南', '2001-08-27 11:20:21', '085222', '', '431.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('540', '仉宰父繁', '513400200310292549', '山西/太原', '山西/太原', '2003-10-29 00:02:36', '085222', '\0', '452.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('541', '乜冉祖', '522629200402273577', '天津', '天津', '2004-02-27 05:52:06', '085222', '', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('542', '施勾梁丘', '440229200404083343', '广东/深圳', '广东/深圳', '2004-04-08 22:15:36', '085222', '\0', '461.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('543', '闫壤驷公羊', '510703200109151456', '香港', '香港', '2001-09-15 11:28:17', '085222', '', '434.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('544', '南胶戎', '330703200105015066', '海南', '海南', '2001-05-01 23:26:15', '085240', '\0', '42.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('545', '燕蓟诸', '360727200202273573', '江苏/无锡', '江苏/无锡', '2002-02-27 08:40:27', '085240', '', '482.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('546', '诸窦百里', '430921200202175229', '天津', '天津', '2002-02-17 15:01:04', '085240', '\0', '468.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('547', '宫太史宁', '654301200207136057', '香港', '香港', '2002-07-13 18:55:52', '085240', '', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('548', '练揭姜', '420000200210276027', '内蒙古', '内蒙古', '2002-10-27 01:08:20', '085240', '\0', '430.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('549', '端木衡况', '230710200104308174', '安徽', '安徽', '2001-04-30 06:50:48', '085240', '', '496.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('550', '长孙权原', '430423200403229305', '山西/太原', '山西/太原', '2004-03-22 03:54:21', '085240', '\0', '435.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('551', '葛龚养', '440402200204040657', '湖北/武汉', '湖北/武汉', '2002-04-04 18:59:50', '085240', '', '461.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('552', '谷梁仰卢', '500233200203121340', '河北/保定', '河北/保定', '2002-03-12 01:13:49', '085240', '\0', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('553', '翁侯疏', '440400200307254970', '福建/泉州', '福建/泉州', '2003-07-25 07:11:19', '085240', '', '423.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('554', '何伊浑', '320903200211205727', '内蒙古', '内蒙古', '2002-11-20 12:05:20', '085240', '\0', '480.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('555', '闻邵仲长', '610114200302121697', '重庆', '重庆', '2003-02-12 22:57:55', '085240', '', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('556', '康西门童', '421023200306255867', '山西/太原', '山西/太原', '2003-06-25 01:54:49', '085240', '\0', '473.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('557', '段干鲁蓟', '32041220011031611X', '福建/泉州', '福建/泉州', '2001-10-31 12:53:51', '085240', '', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('558', '邬权福', '120106200111193341', '浙江/舟山', '浙江/舟山', '2001-11-19 18:24:55', '085240', '\0', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('559', '葛沈邬', '350581200306125191', '上海', '上海', '2003-06-12 02:40:07', '085240', '', '490.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('560', '焦石宦', '13063220011203394X', '湖北/武汉', '湖北/武汉', '2001-12-03 05:29:46', '085240', '\0', '415.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('561', '虎浑衡', '440982200309082115', '广东/深圳', '广东/深圳', '2003-09-08 08:11:59', '085240', '', '426.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('562', '易逄涂', '451400200201042040', '香港', '香港', '2002-01-04 16:01:07', '085240', '\0', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('563', '靳辜封', '421121200112030179', '江西/宜春', '江西/宜春', '2001-12-03 02:01:41', '085240', '', '436.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('564', '司空来房', '52042120021113532X', '江苏/无锡', '江苏/无锡', '2002-11-13 13:01:58', '085240', '\0', '47.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('565', '杞东方赏', '120223200310285478', '陕西/西安', '陕西/西安', '2003-10-28 20:52:39', '085240', '', '494.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('566', '亢曹滕', '360822200111248106', '广西/柳州', '广西/柳州', '2001-11-24 17:40:50', '085240', '\0', '468.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('567', '钭宗正郦', '450122200305136210', '湖南/长沙', '湖南/长沙', '2003-05-13 09:51:24', '085240', '', '435.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('568', '农阙闾丘', '420602200112137387', '湖南/长沙', '湖南/长沙', '2001-12-13 06:20:22', '085240', '\0', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('569', '马沈漆', '542330200305030496', '内蒙古', '内蒙古', '2003-05-03 11:54:18', '085240', '', '492.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('570', '陆吕佘', '450500200311060842', '广东/深圳', '广东/深圳', '2003-11-06 07:40:11', '085240', '\0', '40.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('571', '毕蓝莫', '430721200401070859', '河北/保定', '河北/保定', '2004-01-07 12:08:14', '085240', '', '473.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('572', '连法覃', '130132200309132709', '湖南/长沙', '湖南/长沙', '2003-09-13 06:05:23', '085240', '\0', '496.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('573', '白上官蓟', '610626200212310438', '山东/青岛', '山东/青岛', '2002-12-31 07:24:56', '085240', '', '429.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('574', '公西吉丰', '411502200303264163', '辽宁', '辽宁', '2003-03-26 01:46:53', '083000', '\0', '411.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('575', '车正公仪成', '445281200402230071', '福建/泉州', '福建/泉州', '2004-02-23 11:59:06', '083000', '', '493.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('576', '索程别', '350824200307302486', '内蒙古', '内蒙古', '2003-07-30 04:43:23', '083000', '\0', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('577', '朱阴凤', '231102200310070273', '重庆', '重庆', '2003-10-07 18:13:17', '083000', '', '415.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('578', '公良仇弓', '500200200310188949', '海南', '海南', '2003-10-18 11:17:26', '083000', '\0', '423.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('579', '成勾万俟', '441423200305215738', '陕西/西安', '陕西/西安', '2003-05-21 15:31:23', '083000', '', '493.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('580', '翟包禹', '621202200301245009', '香港', '香港', '2003-01-24 22:52:38', '083000', '\0', '422.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('581', '屋庐元宦', '451030200304294431', '江苏/无锡', '江苏/无锡', '2003-04-29 06:05:56', '083000', '', '478.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('582', '韦公乘车', '360200200212102084', '安徽', '安徽', '2002-12-10 12:32:31', '083000', '\0', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('583', '怀郈贺兰', '610626200402228777', '浙江/舟山', '浙江/舟山', '2004-02-22 23:53:11', '083000', '', '462.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('584', '傅屋庐解', '659004200204292940', '河南、洛阳', '河南、洛阳', '2002-04-29 04:03:53', '083000', '\0', '477.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('585', '赵胡母洪', '331181200205017410', '海南', '海南', '2002-05-01 06:03:36', '083000', '', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('586', '蒯申左丘', '150223200210067268', '湖北/武汉', '湖北/武汉', '2002-10-06 17:53:44', '083000', '\0', '420.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('587', '莫公西南', '211381200401196157', '四川/成都', '四川/成都', '2004-01-19 17:33:53', '083000', '', '492.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('588', '敬鱼祝', '361002200207147221', '广西/柳州', '广西/柳州', '2002-07-14 00:18:52', '083000', '\0', '469.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('589', '云山翟', '13028120010927123X', '湖南/长沙', '湖南/长沙', '2001-09-27 03:49:55', '083000', '', '436.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('590', '韩殳甄', '532326200106184402', '河北/保定', '河北/保定', '2001-06-18 01:44:35', '083000', '\0', '422.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('591', '栾盖法', '420107200403054359', '湖北/武汉', '湖北/武汉', '2004-03-05 09:19:16', '083000', '', '44.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('592', '蔚缑任', '500103200212013823', '陕西/西安', '陕西/西安', '2002-12-01 15:29:29', '083000', '\0', '420.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('593', '言武独孤', '371725200304282235', '江苏/无锡', '江苏/无锡', '2003-04-28 01:45:22', '083000', '', '48.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('594', '端有苗', '542225200105242789', '山东/青岛', '山东/青岛', '2001-05-24 05:18:52', '083000', '\0', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('595', '蓝廉寿', '21011320030728569X', '陕西/西安', '陕西/西安', '2003-07-28 19:53:02', '083000', '', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('596', '南门诸连', '370405200202169749', '海南', '海南', '2002-02-16 19:01:29', '083000', '\0', '469.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('597', '枚眭章', '140500200204119834', '广东/深圳', '广东/深圳', '2002-04-11 04:20:18', '083000', '', '411.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('598', '阿陶扶', '420902200111171440', '江西/宜春', '江西/宜春', '2001-11-17 17:59:51', '083000', '\0', '439.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('599', '窦逄敬', '610200200205092131', '浙江/舟山', '浙江/舟山', '2002-05-09 16:38:53', '083000', '', '463.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('600', '昌贡南门', '231282200205281420', '北京', '北京', '2002-05-28 12:44:52', '083000', '\0', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('601', '鲁敖段干', '150304200307135454', '湖南/长沙', '湖南/长沙', '2003-07-13 08:39:10', '083000', '', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('602', '上官韶弓', '460200200207162908', '辽宁', '辽宁', '2002-07-16 18:53:09', '083000', '\0', '476.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('603', '来冷长孙', '340000200305028093', '广西/柳州', '广西/柳州', '2003-05-02 17:35:38', '083000', '', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('604', '皋壤驷勾', '130821200309241146', '浙江/舟山', '浙江/舟山', '2003-09-24 03:34:45', '085224', '\0', '488.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('605', '訾能居', '360826200402271854', '上海', '上海', '2004-02-27 06:49:47', '085224', '', '490.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('606', '程虎堵', '441502200110012088', '江苏/无锡', '江苏/无锡', '2001-10-01 04:16:24', '085224', '\0', '410.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('607', '桑薛梅', '120105200212257811', '香港', '香港', '2002-12-25 04:38:57', '085224', '', '427.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('608', '福勾公仪', '130534200212011868', '天津', '天津', '2002-12-01 16:45:50', '085224', '\0', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('609', '微生崔家', '430800200112272693', '江西/宜春', '江西/宜春', '2001-12-27 01:50:40', '085224', '', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('610', '尤闾丘蔚', '650109200207106487', '海南', '海南', '2002-07-10 12:48:59', '085224', '\0', '430.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('611', '山阮唐', '410182200203064593', '山东/青岛', '山东/青岛', '2002-03-06 04:42:23', '085224', '', '452.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('612', '艾刘怀', '411200200104250165', '海南', '海南', '2001-04-25 14:23:08', '085224', '\0', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('613', '浑万司', '520200200104309651', '上海', '上海', '2001-04-30 07:49:15', '085224', '', '450.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('614', '枚溥有', '14112120021124356X', '广东/深圳', '广东/深圳', '2002-11-24 10:04:49', '085224', '\0', '460.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('615', '殷羊车正', '650100200201152434', '上海', '上海', '2002-01-15 18:31:11', '085224', '', '454.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('616', '艾羊舌蒙', '330203200107259868', '江苏/无锡', '江苏/无锡', '2001-07-25 09:52:36', '085224', '\0', '498.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('617', '汪宁林', '220303200205303076', '江西/宜春', '江西/宜春', '2002-05-30 00:04:30', '085224', '', '433.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('618', '澹台索独孤', '320111200208171366', '上海', '上海', '2002-08-17 16:11:23', '085224', '\0', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('619', '农景纵', '341225200111042072', '河南、洛阳', '河南、洛阳', '2001-11-04 23:41:44', '085224', '', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('620', '抗咸是', '231025200108145103', '云南/大理', '云南/大理', '2001-08-14 15:14:30', '085224', '\0', '442.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('621', '牟澹台司徒', '451202200312067832', '海南', '海南', '2003-12-06 14:06:37', '085224', '', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('622', '郦萧公良', '420103200110020467', '浙江/舟山', '浙江/舟山', '2001-10-02 14:18:19', '085224', '\0', '456.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('623', '轩辕折上官', '210123200109235977', '重庆', '重庆', '2001-09-23 08:00:10', '085224', '', '414.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('624', '揭巨牟', '350111200312164129', '上海', '上海', '2003-12-16 03:42:36', '085224', '\0', '494.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('625', '雍昝仲长', '130822200311055875', '江苏/无锡', '江苏/无锡', '2003-11-05 10:47:30', '085224', '', '491.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('626', '夔宰父司空', '540127200305172186', '辽宁', '辽宁', '2003-05-17 11:38:08', '085224', '\0', '421.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('627', '谭阴仪', '12010320011005165X', '海南', '海南', '2001-10-05 07:12:45', '085224', '', '431.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('628', '是彭匡', '44030020010902508X', '陕西/西安', '陕西/西安', '2001-09-02 16:32:32', '085224', '\0', '463.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('629', '储福阎', '511826200204022274', '天津', '天津', '2002-04-02 06:54:34', '085224', '', '417.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('630', '壤驷弘况', '53310220030905368X', '四川/成都', '四川/成都', '2003-09-05 18:42:47', '085224', '\0', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('631', '支季祭', '130321200107313554', '上海', '上海', '2001-07-31 19:41:24', '085224', '', '44.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('632', '刘罗况', '130322200211210420', '湖北/武汉', '湖北/武汉', '2002-11-21 08:42:26', '085224', '\0', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('633', '农燕木', '320584200201108559', '上海', '上海', '2002-01-10 04:21:20', '085224', '', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('634', '弥东门佟', '542225200401195841', '海南', '海南', '2004-01-19 18:39:07', '020100', '\0', '431.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('635', '阚太史翟', '131082200208158992', '福建/泉州', '福建/泉州', '2002-08-15 23:57:47', '020100', '', '450.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('636', '赏高鲜于', '500119200209199920', '福建/泉州', '福建/泉州', '2002-09-19 06:08:28', '020100', '\0', '453.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('637', '达公西冉', '654028200308036171', '重庆', '重庆', '2003-08-03 13:07:59', '020100', '', '433.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('638', '关沃公孙', '441502200210294422', '江西/宜春', '江西/宜春', '2002-10-29 16:05:45', '020100', '\0', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('639', '爱展王', '653121200309068191', '陕西/西安', '陕西/西安', '2003-09-06 07:28:20', '020100', '', '457.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('640', '寿闻人诸葛', '511126200108185606', '四川/成都', '四川/成都', '2001-08-18 13:19:26', '020100', '\0', '478.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('641', '随利阙', '230400200211254838', '云南/大理', '云南/大理', '2002-11-25 01:24:36', '020100', '', '483.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('642', '闵仲长罗', '120221200301024329', '山东/青岛', '山东/青岛', '2003-01-02 09:35:20', '020100', '\0', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('643', '钦濮阳欧', '522401200204307898', '浙江/舟山', '浙江/舟山', '2002-04-30 04:44:15', '020100', '', '417.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('644', '轩辕秦周', '511024200201100082', '海南', '海南', '2002-01-10 15:52:13', '020100', '\0', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('645', '侯百里归', '370902200208062772', '江西/宜春', '江西/宜春', '2002-08-06 07:50:43', '020100', '', '482.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('646', '堵柯卜', '230109200312286223', '上海', '上海', '2003-12-28 13:04:47', '020100', '\0', '491.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('647', '东乡陈越', '230123200306025411', '安徽', '安徽', '2003-06-02 17:08:53', '020100', '', '415.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('648', '昝公孙汝', '654025200205151346', '河北/保定', '河北/保定', '2002-05-15 08:52:29', '020100', '\0', '494.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('649', '须南门耿', '141026200111182137', '重庆', '重庆', '2001-11-18 21:24:05', '020100', '', '471.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('650', '余阮叶', '430381200303107165', '辽宁', '辽宁', '2003-03-10 06:16:49', '020100', '\0', '421.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('651', '纪景孙', '530827200309182038', '福建/泉州', '福建/泉州', '2003-09-18 16:35:56', '020100', '', '415.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('652', '晏晁是', '421024200302052743', '陕西/西安', '陕西/西安', '2003-02-05 23:42:56', '020100', '\0', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('653', '火茹慕容', '632525200307226232', '广东/深圳', '广东/深圳', '2003-07-22 19:51:33', '020100', '', '415.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('654', '穆华饶', '120221200312258243', '福建/泉州', '福建/泉州', '2003-12-25 23:35:41', '020100', '\0', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('655', '闾席阎', '331024200305069610', '浙江/舟山', '浙江/舟山', '2003-05-06 13:11:33', '020100', '', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('656', '拓跋周高', '421024200304190808', '陕西/西安', '陕西/西安', '2003-04-19 00:02:22', '020100', '\0', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('657', '毋淳于闵', '430481200204201135', '天津', '天津', '2002-04-20 00:15:41', '020100', '', '446.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('658', '璩益弘', '330424200306266668', '江西/宜春', '江西/宜春', '2003-06-26 09:20:29', '020100', '\0', '424.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('659', '单范淳于', '320681200208227717', '浙江/舟山', '浙江/舟山', '2002-08-22 17:13:33', '020100', '', '454.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('660', '庞潘毋丘', '510682200309267648', '河北/保定', '河北/保定', '2003-09-26 15:39:02', '020100', '\0', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('661', '燕惠燕', '41110420030210285X', '香港', '香港', '2003-02-10 20:29:43', '020100', '', '472.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('662', '蒙东方牧', '62050020030107736X', '山西/太原', '山西/太原', '2003-01-07 20:59:19', '020100', '\0', '422.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('663', '枚文狄', '440825200308024471', '四川/成都', '四川/成都', '2003-08-02 07:40:33', '020100', '', '413.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('664', '敬厉鄂', '140105200210258283', '河南、洛阳', '河南、洛阳', '2002-10-25 13:01:05', '085236', '\0', '495.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('665', '荣刘胡', '222406200207113555', '山东/青岛', '山东/青岛', '2002-07-11 01:14:23', '085236', '', '413.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('666', '晁归邹', '500241200404113929', '浙江/舟山', '浙江/舟山', '2004-04-11 12:06:46', '085236', '\0', '428.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('667', '乐相福', '441624200211148236', '陕西/西安', '陕西/西安', '2002-11-14 13:24:41', '085236', '', '412.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('668', '武宾墨', '140721200207106361', '河南、洛阳', '河南、洛阳', '2002-07-10 03:07:15', '085236', '\0', '414.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('669', '计晏弓', '370304200312126035', '辽宁', '辽宁', '2003-12-12 15:29:38', '085236', '', '466.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('670', '司马牟童', '440703200208103902', '湖南/长沙', '湖南/长沙', '2002-08-10 04:48:37', '085236', '\0', '470.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('671', '季夏廉', '350423200305042897', '河北/保定', '河北/保定', '2003-05-04 23:20:28', '085236', '', '448.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('672', '牛端木广', '41092220030524930X', '内蒙古', '内蒙古', '2003-05-24 17:21:04', '085236', '\0', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('673', '相吉钦', '410882200104287271', '广西/柳州', '广西/柳州', '2001-04-28 10:53:58', '085236', '', '488.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('674', '邝佴郁', '230102200110199303', '重庆', '重庆', '2001-10-19 15:16:15', '085236', '\0', '460.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('675', '古阎溥', '231003200204199437', '河南、洛阳', '河南、洛阳', '2002-04-19 11:43:17', '085236', '', '40.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('676', '寇吕阮', '653000200403032841', '安徽', '安徽', '2004-03-03 08:11:29', '085236', '\0', '48.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('677', '韩蓟汪', '21021320031129173X', '江苏/无锡', '江苏/无锡', '2003-11-29 16:05:17', '085236', '', '450.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('678', '郏澹台徐', '370203200306177627', '海南', '海南', '2003-06-17 02:14:25', '085236', '\0', '48.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('679', '严樊公仪', '370502200212139937', '河南、洛阳', '河南、洛阳', '2002-12-13 22:50:28', '085236', '', '475.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('680', '乐韶祖', '431123200107211949', '上海', '上海', '2001-07-21 13:23:25', '085236', '\0', '492.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('681', '孙贝倪', '130521200111223931', '陕西/西安', '陕西/西安', '2001-11-22 04:15:40', '085236', '', '438.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('682', '高后应', '451481200209192201', '福建/泉州', '福建/泉州', '2002-09-19 06:05:04', '085236', '\0', '459.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('683', '舒漆尹', '230123200212277474', '云南/大理', '云南/大理', '2002-12-27 13:18:14', '085236', '', '489.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('684', '年侯左丘', '350923200404134567', '山东/青岛', '山东/青岛', '2004-04-13 04:20:45', '085236', '\0', '469.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('685', '仉邹窦', '41168120040206001X', '陕西/西安', '陕西/西安', '2004-02-06 21:13:38', '085236', '', '424.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('686', '郁京亢', '610203200111281387', '云南/大理', '云南/大理', '2001-11-28 16:10:46', '085236', '\0', '411.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('687', '茹贺伯', '370921200201160336', '天津', '天津', '2002-01-16 08:07:31', '085236', '', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('688', '百里魏车', '210881200106284900', '北京', '北京', '2001-06-28 09:25:40', '085236', '\0', '478.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('689', '冀舒商', '513433200402069755', '湖北/武汉', '湖北/武汉', '2004-02-06 03:06:40', '085236', '', '468.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('690', '郁祖连', '511322200307033985', '香港', '香港', '2003-07-03 10:20:12', '085236', '\0', '45.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('691', '昌芮莘', '140930200107197179', '辽宁', '辽宁', '2001-07-19 16:51:39', '085236', '', '489.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('692', '容尤董', '430405200212182580', '广西/柳州', '广西/柳州', '2002-12-18 16:56:36', '085236', '\0', '454.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('693', '墨双言', '620921200105207693', '上海', '上海', '2001-05-20 22:57:39', '085236', '', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('694', '曲桓咸', '530111200105076728', '广东/深圳', '广东/深圳', '2001-05-07 03:59:49', '087100', '\0', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('695', '陆刘第五', '421127200403287431', '陕西/西安', '陕西/西安', '2004-03-28 13:59:21', '087100', '', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('696', '何师隆', '340121200211038028', '云南/大理', '云南/大理', '2002-11-03 20:28:49', '087100', '\0', '441.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('697', '诸甘车正', '520114200106046237', '云南/大理', '云南/大理', '2001-06-04 19:27:38', '087100', '', '450.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('698', '席尔朱颜', '33078220020123394X', '广东/深圳', '广东/深圳', '2002-01-23 12:53:45', '087100', '\0', '447.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('699', '白公卜', '540100200304248310', '江苏/无锡', '江苏/无锡', '2003-04-24 17:46:07', '087100', '', '436.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('700', '印薛苌', '511681200311064588', '安徽', '安徽', '2003-11-06 11:19:54', '087100', '\0', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('701', '钦郗盖', '522423200211155413', '广西/柳州', '广西/柳州', '2002-11-15 22:58:41', '087100', '', '452.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('702', '谈伊麻', '320111200402248005', '辽宁', '辽宁', '2004-02-24 18:55:51', '087100', '\0', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('703', '骆水隆', '542425200212181919', '天津', '天津', '2002-12-18 07:46:37', '087100', '', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('704', '唐公冶屈突', '430723200301315524', '江苏/无锡', '江苏/无锡', '2003-01-31 04:43:52', '087100', '\0', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('705', '胶宾佟', '210105200202032530', '山东/青岛', '山东/青岛', '2002-02-03 16:09:39', '087100', '', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('706', '木海揭', '620400200402098180', '山西/太原', '山西/太原', '2004-02-09 22:56:47', '087100', '\0', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('707', '童令狐萧', '330824200111127694', '上海', '上海', '2001-11-12 15:21:52', '087100', '', '475.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('708', '况兀官司城', '210503200301012323', '江西/宜春', '江西/宜春', '2003-01-01 12:36:57', '087100', '\0', '42.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('709', '甄应东门', '431321200311112858', '内蒙古', '内蒙古', '2003-11-11 00:18:00', '087100', '', '433.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('710', '令狐浦令狐', '440705200308260641', '湖南/长沙', '湖南/长沙', '2003-08-26 22:08:53', '087100', '\0', '441.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('711', '邝卓养', '450205200310140619', '广东/深圳', '广东/深圳', '2003-10-14 19:14:13', '087100', '', '460.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('712', '耿蒋苌', '341282200401232867', '广东/深圳', '广东/深圳', '2004-01-23 13:37:41', '087100', '\0', '456.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('713', '鲜于万丁', '420984200311308354', '江西/宜春', '江西/宜春', '2003-11-30 16:40:48', '087100', '', '452.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('714', '东方熊邰', '320102200110281288', '香港', '香港', '2001-10-28 07:52:38', '087100', '\0', '493.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('715', '佟荣桂', '451421200305188759', '香港', '香港', '2003-05-18 16:22:42', '087100', '', '45.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('716', '闫陶杭', '410183200203190043', '浙江/舟山', '浙江/舟山', '2002-03-19 13:05:08', '087100', '\0', '426.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('717', '任漆雕杜', '451281200209051256', '海南', '海南', '2002-09-05 10:08:04', '087100', '', '417.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('718', '巢扈侴', '35042820010806348X', '海南', '海南', '2001-08-06 01:27:21', '087100', '\0', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('719', '禄暴宗', '150981200311197231', '湖南/长沙', '湖南/长沙', '2003-11-19 08:32:34', '087100', '', '47.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('720', '明檀百里', '341600200303032543', '安徽', '安徽', '2003-03-03 15:38:03', '087100', '\0', '433.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('721', '年夔晏', '230421200204283958', '云南/大理', '云南/大理', '2002-04-28 16:01:45', '087100', '', '469.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('722', '鄢羿帅', '542221200403314423', '河北/保定', '河北/保定', '2004-03-31 19:49:18', '087100', '\0', '433.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('723', '东方唐尹', '620923200404087938', '湖北/武汉', '湖北/武汉', '2004-04-08 22:31:47', '087100', '', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('724', '宿苏百里', '433126200303030580', '浙江/舟山', '浙江/舟山', '2003-03-03 15:17:44', '120200', '\0', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('725', '微生祖邬', '445302200301121159', '广西/柳州', '广西/柳州', '2003-01-12 17:52:54', '120200', '', '467.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('726', '尔朱皇甫印', '230421200108130601', '辽宁', '辽宁', '2001-08-13 00:40:36', '120200', '\0', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('727', '暨漆雕邵', '44080420020115467X', '河北/保定', '河北/保定', '2002-01-15 12:27:39', '120200', '', '438.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('728', '竺公良凤', '430122200308254729', '上海', '上海', '2003-08-25 05:38:29', '120200', '\0', '477.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('729', '满袁曹', '13082720030208553X', '四川/成都', '四川/成都', '2003-02-08 16:51:02', '120200', '', '415.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('730', '冷巫马戴', '421002200312224483', '湖北/武汉', '湖北/武汉', '2003-12-22 22:51:35', '120200', '\0', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('731', '竺督随', '210602200109163411', '河北/保定', '河北/保定', '2001-09-16 14:16:39', '120200', '', '496.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('732', '揭尹戚', '430124200201090800', '江苏/无锡', '江苏/无锡', '2002-01-09 06:48:11', '120200', '\0', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('733', '巩阴云', '411282200204103298', '江西/宜春', '江西/宜春', '2002-04-10 08:17:52', '120200', '', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('734', '孔巢蔡', '522230200403228667', '海南', '海南', '2004-03-22 23:40:20', '120200', '\0', '462.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('735', '薛弥司寇', '13072820010928663X', '安徽', '安徽', '2001-09-28 04:40:45', '120200', '', '498.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('736', '孟况汝', '410704200307160460', '湖北/武汉', '湖北/武汉', '2003-07-16 04:02:07', '120200', '\0', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('737', '亢孟铁', '610923200205076214', '广东/深圳', '广东/深圳', '2002-05-07 10:39:27', '120200', '', '480.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('738', '蓬武阳', '513229200107257782', '辽宁', '辽宁', '2001-07-25 08:52:08', '120200', '\0', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('739', '羿鞠聂', '431000200205289790', '上海', '上海', '2002-05-28 14:43:23', '120200', '', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('740', '滕邢岑', '150823200311032163', '山西/太原', '山西/太原', '2003-11-03 22:33:48', '120200', '\0', '464.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('741', '挚党宣', '513422200302109933', '山西/太原', '山西/太原', '2003-02-10 21:49:00', '120200', '', '490.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('742', '蒯上官眭', '360823200110281867', '湖南/长沙', '湖南/长沙', '2001-10-28 17:01:46', '120200', '\0', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('743', '冒东郭苌', '341622200210312536', '北京', '北京', '2002-10-31 10:17:36', '120200', '', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('744', '边公西侯', '410503200310056480', '辽宁', '辽宁', '2003-10-05 05:24:59', '120200', '\0', '481.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('745', '濮抗金', '410300200401193755', '香港', '香港', '2004-01-19 12:14:07', '120200', '', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('746', '邬段百里', '623027200203292942', '云南/大理', '云南/大理', '2002-03-29 06:02:25', '120200', '\0', '45.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('747', '空诸闵', '451300200404065570', '江苏/无锡', '江苏/无锡', '2004-04-06 05:40:35', '120200', '', '411.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('748', '柏姬展', '451200200106110446', '江苏/无锡', '江苏/无锡', '2001-06-11 18:58:48', '120200', '\0', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('749', '兀官须邓', '53282320020421057X', '海南', '海南', '2002-04-21 10:01:26', '120200', '', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('750', '冉干祭', '469026200203155445', '福建/泉州', '福建/泉州', '2002-03-15 15:23:45', '120200', '\0', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('751', '经商东方', '511827200404042997', '湖北/武汉', '湖北/武汉', '2004-04-04 06:26:35', '120200', '', '428.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('752', '白欧羊', '220381200310310269', '江苏/无锡', '江苏/无锡', '2003-10-31 08:58:44', '120200', '\0', '445.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('753', '车俞佴', '130129200112269316', '湖南/长沙', '湖南/长沙', '2001-12-26 15:11:29', '120200', '', '470.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('754', '管百里养', '150626200105106748', '内蒙古', '内蒙古', '2001-05-10 16:02:32', '085216', '\0', '421.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('755', '归鞠邱', '420626200305055119', '江苏/无锡', '江苏/无锡', '2003-05-05 19:52:15', '085216', '', '450.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('756', '常樊籍', '532327200402163426', '云南/大理', '云南/大理', '2004-02-16 03:36:40', '085216', '\0', '441.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('757', '邵闾缑', '511112200210027630', '湖北/武汉', '湖北/武汉', '2002-10-02 02:17:27', '085216', '', '460.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('758', '水韦舜', '360729200401256365', '江苏/无锡', '江苏/无锡', '2004-01-25 05:28:49', '085216', '\0', '44.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('759', '畅邝阿', '533103200105186276', '广西/柳州', '广西/柳州', '2001-05-18 05:06:46', '085216', '', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('760', '申常查', '532525200109215782', '安徽', '安徽', '2001-09-21 22:32:56', '085216', '\0', '436.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('761', '申屠仉宋', '22070020020318765X', '山东/青岛', '山东/青岛', '2002-03-18 00:40:15', '085216', '', '449.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('762', '仲孙乐臧', '532931200202077643', '山西/太原', '山西/太原', '2002-02-07 11:50:54', '085216', '\0', '490.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('763', '梁奚滕', '210283200308193775', '云南/大理', '云南/大理', '2003-08-19 13:20:30', '085216', '', '498.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('764', '寇牛阚', '340504200310114941', '浙江/舟山', '浙江/舟山', '2003-10-11 00:31:59', '085216', '\0', '483.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('765', '乜真闵', '350782200208041311', '陕西/西安', '陕西/西安', '2002-08-04 19:33:50', '085216', '', '44.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('766', '召纵迟', '330881200401305249', '四川/成都', '四川/成都', '2004-01-30 00:42:08', '085216', '\0', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('767', '司翁欧', '340203200305023997', '江苏/无锡', '江苏/无锡', '2003-05-02 18:04:56', '085216', '', '490.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('768', '万俟佘郈', '371626200205099540', '安徽', '安徽', '2002-05-09 04:16:14', '085216', '\0', '439.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('769', '后密仉', '510922200310202112', '浙江/舟山', '浙江/舟山', '2003-10-20 11:13:09', '085216', '', '423.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('770', '葛夹谷巫', '320202200312292044', '江苏/无锡', '江苏/无锡', '2003-12-29 08:12:50', '085216', '\0', '42.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('771', '黄苍练', '341623200403038015', '江西/宜春', '江西/宜春', '2004-03-03 09:57:12', '085216', '', '483.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('772', '党关乜', '220100200211114108', '陕西/西安', '陕西/西安', '2002-11-11 07:21:35', '085216', '\0', '450.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('773', '单于逄伊', '441426200109140694', '陕西/西安', '陕西/西安', '2001-09-14 11:18:29', '085216', '', '411.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('774', '马文琴', '510802200311273288', '天津', '天津', '2003-11-27 02:30:03', '085216', '\0', '492.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('775', '井廉柏', '532626200301139558', '河北/保定', '河北/保定', '2003-01-13 12:02:12', '085216', '', '489.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('776', '顾练华', '34071120030316542X', '陕西/西安', '陕西/西安', '2003-03-16 17:57:26', '085216', '\0', '426.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('777', '迟安饶', '340500200204207698', '陕西/西安', '陕西/西安', '2002-04-20 13:48:35', '085216', '', '464.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('778', '权秋松', '350921200208073602', '湖南/长沙', '湖南/长沙', '2002-08-07 00:48:53', '085216', '\0', '411.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('779', '伯毕左', '230713200312247053', '云南/大理', '云南/大理', '2003-12-24 02:05:23', '085216', '', '418.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('780', '艾漆钟', '341602200310294560', '江西/宜春', '江西/宜春', '2003-10-29 03:54:58', '085216', '\0', '466.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('781', '介有伏', '152202200303060516', '湖南/长沙', '湖南/长沙', '2003-03-06 19:36:33', '085216', '', '417.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('782', '颜宗正明', '445222200310073160', '江西/宜春', '江西/宜春', '2003-10-07 04:17:55', '085216', '\0', '420.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('783', '兀官胥督', '131003200303039259', '云南/大理', '云南/大理', '2003-03-03 09:49:50', '085216', '', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('784', '覃霍练', '510811200308127905', '广西/柳州', '广西/柳州', '2003-08-12 14:05:38', '080202', '\0', '473.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('785', '郁贲鲁', '371524200204294899', '广西/柳州', '广西/柳州', '2002-04-29 20:01:03', '080202', '', '42.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('786', '计阎禹', '310106200201208602', '江苏/无锡', '江苏/无锡', '2002-01-20 02:02:20', '080202', '\0', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('787', '赫连闾越', '330283200204246614', '四川/成都', '四川/成都', '2002-04-24 03:29:48', '080202', '', '459.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('788', '边颜令狐', '210921200305097285', '河南、洛阳', '河南、洛阳', '2003-05-09 00:34:26', '080202', '\0', '45.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('789', '那彭崔', '522623200203292590', '云南/大理', '云南/大理', '2002-03-29 06:25:54', '080202', '', '415.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('790', '郜纪齐', '150300200201070060', '云南/大理', '云南/大理', '2002-01-07 02:07:28', '080202', '\0', '44.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('791', '左莘须', '440903200310274135', '香港', '香港', '2003-10-27 13:54:44', '080202', '', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('792', '宦潘钮', '610300200302220807', '天津', '天津', '2003-02-22 16:29:49', '080202', '\0', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('793', '仲长戚支', '610323200111294410', '广西/柳州', '广西/柳州', '2001-11-29 19:21:41', '080202', '', '472.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('794', '公仪孟禹', '371002200210272463', '河南、洛阳', '河南、洛阳', '2002-10-27 17:26:13', '080202', '\0', '435.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('795', '邬唐宰父', '513424200109294674', '浙江/舟山', '浙江/舟山', '2001-09-29 02:16:45', '080202', '', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('796', '宋敖充', '370883200201082140', '浙江/舟山', '浙江/舟山', '2002-01-08 18:14:12', '080202', '\0', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('797', '怀卢游', '120221200302090838', '上海', '上海', '2003-02-09 04:01:55', '080202', '', '491.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('798', '南糜蔺', '513400200203258601', '北京', '北京', '2002-03-25 23:53:18', '080202', '\0', '450.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('799', '晏公良杜', '533422200402265670', '四川/成都', '四川/成都', '2004-02-26 03:53:56', '080202', '', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('800', '聂隗段', '632323200208265689', '云南/大理', '云南/大理', '2002-08-26 06:37:47', '080202', '\0', '481.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('801', '沈夹谷祝', '140524200401144597', '广西/柳州', '广西/柳州', '2004-01-14 01:47:28', '080202', '', '434.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('802', '卓桑俞', '610621200203224966', '河北/保定', '河北/保定', '2002-03-22 22:49:31', '080202', '\0', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('803', '轩辕闵毋', '410802200307314979', '湖南/长沙', '湖南/长沙', '2003-07-31 18:03:44', '080202', '', '477.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('804', '颛孙游杜', '130435200311121326', '陕西/西安', '陕西/西安', '2003-11-12 11:59:02', '080202', '\0', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('805', '福仲单', '41142120020426441X', '安徽', '安徽', '2002-04-26 00:31:43', '080202', '', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('806', '令狐独孤亓', '411628200204144421', '湖南/长沙', '湖南/长沙', '2002-04-14 21:19:00', '080202', '\0', '441.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('807', '归怀酆', '360322200108309978', '河南、洛阳', '河南、洛阳', '2001-08-30 20:18:40', '080202', '', '460.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('808', '狐滕巢', '230203200306087524', '陕西/西安', '陕西/西安', '2003-06-08 21:34:38', '080202', '\0', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('809', '况纪宰', '450902200211059654', '安徽', '安徽', '2002-11-05 09:20:13', '080202', '', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('810', '漆雕畅雷', '542338200303206647', '云南/大理', '云南/大理', '2003-03-20 02:02:55', '080202', '\0', '489.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('811', '滕敖濮', '610822200301317696', '河南、洛阳', '河南、洛阳', '2003-01-31 11:17:59', '080202', '', '461.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('812', '公良寇皮', '130604200404138860', '河北/保定', '河北/保定', '2004-04-13 14:22:27', '080202', '\0', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('813', '翟缑庄', '360722200212225936', '河北/保定', '河北/保定', '2002-12-22 04:37:38', '080202', '', '433.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('814', '封巴屠', '610926200212228908', '河北/保定', '河北/保定', '2002-12-22 20:32:23', '080204', '\0', '448.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('815', '计秦靳', '140781200207049017', '上海', '上海', '2002-07-04 10:27:58', '080204', '', '477.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('816', '宫昝祭', '610724200301276481', '河南、洛阳', '河南、洛阳', '2003-01-27 21:40:17', '080204', '\0', '449.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('817', '盛冷鲁', '440115200301315275', '河北/保定', '河北/保定', '2003-01-31 12:54:57', '080204', '', '468.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('818', '于司寇翟', '360103200212139948', '广西/柳州', '广西/柳州', '2002-12-13 09:23:54', '080204', '\0', '477.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('819', '栾裘边', '350725200401105295', '香港', '香港', '2004-01-10 16:44:37', '080204', '', '442.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('820', '公乘高涂', '371622200311177287', '香港', '香港', '2003-11-17 06:23:53', '080204', '\0', '453.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('821', '田胡母沙', '411224200106261352', '海南', '海南', '2001-06-26 13:30:28', '080204', '', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('822', '令狐章钟离', '431100200108188061', '河南、洛阳', '河南、洛阳', '2001-08-18 15:20:23', '080204', '\0', '445.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('823', '洪虎萧', '441424200205307252', '河南、洛阳', '河南、洛阳', '2002-05-30 00:31:12', '080204', '', '482.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('824', '壤驷储文', '652824200105103561', '广西/柳州', '广西/柳州', '2001-05-10 19:46:46', '080204', '\0', '493.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('825', '苌羊舌童', '340303200203226051', '广东/深圳', '广东/深圳', '2002-03-22 10:17:23', '080204', '', '472.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('826', '索米萧', '632522200108018124', '重庆', '重庆', '2001-08-01 04:41:03', '080204', '\0', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('827', '池司城卞', '610124200301283494', '内蒙古', '内蒙古', '2003-01-28 09:04:09', '080204', '', '472.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('828', '揭逄侴', '610327200108239862', '北京', '北京', '2001-08-23 08:05:43', '080204', '\0', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('829', '太叔蔡养', '230126200203167838', '湖北/武汉', '湖北/武汉', '2002-03-16 15:51:22', '080204', '', '422.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('830', '种张澹台', '33070020020328894X', '香港', '香港', '2002-03-28 23:03:05', '080204', '\0', '460.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('831', '眭嵇贝', '532524200208241437', '浙江/舟山', '浙江/舟山', '2002-08-24 05:40:26', '080204', '', '452.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('832', '虎有尤', '420626200211160567', '广东/深圳', '广东/深圳', '2002-11-16 04:14:34', '080204', '\0', '434.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('833', '劳敬阎', '330683200210092051', '云南/大理', '云南/大理', '2002-10-09 21:10:58', '080204', '', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('834', '邹雷相里', '350213200211042366', '山西/太原', '山西/太原', '2002-11-04 00:58:08', '080204', '\0', '423.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('835', '孟兀官张廖', '610526200206281233', '河北/保定', '河北/保定', '2002-06-28 16:39:19', '080204', '', '47.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('836', '尔朱潘乌', '371300200201241642', '天津', '天津', '2002-01-24 16:49:19', '080204', '\0', '454.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('837', '狄南宫鄢', '610329200107246879', '安徽', '安徽', '2001-07-24 18:03:59', '080204', '', '495.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('838', '江居田', '130927200108071762', '广东/深圳', '广东/深圳', '2001-08-07 03:07:16', '080204', '\0', '475.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('839', '南法挚', '230407200208033035', '山西/太原', '山西/太原', '2002-08-03 02:31:54', '080204', '', '434.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('840', '过南郭万', '410804200210017524', '山东/青岛', '山东/青岛', '2002-10-01 03:28:28', '080204', '\0', '492.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('841', '司徒濮阳费', '210421200205010990', '广东/深圳', '广东/深圳', '2002-05-01 15:33:22', '080204', '', '422.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('842', '蒙计闾', '320721200111147728', '广西/柳州', '广西/柳州', '2001-11-14 08:56:55', '080204', '\0', '46.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('843', '那傅任', '21032320010827895X', '上海', '上海', '2001-08-27 15:31:21', '080204', '', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('844', '介平习', '422801200108086085', '广东/深圳', '广东/深圳', '2001-08-08 21:10:31', '020401', '\0', '446.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('845', '云卓融', '620722200302099471', '海南', '海南', '2003-02-09 09:35:11', '020401', '', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('846', '叶融巨', '230303200312171621', '海南', '海南', '2003-12-17 10:19:16', '020401', '\0', '440.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('847', '蒙赏仓', '410106200305027498', '香港', '香港', '2003-05-02 23:37:00', '020401', '', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('848', '富戈支', '542622200112249922', '内蒙古', '内蒙古', '2001-12-24 16:12:52', '020401', '\0', '480.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('849', '姬羊蒯', '610116200110141610', '陕西/西安', '陕西/西安', '2001-10-14 03:02:12', '020401', '', '476.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('850', '元司空暨', '440184200310254147', '福建/泉州', '福建/泉州', '2003-10-25 06:00:06', '020401', '\0', '439.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('851', '凌郭边', '650104200112046678', '陕西/西安', '陕西/西安', '2001-12-04 23:25:33', '020401', '', '452.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('852', '倪景公孙', '330602200401173948', '重庆', '重庆', '2004-01-17 06:34:35', '020401', '\0', '454.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('853', '段干席仲孙', '810000200201159956', '天津', '天津', '2002-01-15 23:25:49', '020401', '', '478.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('854', '费南郭羿', '37028520020913374X', '江西/宜春', '江西/宜春', '2002-09-13 18:37:16', '020401', '\0', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('855', '爱那胡', '430103200212012830', '河南、洛阳', '河南、洛阳', '2002-12-01 02:02:24', '020401', '', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('856', '荆公孙令狐', '37142820021214708X', '河北/保定', '河北/保定', '2002-12-14 11:26:19', '020401', '\0', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('857', '邵钦赵', '62010320030330181X', '香港', '香港', '2003-03-30 07:30:04', '020401', '', '490.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('858', '鱼国边', '620623200209098643', '安徽', '安徽', '2002-09-09 13:52:57', '020401', '\0', '477.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('859', '聂公良戈', '150722200204257330', '广西/柳州', '广西/柳州', '2002-04-25 22:06:18', '020401', '', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('860', '田管方', '431100200304088260', '陕西/西安', '陕西/西安', '2003-04-08 12:30:34', '020401', '\0', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('861', '怀邵公良', '340721200308289571', '福建/泉州', '福建/泉州', '2003-08-28 15:50:13', '020401', '', '435.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('862', '钭夹谷阎', '513336200206300765', '天津', '天津', '2002-06-30 03:42:47', '020401', '\0', '483.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('863', '漆雕佟师', '141030200207254857', '江苏/无锡', '江苏/无锡', '2002-07-25 02:20:03', '020401', '', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('864', '铁子车林', '620423200107249542', '四川/成都', '四川/成都', '2001-07-24 09:55:26', '020401', '\0', '40.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('865', '慕谈包', '533323200202277870', '河北/保定', '河北/保定', '2002-02-27 10:49:15', '020401', '', '466.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('866', '邬马金', '370832200212292362', '内蒙古', '内蒙古', '2002-12-29 09:04:49', '020401', '\0', '445.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('867', '孟壤驷干', '320107200206256935', '广西/柳州', '广西/柳州', '2002-06-25 07:41:23', '020401', '', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('868', '焦费公西', '430721200311142927', '广西/柳州', '广西/柳州', '2003-11-14 13:24:47', '020401', '\0', '40.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('869', '童迟达', '341182200204068070', '重庆', '重庆', '2002-04-06 09:05:44', '020401', '', '489.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('870', '郜简任', '370829200207020807', '重庆', '重庆', '2002-07-02 08:11:51', '020401', '\0', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('871', '荆第五木', '420100200107247856', '上海', '上海', '2001-07-24 09:25:51', '020401', '', '44.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('872', '富弘通', '522301200106255304', '海南', '海南', '2001-06-25 19:28:18', '020401', '\0', '412.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('873', '郇公仪疏', '14072520020223129X', '江西/宜春', '江西/宜春', '2002-02-23 21:45:30', '020401', '', '462.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('874', '恽松弥', '522722200108044862', '海南', '海南', '2001-08-04 13:49:46', '080206', '\0', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('875', '潘匡楚', '152526200207237235', '广西/柳州', '广西/柳州', '2002-07-23 13:15:49', '080206', '', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('876', '宿桑汝', '141122200303155344', '北京', '北京', '2003-03-15 19:14:05', '080206', '\0', '496.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('877', '束孟熊', '520402200301127578', '重庆', '重庆', '2003-01-12 05:31:03', '080206', '', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('878', '孔屋庐后', '371323200201254827', '香港', '香港', '2002-01-25 09:25:21', '080206', '\0', '419.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('879', '谈周沈', '141034200303055036', '云南/大理', '云南/大理', '2003-03-05 07:48:18', '080206', '', '422.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('880', '文鲍路', '340602200401314546', '湖南/长沙', '湖南/长沙', '2004-01-31 03:47:18', '080206', '\0', '489.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('881', '左丘阚郎', '640400200301062975', '四川/成都', '四川/成都', '2003-01-06 18:01:10', '080206', '', '431.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('882', '鄂卫步', '150422200305117621', '天津', '天津', '2003-05-11 19:46:35', '080206', '\0', '436.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('883', '眭公孙郭', '441721200404217273', '河南、洛阳', '河南、洛阳', '2004-04-21 19:34:49', '080206', '', '43.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('884', '冒沈司', '45020320020410120X', '河南、洛阳', '河南、洛阳', '2002-04-10 23:37:59', '080206', '\0', '480.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('885', '公良房佟', '330500200104269152', '河南、洛阳', '河南、洛阳', '2001-04-26 20:34:16', '080206', '', '478.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('886', '曲钭郎', '522328200306113327', '河南、洛阳', '河南、洛阳', '2003-06-11 09:58:37', '080206', '\0', '463.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('887', '牧向成', '37052120030222195X', '内蒙古', '内蒙古', '2003-02-22 02:46:19', '080206', '', '460.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('888', '党阎司寇', '37030420020617624X', '湖北/武汉', '湖北/武汉', '2002-06-17 17:00:53', '080206', '\0', '42.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('889', '仉苏樊', '620524200206213117', '重庆', '重庆', '2002-06-21 13:28:15', '080206', '', '470.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('890', '邝鱼糜', '370702200302011663', '云南/大理', '云南/大理', '2003-02-01 20:18:18', '080206', '\0', '423.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('891', '沙邴赫连', '141122200301314110', '山东/青岛', '山东/青岛', '2003-01-31 05:56:27', '080206', '', '434.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('892', '江闾濮', '654000200201213880', '江西/宜春', '江西/宜春', '2002-01-21 03:50:41', '080206', '\0', '454.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('893', '阴亓佘', '410927200111283317', '山东/青岛', '山东/青岛', '2001-11-28 07:10:19', '080206', '', '422.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('894', '籍闾松', '220605200312303329', '内蒙古', '内蒙古', '2003-12-30 02:50:48', '080206', '\0', '464.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('895', '韶花平', '330282200404032075', '河南、洛阳', '河南、洛阳', '2004-04-03 19:56:15', '080206', '', '419.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('896', '牧安滕', '150822200202137488', '北京', '北京', '2002-02-13 11:55:29', '080206', '\0', '453.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('897', '迟宋巴', '140122200401163232', '重庆', '重庆', '2004-01-16 22:29:54', '080206', '', '488.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('898', '宦闫曾', '411327200402241705', '广东/深圳', '广东/深圳', '2004-02-24 19:30:56', '080206', '\0', '45.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('899', '萧施慕', '530425200202201054', '湖北/武汉', '湖北/武汉', '2002-02-20 09:13:18', '080206', '', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('900', '南门萧羿', '622927200108300981', '山西/太原', '山西/太原', '2001-08-30 08:12:07', '080206', '\0', '419.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('901', '怀伏董', '360124200109234471', '江西/宜春', '江西/宜春', '2001-09-23 19:07:23', '080206', '', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('902', '梁丘辜独孤', '632600200306142529', '辽宁', '辽宁', '2003-06-14 15:13:07', '080206', '\0', '445.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('903', '谈郈亢', '350211200107027753', '山东/青岛', '山东/青岛', '2001-07-02 22:48:33', '080206', '', '436.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('904', '强皋公西', '450421200310182860', '北京', '北京', '2003-10-18 05:45:58', '081301', '\0', '40.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('905', '东乡董燕', '441283200209185511', '江西/宜春', '江西/宜春', '2002-09-18 05:32:12', '081301', '', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('906', '房厉李', '532926200306074905', '辽宁', '辽宁', '2003-06-07 05:51:46', '081301', '\0', '456.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('907', '广庾闵', '150203200209275531', '湖北/武汉', '湖北/武汉', '2002-09-27 11:03:55', '081301', '', '493.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('908', '毋蔡应', '510824200212049844', '香港', '香港', '2002-12-04 07:18:30', '081301', '\0', '477.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('909', '扈钮程', '420528200307036832', '湖北/武汉', '湖北/武汉', '2003-07-03 01:43:18', '081301', '', '433.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('910', '尚秘郑', '520103200308233189', '江苏/无锡', '江苏/无锡', '2003-08-23 02:56:55', '081301', '\0', '470.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('911', '庞璩秦', '350503200204125052', '广东/深圳', '广东/深圳', '2002-04-12 00:33:42', '081301', '', '490.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('912', '利溥仓', '32011320031125666X', '福建/泉州', '福建/泉州', '2003-11-25 15:46:02', '081301', '\0', '453.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('913', '越展浑', '13020320030918027X', '湖北/武汉', '湖北/武汉', '2003-09-18 23:44:49', '081301', '', '482.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('914', '解宰父郝', '542627200401056301', '广西/柳州', '广西/柳州', '2004-01-05 22:26:43', '081301', '\0', '472.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('915', '郑班盛', '411528200108127052', '辽宁', '辽宁', '2001-08-12 01:13:13', '081301', '', '48.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('916', '佘车容', '140224200111088906', '湖南/长沙', '湖南/长沙', '2001-11-08 00:52:13', '081301', '\0', '428.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('917', '康都鲁', '211321200111076177', '陕西/西安', '陕西/西安', '2001-11-07 13:30:44', '081301', '', '461.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('918', '符祖芮', '500117200204056960', '天津', '天津', '2002-04-05 15:40:54', '081301', '\0', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('919', '盛舒富', '410223200203131253', '香港', '香港', '2002-03-13 21:57:24', '081301', '', '489.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('920', '真融东方', '141123200202120085', '辽宁', '辽宁', '2002-02-12 16:17:52', '081301', '\0', '454.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('921', '甘燕甄', '140105200401248870', '重庆', '重庆', '2004-01-24 14:51:00', '081301', '', '495.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('922', '姚养亓', '450924200302196783', '浙江/舟山', '浙江/舟山', '2003-02-19 09:57:35', '081301', '\0', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('923', '饶达宓', '500234200303200739', '天津', '天津', '2003-03-20 13:02:42', '081301', '', '427.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('924', '归籍晏', '420981200106233884', '内蒙古', '内蒙古', '2001-06-23 08:11:08', '081301', '\0', '467.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('925', '师戈秋', '350800200305107379', '河南、洛阳', '河南、洛阳', '2003-05-10 03:12:19', '081301', '', '457.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('926', '孙轩辕屋庐', '653226200302173623', '陕西/西安', '陕西/西安', '2003-02-17 20:37:39', '081301', '\0', '494.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('927', '狐沈鲜', '450981200112169474', '内蒙古', '内蒙古', '2001-12-16 03:25:23', '081301', '', '457.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('928', '徐楼亢', '410205200208164641', '湖南/长沙', '湖南/长沙', '2002-08-16 01:40:58', '081301', '\0', '493.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('929', '苗郇缑', '621224200404087291', '北京', '北京', '2004-04-08 18:07:20', '081301', '', '430.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('930', '相练冼', '411503200111252084', '天津', '天津', '2001-11-25 10:18:07', '081301', '\0', '435.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('931', '恽双倪', '320506200305024836', '天津', '天津', '2003-05-02 23:09:31', '081301', '', '474.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('932', '糜靳王', '513231200207304947', '海南', '海南', '2002-07-30 02:17:15', '081301', '\0', '412.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('933', '戴归聂', '21080020040117903X', '福建/泉州', '福建/泉州', '2004-01-17 17:44:30', '081301', '', '479.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('934', '毛司寇钭', '341602200207011243', '广西/柳州', '广西/柳州', '2002-07-01 20:59:42', '081307T', '\0', '496.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('935', '公乘仲孙能', '511826200310101558', '内蒙古', '内蒙古', '2003-10-10 19:17:19', '081307T', '', '416.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('936', '祭冒丁', '360322200106140849', '广东/深圳', '广东/深圳', '2001-06-14 22:40:34', '081307T', '\0', '440.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('937', '闻农段干', '43050020031001973X', '海南', '海南', '2003-10-01 20:32:27', '081307T', '', '468.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('938', '呼延潘鲜', '500107200204023306', '四川/成都', '四川/成都', '2002-04-02 22:49:42', '081307T', '\0', '413.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('939', '邹籍广', '469026200206304151', '湖南/长沙', '湖南/长沙', '2002-06-30 15:15:29', '081307T', '', '497.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('940', '步邬舜', '522702200310230743', '海南', '海南', '2003-10-23 15:44:45', '081307T', '\0', '440.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('941', '敬南郭屈', '211303200311151159', '广东/深圳', '广东/深圳', '2003-11-15 13:31:25', '081307T', '', '459.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('942', '公孙梁密', '51140220031024242X', '安徽', '安徽', '2003-10-24 01:04:56', '081307T', '\0', '451.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('943', '戚平禄', '430408200311300756', '河南、洛阳', '河南、洛阳', '2003-11-30 14:10:21', '081307T', '', '429.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('944', '符封晋', '420527200307023728', '江苏/无锡', '江苏/无锡', '2003-07-02 16:35:21', '081307T', '\0', '461.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('945', '孔花伏', '361130200206122875', '北京', '北京', '2002-06-12 12:05:23', '081307T', '', '473.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('946', '挚骆竺', '341003200109195927', '陕西/西安', '陕西/西安', '2001-09-19 05:16:37', '081307T', '\0', '456.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('947', '仲长宗正羊舌', '500231200209175398', '山东/青岛', '山东/青岛', '2002-09-17 04:05:04', '081307T', '', '444.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('948', '衡达黄', '440515200309177905', '福建/泉州', '福建/泉州', '2003-09-17 22:07:17', '081307T', '\0', '420.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('949', '却充叔孙', '410527200208055554', '河南、洛阳', '河南、洛阳', '2002-08-05 02:47:39', '081307T', '', '445.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('950', '扶宁甘', '620600200207144728', '湖北/武汉', '湖北/武汉', '2002-07-14 22:01:17', '081307T', '\0', '438.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('951', '巫马娄蓝', '350103200310099896', '江西/宜春', '江西/宜春', '2003-10-09 09:31:52', '081307T', '', '413.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('952', '胡弘车正', '530722200108284663', '上海', '上海', '2001-08-28 19:04:31', '081307T', '\0', '43.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('953', '鲁达孔', '621025200402182936', '北京', '北京', '2004-02-18 22:13:13', '081307T', '', '468.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('954', '暴宁廖', '330108200205249627', '香港', '香港', '2002-05-24 07:20:16', '081307T', '\0', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('955', '邹万淳于', '321282200401179732', '山西/太原', '山西/太原', '2004-01-17 07:32:50', '081307T', '', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('956', '西门闵施', '510823200302012221', '浙江/舟山', '浙江/舟山', '2003-02-01 13:04:59', '081307T', '\0', '45.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('957', '汤姚从', '340826200208165253', '辽宁', '辽宁', '2002-08-16 19:10:44', '081307T', '', '436.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('958', '禹常贝', '510108200206193681', '天津', '天津', '2002-06-19 00:10:56', '081307T', '\0', '429.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('959', '许李兀官', '36100220020117283X', '山西/太原', '山西/太原', '2002-01-17 00:21:40', '081307T', '', '467.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('960', '杭包刁', '43080220030731696X', '重庆', '重庆', '2003-07-31 01:34:16', '081307T', '\0', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('961', '荀丁卜', '429021200112129496', '湖北/武汉', '湖北/武汉', '2001-12-12 05:27:58', '081307T', '', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('962', '苏卢隗', '43122120020921318X', '天津', '天津', '2002-09-21 16:53:09', '081307T', '\0', '498.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('963', '班罗鱼', '610826200207050134', '广西/柳州', '广西/柳州', '2002-07-05 03:00:25', '081307T', '', '438.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('964', '元费厍', '41142120030415834X', '浙江/舟山', '浙江/舟山', '2003-04-15 19:10:27', '081504', '\0', '46.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('965', '戚真皇', '371103200205095855', '上海', '上海', '2002-05-09 10:22:41', '081504', '', '470.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('966', '高奚尔朱', '230503200201208146', '湖南/长沙', '湖南/长沙', '2002-01-20 20:13:39', '081504', '\0', '421.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('967', '侴明逯', '36062220020622873X', '重庆', '重庆', '2002-06-22 10:03:26', '081504', '', '467.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('968', '郏乜司城', '331122200208075923', '海南', '海南', '2002-08-07 18:23:44', '081504', '\0', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('969', '干朱褚', '632525200107287276', '山西/太原', '山西/太原', '2001-07-28 20:13:13', '081504', '', '439.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('970', '郭封席', '411624200307048883', '陕西/西安', '陕西/西安', '2003-07-04 10:51:54', '081504', '\0', '449.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('971', '井劳郈', '340504200210066997', '河北/保定', '河北/保定', '2002-10-06 23:42:35', '081504', '', '430.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('972', '养居漆', '610700200111194644', '湖北/武汉', '湖北/武汉', '2001-11-19 14:14:15', '081504', '\0', '466.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('973', '裴郇潘', '411403200205166210', '浙江/舟山', '浙江/舟山', '2002-05-16 06:01:11', '081504', '', '464.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('974', '融吉昌', '621121200304077200', '江苏/无锡', '江苏/无锡', '2003-04-07 18:49:54', '081504', '\0', '40.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('975', '甘林平', '410800200208273419', '河南、洛阳', '河南、洛阳', '2002-08-27 03:25:48', '081504', '', '484.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('976', '邴车那', '513330200204139260', '内蒙古', '内蒙古', '2002-04-13 03:30:03', '081504', '\0', '488.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('977', '弥颜勾', '640181200209050076', '北京', '北京', '2002-09-05 16:39:40', '081504', '', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('978', '冼呼延折', '654027200209086986', '湖北/武汉', '湖北/武汉', '2002-09-08 06:50:02', '081504', '\0', '488.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('979', '史闫于', '350303200203079671', '海南', '海南', '2002-03-07 23:23:05', '081504', '', '49.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('980', '饶覃苗', '150123200203200125', '福建/泉州', '福建/泉州', '2002-03-20 03:29:56', '081504', '\0', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('981', '荣墨刘', '411000200203096975', '广东/深圳', '广东/深圳', '2002-03-09 20:40:01', '081504', '', '437.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('982', '夹谷辛秘', '640502200205291901', '北京', '北京', '2002-05-29 03:02:56', '081504', '\0', '417.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('983', '谷梁从吕', '371622200106058676', '河南、洛阳', '河南、洛阳', '2001-06-05 05:52:32', '081504', '', '459.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('984', '鲁宦冀', '441284200302017404', '湖南/长沙', '湖南/长沙', '2003-02-01 20:52:58', '081504', '\0', '486.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('985', '卓西门阴', '610114200301063755', '北京', '北京', '2003-01-06 13:50:22', '081504', '', '456.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('986', '暴公良闻人', '441721200105267182', '河北/保定', '河北/保定', '2001-05-26 07:09:40', '081504', '\0', '48.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('987', '范褚阎', '341282200109077636', '山西/太原', '山西/太原', '2001-09-07 00:59:28', '081504', '', '470.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('988', '邰门缑', '430902200204285828', '广东/深圳', '广东/深圳', '2002-04-28 19:18:22', '081504', '\0', '424.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('989', '臧廉贲', '371200200311110090', '广东/深圳', '广东/深圳', '2003-11-11 15:58:52', '081504', '', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('990', '沈汤袁', '141002200307030468', '云南/大理', '云南/大理', '2003-07-03 23:20:22', '081504', '\0', '435.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('991', '胶黎钦', '441226200205051690', '江苏/无锡', '江苏/无锡', '2002-05-05 08:42:15', '081504', '', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('992', '佘来盖', '231081200309195300', '安徽', '安徽', '2003-09-19 20:38:59', '081504', '\0', '467.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('993', '车郝司', '420502200209233679', '辽宁', '辽宁', '2002-09-23 05:55:11', '081504', '', '485.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('994', '瞿端秘', '511423200110103566', '陕西/西安', '陕西/西安', '2001-10-10 15:53:03', '151561', '\0', '415.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('995', '杭桂胥', '361023200212113634', '天津', '天津', '2002-12-11 13:37:23', '151561', '', '42.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('996', '弓胶桂', '620802200310153722', '福建/泉州', '福建/泉州', '2003-10-15 12:31:11', '151561', '\0', '43.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('997', '伍北宫古', '513422200302135154', '北京', '北京', '2003-02-13 06:02:36', '151561', '', '442.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('998', '公乘南达', '35062820010906520X', '山西/太原', '山西/太原', '2001-09-06 09:15:25', '151561', '\0', '420.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('999', '糜鲜章', '450302200209115191', '北京', '北京', '2002-09-11 08:37:26', '151561', '', '443.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1000', '邝东乡诸', '350926200307214900', '上海', '上海', '2003-07-21 00:44:31', '151561', '\0', '435.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1001', '鱼酆夹谷', '330922200105121432', '江苏/无锡', '江苏/无锡', '2001-05-12 17:34:46', '151561', '', '475.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1002', '俞贺北宫', '230207200401059148', '山东/青岛', '山东/青岛', '2004-01-05 20:13:05', '151561', '\0', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1003', '练司马元', '320113200205276018', '江苏/无锡', '江苏/无锡', '2002-05-27 17:34:33', '151561', '', '446.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1004', '丁富嵇', '150402200212073345', '海南', '海南', '2002-12-07 11:34:01', '151561', '\0', '468.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1005', '寿雷尔朱', '411525200205184594', '山西/太原', '山西/太原', '2002-05-18 05:49:42', '151561', '', '413.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1006', '燕麻墨', '350203200401147529', '北京', '北京', '2004-01-14 07:47:01', '151561', '\0', '455.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1007', '封充简', '340600200208225952', '湖北/武汉', '湖北/武汉', '2002-08-22 01:22:59', '151561', '', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1008', '慕容姬别', '511781200109306227', '上海', '上海', '2001-09-30 10:18:39', '151561', '\0', '458.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1009', '岳邬葛', '23020420040129937X', '山西/太原', '山西/太原', '2004-01-29 08:33:52', '151561', '', '426.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1010', '巩西门火', '150221200403171026', '香港', '香港', '2004-03-17 20:43:30', '151561', '\0', '41.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1011', '古全索', '321302200108046950', '云南/大理', '云南/大理', '2001-08-04 05:33:38', '151561', '', '476.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1012', '商从桑', '23012620011205458X', '云南/大理', '云南/大理', '2001-12-05 09:08:44', '151561', '\0', '473.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1013', '冷赫连危', '23108120020403261X', '重庆', '重庆', '2002-04-03 02:50:24', '151561', '', '417.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1014', '孟过谷梁', '34102120020721192X', '香港', '香港', '2002-07-21 20:19:44', '151561', '\0', '487.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1015', '水颛孙风', '371624200211149572', '重庆', '重庆', '2002-11-14 13:33:47', '151561', '', '466.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1016', '仇东郭帅', '130624200306229924', '上海', '上海', '2003-06-22 06:45:22', '151561', '\0', '414.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1017', '琴胡长孙', '610423200207228835', '河南、洛阳', '河南、洛阳', '2002-07-22 23:23:40', '151561', '', '432.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1018', '浑连屈', '410300200306159348', '广东/深圳', '广东/深圳', '2003-06-15 02:08:11', '151561', '\0', '465.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1019', '司寇长孙查', '520382200204193510', '云南/大理', '云南/大理', '2002-04-19 20:04:53', '151561', '', '420.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1020', '广屈突翁', '220204200204091308', '天津', '天津', '2002-04-09 20:35:25', '151561', '\0', '448.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1021', '崔寇费', '222402200301035471', '湖南/长沙', '湖南/长沙', '2003-01-03 11:53:52', '151561', '', '483.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1022', '荣鱼游', '130283200207035305', '上海', '上海', '2002-07-03 02:22:48', '151561', '\0', '425.0', '2021-04-18 11:47:36', '0');
INSERT INTO `initial_student_data` VALUES ('1023', '秘爱籍', '330282200105223998', '广东/深圳', '广东/深圳', '2001-05-22 16:32:04', '151561', '', '458.0', '2021-04-18 11:47:36', '0');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `notify_id` int(20) NOT NULL COMMENT '对应通知消息的id',
  `sender_id` int(20) NOT NULL COMMENT '发送者用户ID',
  `reciver_id` bigint(20) NOT NULL COMMENT '接受者用户ID',
  `content` varchar(1000) NOT NULL COMMENT '消息内容,最长长度不允许超过1000',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间:按当前时间自动创建',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='私信信息表,包含了所有用户的私信信息';

-- ----------------------------
-- Records of message
-- ----------------------------

-- ----------------------------
-- Table structure for notify
-- ----------------------------
DROP TABLE IF EXISTS `notify`;
CREATE TABLE `notify` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sender_id` int(20) NOT NULL,
  `reciver_id` int(20) NOT NULL,
  `msg_title` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '消息类型announcement公告/remin提醒/message私信',
  `msg_content` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `is_read` bit(1) NOT NULL DEFAULT b'0' COMMENT '0未读，1已读',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of notify
-- ----------------------------
INSERT INTO `notify` VALUES ('64', '10012', '10012', '12', '21', '', '2021-04-12 15:52:25');
INSERT INTO `notify` VALUES ('65', '10012', '10013', '12', '21', '\0', '2021-04-12 15:08:34');
INSERT INTO `notify` VALUES ('66', '10012', '10014', '12', '21', '', '2021-04-12 15:53:33');
INSERT INTO `notify` VALUES ('67', '10012', '10016', '12', '21', '\0', '2021-04-12 15:08:34');
INSERT INTO `notify` VALUES ('68', '10012', '10017', '12', '21', '\0', '2021-04-12 15:08:34');
INSERT INTO `notify` VALUES ('69', '10012', '10018', '12', '21', '\0', '2021-04-12 15:08:34');
INSERT INTO `notify` VALUES ('70', '10012', '10019', '12', '21', '\0', '2021-04-12 15:08:34');
INSERT INTO `notify` VALUES ('71', '10012', '10012', '测试标题', '123168541325山地车山地车山地车山地车从', '', '2021-04-12 15:56:42');
INSERT INTO `notify` VALUES ('72', '10012', '10013', '测试标题', '123168541325山地车山地车山地车山地车从', '\0', '2021-04-12 15:09:06');
INSERT INTO `notify` VALUES ('73', '10012', '10014', '测试标题', '123168541325山地车山地车山地车山地车从', '', '2021-04-12 15:53:37');
INSERT INTO `notify` VALUES ('74', '10012', '10016', '测试标题', '123168541325山地车山地车山地车山地车从', '\0', '2021-04-12 15:09:06');
INSERT INTO `notify` VALUES ('75', '10012', '10017', '测试标题', '123168541325山地车山地车山地车山地车从', '\0', '2021-04-12 15:09:06');
INSERT INTO `notify` VALUES ('76', '10012', '10018', '测试标题', '123168541325山地车山地车山地车山地车从', '\0', '2021-04-12 15:09:06');
INSERT INTO `notify` VALUES ('77', '10012', '10019', '测试标题', '123168541325山地车山地车山地车山地车从', '\0', '2021-04-12 15:09:06');
INSERT INTO `notify` VALUES ('85', '10012', '10012', '标题1', '内容1', '', '2021-04-12 15:56:40');
INSERT INTO `notify` VALUES ('86', '10012', '10013', '标题1', '内容1', '\0', '2021-04-12 15:56:25');
INSERT INTO `notify` VALUES ('87', '10012', '10014', '标题1', '内容1', '\0', '2021-04-12 15:56:25');
INSERT INTO `notify` VALUES ('88', '10012', '10016', '标题1', '内容1', '\0', '2021-04-12 15:56:25');
INSERT INTO `notify` VALUES ('89', '10012', '10017', '标题1', '内容1', '\0', '2021-04-12 15:56:25');
INSERT INTO `notify` VALUES ('90', '10012', '10018', '标题1', '内容1', '\0', '2021-04-12 15:56:25');
INSERT INTO `notify` VALUES ('91', '10012', '10019', '标题1', '内容1', '\0', '2021-04-12 15:56:25');
INSERT INTO `notify` VALUES ('92', '10012', '10012', '通知2', '内容2', '\0', '2021-04-12 16:00:45');
INSERT INTO `notify` VALUES ('93', '10012', '10013', '通知2', '内容2', '\0', '2021-04-12 16:00:45');
INSERT INTO `notify` VALUES ('94', '10012', '10014', '通知2', '内容2', '\0', '2021-04-12 16:00:45');
INSERT INTO `notify` VALUES ('95', '10012', '10016', '通知2', '内容2', '\0', '2021-04-12 16:00:45');
INSERT INTO `notify` VALUES ('96', '10012', '10017', '通知2', '内容2', '\0', '2021-04-12 16:00:45');
INSERT INTO `notify` VALUES ('97', '10012', '10018', '通知2', '内容2', '\0', '2021-04-12 16:00:45');
INSERT INTO `notify` VALUES ('98', '10012', '10019', '通知2', '内容2', '\0', '2021-04-12 16:00:45');

-- ----------------------------
-- Table structure for pay_item
-- ----------------------------
DROP TABLE IF EXISTS `pay_item`;
CREATE TABLE `pay_item` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pay_item_name` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '收费项名称',
  `pay_type_id` int(10) NOT NULL,
  `release_people_id` int(11) NOT NULL COMMENT '发布人id',
  `charge_amount` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT '收费金额',
  `enabled` int(2) NOT NULL DEFAULT '0' COMMENT '启用状态，0启用，1禁用',
  `verifier_id` int(10) NOT NULL DEFAULT '0' COMMENT '审核人id，1000为系统审核',
  `charged_spec_id` int(10) NOT NULL DEFAULT '0' COMMENT '收费的专业id',
  `audit_status` int(10) NOT NULL DEFAULT '3' COMMENT '审核状态,0未审核，1审核中，2通过，3未通过',
  `audit_msg` varchar(255) CHARACTER SET utf8 DEFAULT '发布失败，请向管理员询问原因!' COMMENT '发布失败原因信息',
  `audit_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '审核时间',
  `remark` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '备注',
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '开始时间',
  `end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creat_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pay_type_id` (`pay_type_id`,`charged_spec_id`)
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pay_item
-- ----------------------------
INSERT INTO `pay_item` VALUES ('273', '学费---控制科学与工程', '2', '10012', '6200.00', '0', '10012', '15', '2', '', '2021-04-18 12:55:22', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:40:45');
INSERT INTO `pay_item` VALUES ('274', '学费---过程装备与控制工程', '2', '10012', '6200.00', '0', '10012', '34', '2', '', '2021-04-18 15:06:12', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('275', '学费---材料科学与工程', '2', '10012', '6200.00', '0', '0', '11', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('276', '学费---车辆工程', '2', '10012', '6200.00', '0', '0', '10', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('277', '学费---材料工程', '2', '10012', '6200.00', '0', '0', '12', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('278', '学费---力学', '2', '10012', '6200.00', '0', '0', '7', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('279', '学费---能源化学工程', '2', '10012', '6200.00', '0', '0', '36', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('280', '学费---物流工程', '2', '10012', '6200.00', '0', '0', '22', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('281', '学费---电力电子与动力传动', '2', '10012', '6200.00', '0', '0', '14', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('282', '学费---油气储运工程', '2', '10012', '6200.00', '0', '0', '37', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('283', '学费---冶金工程', '2', '10012', '6200.00', '0', '10012', '13', '2', '', '2021-04-18 12:57:42', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('284', '学费---电子与通信工程', '2', '10012', '6200.00', '0', '0', '17', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('285', '学费---光学工程', '2', '10012', '6200.00', '0', '0', '8', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('286', '学费---软件工程', '2', '10012', '6200.00', '0', '0', '20', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('287', '学费---化学工程与工艺', '2', '10012', '6200.00', '0', '0', '35', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('288', '学费---机械设计制造及其自动化', '2', '10012', '6200.00', '0', '0', '30', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('289', '学费---工业工程', '2', '10012', '6200.00', '0', '0', '26', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('290', '学费---环境科学与工程', '2', '10012', '6200.00', '0', '0', '23', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('291', '学费---理论经济学', '2', '10012', '6200.00', '0', '0', '25', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('292', '学费---国际经济与贸易', '2', '10012', '6200.00', '0', '0', '33', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('293', '学费---计算机科学与技术', '2', '10012', '6200.00', '0', '0', '19', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('294', '学费---安全工程', '2', '10012', '6200.00', '0', '0', '24', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('295', '学费---化学工程', '2', '10012', '6200.00', '0', '0', '29', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('296', '学费---电气工程', '2', '10012', '6200.00', '0', '0', '16', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('297', '学费---机械电子工程 ', '2', '10012', '6200.00', '0', '0', '32', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('298', '学费---诉讼法学', '2', '10012', '6200.00', '0', '0', '3', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('299', '学费---传递', '2', '10012', '6200.00', '0', '0', '39', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('300', '学费---工商管理', '2', '10012', '6200.00', '0', '0', '28', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('301', '学费---管理科学与工程', '2', '10012', '6200.00', '0', '0', '27', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('302', '学费---数学', '2', '10012', '6200.00', '0', '0', '6', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('303', '学费---交通运输工程', '2', '10012', '6200.00', '0', '0', '21', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('304', '学费---控制工程', '2', '10012', '6200.00', '0', '0', '18', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');
INSERT INTO `pay_item` VALUES ('305', '学费---社会工作硕士', '2', '10012', '6200.00', '0', '0', '5', '0', '', '2021-04-18 12:31:35', '请在规定时效内完成缴费', '2021-04-22 00:00:00', '2021-07-15 00:00:00', '2021-04-18 12:31:35');

-- ----------------------------
-- Table structure for pay_record
-- ----------------------------
DROP TABLE IF EXISTS `pay_record`;
CREATE TABLE `pay_record` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `student_id` int(12) NOT NULL,
  `pay_type` int(10) NOT NULL COMMENT '缴费类型',
  `payment_amount` float(12,0) DEFAULT NULL COMMENT '缴费金额',
  `payment_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pay_record
-- ----------------------------

-- ----------------------------
-- Table structure for pay_type
-- ----------------------------
DROP TABLE IF EXISTS `pay_type`;
CREATE TABLE `pay_type` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pay_type_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '收费类型名称',
  `payment_remark` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '缴费备注',
  `creat_time` date NOT NULL COMMENT '创建时间',
  `release_people_id` int(10) NOT NULL COMMENT '发布人id',
  `release_people_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '创建人姓名',
  `charging_item_id` int(10) DEFAULT NULL COMMENT '收费项',
  `enabled` int(5) NOT NULL DEFAULT '0' COMMENT '启用状态,0启用，1禁用',
  `deadline_time` date DEFAULT NULL COMMENT '收费类型截止日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pay_type
-- ----------------------------
INSERT INTO `pay_type` VALUES ('2', '学费', '各专业提交学费信息', '2021-04-05', '10012', '谭永健', null, '0', null);
INSERT INTO `pay_type` VALUES ('3', '住宿费', '待学生选好床位后自动提交收费项', '2021-04-06', '10012', '谭永健', null, '0', null);
INSERT INTO `pay_type` VALUES ('4', '军训费', '创建学生账号时自动建立子项', '2021-04-06', '10012', '谭永健', null, '0', null);
INSERT INTO `pay_type` VALUES ('5', '体检费', '创建学生帐号时自动建立子项', '2021-04-06', '10012', '谭永健', null, '0', null);
INSERT INTO `pay_type` VALUES ('6', '书本费', '创建学生帐号时自动建立子项', '2021-04-06', '10012', '谭永健', null, '0', null);

-- ----------------------------
-- Table structure for school_roll
-- ----------------------------
DROP TABLE IF EXISTS `school_roll`;
CREATE TABLE `school_roll` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `student_no` char(12) CHARACTER SET utf8 NOT NULL,
  `student_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `id_card` char(18) CHARACTER SET utf8 NOT NULL COMMENT '身份证号',
  `student_id` varchar(12) CHARACTER SET utf8 NOT NULL,
  `native_place` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '生源地',
  `admission_category` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '录取类别',
  `foreign_language` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '外语语种',
  `trainning_form` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '培养形式',
  `learn_form` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '学习形式',
  `degree_category` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '学位类别',
  `training_plan_id` varchar(32) CHARACTER SET utf8 DEFAULT NULL COMMENT '培养计划id',
  `have_credit` int(3) DEFAULT NULL COMMENT '已修学分',
  `is_graduated` int(2) DEFAULT '0' COMMENT '是否毕业，0未毕业，1毕业',
  `creat_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `head_img` varchar(255) DEFAULT NULL COMMENT '学生头像',
  `gender` bit(1) NOT NULL DEFAULT b'0' COMMENT '0为女。1为男',
  PRIMARY KEY (`id`,`student_id`),
  UNIQUE KEY `id_card` (`id_card`) USING BTREE,
  UNIQUE KEY `sno` (`student_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of school_roll
-- ----------------------------
INSERT INTO `school_roll` VALUES ('5', '202108020101', '张三', '430524199912043716', '202108020101', '湖南省/邵阳市', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 10:31:21', null, null, '');
INSERT INTO `school_roll` VALUES ('6', '202108020102', '王五', '745123654789512365', '202108020102', '湖南省/长沙市', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 10:31:21', null, null, '');
INSERT INTO `school_roll` VALUES ('7', '202103040101', '李四', '451247856932145741', '202103040101', '山西省/太原市', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('8', '202103040102', '支蓟墨', '210202200305205007', '202103040102', '河北/保定', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('9', '202103040103', '荀谈长孙', '140600200109136834', '202103040103', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('10', '202103040104', '伯慕班', '150723200311046041', '202103040104', '香港', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('11', '202103040105', '范谭高', '632821200307054953', '202103040105', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('12', '202103040106', '钮干张', '623022200301193305', '202103040106', '内蒙古', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('13', '202103040107', '訾湛却', '150727200212219053', '202103040107', '广西/柳州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('14', '202103040109', '简曾裘', '361021200211205219', '202103040109', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('15', '202103040108', '畅蓬邵', '131128200303217185', '202103040108', '福建/泉州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('16', '202103040110', '蓬东桂', '542428200204305167', '202103040110', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('17', '202109030201', '巴随席', '522229200303317106', '202109030201', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:26', null, null, '\0');
INSERT INTO `school_roll` VALUES ('18', '202109030203', '昝微生牟', '511102200302021348', '202109030203', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:26', null, null, '\0');
INSERT INTO `school_roll` VALUES ('19', '202109030202', '山姚巨', '130821200307230355', '202109030202', '辽宁', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:26', null, null, '');
INSERT INTO `school_roll` VALUES ('20', '202109030204', '叶司空姚', '370828200307155737', '202109030204', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:26', null, null, '');
INSERT INTO `school_roll` VALUES ('21', '202109030205', '蔚柏熊', '360728200107217000', '202109030205', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('22', '202109030206', '哈沃麻', '522325200301180732', '202109030206', '云南/大理', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('23', '202109030207', '蔺年南郭', '321100200107145208', '202109030207', '湖北/武汉', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('24', '202109030208', '公羊宁叔孙', '610403200307035250', '202109030208', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('25', '202109030209', '温仲都', '410725200302068026', '202109030209', '云南/大理', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('26', '202109030210', '林游贾', '610422200202078739', '202109030210', '广西/柳州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('27', '202109030211', '羿法辛', '450200200306237889', '202109030211', '北京', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('28', '202109030212', '靳何尤', '652122200310188831', '202109030212', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('29', '202109030213', '袁利微生', '621022200211297061', '202109030213', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('30', '202109030214', '沃岑师', '41162120030214499X', '202109030214', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('31', '202109030215', '夏郦左丘', '211421200302062427', '202109030215', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('32', '202109030216', '苍南鄂', '610423200302277635', '202109030216', '海南', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('33', '202109030217', '靳阿麻', '440785200307104729', '202109030217', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('34', '202109030218', '索孟刘', '321081200112119517', '202109030218', '河北/保定', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('35', '202109030219', '老和滕', '361023200305115428', '202109030219', '北京', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('36', '202109030220', '阿官班', '220284200403050914', '202109030220', '上海', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('37', '202109030221', '鲜东乡屋庐', '360725200301078041', '202109030221', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('38', '202109030222', '辜还东郭', '652827200309199015', '202109030222', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('39', '202109030223', '郇仉寿', '410823200202093761', '202109030223', '上海', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('40', '202109030224', '陆储揭', '150823200105189530', '202109030224', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('41', '202109030225', '上官太叔齐', '360921200108010382', '202109030225', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('42', '202109030226', '百里羊舌纪', '500000200110271616', '202109030226', '河北/保定', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('43', '202109030227', '唐木黄', '610923200303276180', '202109030227', '河南、洛阳', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('44', '202109030228', '漆鲁栾', '131127200111180254', '202109030228', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('45', '202109030229', '东乡空史', '654026200202198786', '202109030229', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '\0');
INSERT INTO `school_roll` VALUES ('46', '202109030230', '殳樊伯', '21110020030713417X', '202109030230', '海南', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:12:27', null, null, '');
INSERT INTO `school_roll` VALUES ('47', '202103040111', '阙勾韩', '140200200204109410', '202103040111', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 12:58:07', null, null, '');
INSERT INTO `school_roll` VALUES ('48', '202103040112', '况陶仇', '210711200205117484', '202103040112', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:01:01', null, null, '\0');
INSERT INTO `school_roll` VALUES ('49', '202110010101', '雍门蔚池', '361026200108149185', '202110010101', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('50', '202110010102', '酆西门琴', '41142320030926331X', '202110010102', '江西/宜春', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('51', '202110010103', '薛乜韶', '513434200205110962', '202110010103', '香港', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('52', '202110010104', '郭老米', '141022200312273259', '202110010104', '北京', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('53', '202110010105', '年竺司', '65322720020929614X', '202110010105', '内蒙古', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('54', '202110010106', '林佴伏', '320300200110278634', '202110010106', '河北/保定', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('55', '202110010107', '季项铁', '34082420030313054X', '202110010107', '内蒙古', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('56', '202110010108', '陶耿殷', '321112200209164819', '202110010108', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('57', '202110010109', '况伏彭', '130323200304207489', '202110010109', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('58', '202110010110', '雍毛楚', '360302200112122035', '202110010110', '江苏/无锡', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('59', '202110010111', '封沃殷', '340721200208219509', '202110010111', '上海', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('60', '202110010112', '季老闫', '360982200108104875', '202110010112', '湖北/武汉', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('61', '202110010113', '呼延潘逯', '522635200401072105', '202110010113', '广西/柳州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('62', '202110010114', '舒姚夹谷', '653125200307131914', '202110010114', '安徽', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('63', '202110010115', '暴仇池', '532527200105010942', '202110010115', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('64', '202110010116', '李厉詹', '421126200205089139', '202110010116', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('65', '202110010117', '蹇枚熊', '410506200308316641', '202110010117', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('66', '202110010118', '孙火那', '450803200112188478', '202110010118', '广东/深圳', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '');
INSERT INTO `school_roll` VALUES ('67', '202110010119', '骆沙习', '140623200205231828', '202110010119', '山西/太原', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:51', null, null, '\0');
INSERT INTO `school_roll` VALUES ('68', '202110010120', '逯杞曹', '43010020030622427X', '202110010120', '河北/保定', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '');
INSERT INTO `school_roll` VALUES ('69', '202110010121', '余施宇文', '140826200111277024', '202110010121', '四川/成都', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '\0');
INSERT INTO `school_roll` VALUES ('70', '202110010122', '裴郦查', '130400200207291130', '202110010122', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '');
INSERT INTO `school_roll` VALUES ('71', '202110010123', '法诸葛越', '654028200305272101', '202110010123', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '\0');
INSERT INTO `school_roll` VALUES ('72', '202110010124', '哈夏滑', '511525200107319196', '202110010124', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '');
INSERT INTO `school_roll` VALUES ('73', '202110010125', '东乡阎严', '211382200306100803', '202110010125', '湖北/武汉', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '\0');
INSERT INTO `school_roll` VALUES ('74', '202110010126', '盛骆门', '360202200210134173', '202110010126', '内蒙古', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '');
INSERT INTO `school_roll` VALUES ('75', '202110010127', '蔚裘高', '510683200307048640', '202110010127', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '\0');
INSERT INTO `school_roll` VALUES ('76', '202110010128', '宗正水蔡', '22020320010728235X', '202110010128', '内蒙古', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '');
INSERT INTO `school_roll` VALUES ('77', '202110010129', '查那任', '35010520010818570X', '202110010129', '云南/大理', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '\0');
INSERT INTO `school_roll` VALUES ('78', '202110010130', '牛伊公西', '120110200111127357', '202110010130', '安徽', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:10:52', null, null, '');
INSERT INTO `school_roll` VALUES ('79', '202108020103', '冷韩蔚', '511823200304296783', '202108020103', '四川/成都', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('80', '202108020104', '李习法', '220282200403269913', '202108020104', '辽宁', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:06', null, null, '');
INSERT INTO `school_roll` VALUES ('81', '202108020105', '阚贡家', '530524200111190563', '202108020105', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('82', '202108020106', '祖温乔', '513323200111224470', '202108020106', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:06', null, null, '');
INSERT INTO `school_roll` VALUES ('83', '202108020107', '鄢巫马欧', '511602200106299120', '202108020107', '山西/太原', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('84', '202108020108', '花綦邝', '652302200403256317', '202108020108', '四川/成都', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:06', null, null, '');
INSERT INTO `school_roll` VALUES ('85', '202108020109', '栾任项', '350212200105068849', '202108020109', '广西/柳州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('86', '202108020110', '姜舒左', '360921200210175191', '202108020110', '北京', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:06', null, null, '');
INSERT INTO `school_roll` VALUES ('87', '202108020111', '黎祁简', '361121200202109044', '202108020111', '浙江/舟山', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('88', '202108020112', '应瞿钦', '440233200204293391', '202108020112', '河南、洛阳', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:06', null, null, '');
INSERT INTO `school_roll` VALUES ('89', '202108020113', '孟京赫连', '610100200212147289', '202108020113', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('90', '202108020114', '晁仇祁', '33072620010802777X', '202108020114', '香港', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '');
INSERT INTO `school_roll` VALUES ('91', '202108020115', '纵庞申', '500226200111051206', '202108020115', '湖北/武汉', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('92', '202108020116', '沙程狐', '650104200202151332', '202108020116', '辽宁', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '');
INSERT INTO `school_roll` VALUES ('93', '202108020117', '冉荀郝', '140882200107050629', '202108020117', '北京', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('94', '202108020118', '饶花漆', '140430200310187016', '202108020118', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '');
INSERT INTO `school_roll` VALUES ('95', '202108020119', '南郭韩范', '140602200107078282', '202108020119', '上海', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('96', '202108020120', '丁籍挚', '530129200403081211', '202108020120', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '');
INSERT INTO `school_roll` VALUES ('97', '202108020121', '费蹇单', '320482200306162043', '202108020121', '江西/宜春', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('98', '202108020122', '公相里篁', '230833200212099736', '202108020122', '四川/成都', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '');
INSERT INTO `school_roll` VALUES ('99', '202108020123', '林慎柏', '320903200201028886', '202108020123', '江苏/无锡', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('100', '202108020124', '别钭贾', '441500200111062818', '202108020124', '浙江/舟山', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '');
INSERT INTO `school_roll` VALUES ('101', '202108020125', '宇文邝达', '43112120020529188X', '202108020125', '湖北/武汉', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('102', '202108020126', '祁麦广', '542128200211261557', '202108020126', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '');
INSERT INTO `school_roll` VALUES ('103', '202108020127', '晋沈是', '630121200308019068', '202108020127', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('104', '202108020128', '舜宓郁', '130926200302067058', '202108020128', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '');
INSERT INTO `school_roll` VALUES ('105', '202108020129', '门樊关', '130423200203255225', '202108020129', '海南', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('106', '202108020130', '姬连乐', '130721200207145617', '202108020130', '江西/宜春', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '');
INSERT INTO `school_roll` VALUES ('107', '202108020131', '毋还钟', '522725200207152665', '202108020131', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('108', '202108020132', '诸虎相里', '410721200307146673', '202108020132', '河北/保定', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 13:11:07', null, null, '');
INSERT INTO `school_roll` VALUES ('109', '202113010101', '毋寇劳', '542330200202189445', '202113010101', '辽宁', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('110', '202113010103', '相沃山', '330212200202135667', '202113010103', '河北/保定', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('111', '202113010104', '张仰令狐', '513222200401144696', '202113010104', '海南', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('112', '202113010102', '熊虞侴', '152201200210273057', '202113010102', '江苏/无锡', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('113', '202113010105', '费慕容闻', '511681200203157162', '202113010105', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('114', '202113010106', '曹暨敬', '421381200109136172', '202113010106', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('115', '202113010107', '公良鲜凌', '231225200202257006', '202113010107', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('116', '202113010108', '侯詹迟', '370321200107108838', '202113010108', '辽宁', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('117', '202113010109', '裘焦俞', '510700200304091701', '202113010109', '辽宁', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('118', '202113010110', '公羊季游', '421124200208185959', '202113010110', '福建/泉州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('119', '202113010111', '祝篁相里', '450422200303129128', '202113010111', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('120', '202113010112', '相苌牛', '130632200204097393', '202113010112', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('121', '202113010113', '南老萧', '513223200310145868', '202113010113', '海南', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('122', '202113010114', '茅第五乜', '450332200201285557', '202113010114', '广东/深圳', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('123', '202113010115', '聂瞿贝', '130681200110175108', '202113010115', '内蒙古', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('124', '202113010116', '蓬农充', '150522200204019710', '202113010116', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('125', '202113010117', '干钟离封', '510822200310269725', '202113010117', '浙江/舟山', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('126', '202113010118', '费禹官', '230804200201114898', '202113010118', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('127', '202113010119', '井常濮', '451281200309088400', '202113010119', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('128', '202113010120', '权宰父祭', '370725200209232271', '202113010120', '香港', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('129', '202113010121', '巫胶蒋', '610630200109099528', '202113010121', '安徽', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '\0');
INSERT INTO `school_roll` VALUES ('130', '202113010122', '蔡汝何', '530925200302237991', '202113010122', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:10', null, null, '');
INSERT INTO `school_roll` VALUES ('131', '202113010123', '薛戈萧', '620723200303309748', '202113010123', '江西/宜春', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:11', null, null, '\0');
INSERT INTO `school_roll` VALUES ('132', '202113010124', '篁庆柳', '420606200309110837', '202113010124', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:11', null, null, '');
INSERT INTO `school_roll` VALUES ('133', '202113010125', '贾蔚逄', '37010520010708832X', '202113010125', '安徽', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:11', null, null, '\0');
INSERT INTO `school_roll` VALUES ('134', '202113010126', '纵冀言', '340100200207206891', '202113010126', '湖南/长沙', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:11', null, null, '');
INSERT INTO `school_roll` VALUES ('135', '202113010127', '巢缪舒', '411082200203140360', '202113010127', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:11', null, null, '\0');
INSERT INTO `school_roll` VALUES ('136', '202113010128', '宾翁上官', '52262720030531889X', '202113010128', '安徽', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:11', null, null, '');
INSERT INTO `school_roll` VALUES ('137', '202113010129', '揭苗茹', '513229200302071684', '202113010129', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:11', null, null, '\0');
INSERT INTO `school_roll` VALUES ('138', '202113010130', '蒲甘柯', '210905200311097556', '202113010130', '四川/成都', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 14:12:11', null, null, '');
INSERT INTO `school_roll` VALUES ('139', '202111010101', '安厉原', '520113200401238366', '202111010101', '安徽', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:56', null, null, '\0');
INSERT INTO `school_roll` VALUES ('140', '202111010103', '夏壤驷潘', '420528200107277463', '202111010103', '广西/柳州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('141', '202111010104', '折疏公孙', '53332320020606109X', '202111010104', '浙江/舟山', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('142', '202111010105', '长孙元别', '150526200304130921', '202111010105', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('143', '202111010106', '北宫阚家', '361024200212117455', '202111010106', '山西/太原', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('144', '202111010102', '篁帅胶', '330211200209134695', '202111010102', '四川/成都', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('145', '202111010107', '巴召海', '500229200204071483', '202111010107', '上海', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('146', '202111010108', '吉祭邴', '640522200106198619', '202111010108', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('147', '202111010109', '岳孔权', '500232200310263647', '202111010109', '河北/保定', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('148', '202111010110', '太史阎真', '500224200112021479', '202111010110', '江苏/无锡', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('149', '202111010111', '广吉国', '62292520020514456X', '202111010111', '广西/柳州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('150', '202111010112', '辜宰父裘', '513336200208310297', '202111010112', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('151', '202111010113', '桂苏狄', '231224200203116743', '202111010113', '山西/太原', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('152', '202111010114', '厉火谢', '341822200105264377', '202111010114', '香港', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('153', '202111010115', '易刁荆', '370786200108317981', '202111010115', '湖北/武汉', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('154', '202111010116', '司空空张', '532328200404198071', '202111010116', '海南', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('155', '202111010117', '风能艾', '542325200108066149', '202111010117', '广东/深圳', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('156', '202111010118', '左丘钦燕', '110112200302203536', '202111010118', '上海', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('157', '202111010119', '方阚劳', '360321200404136147', '202111010119', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('158', '202111010120', '谯褚苏', '520330200111172773', '202111010120', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('159', '202111010121', '海卢危', '820000200305234721', '202111010121', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('160', '202111010122', '尔朱甄仲长', '411503200107135619', '202111010122', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('161', '202111010123', '第五冼莫', '522401200303308722', '202111010123', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('162', '202111010124', '明冼琴', '350627200305127635', '202111010124', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('163', '202111010125', '姬翟米', '520113200205071925', '202111010125', '福建/泉州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('164', '202111010126', '房叔孙单于', '65282720020506997X', '202111010126', '福建/泉州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('165', '202111010127', '邓蒯须', '420981200111072704', '202111010127', '江苏/无锡', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('166', '202111010128', '官贲欧', '530828200109069715', '202111010128', '广东/深圳', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('167', '202111010129', '盛缪南门', '43092220020702774X', '202111010129', '浙江/舟山', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '\0');
INSERT INTO `school_roll` VALUES ('168', '202111010130', '危慕虞', '350212200307243131', '202111010130', '浙江/舟山', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:31:57', null, null, '');
INSERT INTO `school_roll` VALUES ('169', '202103010101', '法魏百里', '440205200307122901', '202103010101', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('170', '202103010102', '邴全风', '532929200301301813', '202103010102', '广东/深圳', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '');
INSERT INTO `school_roll` VALUES ('171', '202103010103', '余老琴', '500107200305251869', '202103010103', '广东/深圳', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('172', '202103010104', '公仪尤秘', '430481200212050437', '202103010104', '上海', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '');
INSERT INTO `school_roll` VALUES ('173', '202103010105', '祝乐佘', '530121200312145223', '202103010105', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('174', '202103010106', '崔臧司马', '653200200105187213', '202103010106', '内蒙古', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '');
INSERT INTO `school_roll` VALUES ('175', '202103010107', '饶顾乜', '422825200306036784', '202103010107', '安徽', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('176', '202103010108', '杜成公乘', '12010420030429885X', '202103010108', '河南、洛阳', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '');
INSERT INTO `school_roll` VALUES ('177', '202103010109', '暨茹祭', '510922200306217207', '202103010109', '山西/太原', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('178', '202103010110', '林巫谷梁', '440400200305144110', '202103010110', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '');
INSERT INTO `school_roll` VALUES ('179', '202103010111', '季畅宗正', '340825200305239502', '202103010111', '北京', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('180', '202103010112', '阿郑水', '431021200206278454', '202103010112', '天津', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '');
INSERT INTO `school_roll` VALUES ('181', '202103010113', '亢戴盛', '140781200212092327', '202103010113', '重庆', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('182', '202103010114', '段干施元', '610729200301187579', '202103010114', '上海', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '');
INSERT INTO `school_roll` VALUES ('183', '202103010115', '尔朱慎黎', '371200200206211906', '202103010115', '福建/泉州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '\0');
INSERT INTO `school_roll` VALUES ('184', '202103010116', '张甄杨', '371428200402249175', '202103010116', '北京', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:06', null, null, '');
INSERT INTO `school_roll` VALUES ('185', '202103010117', '丰项习', '440115200307125042', '202103010117', '福建/泉州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('186', '202103010118', '邵令狐虞', '11010920010715833X', '202103010118', '北京', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '');
INSERT INTO `school_roll` VALUES ('187', '202103010119', '梅广郑', '430300200204271762', '202103010119', '广东/深圳', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('188', '202103010120', '庾祭符', '532931200206209033', '202103010120', '云南/大理', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '');
INSERT INTO `school_roll` VALUES ('189', '202103010121', '璩谢包', '654225200108114309', '202103010121', '北京', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('190', '202103010122', '畅云扈', '500107200308151492', '202103010122', '辽宁', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '');
INSERT INTO `school_roll` VALUES ('191', '202103010123', '水柏钮', '622927200205295360', '202103010123', '广西/柳州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('192', '202103010124', '柴老易', '340223200311251473', '202103010124', '海南', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '');
INSERT INTO `school_roll` VALUES ('193', '202103010125', '浦西门狐', '330921200306045708', '202103010125', '福建/泉州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('194', '202103010126', '任梅闾', '653001200203255852', '202103010126', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '');
INSERT INTO `school_roll` VALUES ('195', '202103010127', '池毛贝', '420000200210120022', '202103010127', '山东/青岛', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('196', '202103010128', '简阚濮阳', '370725200310207855', '202103010128', '江西/宜春', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '');
INSERT INTO `school_roll` VALUES ('197', '202103010129', '阳冒司寇', '532527200307172181', '202103010129', '陕西/西安', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '\0');
INSERT INTO `school_roll` VALUES ('198', '202103010130', '岑莘季', '231085200301091414', '202103010130', '广西/柳州', '普通统考', '英语', '统招', '全日制', '学士学位', '暂无', '0', '0', '2021-04-18 16:32:07', null, null, '');

-- ----------------------------
-- Table structure for speciality_info
-- ----------------------------
DROP TABLE IF EXISTS `speciality_info`;
CREATE TABLE `speciality_info` (
  `spec_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `spec_code` varchar(30) NOT NULL COMMENT '专业代码',
  `spec_name` varchar(255) NOT NULL COMMENT '专业名称',
  `category_code` char(2) DEFAULT NULL COMMENT '门类代码',
  `category_name` varchar(255) NOT NULL COMMENT '门类名称',
  `belong_college_id` int(11) DEFAULT '0' COMMENT '所属学院id',
  `sort_in_college` int(10) NOT NULL COMMENT '专业在学院中的排序',
  PRIMARY KEY (`spec_id`),
  UNIQUE KEY `belong_college_id` (`belong_college_id`,`sort_in_college`) USING BTREE,
  CONSTRAINT `speciality_info_ibfk_1` FOREIGN KEY (`belong_college_id`) REFERENCES `college_info` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of speciality_info
-- ----------------------------
INSERT INTO `speciality_info` VALUES ('3', '030106', '诉讼法学', '03', '法学', '51', '3');
INSERT INTO `speciality_info` VALUES ('5', '035200', '社会工作硕士', '03', '法学', '52', '1');
INSERT INTO `speciality_info` VALUES ('6', '070100', '数学', '07', '理学', '53', '1');
INSERT INTO `speciality_info` VALUES ('7', '077200', '力学', '07', '理学', '55', '1');
INSERT INTO `speciality_info` VALUES ('8', '080300', '光学工程', '08', '工学', '45', '1');
INSERT INTO `speciality_info` VALUES ('10', '085234', '车辆工程', '08', '工学', '41', '1');
INSERT INTO `speciality_info` VALUES ('11', '080500', '材料科学与工程', '08', '工学', '45', '2');
INSERT INTO `speciality_info` VALUES ('12', '085204', '材料工程', '08', '工学', '45', '3');
INSERT INTO `speciality_info` VALUES ('13', '080600', '冶金工程', '08', '工学', '45', '4');
INSERT INTO `speciality_info` VALUES ('14', '080804', '电力电子与动力传动', '08', '工学', '48', '1');
INSERT INTO `speciality_info` VALUES ('15', '081100', '控制科学与工程', '08', '工学', '48', '2');
INSERT INTO `speciality_info` VALUES ('16', '085207', '电气工程', '08', '工学', '48', '3');
INSERT INTO `speciality_info` VALUES ('17', '085208', '电子与通信工程', '08', '工学', '48', '4');
INSERT INTO `speciality_info` VALUES ('18', '085210', '控制工程', '08', '工学', '48', '5');
INSERT INTO `speciality_info` VALUES ('19', '081200', '计算机科学与技术', '08', '工学', '50', '1');
INSERT INTO `speciality_info` VALUES ('20', '083500', '软件工程', '08', '工学', '50', '2');
INSERT INTO `speciality_info` VALUES ('21', '085222', '交通运输工程', '08', '工学', '56', '1');
INSERT INTO `speciality_info` VALUES ('22', '085240', '物流工程', '08', '工学', '56', '2');
INSERT INTO `speciality_info` VALUES ('23', '083000', '环境科学与工程', '08', '工学', '46', '1');
INSERT INTO `speciality_info` VALUES ('24', '085224', '安全工程', '08', '工学', '46', '2');
INSERT INTO `speciality_info` VALUES ('25', '020100', '理论经济学', '02', '经济学', '49', '1');
INSERT INTO `speciality_info` VALUES ('26', '085236', '工业工程', '08', '工学', '47', '1');
INSERT INTO `speciality_info` VALUES ('27', '087100', '管理科学与工程', '08', '工学', '54', '1');
INSERT INTO `speciality_info` VALUES ('28', '120200', '工商管理', '12', '管理学', '53', '2');
INSERT INTO `speciality_info` VALUES ('29', '085216', '化学工程', '08', '工学', '42', '1');
INSERT INTO `speciality_info` VALUES ('30', '080202', '机械设计制造及其自动化', '08', '工学', '41', '2');
INSERT INTO `speciality_info` VALUES ('32', '080204', '机械电子工程 ', '08', '工学', '41', '3');
INSERT INTO `speciality_info` VALUES ('33', '020401', '国际经济与贸易', '02', '经济学', '49', '2');
INSERT INTO `speciality_info` VALUES ('34', '080206', '过程装备与控制工程', '08', '工学', '42', '2');
INSERT INTO `speciality_info` VALUES ('35', '081301', '化学工程与工艺', '08', '工学', '42', '3');
INSERT INTO `speciality_info` VALUES ('36', '081307T', '能源化学工程', '08', '工学', '42', '4');
INSERT INTO `speciality_info` VALUES ('37', '081504', '油气储运工程', '08', '工学', '56', '4');
INSERT INTO `speciality_info` VALUES ('39', '151561', '传递', '08', '使得', '41', '4');

-- ----------------------------
-- Table structure for student_info
-- ----------------------------
DROP TABLE IF EXISTS `student_info`;
CREATE TABLE `student_info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `stu_no` char(12) NOT NULL COMMENT '0代表未分配',
  `id_card` char(18) NOT NULL COMMENT '准考证号',
  `stu_detail_id` int(20) DEFAULT NULL,
  `stu_name` varchar(20) NOT NULL,
  `belong_spec_id` int(20) DEFAULT NULL COMMENT '所属专业id',
  `belong_class_id` int(20) DEFAULT '0' COMMENT '所属班级，为0代表未分配',
  `gender` int(2) DEFAULT '1' COMMENT '性别1男0女',
  `belong_dorm_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_card` (`id_card`) USING BTREE,
  KEY `student_info_ibfk_1` (`belong_class_id`),
  KEY `stu_no` (`stu_no`) USING BTREE,
  CONSTRAINT `student_info_ibfk_1` FOREIGN KEY (`belong_class_id`) REFERENCES `classes_info` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student_info
-- ----------------------------
INSERT INTO `student_info` VALUES ('1', '202108020101', '430524199912043716', null, '张三', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('2', '202108020102', '745123654789512365', null, '王五', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('3', '202103040101', '451247856932145741', null, '李四', '13', '372', '1', '0');
INSERT INTO `student_info` VALUES ('4', '202103040102', '210202200305205007', null, '支蓟墨', '13', '372', '0', '0');
INSERT INTO `student_info` VALUES ('5', '202103040103', '140600200109136834', null, '荀谈长孙', '13', '372', '1', '0');
INSERT INTO `student_info` VALUES ('6', '202103040104', '150723200311046041', null, '伯慕班', '13', '372', '0', '0');
INSERT INTO `student_info` VALUES ('7', '202103040105', '632821200307054953', null, '范谭高', '13', '372', '1', '0');
INSERT INTO `student_info` VALUES ('8', '202103040106', '623022200301193305', null, '钮干张', '13', '372', '0', '0');
INSERT INTO `student_info` VALUES ('9', '202103040107', '150727200212219053', null, '訾湛却', '13', '372', '1', '0');
INSERT INTO `student_info` VALUES ('10', '202103040108', '131128200303217185', null, '畅蓬邵', '13', '372', '0', '0');
INSERT INTO `student_info` VALUES ('11', '202103040109', '361021200211205219', null, '简曾裘', '13', '372', '1', '0');
INSERT INTO `student_info` VALUES ('12', '202103040110', '542428200204305167', null, '蓬东桂', '13', '372', '0', '0');
INSERT INTO `student_info` VALUES ('13', '202109030201', '522229200303317106', null, '巴随席', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('14', '202109030202', '130821200307230355', null, '山姚巨', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('15', '202109030203', '511102200302021348', null, '昝微生牟', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('16', '202109030204', '370828200307155737', null, '叶司空姚', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('17', '202109030205', '360728200107217000', null, '蔚柏熊', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('18', '202109030206', '522325200301180732', null, '哈沃麻', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('19', '202109030207', '321100200107145208', null, '蔺年南郭', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('20', '202109030208', '610403200307035250', null, '公羊宁叔孙', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('21', '202109030209', '410725200302068026', null, '温仲都', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('22', '202109030210', '610422200202078739', null, '林游贾', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('23', '202109030211', '450200200306237889', null, '羿法辛', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('24', '202109030212', '652122200310188831', null, '靳何尤', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('25', '202109030213', '621022200211297061', null, '袁利微生', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('26', '202109030214', '41162120030214499X', null, '沃岑师', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('27', '202109030215', '211421200302062427', null, '夏郦左丘', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('28', '202109030216', '610423200302277635', null, '苍南鄂', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('29', '202109030217', '440785200307104729', null, '靳阿麻', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('30', '202109030218', '321081200112119517', null, '索孟刘', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('31', '202109030219', '361023200305115428', null, '老和滕', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('32', '202109030220', '220284200403050914', null, '阿官班', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('33', '202109030221', '360725200301078041', null, '鲜东乡屋庐', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('34', '202109030223', '410823200202093761', null, '郇仉寿', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('35', '202109030222', '652827200309199015', null, '辜还东郭', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('36', '202109030225', '360921200108010382', null, '上官太叔齐', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('37', '202109030224', '150823200105189530', null, '陆储揭', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('38', '202109030226', '500000200110271616', null, '百里羊舌纪', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('39', '202109030227', '610923200303276180', null, '唐木黄', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('40', '202109030228', '131127200111180254', null, '漆鲁栾', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('41', '202109030229', '654026200202198786', null, '东乡空史', '3', '349', '0', '0');
INSERT INTO `student_info` VALUES ('42', '202109030230', '21110020030713417X', null, '殳樊伯', '3', '349', '1', '0');
INSERT INTO `student_info` VALUES ('43', '202103040111', '140200200204109410', null, '阙勾韩', '13', '372', '1', '0');
INSERT INTO `student_info` VALUES ('44', '202103040112', '210711200205117484', null, '况陶仇', '13', '372', '0', '0');
INSERT INTO `student_info` VALUES ('45', '202110010102', '41142320030926331X', null, '酆西门琴', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('46', '202110010103', '513434200205110962', null, '薛乜韶', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('47', '202110010101', '361026200108149185', null, '雍门蔚池', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('48', '202110010104', '141022200312273259', null, '郭老米', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('49', '202110010105', '65322720020929614X', null, '年竺司', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('50', '202110010106', '320300200110278634', null, '林佴伏', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('51', '202110010107', '34082420030313054X', null, '季项铁', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('52', '202110010108', '321112200209164819', null, '陶耿殷', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('53', '202110010109', '130323200304207489', null, '况伏彭', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('54', '202110010110', '360302200112122035', null, '雍毛楚', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('55', '202110010111', '340721200208219509', null, '封沃殷', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('56', '202110010112', '360982200108104875', null, '季老闫', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('57', '202110010113', '522635200401072105', null, '呼延潘逯', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('58', '202110010114', '653125200307131914', null, '舒姚夹谷', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('59', '202110010115', '532527200105010942', null, '暴仇池', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('60', '202110010116', '421126200205089139', null, '李厉詹', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('61', '202110010117', '410506200308316641', null, '蹇枚熊', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('62', '202110010118', '450803200112188478', null, '孙火那', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('63', '202110010119', '140623200205231828', null, '骆沙习', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('64', '202110010120', '43010020030622427X', null, '逯杞曹', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('65', '202110010121', '140826200111277024', null, '余施宇文', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('66', '202110010122', '130400200207291130', null, '裴郦查', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('67', '202110010123', '654028200305272101', null, '法诸葛越', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('68', '202110010124', '511525200107319196', null, '哈夏滑', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('69', '202110010125', '211382200306100803', null, '东乡阎严', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('70', '202110010126', '360202200210134173', null, '盛骆门', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('71', '202110010127', '510683200307048640', null, '蔚裘高', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('72', '202110010128', '22020320010728235X', null, '宗正水蔡', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('73', '202110010129', '35010520010818570X', null, '查那任', '5', '351', '0', '0');
INSERT INTO `student_info` VALUES ('74', '202110010130', '120110200111127357', null, '牛伊公西', '5', '351', '1', '0');
INSERT INTO `student_info` VALUES ('75', '202108020103', '511823200304296783', null, '冷韩蔚', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('76', '202108020104', '220282200403269913', null, '李习法', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('77', '202108020105', '530524200111190563', null, '阚贡家', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('78', '202108020106', '513323200111224470', null, '祖温乔', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('79', '202108020107', '511602200106299120', null, '鄢巫马欧', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('80', '202108020108', '652302200403256317', null, '花綦邝', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('81', '202108020109', '350212200105068849', null, '栾任项', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('82', '202108020110', '360921200210175191', null, '姜舒左', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('83', '202108020111', '361121200202109044', null, '黎祁简', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('84', '202108020112', '440233200204293391', null, '应瞿钦', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('85', '202108020113', '610100200212147289', null, '孟京赫连', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('86', '202108020114', '33072620010802777X', null, '晁仇祁', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('87', '202108020115', '500226200111051206', null, '纵庞申', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('88', '202108020116', '650104200202151332', null, '沙程狐', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('89', '202108020117', '140882200107050629', null, '冉荀郝', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('90', '202108020118', '140430200310187016', null, '饶花漆', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('91', '202108020119', '140602200107078282', null, '南郭韩范', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('92', '202108020120', '530129200403081211', null, '丁籍挚', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('93', '202108020121', '320482200306162043', null, '费蹇单', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('94', '202108020122', '230833200212099736', null, '公相里篁', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('95', '202108020123', '320903200201028886', null, '林慎柏', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('96', '202108020124', '441500200111062818', null, '别钭贾', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('97', '202108020125', '43112120020529188X', null, '宇文邝达', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('98', '202108020126', '542128200211261557', null, '祁麦广', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('99', '202108020127', '630121200308019068', null, '晋沈是', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('100', '202108020128', '130926200302067058', null, '舜宓郁', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('101', '202108020129', '130423200203255225', null, '门樊关', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('102', '202108020130', '130721200207145617', null, '姬连乐', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('103', '202108020131', '522725200207152665', null, '毋还钟', '20', '393', '0', '0');
INSERT INTO `student_info` VALUES ('104', '202108020132', '410721200307146673', null, '诸虎相里', '20', '393', '1', '0');
INSERT INTO `student_info` VALUES ('105', '202113010102', '152201200210273057', null, '熊虞侴', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('106', '202113010101', '542330200202189445', null, '毋寇劳', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('107', '202113010103', '330212200202135667', null, '相沃山', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('108', '202113010104', '513222200401144696', null, '张仰令狐', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('109', '202113010105', '511681200203157162', null, '费慕容闻', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('110', '202113010106', '421381200109136172', null, '曹暨敬', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('111', '202113010108', '370321200107108838', null, '侯詹迟', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('112', '202113010107', '231225200202257006', null, '公良鲜凌', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('113', '202113010109', '510700200304091701', null, '裘焦俞', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('114', '202113010110', '421124200208185959', null, '公羊季游', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('115', '202113010111', '450422200303129128', null, '祝篁相里', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('116', '202113010112', '130632200204097393', null, '相苌牛', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('117', '202113010113', '513223200310145868', null, '南老萧', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('118', '202113010114', '450332200201285557', null, '茅第五乜', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('119', '202113010115', '130681200110175108', null, '聂瞿贝', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('120', '202113010116', '150522200204019710', null, '蓬农充', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('121', '202113010117', '510822200310269725', null, '干钟离封', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('122', '202113010118', '230804200201114898', null, '费禹官', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('123', '202113010119', '451281200309088400', null, '井常濮', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('124', '202113010120', '370725200209232271', null, '权宰父祭', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('125', '202113010121', '610630200109099528', null, '巫胶蒋', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('126', '202113010122', '530925200302237991', null, '蔡汝何', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('127', '202113010123', '620723200303309748', null, '薛戈萧', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('128', '202113010124', '420606200309110837', null, '篁庆柳', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('129', '202113010125', '37010520010708832X', null, '贾蔚逄', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('130', '202113010126', '340100200207206891', null, '纵冀言', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('131', '202113010127', '411082200203140360', null, '巢缪舒', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('132', '202113010128', '52262720030531889X', null, '宾翁上官', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('133', '202113010129', '513229200302071684', null, '揭苗茹', '7', '357', '0', '0');
INSERT INTO `student_info` VALUES ('134', '202113010130', '210905200311097556', null, '蒲甘柯', '7', '357', '1', '0');
INSERT INTO `student_info` VALUES ('135', '202111010101', '520113200401238366', null, '安厉原', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('136', '202111010103', '420528200107277463', null, '夏壤驷潘', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('137', '202111010104', '53332320020606109X', null, '折疏公孙', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('138', '202111010105', '150526200304130921', null, '长孙元别', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('139', '202111010102', '330211200209134695', null, '篁帅胶', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('140', '202111010106', '361024200212117455', null, '北宫阚家', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('141', '202111010107', '500229200204071483', null, '巴召海', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('142', '202111010108', '640522200106198619', null, '吉祭邴', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('143', '202111010109', '500232200310263647', null, '岳孔权', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('144', '202111010110', '500224200112021479', null, '太史阎真', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('145', '202111010111', '62292520020514456X', null, '广吉国', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('146', '202111010112', '513336200208310297', null, '辜宰父裘', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('147', '202111010113', '231224200203116743', null, '桂苏狄', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('148', '202111010114', '341822200105264377', null, '厉火谢', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('149', '202111010115', '370786200108317981', null, '易刁荆', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('150', '202111010116', '532328200404198071', null, '司空空张', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('151', '202111010117', '542325200108066149', null, '风能艾', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('152', '202111010118', '110112200302203536', null, '左丘钦燕', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('153', '202111010119', '360321200404136147', null, '方阚劳', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('154', '202111010120', '520330200111172773', null, '谯褚苏', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('155', '202111010121', '820000200305234721', null, '海卢危', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('156', '202111010122', '411503200107135619', null, '尔朱甄仲长', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('157', '202111010123', '522401200303308722', null, '第五冼莫', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('158', '202111010124', '350627200305127635', null, '明冼琴', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('159', '202111010125', '520113200205071925', null, '姬翟米', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('160', '202111010126', '65282720020506997X', null, '房叔孙单于', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('161', '202111010127', '420981200111072704', null, '邓蒯须', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('162', '202111010128', '530828200109069715', null, '官贲欧', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('163', '202111010129', '43092220020702774X', null, '盛缪南门', '6', '354', '0', '0');
INSERT INTO `student_info` VALUES ('164', '202111010130', '350212200307243131', null, '危慕虞', '6', '354', '1', '0');
INSERT INTO `student_info` VALUES ('165', '202103010101', '440205200307122901', null, '法魏百里', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('166', '202103010102', '532929200301301813', null, '邴全风', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('167', '202103010103', '500107200305251869', null, '余老琴', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('168', '202103010104', '430481200212050437', null, '公仪尤秘', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('169', '202103010105', '530121200312145223', null, '祝乐佘', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('170', '202103010106', '653200200105187213', null, '崔臧司马', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('171', '202103010107', '422825200306036784', null, '饶顾乜', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('172', '202103010108', '12010420030429885X', null, '杜成公乘', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('173', '202103010109', '510922200306217207', null, '暨茹祭', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('174', '202103010110', '440400200305144110', null, '林巫谷梁', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('175', '202103010111', '340825200305239502', null, '季畅宗正', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('176', '202103010112', '431021200206278454', null, '阿郑水', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('177', '202103010113', '140781200212092327', null, '亢戴盛', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('178', '202103010114', '610729200301187579', null, '段干施元', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('179', '202103010115', '371200200206211906', null, '尔朱慎黎', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('180', '202103010116', '371428200402249175', null, '张甄杨', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('181', '202103010117', '440115200307125042', null, '丰项习', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('182', '202103010118', '11010920010715833X', null, '邵令狐虞', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('183', '202103010119', '430300200204271762', null, '梅广郑', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('184', '202103010120', '532931200206209033', null, '庾祭符', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('185', '202103010121', '654225200108114309', null, '璩谢包', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('186', '202103010122', '500107200308151492', null, '畅云扈', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('187', '202103010123', '622927200205295360', null, '水柏钮', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('188', '202103010124', '340223200311251473', null, '柴老易', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('189', '202103010125', '330921200306045708', null, '浦西门狐', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('190', '202103010126', '653001200203255852', null, '任梅闾', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('191', '202103010127', '420000200210120022', null, '池毛贝', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('192', '202103010128', '370725200310207855', null, '简阚濮阳', '8', '360', '1', '0');
INSERT INTO `student_info` VALUES ('193', '202103010129', '532527200307172181', null, '阳冒司寇', '8', '360', '0', '0');
INSERT INTO `student_info` VALUES ('194', '202103010130', '231085200301091414', null, '岑莘季', '8', '360', '1', '0');

-- ----------------------------
-- Table structure for student_pay_item
-- ----------------------------
DROP TABLE IF EXISTS `student_pay_item`;
CREATE TABLE `student_pay_item` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `student_id` int(10) DEFAULT NULL COMMENT '学号',
  `student_no` char(12) CHARACTER SET utf8 NOT NULL,
  `id_card` varchar(255) NOT NULL,
  `pay_item_id` int(10) NOT NULL COMMENT '收费项id',
  `receivable_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '应收金额',
  `amount_received` decimal(10,2) NOT NULL COMMENT '已收金额',
  `is_completed` int(5) NOT NULL DEFAULT '0' COMMENT '0未完成，1已经完成,3绿色通道',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_card` (`id_card`,`pay_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of student_pay_item
-- ----------------------------
INSERT INTO `student_pay_item` VALUES ('1', null, '202103040101', '451247856932145741', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('2', null, '202103040102', '210202200305205007', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('3', null, '202103040103', '140600200109136834', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('4', null, '202103040104', '150723200311046041', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('5', null, '202103040105', '632821200307054953', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('6', null, '202103040106', '623022200301193305', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('7', null, '202103040107', '150727200212219053', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('8', null, '202103040108', '131128200303217185', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('9', null, '202103040109', '361021200211205219', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('10', null, '202103040110', '542428200204305167', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('11', null, '202103040111', '140200200204109410', '283', '0.00', '6200.00', '0');
INSERT INTO `student_pay_item` VALUES ('12', null, '202103040112', '210711200205117484', '283', '0.00', '6200.00', '0');

-- ----------------------------
-- Table structure for student_support
-- ----------------------------
DROP TABLE IF EXISTS `student_support`;
CREATE TABLE `student_support` (
  `id` int(10) NOT NULL,
  `student_id` int(12) NOT NULL,
  `family_situdation` varchar(255) DEFAULT NULL COMMENT '家庭经济情况',
  `level_of_situation` int(2) DEFAULT NULL COMMENT '经济情况等级',
  `loan_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '贷款时间',
  `loan_amount` float(12,0) DEFAULT NULL COMMENT '贷款金额',
  `free_content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of student_support
-- ----------------------------

-- ----------------------------
-- Table structure for stu_class_relation
-- ----------------------------
DROP TABLE IF EXISTS `stu_class_relation`;
CREATE TABLE `stu_class_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `stu_no` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stu_class_relation
-- ----------------------------

-- ----------------------------
-- Table structure for stu_detail_info
-- ----------------------------
DROP TABLE IF EXISTS `stu_detail_info`;
CREATE TABLE `stu_detail_info` (
  `stu_detail_id` int(10) NOT NULL AUTO_INCREMENT,
  `home_address` varchar(50) DEFAULT NULL,
  `stu_phone` varchar(50) DEFAULT NULL COMMENT '电话',
  `father_name` varchar(10) DEFAULT NULL,
  `father_phone` int(10) DEFAULT NULL,
  `father_work` varchar(50) DEFAULT NULL,
  `mather_name` varchar(10) DEFAULT NULL,
  `mother_phone` int(10) DEFAULT NULL,
  PRIMARY KEY (`stu_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stu_detail_info
-- ----------------------------
INSERT INTO `stu_detail_info` VALUES ('1', '山西太原', '18825412337', '', null, null, null, null);

-- ----------------------------
-- Table structure for stu_status
-- ----------------------------
DROP TABLE IF EXISTS `stu_status`;
CREATE TABLE `stu_status` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_card` varchar(255) NOT NULL,
  `stu_no` char(12) NOT NULL,
  `print_report` int(11) DEFAULT '0',
  `allocation_of_dorm` int(50) DEFAULT '0' COMMENT '宿舍分配状态，0未分配，1已分配',
  `medical_insura` int(11) DEFAULT '0',
  `regist` int(11) DEFAULT '0',
  `file_submit` int(11) DEFAULT '0',
  `uniform_comfirm` int(11) DEFAULT '0',
  `pay_status` int(11) DEFAULT '0',
  `belong_spec_id` int(20) DEFAULT NULL,
  `belong_class_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_card` (`id_card`) USING BTREE,
  UNIQUE KEY `stu_no` (`stu_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of stu_status
-- ----------------------------
INSERT INTO `stu_status` VALUES ('1', '745123654789512365', '202108020102', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('2', '430524199912043716', '202108020101', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('3', '451247856932145741', '202103040101', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('4', '140600200109136834', '202103040103', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('5', '210202200305205007', '202103040102', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('6', '150723200311046041', '202103040104', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('7', '632821200307054953', '202103040105', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('8', '623022200301193305', '202103040106', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('9', '150727200212219053', '202103040107', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('10', '131128200303217185', '202103040108', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('11', '361021200211205219', '202103040109', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('12', '542428200204305167', '202103040110', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('13', '522229200303317106', '202109030201', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('14', '130821200307230355', '202109030202', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('15', '511102200302021348', '202109030203', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('16', '370828200307155737', '202109030204', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('17', '360728200107217000', '202109030205', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('18', '522325200301180732', '202109030206', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('19', '321100200107145208', '202109030207', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('20', '610403200307035250', '202109030208', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('21', '410725200302068026', '202109030209', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('22', '610422200202078739', '202109030210', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('23', '450200200306237889', '202109030211', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('24', '652122200310188831', '202109030212', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('25', '621022200211297061', '202109030213', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('26', '41162120030214499X', '202109030214', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('27', '211421200302062427', '202109030215', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('28', '610423200302277635', '202109030216', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('29', '440785200307104729', '202109030217', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('30', '321081200112119517', '202109030218', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('31', '361023200305115428', '202109030219', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('32', '220284200403050914', '202109030220', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('33', '360725200301078041', '202109030221', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('34', '410823200202093761', '202109030223', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('35', '652827200309199015', '202109030222', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('36', '150823200105189530', '202109030224', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('37', '360921200108010382', '202109030225', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('38', '500000200110271616', '202109030226', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('39', '610923200303276180', '202109030227', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('40', '131127200111180254', '202109030228', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('41', '654026200202198786', '202109030229', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('42', '21110020030713417X', '202109030230', '0', '0', '0', '0', '0', '0', '0', '3', '349');
INSERT INTO `stu_status` VALUES ('43', '140200200204109410', '202103040111', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('44', '210711200205117484', '202103040112', '0', '0', '0', '0', '0', '0', '0', '13', '372');
INSERT INTO `stu_status` VALUES ('45', '361026200108149185', '202110010101', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('46', '513434200205110962', '202110010103', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('47', '41142320030926331X', '202110010102', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('48', '141022200312273259', '202110010104', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('49', '65322720020929614X', '202110010105', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('50', '320300200110278634', '202110010106', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('51', '34082420030313054X', '202110010107', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('52', '321112200209164819', '202110010108', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('53', '130323200304207489', '202110010109', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('54', '360302200112122035', '202110010110', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('55', '340721200208219509', '202110010111', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('56', '360982200108104875', '202110010112', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('57', '522635200401072105', '202110010113', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('58', '653125200307131914', '202110010114', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('59', '532527200105010942', '202110010115', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('60', '421126200205089139', '202110010116', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('61', '410506200308316641', '202110010117', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('62', '450803200112188478', '202110010118', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('63', '140623200205231828', '202110010119', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('64', '43010020030622427X', '202110010120', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('65', '140826200111277024', '202110010121', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('66', '130400200207291130', '202110010122', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('67', '654028200305272101', '202110010123', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('68', '511525200107319196', '202110010124', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('69', '211382200306100803', '202110010125', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('70', '360202200210134173', '202110010126', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('71', '510683200307048640', '202110010127', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('72', '22020320010728235X', '202110010128', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('73', '35010520010818570X', '202110010129', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('74', '120110200111127357', '202110010130', '0', '0', '0', '0', '0', '0', '0', '5', '351');
INSERT INTO `stu_status` VALUES ('75', '511823200304296783', '202108020103', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('76', '220282200403269913', '202108020104', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('77', '530524200111190563', '202108020105', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('78', '513323200111224470', '202108020106', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('79', '511602200106299120', '202108020107', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('80', '652302200403256317', '202108020108', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('81', '350212200105068849', '202108020109', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('82', '360921200210175191', '202108020110', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('83', '361121200202109044', '202108020111', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('84', '440233200204293391', '202108020112', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('85', '610100200212147289', '202108020113', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('86', '33072620010802777X', '202108020114', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('87', '500226200111051206', '202108020115', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('88', '650104200202151332', '202108020116', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('89', '140882200107050629', '202108020117', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('90', '140430200310187016', '202108020118', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('91', '140602200107078282', '202108020119', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('92', '530129200403081211', '202108020120', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('93', '320482200306162043', '202108020121', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('94', '230833200212099736', '202108020122', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('95', '320903200201028886', '202108020123', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('96', '441500200111062818', '202108020124', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('97', '43112120020529188X', '202108020125', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('98', '542128200211261557', '202108020126', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('99', '630121200308019068', '202108020127', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('100', '130926200302067058', '202108020128', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('101', '130423200203255225', '202108020129', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('102', '130721200207145617', '202108020130', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('103', '522725200207152665', '202108020131', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('104', '410721200307146673', '202108020132', '0', '0', '0', '0', '0', '0', '0', '20', '393');
INSERT INTO `stu_status` VALUES ('105', '542330200202189445', '202113010101', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('106', '152201200210273057', '202113010102', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('107', '330212200202135667', '202113010103', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('108', '513222200401144696', '202113010104', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('109', '511681200203157162', '202113010105', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('110', '421381200109136172', '202113010106', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('111', '231225200202257006', '202113010107', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('112', '370321200107108838', '202113010108', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('113', '510700200304091701', '202113010109', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('114', '421124200208185959', '202113010110', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('115', '450422200303129128', '202113010111', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('116', '130632200204097393', '202113010112', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('117', '513223200310145868', '202113010113', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('118', '450332200201285557', '202113010114', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('119', '130681200110175108', '202113010115', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('120', '150522200204019710', '202113010116', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('121', '510822200310269725', '202113010117', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('122', '230804200201114898', '202113010118', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('123', '451281200309088400', '202113010119', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('124', '370725200209232271', '202113010120', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('125', '610630200109099528', '202113010121', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('126', '530925200302237991', '202113010122', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('127', '620723200303309748', '202113010123', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('128', '420606200309110837', '202113010124', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('129', '37010520010708832X', '202113010125', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('130', '340100200207206891', '202113010126', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('131', '411082200203140360', '202113010127', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('132', '52262720030531889X', '202113010128', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('133', '513229200302071684', '202113010129', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('134', '210905200311097556', '202113010130', '0', '0', '0', '0', '0', '0', '0', '7', '357');
INSERT INTO `stu_status` VALUES ('135', '520113200401238366', '202111010101', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('136', '420528200107277463', '202111010103', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('137', '53332320020606109X', '202111010104', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('138', '330211200209134695', '202111010102', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('139', '150526200304130921', '202111010105', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('140', '361024200212117455', '202111010106', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('141', '500229200204071483', '202111010107', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('142', '640522200106198619', '202111010108', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('143', '500232200310263647', '202111010109', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('144', '500224200112021479', '202111010110', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('145', '62292520020514456X', '202111010111', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('146', '513336200208310297', '202111010112', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('147', '231224200203116743', '202111010113', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('148', '341822200105264377', '202111010114', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('149', '370786200108317981', '202111010115', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('150', '532328200404198071', '202111010116', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('151', '542325200108066149', '202111010117', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('152', '110112200302203536', '202111010118', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('153', '360321200404136147', '202111010119', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('154', '520330200111172773', '202111010120', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('155', '820000200305234721', '202111010121', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('156', '411503200107135619', '202111010122', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('157', '522401200303308722', '202111010123', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('158', '350627200305127635', '202111010124', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('159', '520113200205071925', '202111010125', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('160', '65282720020506997X', '202111010126', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('161', '420981200111072704', '202111010127', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('162', '530828200109069715', '202111010128', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('163', '43092220020702774X', '202111010129', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('164', '350212200307243131', '202111010130', '0', '0', '0', '0', '0', '0', '0', '6', '354');
INSERT INTO `stu_status` VALUES ('165', '440205200307122901', '202103010101', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('166', '532929200301301813', '202103010102', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('167', '500107200305251869', '202103010103', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('168', '430481200212050437', '202103010104', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('169', '530121200312145223', '202103010105', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('170', '653200200105187213', '202103010106', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('171', '422825200306036784', '202103010107', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('172', '12010420030429885X', '202103010108', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('173', '440400200305144110', '202103010110', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('174', '340825200305239502', '202103010111', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('175', '510922200306217207', '202103010109', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('176', '431021200206278454', '202103010112', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('177', '140781200212092327', '202103010113', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('178', '610729200301187579', '202103010114', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('179', '371200200206211906', '202103010115', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('180', '371428200402249175', '202103010116', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('181', '440115200307125042', '202103010117', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('182', '11010920010715833X', '202103010118', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('183', '430300200204271762', '202103010119', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('184', '532931200206209033', '202103010120', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('185', '654225200108114309', '202103010121', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('186', '500107200308151492', '202103010122', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('187', '622927200205295360', '202103010123', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('188', '340223200311251473', '202103010124', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('189', '330921200306045708', '202103010125', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('190', '653001200203255852', '202103010126', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('191', '420000200210120022', '202103010127', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('192', '370725200310207855', '202103010128', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('193', '532527200307172181', '202103010129', '0', '0', '0', '0', '0', '0', '0', '8', '360');
INSERT INTO `stu_status` VALUES ('194', '231085200301091414', '202103010130', '0', '0', '0', '0', '0', '0', '0', '8', '360');

-- ----------------------------
-- Table structure for subsection_info
-- ----------------------------
DROP TABLE IF EXISTS `subsection_info`;
CREATE TABLE `subsection_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `source_type` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of subsection_info
-- ----------------------------
INSERT INTO `subsection_info` VALUES ('4', 'CDC删除', '测试白哦提未知', '未知', '222.abidu.com');
INSERT INTO `subsection_info` VALUES ('5', '正文2', '标题2', '来源2', '网址2');
INSERT INTO `subsection_info` VALUES ('6', '11', '22', '33', '44');
INSERT INTO `subsection_info` VALUES ('7', '55', '66', '77', '88');
INSERT INTO `subsection_info` VALUES ('8', '99', '1010', '1111', '1212');
INSERT INTO `subsection_info` VALUES ('9', '11', '22', '33', '44');
INSERT INTO `subsection_info` VALUES ('10', '55', '66', '77', '88');
INSERT INTO `subsection_info` VALUES ('11', '99', '1010', '1111', '1212');
INSERT INTO `subsection_info` VALUES ('12', '11', '22', '33', '44');
INSERT INTO `subsection_info` VALUES ('13', '55', '66', '77', '88');
INSERT INTO `subsection_info` VALUES ('14', '99', '1010', '1111', '1212');
INSERT INTO `subsection_info` VALUES ('15', '11', '22', '33', '44');
INSERT INTO `subsection_info` VALUES ('16', '55', '66', '77', '88');
INSERT INTO `subsection_info` VALUES ('17', '99', '1010', '1111', '1212');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `parent_id` int(11) DEFAULT NULL COMMENT '父菜单id',
  `menu_desc` varchar(256) DEFAULT NULL COMMENT '菜单描述',
  `menu_url` varchar(256) DEFAULT NULL COMMENT '菜单url',
  `permis_id` int(11) DEFAULT NULL COMMENT '权限id',
  `enable_flag` tinyint(4) DEFAULT NULL COMMENT '开启状态',
  `menu_sort` int(11) DEFAULT NULL COMMENT '菜单排序',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_permis
-- ----------------------------
DROP TABLE IF EXISTS `sys_permis`;
CREATE TABLE `sys_permis` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自定id,主要供前端展示权限列表分类排序使用.',
  `permis_code` varchar(100) DEFAULT NULL COMMENT '权限代码',
  `permis_name` varchar(255) DEFAULT '' COMMENT '菜单的中文释义',
  `parent_id` int(4) DEFAULT '0' COMMENT '归属菜单,前端判断并展示菜单使用,',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  `permis_sort` int(11) DEFAULT NULL COMMENT '权限排序',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='后台权限表';

-- ----------------------------
-- Records of sys_permis
-- ----------------------------
INSERT INTO `sys_permis` VALUES ('1', 'sys:permisMgr', '系统权限管理', '0', '', null, '2021-02-07 13:03:10', '2021-02-07 13:06:11');
INSERT INTO `sys_permis` VALUES ('11', 'sys:roleMgr', '角色管理', '1', '', null, '2021-01-27 23:02:50', '2021-02-07 13:06:16');
INSERT INTO `sys_permis` VALUES ('12', 'sys:permisMgr', '权限管理', '0', '', null, '2021-01-27 23:03:35', '2021-01-30 01:27:48');
INSERT INTO `sys_permis` VALUES ('13', 'sys:enrollDataMgr', '迎新数据管理', '0', '', null, '2021-01-27 23:03:51', '2021-01-27 23:19:39');
INSERT INTO `sys_permis` VALUES ('14', 'sys:columnMgr', '栏目管理', '0', '', null, '2021-01-27 23:04:04', '2021-01-27 23:19:44');
INSERT INTO `sys_permis` VALUES ('15', 'sys:roleMgrAdd', '添加角色', '11', '', null, '2021-01-27 23:17:59', '2021-01-30 12:37:30');
INSERT INTO `sys_permis` VALUES ('16', 'sys:roleMgrExport', '角色信息导出', '11', '', null, '2021-01-27 23:19:30', '2021-01-30 12:37:36');
INSERT INTO `sys_permis` VALUES ('17', 'entranceHandle', '入学办理', '0', '', null, '2021-01-27 23:27:34', '2021-03-04 15:22:19');
INSERT INTO `sys_permis` VALUES ('18', 'walkIntoCollage', '走进大学', '0', '', null, '2021-01-27 23:27:45', '2021-03-04 15:22:22');
INSERT INTO `sys_permis` VALUES ('19', 'campusRules', '校园校规', '0', '', null, '2021-01-27 23:28:04', '2021-03-04 15:22:25');
INSERT INTO `sys_permis` VALUES ('20', 'onLineQA', '在线咨询', '0', '', null, '2021-01-27 23:28:17', '2021-03-04 15:22:28');
INSERT INTO `sys_permis` VALUES ('21', 'commonQA', '常见问题', '0', '', null, '2021-01-27 23:28:44', '2021-03-04 15:22:38');
INSERT INTO `sys_permis` VALUES ('22', 'enrollDataQuery', '迎新数据查询', '0', '', null, '2021-01-30 21:23:11', '2021-01-30 21:23:11');
INSERT INTO `sys_permis` VALUES ('23', 'enrollHandle', '迎新办理', '0', '', null, '2021-01-30 21:24:08', '2021-01-30 21:24:08');
INSERT INTO `sys_permis` VALUES ('24', 'enrollReportForm', '迎新报表', '0', '', null, '2021-01-30 21:24:21', '2021-01-30 21:24:21');
INSERT INTO `sys_permis` VALUES ('25', 'enrolNotice', '迎新公告', '0', '', null, '2021-01-30 21:25:11', '2021-01-30 21:25:11');
INSERT INTO `sys_permis` VALUES ('26', 'QAReplay', '新生咨询回复', '0', '', null, '2021-01-30 21:27:14', '2021-01-31 01:46:26');
INSERT INTO `sys_permis` VALUES ('27', 'initSetting', '初始化信息设置', '39', '', null, '2021-03-04 15:01:44', '2021-03-05 14:46:03');
INSERT INTO `sys_permis` VALUES ('28', 'enrollDataMgr', '基础数据管理', '0', '', null, '2021-03-04 15:02:04', '2021-03-04 15:03:40');
INSERT INTO `sys_permis` VALUES ('29', 'collegeMgr', '学院管理', '0', '', null, '2021-03-04 15:04:07', '2021-03-07 14:47:56');
INSERT INTO `sys_permis` VALUES ('30', 'specMgr', '专业管理', '0', '', null, '2021-03-04 15:04:42', '2021-03-07 14:49:02');
INSERT INTO `sys_permis` VALUES ('31', 'classesMgr', '班级管理', '0', '', null, '2021-03-04 15:04:58', '2021-03-07 14:52:24');
INSERT INTO `sys_permis` VALUES ('32', 'dataOptionLog', '数据操作日志', '28', '', null, '2021-03-04 15:05:41', '2021-03-04 15:19:07');
INSERT INTO `sys_permis` VALUES ('33', 'dormMgr', '公寓管理', '0', '', null, '2021-03-04 15:06:48', '2021-03-04 15:06:48');
INSERT INTO `sys_permis` VALUES ('34', 'buildList', '公寓清单', '33', '', null, '2021-03-04 15:07:35', '2021-03-04 15:07:35');
INSERT INTO `sys_permis` VALUES ('35', 'roomDistribution', '宿舍分配', '33', '', null, '2021-03-04 15:08:15', '2021-03-04 15:08:15');
INSERT INTO `sys_permis` VALUES ('36', 'checkInRecord', '入住登记', '33', '', null, '2021-03-04 15:08:47', '2021-03-04 15:08:47');
INSERT INTO `sys_permis` VALUES ('37', 'roomMgr', '房间管理', '33', '', null, '2021-03-04 15:09:26', '2021-03-04 15:09:26');
INSERT INTO `sys_permis` VALUES ('38', 'stuHandle', '迎新办理', '17', '', null, '2021-03-04 15:13:02', '2021-03-04 15:13:19');
INSERT INTO `sys_permis` VALUES ('39', 'enrollConfig', '迎新配置', '0', '', null, '2021-03-05 14:45:48', '2021-03-05 14:45:48');
INSERT INTO `sys_permis` VALUES ('40', 'batchAllocationDorm', '批量分配宿舍', '39', '', null, '2021-03-05 14:46:22', '2021-03-05 14:46:22');
INSERT INTO `sys_permis` VALUES ('41', 'registerTemplate', '新生报到模板', '39', '', null, '2021-03-07 14:14:24', '2021-03-07 14:14:24');
INSERT INTO `sys_permis` VALUES ('42', 'enrollProcessMgr', '报道流程管理', '39', '', null, '2021-03-07 14:14:39', '2021-03-07 14:14:41');
INSERT INTO `sys_permis` VALUES ('43', 'schoolRollMgr', '学籍管理', '0', '', null, '2021-03-07 14:15:29', '2021-03-07 14:15:29');
INSERT INTO `sys_permis` VALUES ('44', 'newStuInfoMgr', '新生信息管理', '0', '', null, '2021-03-07 14:16:11', '2021-03-07 14:16:11');
INSERT INTO `sys_permis` VALUES ('45', 'instructorMgr', '辅导员管理', '0', '', null, '2021-03-07 14:16:21', '2021-03-07 14:16:21');
INSERT INTO `sys_permis` VALUES ('46', 'publishFeeType', '标定收费类型', '48', '', null, '2021-03-07 14:16:30', '2021-04-09 11:20:25');
INSERT INTO `sys_permis` VALUES ('47', 'publishFeeItem', '发布收费项', '48', '', null, '2021-04-06 08:48:46', '2021-04-09 11:20:24');
INSERT INTO `sys_permis` VALUES ('48', 'financialMgr', '财务管理', '0', '', null, '2021-04-09 11:20:19', '2021-04-09 11:20:19');
INSERT INTO `sys_permis` VALUES ('49', 'queryFeeItem', '查询收费项', '48', '', null, '2021-04-10 10:45:43', '2021-04-11 08:56:08');
INSERT INTO `sys_permis` VALUES ('50', 'allowPublishFeeItem', '允许发布收费项', '49', '', null, '2021-04-11 10:18:16', '2021-04-11 10:18:16');
INSERT INTO `sys_permis` VALUES ('51', 'noticeMgr', '通知管理', '0', '', null, '2021-04-12 09:53:51', '2021-04-12 09:53:51');
INSERT INTO `sys_permis` VALUES ('52', 'releaseNotice', '发布通知', '51', '', null, '2021-04-12 09:53:58', '2021-04-12 09:53:58');
INSERT INTO `sys_permis` VALUES ('53', 'importStudentInfo', '导入新生信息', '44', '', null, '2021-04-16 10:41:30', '2021-04-16 10:41:39');
INSERT INTO `sys_permis` VALUES ('54', 'initiNewStuInfo', '批量分配班级', '44', '', null, '2021-04-16 10:43:13', '2021-04-18 13:50:24');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_code` varchar(20) DEFAULT NULL COMMENT '角色代码',
  `role_desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role_status` varchar(1) DEFAULT '0' COMMENT '是否有效  1有效  2无效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='后台角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('4', 'sysAdmin', '系统管理员', '2021-01-22 20:41:11', '2021-01-29 23:12:41', '0');
INSERT INTO `sys_role` VALUES ('5', 'newStudent', '新生', '2021-01-22 20:41:28', '2021-03-09 18:00:48', '0');
INSERT INTO `sys_role` VALUES ('7', 'collegeTecher', '学院教师', '2021-01-23 22:07:34', '2021-01-23 23:47:37', '0');
INSERT INTO `sys_role` VALUES ('8', 'collegeLeader', '学院领导', '2021-01-23 22:07:42', '2021-01-30 21:30:07', '0');
INSERT INTO `sys_role` VALUES ('11', 'dormManage', '宿舍管理员', '2021-01-24 23:13:28', '2021-03-08 13:55:30', '1');
INSERT INTO `sys_role` VALUES ('32', 'test', '测试员', '2021-01-27 13:26:59', '2021-03-06 14:41:38', '1');
INSERT INTO `sys_role` VALUES ('33', 'ghfhvh', '何规划工', '2021-03-08 23:33:59', '2021-03-08 23:33:59', '0');

-- ----------------------------
-- Table structure for sys_role_permis
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permis`;
CREATE TABLE `sys_role_permis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `permission_id` int(11) DEFAULT NULL COMMENT '权限id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_status` varchar(1) CHARACTER SET utf8mb4 DEFAULT '0' COMMENT '是否有效 0有效     1无效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=684 DEFAULT CHARSET=utf8 COMMENT='角色-权限关联表';

-- ----------------------------
-- Records of sys_role_permis
-- ----------------------------
INSERT INTO `sys_role_permis` VALUES ('424', '5', '17', '2021-03-09 21:16:36', '2021-03-09 21:16:36', '0');
INSERT INTO `sys_role_permis` VALUES ('425', '5', '38', '2021-03-09 21:16:36', '2021-03-09 21:16:36', '0');
INSERT INTO `sys_role_permis` VALUES ('426', '5', '20', '2021-03-09 21:16:36', '2021-03-09 21:16:36', '0');
INSERT INTO `sys_role_permis` VALUES ('651', '4', '1', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('652', '4', '11', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('653', '4', '15', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('654', '4', '16', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('655', '4', '12', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('656', '4', '13', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('657', '4', '29', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('658', '4', '30', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('659', '4', '31', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('660', '4', '33', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('661', '4', '34', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('662', '4', '35', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('663', '4', '36', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('664', '4', '37', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('665', '4', '39', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('666', '4', '27', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('667', '4', '40', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('668', '4', '41', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('669', '4', '42', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('670', '4', '44', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('671', '4', '53', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('672', '4', '54', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('673', '4', '48', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('674', '4', '46', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('675', '4', '47', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('676', '4', '49', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('677', '4', '50', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('678', '4', '51', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('679', '4', '52', '2021-04-16 10:50:06', '2021-04-16 10:50:06', '0');
INSERT INTO `sys_role_permis` VALUES ('680', '8', '48', '2021-04-18 16:20:23', '2021-04-18 16:20:23', '0');
INSERT INTO `sys_role_permis` VALUES ('681', '8', '47', '2021-04-18 16:20:23', '2021-04-18 16:20:23', '0');
INSERT INTO `sys_role_permis` VALUES ('682', '8', '49', '2021-04-18 16:20:23', '2021-04-18 16:20:23', '0');
INSERT INTO `sys_role_permis` VALUES ('683', '8', '50', '2021-04-18 16:20:23', '2021-04-18 16:20:23', '0');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `realname` varchar(255) DEFAULT NULL COMMENT '昵称',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `delete_status` varchar(1) DEFAULT '0' COMMENT '是否有效  0有效  1无效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10238 DEFAULT CHARSET=utf8 COMMENT='运营后台用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('10012', 'admin', '202cb962ac59075b964b07152d234b70', '谭永健', '2021-01-23 22:30:11', '2021-01-26 22:44:47', '0');
INSERT INTO `sys_user` VALUES ('10013', 'stu', '202cb962ac59075b964b07152d234b70', '王小二', '2021-01-23 22:32:37', '2021-01-24 16:16:51', '0');
INSERT INTO `sys_user` VALUES ('10014', 'leader', '202cb962ac59075b964b07152d234b70', '张明', '2021-01-23 22:32:58', '2021-01-24 16:16:58', '0');
INSERT INTO `sys_user` VALUES ('10016', '21', '202cb962ac59075b964b07152d234b70', 'vdsv', '2021-02-09 19:55:44', '2021-02-09 19:55:44', '0');
INSERT INTO `sys_user` VALUES ('10017', '324', '202cb962ac59075b964b07152d234b70', '3rfw', '2021-02-09 19:55:53', '2021-02-09 19:55:53', '0');
INSERT INTO `sys_user` VALUES ('10018', '10001', '202cb962ac59075b964b07152d234b70', '测试教师角色', '2021-02-13 14:43:06', '2021-02-13 14:43:28', '0');
INSERT INTO `sys_user` VALUES ('10039', '193560060', '9b070e06062711b1203acec0ad690590', '王五', '2021-04-17 19:46:13', '2021-04-17 19:46:13', '0');
INSERT INTO `sys_user` VALUES ('10044', '202108020101', '4d95bf64a2f7829777226e3d60615387', '张三', '2021-04-18 10:31:21', '2021-04-18 10:31:21', '0');
INSERT INTO `sys_user` VALUES ('10045', '202108020102', '9b070e06062711b1203acec0ad690590', '王五', '2021-04-18 10:31:21', '2021-04-18 10:31:21', '0');
INSERT INTO `sys_user` VALUES ('10046', '202103040101', 'eaa3127c72a26a4d525f0371467418f3', '李四', '2021-04-18 12:10:51', '2021-04-18 12:10:51', '0');
INSERT INTO `sys_user` VALUES ('10047', '202103040102', 'fec35266f3d46bf866ed351174899ad7', '支蓟墨', '2021-04-18 12:10:51', '2021-04-18 12:10:51', '0');
INSERT INTO `sys_user` VALUES ('10048', '202103040103', 'dd8168940ae415bd52d70db827b376dc', '荀谈长孙', '2021-04-18 12:10:51', '2021-04-18 12:10:51', '0');
INSERT INTO `sys_user` VALUES ('10049', '202103040104', 'b4f40dc24cf2aa80bf36a8084066e569', '伯慕班', '2021-04-18 12:10:51', '2021-04-18 12:10:51', '0');
INSERT INTO `sys_user` VALUES ('10050', '202103040105', '31745d8507489627255bbf5763343a1e', '范谭高', '2021-04-18 12:10:51', '2021-04-18 12:10:51', '0');
INSERT INTO `sys_user` VALUES ('10051', '202103040106', '74b66ca3fa3d1a5397c33313a47a0049', '钮干张', '2021-04-18 12:10:51', '2021-04-18 12:10:51', '0');
INSERT INTO `sys_user` VALUES ('10052', '202103040108', '3b0056d8e7b331961671849efcfdb7d5', '畅蓬邵', '2021-04-18 12:10:51', '2021-04-18 12:10:51', '0');
INSERT INTO `sys_user` VALUES ('10053', '202103040109', 'dcc212284244c10ebd5606c0b8dcd5ca', '简曾裘', '2021-04-18 12:10:51', '2021-04-18 12:10:51', '0');
INSERT INTO `sys_user` VALUES ('10054', '202103040110', 'ff14d28cd0ac5da6b87f2790e2692789', '蓬东桂', '2021-04-18 12:10:51', '2021-04-18 12:10:51', '0');
INSERT INTO `sys_user` VALUES ('10055', '202103040107', '3c541fdf373dcfe858d373abbe664dfb', '訾湛却', '2021-04-18 12:10:51', '2021-04-18 12:10:51', '0');
INSERT INTO `sys_user` VALUES ('10056', '202109030201', 'eeee14a8a91d8b5b3336a707761a7b3d', '巴随席', '2021-04-18 12:12:26', '2021-04-18 12:12:26', '0');
INSERT INTO `sys_user` VALUES ('10057', '202109030202', '0c0fcb4aef8de5b95e2cfe5ddc19a90e', '山姚巨', '2021-04-18 12:12:26', '2021-04-18 12:12:26', '0');
INSERT INTO `sys_user` VALUES ('10058', '202109030203', '7dfe814fd33f97cf7ab1473e75fd8e67', '昝微生牟', '2021-04-18 12:12:26', '2021-04-18 12:12:26', '0');
INSERT INTO `sys_user` VALUES ('10059', '202109030204', 'df0aa9364f6a9a9e1d1663e45ea23396', '叶司空姚', '2021-04-18 12:12:26', '2021-04-18 12:12:26', '0');
INSERT INTO `sys_user` VALUES ('10060', '202109030205', '936adf30a6da73d4799f4e3b7812db2d', '蔚柏熊', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10061', '202109030206', 'fbd0e8899a9c3971adb9aaff9451da39', '哈沃麻', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10062', '202109030207', '00fcb3f433e55c072aa51fd229cdb570', '蔺年南郭', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10063', '202109030208', 'ca6ae86f0e274bb16394d21f2386b47e', '公羊宁叔孙', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10064', '202109030209', '2f479bf4040a7e7db5397e3fdd82dab9', '温仲都', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10065', '202109030210', '5b3c0aad838d50fb48288079d0057b77', '林游贾', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10066', '202109030211', '9a9c04787eca1ad7177ba33a8300085c', '羿法辛', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10067', '202109030212', '4a591fcebac43e5f2fde054ba67fb27e', '靳何尤', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10068', '202109030213', 'eda42c07a3472f4a08e450297d25f1e6', '袁利微生', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10069', '202109030214', 'b8937954db55ca029471ba13b9d8932e', '沃岑师', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10070', '202109030215', 'f9130d3d7df2f90396cab63213e25a0e', '夏郦左丘', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10071', '202109030216', '2ab7aed21d1a124062d6522b42cb6b42', '苍南鄂', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10072', '202109030217', 'c6755a65a1ff02e9db3a44fd2459df3e', '靳阿麻', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10073', '202109030218', '2c640a0cbd773f3cd74e3d19da294727', '索孟刘', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10074', '202109030219', 'ac0fb667ebd3df08ba539dc8c6c32b84', '老和滕', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10075', '202109030220', 'ae9d1d71448a522e49a55ac3b04e465e', '阿官班', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10076', '202109030221', 'cf13ad27b648988c85ecd259fed9c69e', '鲜东乡屋庐', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10077', '202109030222', 'a8628b669a0078a4ed4fbf82b6e047d7', '辜还东郭', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10078', '202109030223', '25372d821157530246079bcae4d96697', '郇仉寿', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10079', '202109030224', 'a6c615de0d5d711614883604f9f91aba', '陆储揭', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10080', '202109030225', '96c5c7836f126355f72be69841524955', '上官太叔齐', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10081', '202109030226', '292f47e92db51559b48eacfb72ed0443', '百里羊舌纪', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10082', '202109030227', '81f0d0eb651d8022468a10d2afd52bf8', '唐木黄', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10083', '202109030228', '2d3d8c545a23b695eb7bb60084baa5be', '漆鲁栾', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10084', '202109030229', '7a3d51bd86ae2b5cb7680e7d0e649ecf', '东乡空史', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10085', '202109030230', '17ac147c57ad2a5629e13cc76880f293', '殳樊伯', '2021-04-18 12:12:27', '2021-04-18 12:12:27', '0');
INSERT INTO `sys_user` VALUES ('10086', '202103040111', 'b33b5695ed004b0c31b50ec8c6284fc8', '阙勾韩', '2021-04-18 12:58:07', '2021-04-18 12:58:07', '0');
INSERT INTO `sys_user` VALUES ('10087', '202103040112', 'bbed151258e4bc1d73aaddc255760961', '况陶仇', '2021-04-18 13:01:01', '2021-04-18 13:01:01', '0');
INSERT INTO `sys_user` VALUES ('10088', '202110010101', '90b23b5b5e46c490a387d01153b73ca2', '雍门蔚池', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10089', '202110010102', 'ef11bab451cf7ec3f4df1494c1f94005', '酆西门琴', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10090', '202110010103', 'd89376989c5972de14f512d4d5f02545', '薛乜韶', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10091', '202110010104', '7c0ea1bb1802a5943fdd9eb3bc9398c4', '郭老米', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10092', '202110010105', '5d55bee908e547a64b53d44972256b88', '年竺司', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10093', '202110010106', 'e8fbe55e5b916f38e9bec04666d9dde6', '林佴伏', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10094', '202110010107', '6d5f26c8731fcf8780918b27943d24db', '季项铁', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10095', '202110010108', 'a56285c3a22b557f55af7afd1130f0c6', '陶耿殷', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10096', '202110010109', 'ba7165a5f0f9826ddb8a97d7c6ca26fb', '况伏彭', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10097', '202110010110', '955e9aeb1bba63961ece64ab8d0e6e41', '雍毛楚', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10098', '202110010111', '22e9a4cfd70c3928584c4c33e8dbe2f1', '封沃殷', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10099', '202110010112', '8a99bd1d55ddc63b07b6b47c4180c51d', '季老闫', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10100', '202110010113', '44d01cd37d9f64d4db45bd9118f54024', '呼延潘逯', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10101', '202110010114', 'efb7edc144b2683a51095303e734afc3', '舒姚夹谷', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10102', '202110010115', 'abd4db92bc64ed97f89bf162ffdea219', '暴仇池', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10103', '202110010116', '53111bc11fb048257c439312cfe60a14', '李厉詹', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10104', '202110010117', '1c64ba2f624e12eabcf1b520ed191ddb', '蹇枚熊', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10105', '202110010118', '9677d995b35b165863031b3dadf593b7', '孙火那', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10106', '202110010119', '7815d7aff85761f425c9f025b2bfae62', '骆沙习', '2021-04-18 13:10:51', '2021-04-18 13:10:51', '0');
INSERT INTO `sys_user` VALUES ('10107', '202110010120', '1a8224149d92b58abbcaa2800c14e790', '逯杞曹', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10108', '202110010121', '23dae436e5fdf775a6a0995e175704a8', '余施宇文', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10109', '202110010122', '6c047b254b0bfab325eec85ad85cc516', '裴郦查', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10110', '202110010123', '4322fca0028591637eae44f886264385', '法诸葛越', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10111', '202110010124', 'f506d3b68a2195c0ca49f0dcf0ede5ff', '哈夏滑', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10112', '202110010125', '7af9847769434df16b179cc7f1d0de60', '东乡阎严', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10113', '202110010126', '3b64d167b128cd8ae0c5a7b4f76c8e44', '盛骆门', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10114', '202110010127', '377a4af3c0feee784245991d654b0693', '蔚裘高', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10115', '202110010128', '8ab7338cc7c28667dd8398bd520b6e6a', '宗正水蔡', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10116', '202110010129', 'f4a8eff094311e7300de23df93192f0c', '查那任', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10117', '202110010130', 'a69cba91d1291801821772c12059c93a', '牛伊公西', '2021-04-18 13:10:52', '2021-04-18 13:10:52', '0');
INSERT INTO `sys_user` VALUES ('10118', '202108020103', '3c350b21b661003b34bc0a3174c40c63', '冷韩蔚', '2021-04-18 13:11:06', '2021-04-18 13:11:06', '0');
INSERT INTO `sys_user` VALUES ('10119', '202108020104', '699d73a38c929377d8fde174bb8cb863', '李习法', '2021-04-18 13:11:06', '2021-04-18 13:11:06', '0');
INSERT INTO `sys_user` VALUES ('10120', '202108020105', 'cd0d38dfeb7b86562529876d8b6153b7', '阚贡家', '2021-04-18 13:11:06', '2021-04-18 13:11:06', '0');
INSERT INTO `sys_user` VALUES ('10121', '202108020106', 'a8b39a58f5e0a4dd914b25238be22c1e', '祖温乔', '2021-04-18 13:11:06', '2021-04-18 13:11:06', '0');
INSERT INTO `sys_user` VALUES ('10122', '202108020107', '3f3e953f9465f7c1946319576ca754ad', '鄢巫马欧', '2021-04-18 13:11:06', '2021-04-18 13:11:06', '0');
INSERT INTO `sys_user` VALUES ('10123', '202108020108', '9ba57787eacffc39074f7dd19140b56e', '花綦邝', '2021-04-18 13:11:06', '2021-04-18 13:11:06', '0');
INSERT INTO `sys_user` VALUES ('10124', '202108020109', '333d7aed342053d8ad6c0bf13bcd18d7', '栾任项', '2021-04-18 13:11:06', '2021-04-18 13:11:06', '0');
INSERT INTO `sys_user` VALUES ('10125', '202108020110', 'aaa9ad731cf1b8325aab7cecd61f7e5d', '姜舒左', '2021-04-18 13:11:06', '2021-04-18 13:11:06', '0');
INSERT INTO `sys_user` VALUES ('10126', '202108020111', 'f77eb26d4a319437de471b302e4cb53c', '黎祁简', '2021-04-18 13:11:06', '2021-04-18 13:11:06', '0');
INSERT INTO `sys_user` VALUES ('10127', '202108020112', '039d367fdb3cfd5d416099a410557b77', '应瞿钦', '2021-04-18 13:11:06', '2021-04-18 13:11:06', '0');
INSERT INTO `sys_user` VALUES ('10128', '202108020113', '6c100265d3c1b160ac4808b884fdefda', '孟京赫连', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10129', '202108020114', '9041d3eec5355a282af28126c4b86155', '晁仇祁', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10130', '202108020115', 'b367caa35a8f23f47db0838d4f62aaf0', '纵庞申', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10131', '202108020116', 'b17eedfae1f0c6bc5bca4cf899302b2b', '沙程狐', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10132', '202108020117', 'e6f1fefc4ef2b27ffd7524f27575aef2', '冉荀郝', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10133', '202108020118', '7eaacea3438706d066b1d29a106560e7', '饶花漆', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10134', '202108020119', 'a067d0f59c15012e858defd52f3d2f30', '南郭韩范', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10135', '202108020120', '1ff7ee85a43b1f58fffb6ac559dcc51d', '丁籍挚', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10136', '202108020121', 'b207b3db336b11e4bdacd2a108abef62', '费蹇单', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10137', '202108020122', '6ed5e34da051eeace0ef3bc9dc761fa2', '公相里篁', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10138', '202108020123', '3e870a92cf2cf7422f149e8cf3e1cc65', '林慎柏', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10139', '202108020124', '31f839578cecbc14433364f2ef7c0e26', '别钭贾', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10140', '202108020125', 'a46b8a27d970de7b694307f369eabd94', '宇文邝达', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10141', '202108020126', '584e7f15de734c424b0e82afb9fb57dd', '祁麦广', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10142', '202108020127', 'a70dbc7165fbd7f092a2eea6537d5920', '晋沈是', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10143', '202108020128', '0d4504744b3f5ec04f344a537c008fcb', '舜宓郁', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10144', '202108020129', 'b317ed952c633668bd6da2e7d1b38226', '门樊关', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10145', '202108020130', '475417c995aa725521cc58262714e16e', '姬连乐', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10146', '202108020131', 'c43f40ad040ae943c89fad2ed9e555e0', '毋还钟', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10147', '202108020132', 'ced61d23757911ea0213a2bed09762b4', '诸虎相里', '2021-04-18 13:11:07', '2021-04-18 13:11:07', '0');
INSERT INTO `sys_user` VALUES ('10148', '202113010101', 'ad0c17cc4b8364ae755b12732c388114', '毋寇劳', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10149', '202113010103', '2cc9e73d768246f2569c6f1ce43c163e', '相沃山', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10150', '202113010104', 'bd5c22fef8601d44732bdbb8d7d2f36e', '张仰令狐', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10151', '202113010102', '7e5988ca0f2dd37508689c307e25e235', '熊虞侴', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10152', '202113010105', '4e8cb4fb5a612c4e79c0fcee6047b0e6', '费慕容闻', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10153', '202113010106', '570448c07ea94f3620eb8abebf0f5691', '曹暨敬', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10154', '202113010107', '48f8f49c061f05d8ffebeb899294abe2', '公良鲜凌', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10155', '202113010108', '6f600fe2366ba5213f19329543d5f2da', '侯詹迟', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10156', '202113010109', '7d7fa3dc3cb3d40e807f6337c36778f3', '裘焦俞', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10157', '202113010110', '31076f81d094ae1a7f6451016f2570f7', '公羊季游', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10158', '202113010111', 'cb9822911181e16846efee86cb73de62', '祝篁相里', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10159', '202113010112', '3fac88a8530959de018c9eefa360d838', '相苌牛', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10160', '202113010113', '53fb2643646a2aea2f9b9ec710967f56', '南老萧', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10161', '202113010114', 'f3d0a047b8e3d6f3ee513e3860865114', '茅第五乜', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10162', '202113010115', 'ad6ab5955aaea33373fdbbddfce2059e', '聂瞿贝', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10163', '202113010116', 'c6e7b0055253ca7b9365e863e0ee3755', '蓬农充', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10164', '202113010117', '8cf03c6908784c32cb3cbef5fdeb1153', '干钟离封', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10165', '202113010118', 'ff2c3bbadf1c388fd582423adddaf94a', '费禹官', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10166', '202113010119', '889eee4a1c376ea322b0108857b9c6f7', '井常濮', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10167', '202113010120', '6cacafbf176aedde70eae8ec8f615d3a', '权宰父祭', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10168', '202113010121', '585d159d6f5f07b6df1f5a618b8391d6', '巫胶蒋', '2021-04-18 14:12:10', '2021-04-18 14:12:10', '0');
INSERT INTO `sys_user` VALUES ('10169', '202113010122', 'f89317560716dfc4a614b9fc0723b31a', '蔡汝何', '2021-04-18 14:12:11', '2021-04-18 14:12:11', '0');
INSERT INTO `sys_user` VALUES ('10170', '202113010123', 'fd04653e92ff01e5cb5f3745820abb6b', '薛戈萧', '2021-04-18 14:12:11', '2021-04-18 14:12:11', '0');
INSERT INTO `sys_user` VALUES ('10171', '202113010124', 'cf5cc88c1a35ca56191f7c6f5609c2e4', '篁庆柳', '2021-04-18 14:12:11', '2021-04-18 14:12:11', '0');
INSERT INTO `sys_user` VALUES ('10172', '202113010125', '0b062b60a200bae2aae3744bdb7570df', '贾蔚逄', '2021-04-18 14:12:11', '2021-04-18 14:12:11', '0');
INSERT INTO `sys_user` VALUES ('10173', '202113010126', '88ad8ed85af6e04d45f70e76920ba909', '纵冀言', '2021-04-18 14:12:11', '2021-04-18 14:12:11', '0');
INSERT INTO `sys_user` VALUES ('10174', '202113010127', 'cb571eae60307941f6564d5e6fff451a', '巢缪舒', '2021-04-18 14:12:11', '2021-04-18 14:12:11', '0');
INSERT INTO `sys_user` VALUES ('10175', '202113010128', '191346b3da5f9c30ab8c5f107df7e1f8', '宾翁上官', '2021-04-18 14:12:11', '2021-04-18 14:12:11', '0');
INSERT INTO `sys_user` VALUES ('10176', '202113010129', '75e7661f3049a36f51c845c0d175b7e9', '揭苗茹', '2021-04-18 14:12:11', '2021-04-18 14:12:11', '0');
INSERT INTO `sys_user` VALUES ('10177', '202113010130', '9e1002a5bb50ca6e2009d76194b84106', '蒲甘柯', '2021-04-18 14:12:11', '2021-04-18 14:12:11', '0');
INSERT INTO `sys_user` VALUES ('10178', '202111010101', '28603e21bca1ced2ea1d78a9a60f8ca7', '安厉原', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10179', '202111010103', 'd5d2ecac1b67c0345a1c7304d14ec4d7', '夏壤驷潘', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10180', '202111010104', '0458467c5e667dc2a58aa247d13aafb7', '折疏公孙', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10181', '202111010106', '99f4891bb763dddad06311b0552de8b3', '北宫阚家', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10182', '202111010102', '523211bb5b6c544adb0f6668c8d97e38', '篁帅胶', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10183', '202111010107', 'a67791aad526caa8d33c4c51b52753cf', '巴召海', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10184', '202111010105', 'a4a8b0444de65a37fb689ea9c7ee3b47', '长孙元别', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10185', '202111010108', '3db4ae074a82572f3023cdbabb94a20d', '吉祭邴', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10186', '202111010109', 'fe50d9aec611669e32e9239ba21c9942', '岳孔权', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10187', '202111010110', '2c8575f649a933d563df9c6eca7b2219', '太史阎真', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10188', '202111010111', 'aaa7f5f01ddedea9a2e81ba5c7cab404', '广吉国', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10189', '202111010112', 'e5066e7afe85d562c9a3eed515e52aee', '辜宰父裘', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10190', '202111010113', '37ade3d446a13c178feae22fee3f6907', '桂苏狄', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10191', '202111010114', '1657210f21a7242ced618405f1e70060', '厉火谢', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10192', '202111010115', '58cd63a691aae994f0548111cd843f74', '易刁荆', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10193', '202111010116', '3623cb3461344e65956060900b04d4e0', '司空空张', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10194', '202111010117', 'a088b10e5e6bd791d3790d7253188fb9', '风能艾', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10195', '202111010118', '4c44626070fdb2fef9c33693adaf5c8d', '左丘钦燕', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10196', '202111010119', 'db9f6a23d9e0f524851f746af0fa3b87', '方阚劳', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10197', '202111010120', 'f566e59bf5bfa91ab919f1d61b3a86c7', '谯褚苏', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10198', '202111010121', 'cd829c1c13fbb274f115924b725022ec', '海卢危', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10199', '202111010122', '3bc121c7193eda28237572764013b269', '尔朱甄仲长', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10200', '202111010123', '7c7d42a83d826eb5137c8825ef44ad7f', '第五冼莫', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10201', '202111010124', 'd75d0edada382b9ee8297e941d4f5265', '明冼琴', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10202', '202111010125', '2351bf386d0e042bd26e018a12ac2561', '姬翟米', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10203', '202111010126', 'bad1253d87502706afa26e82c8814e3a', '房叔孙单于', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10204', '202111010127', 'f2f7d74b5d03f4929d0ca535f85699d3', '邓蒯须', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10205', '202111010128', '8481e7c8990850d9403236029c5d770a', '官贲欧', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10206', '202111010129', '8844ef3906858dc233c0a7b2e70c3103', '盛缪南门', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10207', '202111010130', '3e692a2f8a2552222e348218f2be7b69', '危慕虞', '2021-04-18 16:31:57', '2021-04-18 16:31:57', '0');
INSERT INTO `sys_user` VALUES ('10208', '202103010101', '34da7d0f8ad733f2d6ab3cd9fcb410e0', '法魏百里', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10209', '202103010102', 'fd80c47b0158de1d03602d5741c50406', '邴全风', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10210', '202103010103', '6a821fc5181841584bc1e238883272af', '余老琴', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10211', '202103010104', '14ff0d5e8232e8b2950b3e3fd8f2c006', '公仪尤秘', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10212', '202103010105', '3532ee676d13916210e903ff4d85c04f', '祝乐佘', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10213', '202103010106', '1ab190155574cc1f3da00c64f444f262', '崔臧司马', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10214', '202103010107', '5de0216017f41c13afb8ac336d97a80a', '饶顾乜', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10215', '202103010108', '256ba8ae9263b50d3403b1db0da5845f', '杜成公乘', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10216', '202103010109', '4dc7d760d53a965985b09ebfb7c7ece5', '暨茹祭', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10217', '202103010110', 'e0f53b386b15f46f12bdc60ff1f8bb53', '林巫谷梁', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10218', '202103010111', '7cd0d14c51c6eb23aabf3f5fea5384fb', '季畅宗正', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10219', '202103010112', '0df747d2dcfbae1d82291950c6877838', '阿郑水', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10220', '202103010113', 'a9e6f562b9079c1cc49f926cfc22b3b1', '亢戴盛', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10221', '202103010114', '775c3df2ee90c39cdd78fcbc24a2210c', '段干施元', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10222', '202103010115', 'f4a67d6ac261c495226fa236f7580dfe', '尔朱慎黎', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10223', '202103010116', '0a2f42733b287b0498ce4e14c19762e6', '张甄杨', '2021-04-18 16:32:06', '2021-04-18 16:32:06', '0');
INSERT INTO `sys_user` VALUES ('10224', '202103010117', '348de334ad32d4a7c23d674a61a55f2a', '丰项习', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10225', '202103010118', '98b896bd12fbf7c666273c6c109b6bdd', '邵令狐虞', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10226', '202103010119', 'abbe352037fdf90ece6d54f4bb43ff69', '梅广郑', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10227', '202103010120', 'dfd9f9583378638ff94a6f2e929824f9', '庾祭符', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10228', '202103010121', '68a0e2670e977096141663bb022c99c6', '璩谢包', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10229', '202103010122', '665ad61d26c7332030351ff1cb006e3c', '畅云扈', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10230', '202103010123', 'f1b3cb94e18f6de51107bf52607f649a', '水柏钮', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10231', '202103010124', 'b3a8a798108ed8871a61ebbb4130f80d', '柴老易', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10232', '202103010125', '73cb933c6609e573f5bbfe4d35ca5bc4', '浦西门狐', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10233', '202103010126', '41d772a4888eb53e74308ea3fbd9614b', '任梅闾', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10234', '202103010127', '7bcb5f10f996c43a8ae2396afd992a82', '池毛贝', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10235', '202103010128', 'c72076f61954f4859e49cbafaf5a685b', '简阚濮阳', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10236', '202103010129', 'd83d8841b381aad8640d862e035770d1', '阳冒司寇', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');
INSERT INTO `sys_user` VALUES ('10237', '202103010130', '11a6d443b31e2fd23c14950d31eebf3e', '岑莘季', '2021-04-18 16:32:07', '2021-04-18 16:32:07', '0');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `user_id` int(6) DEFAULT NULL,
  `role_id` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '10012', '4');
INSERT INTO `sys_user_role` VALUES ('2', '10013', '5');
INSERT INTO `sys_user_role` VALUES ('11', '10014', '8');
INSERT INTO `sys_user_role` VALUES ('13', '10019', '5');
INSERT INTO `sys_user_role` VALUES ('20', '10026', '5');
INSERT INTO `sys_user_role` VALUES ('21', '10027', '5');
INSERT INTO `sys_user_role` VALUES ('22', '10028', '5');
INSERT INTO `sys_user_role` VALUES ('23', '10029', '5');
INSERT INTO `sys_user_role` VALUES ('24', '10020', '5');
INSERT INTO `sys_user_role` VALUES ('25', '10021', '5');
INSERT INTO `sys_user_role` VALUES ('26', '10022', '5');
INSERT INTO `sys_user_role` VALUES ('27', '10023', '5');
INSERT INTO `sys_user_role` VALUES ('28', '10024', '5');
INSERT INTO `sys_user_role` VALUES ('29', '10025', '5');
INSERT INTO `sys_user_role` VALUES ('30', '10026', '5');
INSERT INTO `sys_user_role` VALUES ('31', '10027', '5');
INSERT INTO `sys_user_role` VALUES ('32', '10028', '5');
INSERT INTO `sys_user_role` VALUES ('33', '10029', '5');
INSERT INTO `sys_user_role` VALUES ('34', '10030', '5');
INSERT INTO `sys_user_role` VALUES ('35', '10031', '5');
INSERT INTO `sys_user_role` VALUES ('36', '10032', '5');
INSERT INTO `sys_user_role` VALUES ('37', '10033', '5');
INSERT INTO `sys_user_role` VALUES ('38', '10034', '5');
INSERT INTO `sys_user_role` VALUES ('39', '10035', '5');
INSERT INTO `sys_user_role` VALUES ('40', '10036', '5');
INSERT INTO `sys_user_role` VALUES ('41', '10037', '5');
INSERT INTO `sys_user_role` VALUES ('42', '10038', '5');
INSERT INTO `sys_user_role` VALUES ('43', '10039', '5');
INSERT INTO `sys_user_role` VALUES ('44', '10040', '5');
INSERT INTO `sys_user_role` VALUES ('45', '10040', '5');
INSERT INTO `sys_user_role` VALUES ('46', '10041', '5');
INSERT INTO `sys_user_role` VALUES ('47', '10042', '5');
INSERT INTO `sys_user_role` VALUES ('48', '10043', '5');
INSERT INTO `sys_user_role` VALUES ('49', '10044', '5');
INSERT INTO `sys_user_role` VALUES ('50', '10045', '5');
INSERT INTO `sys_user_role` VALUES ('51', '10046', '5');
INSERT INTO `sys_user_role` VALUES ('52', '10047', '5');
INSERT INTO `sys_user_role` VALUES ('53', '10048', '5');
INSERT INTO `sys_user_role` VALUES ('54', '10049', '5');
INSERT INTO `sys_user_role` VALUES ('55', '10050', '5');
INSERT INTO `sys_user_role` VALUES ('56', '10051', '5');
INSERT INTO `sys_user_role` VALUES ('57', '10052', '5');
INSERT INTO `sys_user_role` VALUES ('58', '10053', '5');
INSERT INTO `sys_user_role` VALUES ('59', '10054', '5');
INSERT INTO `sys_user_role` VALUES ('60', '10055', '5');
INSERT INTO `sys_user_role` VALUES ('61', '10056', '5');
INSERT INTO `sys_user_role` VALUES ('62', '10057', '5');
INSERT INTO `sys_user_role` VALUES ('63', '10058', '5');
INSERT INTO `sys_user_role` VALUES ('64', '10059', '5');
INSERT INTO `sys_user_role` VALUES ('65', '10060', '5');
INSERT INTO `sys_user_role` VALUES ('66', '10061', '5');
INSERT INTO `sys_user_role` VALUES ('67', '10062', '5');
INSERT INTO `sys_user_role` VALUES ('68', '10063', '5');
INSERT INTO `sys_user_role` VALUES ('69', '10064', '5');
INSERT INTO `sys_user_role` VALUES ('70', '10065', '5');
INSERT INTO `sys_user_role` VALUES ('71', '10066', '5');
INSERT INTO `sys_user_role` VALUES ('72', '10067', '5');
INSERT INTO `sys_user_role` VALUES ('73', '10068', '5');
INSERT INTO `sys_user_role` VALUES ('74', '10069', '5');
INSERT INTO `sys_user_role` VALUES ('75', '10070', '5');
INSERT INTO `sys_user_role` VALUES ('76', '10071', '5');
INSERT INTO `sys_user_role` VALUES ('77', '10072', '5');
INSERT INTO `sys_user_role` VALUES ('78', '10073', '5');
INSERT INTO `sys_user_role` VALUES ('79', '10074', '5');
INSERT INTO `sys_user_role` VALUES ('80', '10075', '5');
INSERT INTO `sys_user_role` VALUES ('81', '10076', '5');
INSERT INTO `sys_user_role` VALUES ('82', '10077', '5');
INSERT INTO `sys_user_role` VALUES ('83', '10078', '5');
INSERT INTO `sys_user_role` VALUES ('84', '10079', '5');
INSERT INTO `sys_user_role` VALUES ('85', '10080', '5');
INSERT INTO `sys_user_role` VALUES ('86', '10081', '5');
INSERT INTO `sys_user_role` VALUES ('87', '10082', '5');
INSERT INTO `sys_user_role` VALUES ('88', '10083', '5');
INSERT INTO `sys_user_role` VALUES ('89', '10084', '5');
INSERT INTO `sys_user_role` VALUES ('90', '10085', '5');
INSERT INTO `sys_user_role` VALUES ('91', '10086', '5');
INSERT INTO `sys_user_role` VALUES ('92', '10087', '5');
INSERT INTO `sys_user_role` VALUES ('93', '10088', '5');
INSERT INTO `sys_user_role` VALUES ('94', '10089', '5');
INSERT INTO `sys_user_role` VALUES ('95', '10090', '5');
INSERT INTO `sys_user_role` VALUES ('96', '10091', '5');
INSERT INTO `sys_user_role` VALUES ('97', '10092', '5');
INSERT INTO `sys_user_role` VALUES ('98', '10093', '5');
INSERT INTO `sys_user_role` VALUES ('99', '10094', '5');
INSERT INTO `sys_user_role` VALUES ('100', '10095', '5');
INSERT INTO `sys_user_role` VALUES ('101', '10096', '5');
INSERT INTO `sys_user_role` VALUES ('102', '10097', '5');
INSERT INTO `sys_user_role` VALUES ('103', '10098', '5');
INSERT INTO `sys_user_role` VALUES ('104', '10099', '5');
INSERT INTO `sys_user_role` VALUES ('105', '10100', '5');
INSERT INTO `sys_user_role` VALUES ('106', '10101', '5');
INSERT INTO `sys_user_role` VALUES ('107', '10102', '5');
INSERT INTO `sys_user_role` VALUES ('108', '10103', '5');
INSERT INTO `sys_user_role` VALUES ('109', '10104', '5');
INSERT INTO `sys_user_role` VALUES ('110', '10105', '5');
INSERT INTO `sys_user_role` VALUES ('111', '10106', '5');
INSERT INTO `sys_user_role` VALUES ('112', '10107', '5');
INSERT INTO `sys_user_role` VALUES ('113', '10108', '5');
INSERT INTO `sys_user_role` VALUES ('114', '10109', '5');
INSERT INTO `sys_user_role` VALUES ('115', '10110', '5');
INSERT INTO `sys_user_role` VALUES ('116', '10111', '5');
INSERT INTO `sys_user_role` VALUES ('117', '10112', '5');
INSERT INTO `sys_user_role` VALUES ('118', '10113', '5');
INSERT INTO `sys_user_role` VALUES ('119', '10114', '5');
INSERT INTO `sys_user_role` VALUES ('120', '10115', '5');
INSERT INTO `sys_user_role` VALUES ('121', '10116', '5');
INSERT INTO `sys_user_role` VALUES ('122', '10117', '5');
INSERT INTO `sys_user_role` VALUES ('123', '10118', '5');
INSERT INTO `sys_user_role` VALUES ('124', '10119', '5');
INSERT INTO `sys_user_role` VALUES ('125', '10120', '5');
INSERT INTO `sys_user_role` VALUES ('126', '10121', '5');
INSERT INTO `sys_user_role` VALUES ('127', '10122', '5');
INSERT INTO `sys_user_role` VALUES ('128', '10123', '5');
INSERT INTO `sys_user_role` VALUES ('129', '10124', '5');
INSERT INTO `sys_user_role` VALUES ('130', '10125', '5');
INSERT INTO `sys_user_role` VALUES ('131', '10126', '5');
INSERT INTO `sys_user_role` VALUES ('132', '10127', '5');
INSERT INTO `sys_user_role` VALUES ('133', '10128', '5');
INSERT INTO `sys_user_role` VALUES ('134', '10129', '5');
INSERT INTO `sys_user_role` VALUES ('135', '10130', '5');
INSERT INTO `sys_user_role` VALUES ('136', '10131', '5');
INSERT INTO `sys_user_role` VALUES ('137', '10132', '5');
INSERT INTO `sys_user_role` VALUES ('138', '10133', '5');
INSERT INTO `sys_user_role` VALUES ('139', '10134', '5');
INSERT INTO `sys_user_role` VALUES ('140', '10135', '5');
INSERT INTO `sys_user_role` VALUES ('141', '10136', '5');
INSERT INTO `sys_user_role` VALUES ('142', '10137', '5');
INSERT INTO `sys_user_role` VALUES ('143', '10138', '5');
INSERT INTO `sys_user_role` VALUES ('144', '10139', '5');
INSERT INTO `sys_user_role` VALUES ('145', '10140', '5');
INSERT INTO `sys_user_role` VALUES ('146', '10141', '5');
INSERT INTO `sys_user_role` VALUES ('147', '10142', '5');
INSERT INTO `sys_user_role` VALUES ('148', '10143', '5');
INSERT INTO `sys_user_role` VALUES ('149', '10144', '5');
INSERT INTO `sys_user_role` VALUES ('150', '10145', '5');
INSERT INTO `sys_user_role` VALUES ('151', '10146', '5');
INSERT INTO `sys_user_role` VALUES ('152', '10147', '5');
INSERT INTO `sys_user_role` VALUES ('153', '10148', '5');
INSERT INTO `sys_user_role` VALUES ('154', '10149', '5');
INSERT INTO `sys_user_role` VALUES ('155', '10150', '5');
INSERT INTO `sys_user_role` VALUES ('156', '10151', '5');
INSERT INTO `sys_user_role` VALUES ('157', '10152', '5');
INSERT INTO `sys_user_role` VALUES ('158', '10153', '5');
INSERT INTO `sys_user_role` VALUES ('159', '10154', '5');
INSERT INTO `sys_user_role` VALUES ('160', '10155', '5');
INSERT INTO `sys_user_role` VALUES ('161', '10156', '5');
INSERT INTO `sys_user_role` VALUES ('162', '10157', '5');
INSERT INTO `sys_user_role` VALUES ('163', '10158', '5');
INSERT INTO `sys_user_role` VALUES ('164', '10159', '5');
INSERT INTO `sys_user_role` VALUES ('165', '10160', '5');
INSERT INTO `sys_user_role` VALUES ('166', '10161', '5');
INSERT INTO `sys_user_role` VALUES ('167', '10162', '5');
INSERT INTO `sys_user_role` VALUES ('168', '10163', '5');
INSERT INTO `sys_user_role` VALUES ('169', '10164', '5');
INSERT INTO `sys_user_role` VALUES ('170', '10165', '5');
INSERT INTO `sys_user_role` VALUES ('171', '10166', '5');
INSERT INTO `sys_user_role` VALUES ('172', '10167', '5');
INSERT INTO `sys_user_role` VALUES ('173', '10168', '5');
INSERT INTO `sys_user_role` VALUES ('174', '10169', '5');
INSERT INTO `sys_user_role` VALUES ('175', '10170', '5');
INSERT INTO `sys_user_role` VALUES ('176', '10171', '5');
INSERT INTO `sys_user_role` VALUES ('177', '10172', '5');
INSERT INTO `sys_user_role` VALUES ('178', '10173', '5');
INSERT INTO `sys_user_role` VALUES ('179', '10174', '5');
INSERT INTO `sys_user_role` VALUES ('180', '10175', '5');
INSERT INTO `sys_user_role` VALUES ('181', '10176', '5');
INSERT INTO `sys_user_role` VALUES ('182', '10177', '5');
INSERT INTO `sys_user_role` VALUES ('183', '10178', '5');
INSERT INTO `sys_user_role` VALUES ('184', '10179', '5');
INSERT INTO `sys_user_role` VALUES ('185', '10180', '5');
INSERT INTO `sys_user_role` VALUES ('186', '10181', '5');
INSERT INTO `sys_user_role` VALUES ('187', '10182', '5');
INSERT INTO `sys_user_role` VALUES ('188', '10183', '5');
INSERT INTO `sys_user_role` VALUES ('189', '10184', '5');
INSERT INTO `sys_user_role` VALUES ('190', '10185', '5');
INSERT INTO `sys_user_role` VALUES ('191', '10186', '5');
INSERT INTO `sys_user_role` VALUES ('192', '10187', '5');
INSERT INTO `sys_user_role` VALUES ('193', '10188', '5');
INSERT INTO `sys_user_role` VALUES ('194', '10189', '5');
INSERT INTO `sys_user_role` VALUES ('195', '10190', '5');
INSERT INTO `sys_user_role` VALUES ('196', '10191', '5');
INSERT INTO `sys_user_role` VALUES ('197', '10192', '5');
INSERT INTO `sys_user_role` VALUES ('198', '10193', '5');
INSERT INTO `sys_user_role` VALUES ('199', '10194', '5');
INSERT INTO `sys_user_role` VALUES ('200', '10195', '5');
INSERT INTO `sys_user_role` VALUES ('201', '10196', '5');
INSERT INTO `sys_user_role` VALUES ('202', '10197', '5');
INSERT INTO `sys_user_role` VALUES ('203', '10198', '5');
INSERT INTO `sys_user_role` VALUES ('204', '10199', '5');
INSERT INTO `sys_user_role` VALUES ('205', '10200', '5');
INSERT INTO `sys_user_role` VALUES ('206', '10201', '5');
INSERT INTO `sys_user_role` VALUES ('207', '10202', '5');
INSERT INTO `sys_user_role` VALUES ('208', '10203', '5');
INSERT INTO `sys_user_role` VALUES ('209', '10204', '5');
INSERT INTO `sys_user_role` VALUES ('210', '10205', '5');
INSERT INTO `sys_user_role` VALUES ('211', '10206', '5');
INSERT INTO `sys_user_role` VALUES ('212', '10207', '5');
INSERT INTO `sys_user_role` VALUES ('213', '10208', '5');
INSERT INTO `sys_user_role` VALUES ('214', '10209', '5');
INSERT INTO `sys_user_role` VALUES ('215', '10210', '5');
INSERT INTO `sys_user_role` VALUES ('216', '10211', '5');
INSERT INTO `sys_user_role` VALUES ('217', '10212', '5');
INSERT INTO `sys_user_role` VALUES ('218', '10213', '5');
INSERT INTO `sys_user_role` VALUES ('219', '10214', '5');
INSERT INTO `sys_user_role` VALUES ('220', '10215', '5');
INSERT INTO `sys_user_role` VALUES ('221', '10216', '5');
INSERT INTO `sys_user_role` VALUES ('222', '10217', '5');
INSERT INTO `sys_user_role` VALUES ('223', '10218', '5');
INSERT INTO `sys_user_role` VALUES ('224', '10219', '5');
INSERT INTO `sys_user_role` VALUES ('225', '10220', '5');
INSERT INTO `sys_user_role` VALUES ('226', '10221', '5');
INSERT INTO `sys_user_role` VALUES ('227', '10222', '5');
INSERT INTO `sys_user_role` VALUES ('228', '10223', '5');
INSERT INTO `sys_user_role` VALUES ('229', '10224', '5');
INSERT INTO `sys_user_role` VALUES ('230', '10225', '5');
INSERT INTO `sys_user_role` VALUES ('231', '10226', '5');
INSERT INTO `sys_user_role` VALUES ('232', '10227', '5');
INSERT INTO `sys_user_role` VALUES ('233', '10228', '5');
INSERT INTO `sys_user_role` VALUES ('234', '10229', '5');
INSERT INTO `sys_user_role` VALUES ('235', '10230', '5');
INSERT INTO `sys_user_role` VALUES ('236', '10231', '5');
INSERT INTO `sys_user_role` VALUES ('237', '10232', '5');
INSERT INTO `sys_user_role` VALUES ('238', '10233', '5');
INSERT INTO `sys_user_role` VALUES ('239', '10234', '5');
INSERT INTO `sys_user_role` VALUES ('240', '10235', '5');
INSERT INTO `sys_user_role` VALUES ('241', '10236', '5');
INSERT INTO `sys_user_role` VALUES ('242', '10237', '5');

-- ----------------------------
-- Table structure for teacher_info
-- ----------------------------
DROP TABLE IF EXISTS `teacher_info`;
CREATE TABLE `teacher_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `

faculty_id` int(20) NOT NULL COMMENT '教工号',
  `teacher_name` varchar(255) NOT NULL COMMENT '教师姓名',
  `detail_info_id` int(20) DEFAULT NULL,
  `belong_college_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of teacher_info
-- ----------------------------
INSERT INTO `teacher_info` VALUES ('1', '12154', '王老师', '10012', '50');

-- ----------------------------
-- Table structure for user_dept
-- ----------------------------
DROP TABLE IF EXISTS `user_dept`;
CREATE TABLE `user_dept` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `dept_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_dept
-- ----------------------------

-- ----------------------------
-- Table structure for user_detail_info
-- ----------------------------
DROP TABLE IF EXISTS `user_detail_info`;
CREATE TABLE `user_detail_info` (
  `id` int(11) NOT NULL COMMENT '学号/教工号/角色号',
  `id_card` varchar(255) DEFAULT NULL COMMENT '身份证号',
  `head_img` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `gender` varchar(5) DEFAULT '0' COMMENT '“0”为女，“1”为男',
  `birthdate` date DEFAULT NULL COMMENT '出生日期',
  `tel_phone` int(10) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `hone_address` varchar(255) DEFAULT NULL COMMENT '家庭地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_detail_info
-- ----------------------------
INSERT INTO `user_detail_info` VALUES ('10012', '430524199912048512', null, '0', '1999-12-04', '1882451322', '1545@qq.com', '福建');

-- ----------------------------
-- Table structure for user_follow
-- ----------------------------
DROP TABLE IF EXISTS `user_follow`;
CREATE TABLE `user_follow` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(20) NOT NULL COMMENT '用户ID',
  `follow_uid` int(20) NOT NULL COMMENT '关注的用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户关注表,记录了所有用户的关注信息';

-- ----------------------------
-- Records of user_follow
-- ----------------------------
DROP TRIGGER IF EXISTS `add_floor_count`;
DELIMITER ;;
CREATE TRIGGER `add_floor_count` AFTER INSERT ON `dorm_info` FOR EACH ROW begin
declare c int;
set c = (select COUNT(*) FROM dorm_info where belong_floor_id =new.belong_floor_id);
update floor_info set dorm_count = c + 1 where floor_id =new.belong_floor_id;
end
;;
DELIMITER ;
